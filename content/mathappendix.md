(app:math)=
# Math

```{figure} images/appendices/xkcd_advanced_techniques.png
Warning: Knowing math may give you mythical powers. Cartoon by Randall Munroe, [xkcd.com/2595](https://xkcd.com/2595/), CC-BY-NC 2.5.
```

Parts of this appendix are included in the previously published open textbook *[Mechanics and Relativity](https://textbooks.open.tudelft.nl/index.php/textbooks/catalog/book/14)* and the whole appendix is included in the open interactive textbook *[Introduction to particle and continuum mechanics](https://textbooks.open.tudelft.nl/textbooks/catalog/book/81)*.