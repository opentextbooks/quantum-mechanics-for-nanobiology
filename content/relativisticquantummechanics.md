(ch:relativisticcquantummechanics)=
# Relativistic quantum mechanics

So far, our quantum mechanical descriptions have been of massive particles with velocities much lower than the speed of light. This formalism gets us a long way: we can use it to describe all of chemistry, and, by extension, it has many applications in biology. Nonrelativistic quantum mechanics also gives us basic physics insights, like in the emission spectra of atoms, and engineering applications including lasers, NMR, qubits, and quantum computers. However, it intrinsically cannot give us a description of light, while light does have an innate quantum nature, and it was the quantum nature of light that triggered the quantum revolution. Because light, inevitably, travels at the speed of light, we will need to include relativistic effects in our theory if we want it to describe light (and all processes where light interacts with matter), not just as a correction like in our discussion of the fine structure of hydrogen in {numref}`sec:hydrogenfinestructure`, but at the basis of our theory. In this theory, we will combine quantum mechanics with the special theory of relativity. Its ultimate form, quantum chromodynamics (QCD), is very powerful and extremely accurate, combining three of the four known fundamental forces. It is however not complete: gravity isn't part of the theory, and at present nobody knows how to integrate QCD with the general theory of relativity.

(sec:relativisticqmcentraleq)=
## The central equation of relativistic quantum mechanics

(sec:KleinGordonequation)=
### The Klein-Gordon equation

A first attempt at constructing a base equation for relativistic quantum mechanics could be to 'quantize' the special theory of relativity. This attempt is based on the observation that the Schr&ouml;dinger equation, after a fashion, can be seen as the 'quantization' of the classical equation for conservation of energy:

$$
E = K + V = \frac{p^2}{2m} + V.
$$ (classicalenergyconservation)

To turn equation&nbsp;{eq}`classicalenergyconservation` into a quantum one, we apply the same procedure we used to arrive at quantum-mechanical analogs of the angular momentum (see {numref}`sec:angularmomentumoperators`): we replace the momentum and energy with quantum operators:

$$
\bm{p} \to i \hbar \bm{\nabla} \qquad \text{and} \qquad E \to i \hbar \frac{\partial}{\partial t}.
$$ (momentumenergyquantization)

We also replace the potential $V$ with the potential energy operator $\hat{V}$. If we make these substitutions in equation&nbsp;{eq}`classicalenergyconservation` and then have both sides act on a wave function $\Psi(\bm{x}, t)$, we indeed arrive at the Schr&ouml;dinger equation:

$$
i \hbar \frac{\partial \Psi}{\partial t} = - \frac{\hbar^2}{2m} \nabla^2 \Psi + \hat{V} \Psi.
$$

Note that this procedure does not give us a true 'derivation' of the Schr&ouml;dinger equation (we still need {prf:ref}`axiom:SE`), as the 'quantization' recipe in equation&nbsp;{eq}`momentumenergyquantization` follows from the Schr&ouml;dinger equation. However, it does give us an idea about how we could extend quantum mechanics to relativistic systems, as we know that in relativity, the energy equation gets an extra term (see equation&nbsp;{eq}`relativsiticenergymomentumequation`):

$$
E^2 = m^2 c^4 + p^2 c^2.
$$ (relativsiticenergymomentumequation2)

Giving equation&nbsp;{eq}`relativsiticenergymomentumequation2` the same 'quantization treatment' as equation&nbsp;{eq}`classicalenergyconservation`, we arrive at

$$
-\frac{1}{c^2} \frac{\partial^2 \psi}{\partial t^2} + \nabla^2 \psi = \frac{m^2 c^2}{\hbar^2} \psi.
$$ (KleinGordoneq)

```{index} Klein-Gordon equation
```
Equation&nbsp;{eq}`KleinGordoneq` is known as the *Klein-Gordon equation*. Unlike the Schr&ouml;dinger equation, it is a proper wave equation, and it is the correct relativistic quantum equation for spin-$0$ particles. Unfortunately however, the particles of interest all have nonzero spin, and we need a more general form.

### Four-vectors
In equation&nbsp;{eq}`relativsiticenergymomentumequation2`, the momentum $p$ is the 'three-momentum', the (length of) the classical momentum vector $\bm{p}$. In relativity theory, we work with four-vectors, which describe relativistic quantities in the four dimensions of spacetime, reflecting the relativistic notion that time is no longer 'just' a parameter, but a dimension, and transformations from one (inertial) frame of reference to another affect both the spatial and temporal coordinates. In short, four-vectors get an extra ('zeroth') component, which for the position vector represents the time, and for the momentum vector the energy (times factors of $c$, the speed of light, which is a universal constant):

$$
\bm{\bar{x}} = \begin{pmatrix} ct \\ x \\ y \\ z \end{pmatrix} = \begin{pmatrix} x^0 \\ x^1 \\ x^2 \\ x^3 \end{pmatrix} \qquad \text{and} \qquad \bm{\bar{p}} = \begin{pmatrix} E/c \\ p_x \\ p_y \\ p_z \end{pmatrix} = \begin{pmatrix} p^0 \\ p^1 \\ p^2 \\ p^3 \end{pmatrix}.
$$ (fourvectors)

When making a transformation from one inertial frame to another (e.g. from that of an observer on a platform, to that of an observer on a train moving at constant velocity), the coordinates change according to the Lorentz transformations, $\bm{\bar{x}}' = \bm{L} \bm{\bar{x}}$, which can be expressed in matrix form:

$$
\begin{pmatrix} x'^0 \\ x'^1 \\ x'^2 \\ x'^3 \end{pmatrix} = \begin{pmatrix}
\gamma(u) & - \gamma(u) \frac{u}{c} & 0 & 0 \\
- \gamma(u) \frac{u}{c} & \gamma(u) & 0 & 0 \\
0 & 0 & 1 & 0 \\
0 & 0 & 0 & 1
\end{pmatrix} \begin{pmatrix} x^0 \\ x^1 \\ x^2 \\ x^3 \end{pmatrix}
$$ (Lorentztransformations)

for a transformation between a stationary frame $S$ with coordinates $\bm{x}$ and a frame $S'$ with coordinates $\bm{x}'$ moving in the positive $x$ direction with speed $u$ with respect to frame $S$. Here $\gamma(u)$ is the contraction factor from special relativity,

$$
\gamma(u) = \frac{1}{\sqrt{1-(u/v)^2}}.
$$

Relativistic four-vectors form a space which is close but not equal to $\mathbb{R}^4$, as they have an inner product<sup>[^1]</sup> which is defined<sup>[^2]</sup> differently<sup>[^3]</sup>:

$$
\bm{\bar{x}} \cdot \bm{\bar{y}} = x^0 y^0 - x^1 y^1 - x^2 y^2 - x^3 y^3.
$$ (fourvecinnerproduct)

An easy calculation shows that the 'length' of a four-vector (the quantity $\bm{\bar{x}} \cdot \bm{\bar{x}}$), and by extension, the inner product between any two four-vectors, is invariant under Lorentz transformations.

```{index} metric tensor
```
To distinguish between three-vectors and four-vectors, components of three-vectors are indicated with Roman indices like $p_i$, while those of four-vectors are indicated with Greek ones, like $p^\mu$. The upper index represents a (standard) column-vector like configuration (known as the contravariant components of the vector). We also have a version with a lower index, corresponding to a row-vector like configuration (known as the covariant components); the inner product can be written as $\bm{\bar{x}} \cdot \bm{\bar{y}} = x_\mu y^\mu = x^\mu y_\mu$, where the sum over $\mu$ (ranging from $0$ to $3$) is implicit<sup>[^4]</sup>. Within special relativity, the covariant components of a vector are the same as the contravariant ones, except for a minus sign on the space components: $x_0 = x^0$, but $x_i = - x^i$. We can summarize these relations using the *metric tensor*, which for special relativity is usually written as $\eta^{\mu \nu}$ (and its inverse, $\eta_{\mu \nu}$), to distinguish from the general relativity version $g^{\mu \nu}$. Using the Einstein summation convention, the metric tensor is defined through

$$
\bm{\bar{x}} \cdot \bm{\bar{y}} = x^\mu y_\mu = x_\mu y^\mu = \eta_{\mu \nu} x^\mu y^\nu = \eta^{\mu \nu} x_\mu y_\nu,
$$ (metricdef)

from which we can read off that

$$
\eta^{00} = 1, \quad \eta^{11} = \eta^{22} = \eta^{33} = -1, \quad \eta^{\mu \nu} = 0 \;\;\text{if}\;\; \mu \neq \nu.
$$ (SRTmetric)

The coefficients of $\eta_{\mu \nu}$ are the same as those of $\eta^{\mu \nu}$.

Derivatives can be taken with respect to any of the four components of the position (or time-position) vector; in short-hand notation, we have
```{math}
:label: fourvecderivative
\begin{align*}
\partial_\mu f &= \frac{\partial f}{\partial x^\mu} \
\end{align*}
```

```{math}
:label: dAlembertian
\begin{align*}
\square f = \partial^\mu \partial_\mu f &= \frac{1}{c^2} \frac{\partial^2 f}{\partial t} - \nabla^2 f.
\end{align*}
```

```{index} d'Alembertian
```
Equation&nbsp;{eq}`fourvecderivative` thus generalizes the partial derivative, and&nbsp;{eq}`dAlembertian` the Laplacian; the operator $\square$ is known as the *d'Alembertian*.

In terms of four-vectors, we can re-write equation&nbsp;{eq}`relativsiticenergymomentumequation2` in (even) more concise form:

$$
\bm{\bar{p}} \cdot \bm{\bar{p}} = m^2 c^2.
$$ (relativsiticenergymomentumequation3)

If we now apply the 'quantization recipe' of equation&nbsp;{eq}`momentumenergyquantization` to our four-vectors, we get

```{math}
:label: fourmomentumquantization
\begin{align*}
p_\mu &\to i \hbar \partial_\mu = i \hbar \frac{\partial}{\partial x^\mu}\\
p_0 &\to i \hbar \partial_0 = \frac{i\hbar}{c} \frac{\partial}{\partial t} \qquad \text{and} \qquad p_i \to i \hbar \partial_i = i \hbar \frac{\partial}{\partial x^i}.
\end{align*}
```

Unsurprisingly, just substituting the 'quantization' of the four-momentum in equation&nbsp;{eq}`relativsiticenergymomentumequation3` and have it act on a wave function again gives us the Klein-Gordon equation, albeit in more concise form:

$$
-\hbar^2 \square \psi = -\hbar^2 \partial^\mu \partial_\mu \psi = m^2 c^2 \psi.
$$ (KleinGordonfourvectorform)

However, you might now guess where things go wrong: rather than 'applying' $\bm{\bar{p}} \cdot \bm{\bar{p}}$, which only gives us the magnitude of the four-momentum, we'll want each individual component, and will therefore have to 'factorize' equation&nbsp;{eq}`relativsiticenergymomentumequation3` to get a more detailed view.

(sec:Diracequation)=
### The Dirac equation

To motivate why we'd want to factorize equation&nbsp;{eq}`relativsiticenergymomentumequation3`, let's consider the case of a stationary particle<sup>[^5]</sup>, for which the three-momentum is zero, and we only have one nonzero component of the four-momentum, $p^0$, directly related to its energy. In that case, equation&nbsp;{eq}`relativsiticenergymomentumequation3` simplifies to

$$
0 = p^0 p_0 - m^2 c^2 = (p^0)^2 - m^2 c^2 = (p^0 + mc)(p^0 - mc),
$$

where we used that $p_0 = p^0$ (i.e., the zeroth component of the covariant and contravariant representations are identical) in special relativity. We find that we have two solutions: either $p^0 = mc$ or $p^0 = -mc$. As $p^0$ is the energy of our particle, classically we'd dismiss the second solution, but as we'll see below, we will in fact always get two solutions in relativistic quantum mechanics.

Unfortunately, the factorization for a moving particle is more involved, because with the extra components we get cross terms, and moreover the covariant and contravariant components are no longer identical. Introducing new components $\beta^\nu$ and $\gamma^\lambda$ (so four each, for $\nu, \lambda = 0, 1, 2, 3$), we can formally proceed with the factorization:
```{math}
:label: relativsiticenergymomentumfactorization
\begin{align*}
0 &= \bm{\bar{p}} \cdot \bm{\bar{p}} - m^2 c^2 = p^\mu p_\mu - m^2 c^2  \\
&= \left( \beta^\nu p_\nu + mc \right) \left( \gamma^\lambda p_\lambda - mc \right)  \\
&= \beta^\nu \gamma^\lambda p_\nu p_\lambda - mc \left(\beta^\nu - \gamma^\nu \right) p_\nu - m^2 c^2
\end{align*}
```
Note that the first term in the last line of&nbsp;{eq}`relativsiticenergymomentumfactorization` is a sum over sixteen terms, and the second a sum over four terms. The second term however should vanish, as in the original sum in the first line of&nbsp;{eq}`relativsiticenergymomentumfactorization` there are no linear terms in the momentum. Therefore, we have $\beta^\nu = \gamma^\nu$, and we're left with four unknowns, the coefficients $\gamma^\nu$, which satisfy $p^\mu p_\mu = \gamma^\nu \gamma^\lambda p_\nu p_\lambda$. By writing out the four terms on the left and sixteen terms on the right of this equation, we get

```{math}
:label: Diraceqcoefficients1
\begin{align*}
&\left(\gamma^0\right)^2 = 1, \qquad \left(\gamma^1\right)^2 = \left(\gamma^2\right)^2 = \left(\gamma^3\right)^2 = -1, \\
&\gamma^\nu \gamma^\lambda + \gamma^\lambda \gamma^\nu = 0 \qquad \text{if}\; \nu \neq \lambda.
\end{align*}
```

We can summarize equations&nbsp;{eq}`Diraceqcoefficients1` using the *anticommutator*, $\{a, b\} = ab + ba$, and the metric tensor $\eta^{\mu \nu}$:

$$
\left\{ \gamma^\mu, \gamma^\nu \right\} = 2 \eta^{\mu \nu}.
$$ (Diraceqcoefficients)

There is no solution of equations&nbsp;{eq}`Diraceqcoefficients` in terms of numbers. However, there are solutions in which the coefficients $\gamma^\mu$ are matrices. The smallest solutions are $4 \times 4$ matrices, which can be expressed in terms of the $2 \times 2$ identity matrix $I_2$, the $2 \times 2$ Pauli spin matrices $\sigma^i$, and the $2 \times 2$ zero matrices $0_2$:

$$
\gamma^0 = \begin{pmatrix}
I_2 & 0_2 \\
0_2 & I_2
\end{pmatrix}, \qquad
\gamma^i = \begin{pmatrix}
0_2 & \sigma^i \\
-\sigma^i & 0_2
\end{pmatrix}.
$$ (Diraceqcoefficientmatrices)

```{index} Dirac equation
```
With these matrices as the coefficients, we can finally factorize equation&nbsp;{eq}`relativsiticenergymomentumfactorization`, 'quantize' the momenta $p_\mu$, and have them act on a quantum function $\psi$, which gives us the *Dirac equation*:

$$
i \hbar \gamma^\mu \partial_\mu \psi - m c \psi = 0,
$$ (Diraceq)

where

$$
\psi = \begin{pmatrix} \psi_1 \\ \psi_2 \\ \psi_3 \\ \psi_4 \end{pmatrix}
$$ (defbispinor)

```{index} bispinor
```
is known as the *bispinor* or Dirac spinor. Note that $\psi$ is not a four-vector; it simply contains four pieces of information that can be cast in (regular) vector form.

[^1]: You might (rightly) protest that equation&nbsp;{eq}`fourvecinnerproduct` does not actually define an inner product, as clearly by this definition we can have a nonzero four-vector of which the inner product with itself is zero, or even negative, and thus the vector would have zero or imaginary length. We indeed distinguish three types of four-vectors: those whose inner product with themselves is positive (timelike), zero (lightlike) and negative (spacelike). The trajectory of a ray of light is always described by a lightlike four-vector, while that of a massive particle is described by a timelike four-vector; a spacelike four-vector trajactory would correspond to a particle traveling faster than light, violating causality.

[^2]: The inner product of equation&nbsp;{eq}`fourvecinnerproduct` represents the flat (known as 'Minkowsky') spacetime of special relativity. In general relativity, spacetime can be deformed by mass or energy, leading to a generalized form of equation&nbsp;{eq}`fourvecinnerproduct`, which includes a metric tensor $g_{\mu \nu}$, representing the shape of spacetime.

[^3]: An equivalent definition would be to put the minus sign in front of the time components, and have plus signs for the space components. Naturally the interpretation of positive and negative lengths then swaps, but the physics is not affected. Unfortunately, this is one of the cases where neither possible convention has won out, and so you can find both options in books and articles; you'll just have to check which choice the authors made.

[^4]: This implicit summation over repeated indices is the famous Einstein summation convention, jokingly referred to by Einstein himself as 'his biggest contribution to science'.

[^5]: Such a particle obviously can't be a light particle, as those always move. But (from a relativistic perspective), as long as the particle is not accelerating, we can always make a transformation to a co-moving frame, in which the particle will indeed be at rest.

