---
jupytext:
    formats: md:myst
    text_representation:
        extension: .md
        format_name: myst
kernelspec:
    display_name: Python 3 (ipykernel)
    language: python
    name: python3
---
(ch:SEsolutions)=
# Solutions to the Schr&ouml;dinger equation

(sec:TISE)=
## The time-independent Schr&ouml;dinger equation

```{index} separation of variables
```
The full Schr&ouml;dinger equation&nbsp;{eq}`SE` is a partial differential equation, as it involves derivatives to both time and space. Finding general solutions to partial differential equations is often difficult. However, for the case that the potential $\hat{V}$ is independent of time, we can use *separation of variables* to reduce the equation to two ordinary differential equations, in time and space separately; from the solutions of these ordinary differential equations we can then construct the general solution to the partial differential equation.

The first step in separating variables is to look for a solution that can be written as a product of two functions, one of time and one of space. We'll use a lowercase psi, $\psi(x)$, for the space-dependent function, and an uppercase phi, $\Phi(t)$, for the time-dependent one. Substituting $\Psi(x, t) = \Phi(t) \psi(x)$ into the Schr&ouml;dinger equation&nbsp;{eq}`SE` we get

$$
i \hbar \psi(x) \frac{\partial \Phi(t)}{\partial t} = - \frac{\hbar^2}{2m} \Phi(t) \frac{\partial^2 \psi(x)}{\partial x^2} + V(x) \Phi(t) \psi(x).
$$ (SEseparation1)

We can now divide both sides of equation&nbsp;{eq}`SEseparation1` by the product $\Phi(t) \psi(x)$, which gives

$$
\frac{i \hbar}{\Phi(t)} \frac{\partial \Phi(t)}{\partial t} = - \frac{\hbar^2}{2m} \frac{1}{\psi(x)} \frac{\partial^2 \psi(x)}{\partial x^2} + V(x).
$$ (SEseparation2)

Note that in equation&nbsp;{eq}`SEseparation2`, the left-hand side only depends on time, while the right-hand side only depends on space. That means that if we change the time but stay at the same place, the right-hand side doesn't change, so neither can the left-hand side. Likewise, if we compare two positions at the same time, the left-hand side doesn't change, so neither can the right-hand side. Therefore, both sides must be equal to some constant. For later purposes, we'll call this constant $E$. Consequently, we can write equation&nbsp;{eq}`SEseparation2` as two separate ordinary differential equations:

```{math}
:label: SEseparation3
\begin{align*}
i \hbar \frac{\mathrm{d}\Phi}{\mathrm{d}t} &= E \Phi(t) \\
- \frac{\hbar^2}{2m} \frac{\partial^2 \psi(x)}{\partial x^2} + V(x) \psi(x) &= E \psi(x).
\end{align*}
```

We multiplied equations&nbsp;{eq}`SEseparation3`a and&nbsp;{eq}`SEseparation3`b by $\Phi(t)$ and $\psi(x)$ again to make them more readable. Equation {eq}`SEseparation3`a is a first-order differential equation that can be solved easily (see {numref}`app:folode`); the solution is given by

$$
\Phi(t) = A \exp\left(-\frac{i E t}{\hbar} \right),
$$ (SEseparation4)

where $A$ is an integration constant. Because the final solution, $\Psi(x, t) = \Phi(t) \psi(x)$ must be normalized, and we'll get an integration constant in $\psi(x)$ as well, we can set $A$ to $1$ here.

The solution of equation&nbsp;{eq}`SEseparation3`b will depend on the choice of the potential function&nbsp;$V(x)$. We will work out some examples in {numref}`sec:onedimsolutions`. You may have noticed that the left-hand side of equation&nbsp;{eq}`SEseparation3`b is simply the Hamiltonian operator&nbsp;$\hat{H}$, equation&nbsp;{eq}`defhamiltonian`, applied to the space-dependent part of the wavefunction, $\psi(x)$, which allows us to write the equation as

$$
\hat{H} \psi(x) = - \frac{\hbar^2}{2m} \frac{\mathrm{d}^2 \psi(x)}{\mathrm{d}x^2} + V(x) \psi(x) = E \psi(x).
$$ (TISE)

```{index} time-independent Schr&ouml;dinger equation
```
Equation&nbsp;{eq}`TISE` is known as the *time-independent Schr&ouml;dinger equation*. The solutions to this equation are pairs of eigenfunctions<sup>[^1]</sup>&nbsp;$\psi(x)$ and eigenvalues&nbsp;$E$ of the Hamiltonian operator&nbsp;$\hat{H}$. As the Hamiltonian represents the energy of the system, you'll understand why we chose to label the constant in equation&nbsp;{eq}`SEseparation3` as $E$.

In general, we will find a collection of solutions of equation&nbsp;{eq}`TISE`. The spectrum (collection of eigenvalues) could be discrete, continuous, or a combination of both. If the spectrum is discrete, we can label the solutions and their eigenvalues by integers; in one dimension, that will be a single integer for which we usually will use the letter&nbsp;$n$. The solutions are then given as combinations ($\psi_n(x)$, $E_n$). For each of the solutions $\psi_n(x)$, we have a corresponding time-dependent part $\Phi_n(t) = \exp(-i E_n t / \hbar)$. By {prf:ref}`axiom:eigenfunctioncompleteness`, the general solution to the time-independent Schr&ouml;dinger equation can then be written as a sum over the eigenfunctions<sup>[^2]</sup> of the Hamiltonian:

$$
\Psi(x, t) = \sum_{n=1}^\infty c_n e^{-i E_n t / \hbar} \psi_n(x).
$$ (SEgeneralsolution)

Given an initial condition, i.e. a wavefunction $\Psi(x, 0)$ at $t=0$, we can determine each of the $c_n$ values using the orthogonality of the eigenfunctions ({prf:ref}`lemma:orthogonaleigenfunctions`):

$$
\Braket{\psi_n | \Psi(x, 0)} = \sum_{k=1}^\infty c_k \Braket{\psi_n | \psi_k(x)} = \sum_{k=1}^\infty c_k \delta_{kn} = c_n.
$$ (eigenvalueexpansionnthcoefficient)

We will get examples of discrete spectra for the infinite well and the harmonic potential, as well as for the bound states of hydrogen. If the spectrum of the Hamiltonian is continuous, we can also write the general solution in terms of the eigenfunctions, but now the sum becomes an integral, and the coefficients become functions of space; we will work out the details when considering the free particle. In both cases, the eigenstates of the Hamiltonian have two more properties. First, they are stationary states. Although the full wavefunction $\Psi(x, t)$ is time-dependent, the probability density $|\Psi(x, t)|^2$ is not, because the time-dependent part is a complex exponential:

$$
|\Psi(x, t)|^2 = \Psi^*(x, t) \Psi(x, t) = \psi^*(x) e^{i E t / \hbar} e^{-i E t / \hbar} \psi(x) = |\psi(x)|^2.
$$

Consequently, all probabilities and all expectation values of particles in an eigenstate are time-independent. Second, the eigenstates have definite energy, which is simply an application of the fact that the energies are the eigenvalues of the Hamiltonian (see {numref}`sec:operatoreigenvalues`). We can easily verify this statement, as we have

$$
\Braket{\hat{H}} = \Braket{\Psi | \hat{H} \Psi} = \Braket{\Psi | E \Psi} = E \Braket{\Psi | \Psi} = E
$$

and

$$
\Braket{\hat{H}^2} = \Braket{\Psi | \hat{H} \hat{H} \Psi} = \Braket{\Psi | \hat{H} E \Psi} = E^2 \Braket{\Psi | \Psi} = E^2
$$

so

$$
\sigma_{H}^2 = \Braket{\Psi | \left(\hat{H} - E\right)^2 \Psi} = \Braket{\hat{H}^2} - \Braket{\hat{H}}^2 = E^2 - E^2 = 0.
$$

(sec:onedimsolutions)=
## Some one-dimensional examples

(sec:inifinitesquarewell)=
### The infinite square well

As our first example, we'll consider a potential that is utterly unrealistic, but nicely illustrates some of the quantum effects while keeping the maths (relatively) simple. This example is known as the (one-dimensional) infinite square well potential, defined as

$$
V(x) = \begin{cases}
0 & \text{if}\; 0 < x < a, \\
\infty & \text{otherwise}.
\end{cases}
$$ (infinitewellpotential)

As long as the particle is inside the well ($0 < x < a$), it's potential energy is zero; outside the well it's potential energy is infinite. Therefore, the particle will be constricted to the well. The Hamiltonian inside the well is simply the kinetic energy; finding the eigenfunctions of that energy is a straightforward exercise. We have

$$
- \frac{\hbar^2}{2m} \frac{\mathrm{d}^2 \psi}{\mathrm{d}x^2} = E \psi,
$$

or, defining $k = \sqrt{2 m E / \hbar^2}$ and rearranging terms

$$
\frac{\mathrm{d}^2 \psi}{\mathrm{d}x^2} + k^2 \psi = 0.
$$ (infinitewellSE)

Mathematically, equation&nbsp;{eq}`infinitewellSE` is identical to the equation for a harmonic oscillator in classical mechanics. Its solutions can be written either as sines and cosines, or as complex exponentials. Taking the first approach, we have for the general solution to&nbsp;{eq}`infinitewellSE`:

$$
\psi(x) = A \sin(kx) + B \cos(kx),
$$ (infinitewellSEgensol)

where $A$ and $B$ are integration constants, to be determined by the boundary conditions. Because the potential is infinite outside the well, we must have $\psi(x) = 0$ for $x<0$ or $x>a$. We demand that $\psi(x)$ is continuous; if not, we'd also get a discontinuity in our probability density. Therefore

$$
\psi(0) = A \sin(k \cdot 0) + B \cos(k \cdot 0) = B  = 0
$$ (infinitewellSEbc0)

and

$$
\psi(a) = A \sin(k a) = 0.
$$ (infinitewellSEbca)

While equation&nbsp;{eq}`infinitewellSEbc0` simply gives $B=0$, equation&nbsp;{eq}`infinitewellSEbca` gives us a collection of possible values for $k$, which in turn determine the eigenvalues $E$. Equation&nbsp;{eq}`infinitewellSEbca` is satisfied for any value $k = n \pi / a$, where $n$ is an integer. For physical reasons, we cannot have $n=0$, as then the solution would be $\psi(x) = 0$, which is not normalizible (the particle has to be somewhere). As $\sin(x) = -\sin(-x)$, we can also exclude the negative values of $n$, as the probability density goes with the square of $\psi$ and thus we won't be able to distinguish between the solution for $n$ and $-n$. Therefore, the solutions to the time-independent Schr&ouml;dinger equation with the infinite well potential are given by

$$
\psi_n(x) = A_n \sin(\frac{n\pi}{a} x), \qquad E_n = \frac{\pi^2 \hbar^2}{2m a^2} n^2.
$$ (infinitewellSEsol)

Note that the solutions are pairs of eigenfunctions and corresponding eigenvalues. We still need to determine the value of the integration constants $A_n$ (which in principle can depend on $n$, though in this case they won't); these are set by the normalization condition&nbsp;{eq}`wavefunctionnormalization`:

$$
1 = \int_{-\infty}^\infty \left|\Psi(x,t)\right|^2 \mathrm{d}{x} = \int_0^a \psi_n(x)^* \psi_n(x) \mathrm{d}x = A_n^* A_n \int_0^a \sin[2](\frac{n\pi}{a} x) \mathrm{d}x = \left|A_n\right|^2 \frac{a}{2}.
$$

In principle, $A_n$ could contain a complex part, which we intrinsically cannot determine as we cannot measure $\psi(x)$ directly. This complex part is known as the *phase* of the wavefunction; we can write any complex number&nbsp;$A$ as a magnitude times a phase factor:

$$
A = |A| e^{i \phi}.
$$

As we cannot determine the phase, we'll usually take the magnitude of the integration constant for the normalization factor; in this case we find that $A_n = \sqrt{2/a}$ for all values of $n$.

In the calculation above, we found the possible energy eigenvalues of the Hamiltonian for a particle in an infinite square well. As we've asserted in {prf:ref}`axiom:measurement`, a measurement of the energy of such a particle will always yield an eigenvalue of the Hamiltonian. Therefore, the possible energies of the particle are *quantized*: only specific values are allowed, all values in between are explicitly excluded. This quantization is what gives quantum mechanics its name, and is a clear distinction from classical mechanics, where there is no reason the particle inside the well could not have a specific energy. In particular, the quantum particle will always have a nonzero energy, because the lowest eigenvalue is nonzero. As the energy inside the well is kinetic energy only, this implies that a quantum particle in such a well cannot be standing still, in correspondence with the Heisenberg uncertainty principle; after all, a particle that would stand still has a definite position and a definite (namely zero) momentum.

```{index} ground state, excited state
```
The eigenfunction corresponding to the lowest energy eigenvalue is known as the *ground state*. The other eigenfunctions are known as *excited states*. The ground state and excited states closely correspond to the fundamental and higher-order modes of waves in a string; for the current example, they are mathematically identical. The first few eigenstates of the square well are plotted in {numref}`fig:infinitewelleigenstates`.

```{code-cell} ipython3
:tags: [hide-input, remove-output]

%config InlineBackend.figure_formats = ['svg']
import numpy as np
import matplotlib.pyplot as plt
from myst_nb import glue

#define the wavefunction
def psi(x,n,a):
    return np.sqrt(2/a)*np.sin(n*np.pi*x)


a = 1
x = np.linspace(0.0,a,500)

fig,ax = plt.subplots(figsize = (6.5,5))

ax.spines["bottom"].set_linewidth(30)
ax.spines["bottom"].set_color("grey")

ax.spines["right"].set_linewidth(30)
ax.spines["right"].set_color("grey")

ax.spines["left"].set_linewidth(30)
ax.spines["left"].set_color("grey")

ax.spines["top"].set_visible(False)

ax.set_xlim(-0.05,a+0.05)
ax.set_ylim(-2,19)

ax.set_xticks([])
ax.set_yticks([])

ax.hlines(0.0, 0.0, a, linewidth=1, linestyle=(5,(10,3)), color="black")
ax.plot(x, psi(x,1,a), color="slateblue", linewidth=2.8) 
ax.text(0.05,2,r"$\psi_1$",c = "slateblue",fontsize = 20)

ax.hlines(5, 0.0, a, linewidth=1, linestyle=(5,(10,3)), color="black")
ax.plot(x, psi(x,2,a)+5, color="orange", linewidth=2.8) 
ax.text(0.05,7,r"$\psi_2$",c = "orange",fontsize = 20)

ax.hlines(10, 0.0, a, linewidth=1, linestyle=(5,(10,3)), color="black")
ax.plot(x, psi(x,3,a)+10, color="olive", linewidth=2.8) 
ax.text(0.05,12,r"$\psi_3$",c = "olive",fontsize = 20)

ax.hlines(15, 0.0, a, linewidth=1, linestyle=(5,(10,3)), color="black")
ax.plot(x, psi(x,4,a)+15, color="firebrick", linewidth=2.8) 
ax.text(0.05,17,r"$\psi_4$",c = "firebrick",fontsize = 20)
# Save graph to load in figure later (special Jupyter Book feature)
glue("infinitewelleigenstates", fig, display=False)
```

```{glue:figure} infinitewelleigenstates
:name: fig:infinitewelleigenstates
The first few eigenstates of the one-dimensional Hamiltonian with an infinite square well potential. The eigenstates are simply the sines which are zero at both sides of the well; while the ground state $\psi_1(x)$ has no nodes inside the well, the excited states have increasing numbers of nodes, where the probability of finding the particle equals zero. Eigenstates have been shifted vertically to show them more clearly, for each the corresponding dashed line represents $\psi = 0$.
```


It is a straightforward exercise to verify that the eigenstates $\psi_n(x)$ of the Hamiltonian as given by equation&nbsp;{eq}`infinitewellSEsol` are orthogonal (as they have to be by {prf:ref}`lemma:orthogonaleigenfunctions`). Once the eigenfunctions are normalized, they are also orthonormal, i.e.

$$
\Braket{\psi_m(x) | \psi_n(x)} = \delta_{mn}.
$$

In this case, you can also prove that the eigenfunctions form a complete set; the fact that the sines form a complete set is known as Dirichlet's theorem and is why you can write any function $f(x)$ as a Fourier sine series. As we've seen in {numref}`sec:TISE`, the general solution to the Schr&ouml;dinger equation will be a linear combination of eigenstates $\psi_n(x)$ with coefficients&nbsp;$c_n$. If we then measure the energy of a particle in such an infinite well, by {prf:ref}`axiom:measurement` we force it to choose one of the eigenvalues of the Hamiltonian, resulting in a 'collapse' of the wavefunction to the corresponding eigenstate. A concrete example may help illustrate these ideas.

````{prf:example} particle in an infinite well
:class: example
Suppose a particle in an infinite well has an initial wave function given by
```{math}
\Psi(x, 0) = A x (a-x) \quad\text{for}\quad 0 \leq x \leq a,
```
and of course $\Psi(x, 0) = 0$ outside the well. Find (a) the normalization constant $A$, (b) $\Psi(x, t)$, (c) the probability that a measurement of the energy of this particle yields the value $E_2$, and (d) the probability that a measurement of the energy of this particle yields the value $E_1$.

---
**Solution**
1. We can find the normalization constant $A$ by imposing the normalization condition&nbsp;{eq}`wavefunctionnormalization`, which gives
	```{math}
	1 = \int_{-\infty}^\infty \left|\Psi(x,t)\right|^2 \mathrm{d}x = |A|^2 \int_0^a x^2 (a-x)^2 \mathrm{d}x = |A|^2 \frac{a^5}{30},
	```
	so $A = \sqrt{30/a^5}$.
1. To find $\Psi(x, t)$, we need to expand $\Psi(x, 0)$ in the eigenstates of the Hamiltonian. By equation&nbsp;{eq}`eigenvalueexpansionnthcoefficient`, we get the coefficient of the $n$th eigenstate by taking the inner product between $\Psi(x, 0)$ and $\psi_n(x)$:
	```{math}
	\begin{align*}
	c_n &= \Braket{\psi_n(x) | \Psi(x, 0)} = \int_0^a \sqrt{\frac{2}{a}} \sin(\frac{n\pi}{a} x) \sqrt{\frac{30}{a^5}} x (a-x) \mathrm{d}x \\
	&= \frac{2\sqrt{15}}{a^2} \int_0^a x \sin(\frac{n\pi}{a} x) \mathrm{d}x - \frac{2\sqrt{15}}{a^3} \int_0^a x^2 \sin(\frac{n\pi}{a} x) \mathrm{d}x \\
	&= \frac{4 \sqrt{15}}{n^3 \pi^3} \left[ \cos(0) - \cos(n\pi) \right]\\
	&= \begin{cases} 0 & n \;\text{even} \\ 8 \sqrt{15}/(n\pi)^3 & n \;\text{odd} \end{cases}
	\end{align*}
	```
	Given the coefficients, the time-dependent solution is given by equation&nbsp;{eq}`SEgeneralsolution`, which in this case reads
	```{math}
	\Psi(x, t) = \sqrt{\frac{30}{a}} \left( \frac{2}{a} \right)^3 \sum_{n\;\text{odd}} \frac{1}{n^3} \sin(\frac{n\pi}{a} x) \exp\left(- \frac{i \pi^2 \hbar^2}{2 m a^2} n^2 t \right).
	```
1. As $c_2 = 0$, the probability of measuring $E_2$ equals zero.
1. The probability of measuring $E_1$ is given by
	```{math}
	|c_1|^2 = \left|\frac{8 \sqrt{15}}{\pi^3} \right|^2 = \frac{960}{\pi^6} \approx 0.998.
	```
	The probability of measuring $E_1$ is thus very high, which makes sense if you consider the shape of the function we started with: a parabola with a maximum halfway the well, strongly resembling the ground state.

````

(sec:freeparticles)=
### Free particles

You might wonder why we bothered restricting the particle to the infinite well in {numref}`sec:inifinitesquarewell`. Inside the well, the potential is zero, so wouldn't it be easier to start with a potential that is zero everywhere? Such a particle is known as a free particle. As we'll see, there's a subtlety involved with the absence of boundary conditions that makes the analysis of these particles a bit more tricky; also, the spectrum of energy eigenvalues of these particles will be continuous rather than discrete (and thus not 'quantized').

Of course, the differential equation for the free particle is identical to that of the particle inside the well, as in both cases the potential energy is zero. We thus retrieve equation&nbsp;{eq}`infinitewellSE`, but this time we'll write the solutions as complex exponentials:

$$
\psi(x) = A e^{ikx} + B e^{-ikx}.
$$

Writing the eigenstates in this form, you might recognize that they are the same as the plane wave eigenfunctions of the momentum operator&nbsp;$\hat{p}$ we found in {numref}`sec:operatoreigenvalues` (see equation&nbsp;{eq}`momentumeigenfunctionsgeneralnormalization`). This is hardly surprising, as for the free particle, the Hamiltonian is simply the kinetic energy, and the kinetic energy operator is proportional to the momentum squared, $\hat{K} = \hat{p}^2 / 2m$, so eigenfunctions of the momentum operator become eigenfunctions of the kinetic energy as well. If we have a momentum eigenfunction $f_p(x)$ with eigenvalue $p$, we can find its (kinetic) energy eigenvalue by applying the kinetic energy operator to it:

```{math}
\hat{K} f_p(x) = \frac{1}{2m} \hat{p}^2 f_p(x) = \frac{p^2}{2m} f_p(x),
```

so we unsurprisingly find that $E = p^2 / 2m$, and $k = \sqrt{2 m E / \hbar^2} = p/\hbar$. We already encountered this exact same relation in equation&nbsp;{eq}`deBrogliemomentum`, relating the momentum&nbsp;$p$ of a particle to its wave number&nbsp;$k$, as postulated by De Broglie; we now find that this is a direct consequence of the Schr&ouml;dinger equation. Moreover, we can explicitly write the time-dependent wave function of a free particle as a traveling wave (where those of the particle in a well were stationary, standing waves), as we get

$$
\Psi(x, t) = \psi(x) e^{-i E t / \hbar} = A e^{i k \left(x - \frac{\hbar k}{2m} t\right)} + B e^{-i k \left(x + \frac{\hbar k}{2m} t\right)}.
$$ (planewavetimedependent)

Our plane wave solutions thus consist of a wave traveling to the right (first part) and a wave traveling to the left (second part), both at speed

$$
v = \frac{\hbar |k|}{2m}
$$ (planewavephasevelocity)

The shape of the wave doesn't change. The wave is a function of the combination $x \pm v t$, so after a time $t$, the whole wave has shifted a distance $x = vt$ to the right or left. To simplify the notation, we can allow $k$ to assume negative values, and write the plane wave for wavevector $k$ as

$$
\Psi_k(x, t) = A e^{i k \left(x - \frac{\hbar k}{2m} t\right)}.
$$ (planewavetimedependent2)

Unfortunately, our plane wave solution suffers from the same problem we had with the eigenfunctions of the momentum operator: they are orthogonal but not normalizable, as we get

$$
\Braket{\Psi_k (x, t)| \Psi_k (x, t)} = |A|^2 \int_{-\infty}^\infty e^{-i k \left(x - \frac{\hbar k}{2m} t\right)} e^{i k \left(x - \frac{\hbar k}{2m} t\right)} \mathrm{d}x = |A|^2 \int_{-\infty}^\infty 1 \mathrm{d}x,
$$

so the integral becomes infinite. Therefore, just like there are no free particles with a definite momentum (as the eigenfunctions are outside the quantum mechanical Hilbert space), there are no eigenstates with a definite energy. However, the eigenfunctions of the Hamiltonian still form a basis for all functions that are in the Hilbert space, and the general solution to the time-dependent Schr&ouml;dinger equation can therefore be written as a linear combination of the eigenstates. Because the spectrum is now continuous, this linear combination comes in the form of an integral rather than a sum. The coefficients of the various eigenstates in this integral are usually denoted as functions $\phi(k)$ rather than $c(k)$ (which would be more consistent with our earlier notation), which gives the general solution as

$$
\Psi(x, t) = \frac{1}{\sqrt{2\pi}}\int_{-\infty}^\infty \phi(k) e^{i k \left(x - \frac{\hbar k}{2m} t\right)} \mathrm{d}{k}.
$$ (freeparticleSEgensol)

Note the similarity with equation&nbsp;{eq}`Fouriertransform`; the only difference is that we now integrate over the wavenumber instead of the momentum. At $t=0$ equation&nbsp;{eq}`freeparticleSEgensol` simplifies to the inverse Fourier transform of $\phi(k)$. Finding the function $\phi(k)$ from the initial condition $\Psi(x, 0)$ then boils down to taking the Fourier transform of that function, as we have

$$
\phi(k) = \Braket{\frac{1}{\sqrt{2\pi}} e^{ikx} | \Psi(x, 0)} = \frac{1}{\sqrt{2\pi}}\int_{-\infty}^\infty \Psi(x, 0) e^{-ikx} \mathrm{d}x.
$$ (freeparticleSEwavepacket)

```{index} wave packet
```
The solution given in equation&nbsp;{eq}`freeparticleSEgensol` is known as a *wave packet*, a collection of plane waves that together (through mutual interference) gives the particle a finite probability density for a limited range in space. The price is that the particle no longer has a well-defined momentum, but will carry a range of possible momenta, and the more precisely we determine the momentum (meaning a narrower function&nbsp;$\phi(k)$), the more spread out the position probability density will be, and vice versa. The plane wave solution itself is the (mathematically valid but unphysical) case where the momentum is precisely defined but the probability of finding the particle anywhere becomes zero.

```{figure} images/SE/wavepackets.svg
:name: fig:wavepackets
Wave packets. (a) Interference between two waves with close but not identical wavelengths results in alternating constructive and destructive interference, and the emergence of beats. (b) Wave packets with a carrier wave (blue) 'supporting' a modulation (purple). The speed of the carrier wave is known as the phase velocity, $\omega / k$, while the envelope travels at the group velocity, $\mathrm{d}\omega / \mathrm{d}k$.
```

```{index} group velocity, phase velocity, dispersion relation
```
Wave packets are familiar concepts from classical mechanics. There too, they emerge from wave superposition, for instance by combining two traveling waves with slightly different wavelengths, see {numref}`fig:wavepackets`. The resulting modulation wave can be found as the envelope of the generating waves: it describes how the maximum amplitude of the waves changes. In both classical and quantum mechanics, it is the envelope that carries information, not the generating waves (known in classical mechanics as the carrier waves). Moreover, the envelope and carrier waves travel at different velocities, known as the *group* and *phase* velocities respectively. As we'll see, the velocity we found in equation&nbsp;{eq}`planewavephasevelocity` is the phase velocity. To find the velocity, we need the relation between the frequency $\omega$ and the wave number&nbsp;$k$ of the wave; this relation is known as the wave's *dispersion relation*. Writing the wave in the form $\exp\left(i (k x - \omega t) \right)$, we can read off from equation&nbsp;{eq}`planewavetimedependent2` that the dispersion relation for a plane wave solution is given by

$$
\omega(k) = \frac{\hbar k^2}{2m}.
$$ (planewavedispersionrelation)

The phase velocity is simply the ratio of $\omega$ and $k$, for which we find equation&nbsp;{eq}`planewavephasevelocity`. However, the group velocity measures how quickly the envelope changes over time, which is the derivative of the dispersion relation, given by

$$
v_\mathrm{group} = \frac{\mathrm{d}\omega}{\mathrm{d}k} = \frac{\hbar k}{m}.
$$ (wavepacketgroupvelocity)

The group velocity is a better measure for the actual speed of the quantum particle. Note that it is double the phase velocity of the underlying plane wave; this behavior differs from the typical classical case, where the carrier wave travels faster than the envelope. However, the group velocity does match the classical prediction of the particle's velocity, which would be $p / m = \hbar k / m$.

(sec:tunneling)=
### Tunneling

```{index} tunneling
```
In classical mechanics, in the absence of dissipative forces like friction or drag, mechanical energy is conserved. Energy can be converted from kinetic to potential energy, for instance when throwing up a stone, or the other way around, when the stone falls. However, without external action, the internal energy of the system cannot increase, and therefore a potential energy barrier higher than the total energy of the system cannot be overcome. In quantum mechanics, this is not the case. One of the best-known quantum effects, *tunneling*, tells us that a quantum particle has a finite chance of passing through a potential energy barrier, even if its total energy is less than the height of that barrier.

To see how tunneling emerges from the basic laws of quantum mechanics, we're going to use another totally unrealistic potential: a single Dirac delta function peak or well (we'll cover both cases in one go). We encountered the delta function as the eigenfunction of the position operator in {numref}`sec:operatoreigenvalues`; here we'll use it for our potential, with the caveat that it cannot really stand by itself, as it is defined under an integral only (see equation&nbsp;{eq}`defDiracdeltafunction`). In a sense, the potential represents the limit of an infinitely high but also infinitely narrow barrier, constructed such that the area under the barrier is normalized to&nbsp;$1$. For our potential energy we then write

$$
V(x) = \varepsilon \delta(x).
$$ (Diracdeltapotential)

```{index} scattering states, bound states
```
For positive values of $\varepsilon$, we have a barrier, for negative values a well. In both cases, the potential energy is zero outside the region of interest. As we'll see, that will also be the case for many real-life examples, in particular atoms and molecules. Consequently, we can distinguish between two options: if the total energy is positive, the particle can travel infinitely far away from the region of interest, but if it comes close to it, it will interact. In experiments, this interaction will often be of the form of a collision resulting in a scattering of our particle; states with positive energy are therefore known as *scattering states*. States with negative energy are trapped; they are known as *bound states*<sup>[^3]</sup>. Of the two examples we've worked out, all states of the infinite well are bound states (as the potential at infinity is infinite), while the free particle is (by definition) in a scattering state. In general, bound states will have a discrete energy spectrum, while scattering states will have a continuous one. 

For a Dirac-delta well, we get both bound and scattering states. We'll get the same for the more realistic potential of the hydrogen atom in {numref}`sec:hydrogenatom`. For atoms and molecules, we'll mostly be interested in the bound states, as they are involved in their chemistry. For the Dirac delta potential however, we're interested in the scattering states, as those are the ones that exhibit tunneling. Solving for the bound state is a straightforward exercise (see {numref}`pb:Diracdeltaboundstate`). We'll focus on the scattering state here. Outside the region of interest, we'll have a free particle, for which we already solved in {numref}`sec:freeparticles`. The solutions there are linear combinations of plane waves; which linear combination depends on the initial conditions. Therefore, if we know what happens with an arbitrary plane wave, we can write down the total wave function, and it suffices to study the plane wave solution at the delta potential barrier. We do have to distinguish between solutions left and right of the barrier though, as they may (and will) not be identical. We therefore write the general solution of the time-independent Schr&ouml;dinger equation for $x<0$ as

$$
\psi(x) = A e^{ikx} + B e^{-ikx},
$$ (DiracdeltaSEgensolleft)

and for $x>0$ as

$$
\psi(x) = F e^{ikx} + G e^{-ikx},
$$ (DiracdeltaSEgensolright)

where as before $k = \sqrt{2 m E / \hbar^2}$, and we have $E>0$. To tie these solutions together, we need to know what happens at the well. We established earlier that the wave function itself needs to be continuous, which gives

```{math}
\begin{align*}
\lim_{x \uparrow 0} \psi(x) &= \lim_{x \downarrow 0} \psi(x) \\
A+B &= F+G.
\end{align*}
```

Usually we'd also demand that the derivative of the wave function is continuous, but we won't get that here, as the potential is infinite at $x=0$. To find out what we get (and to prove in one go that the wave function has a continuous derivative for finite $V(x)$), we go back to the Schr&ouml;dinger equation, which is a second-order differential equation. We integrate this equation over a small interval of width<sup>[^4]</sup> $2\epsilon$ around $x=0$, then take the limit that $\epsilon \to 0$ to determine the value of the derivative of $\psi(x)$ at $x=0$. First taking the integrals, we have:

$$
-\frac{\hbar^2}{2m} \int_{-\epsilon}^\epsilon \frac{\mathrm{d}^2 \psi}{\mathrm{d}x^2} \mathrm{d}x + \int_{-\epsilon}^\epsilon V(x) \psi(x) \mathrm{d}x = E \int_{-\epsilon}^\epsilon \psi(x) \mathrm{d}x.
$$ (TISEintegrated)

If $\psi(x)$ is continuous, the integral on the right-hand side of&nbsp;{eq}`TISEintegrated` evaluates to $0$ in the limit that $\epsilon \to 0$. The first integral on the left-hand side equals the derivative of $\psi(x)$ evaluated at $\epsilon$ and $-\epsilon$, or the difference in the derivative between the points $x = \epsilon$ and $x = -\epsilon$. If the potential $V(x)$ is continuous, the second term on the left-hand side of&nbsp;{eq}`TISEintegrated` also vanishes, making the difference between the derivative at the two close points disappear in the limit that $\epsilon \to 0$, i.e., the derivative becomes continuous. However, if $V(x)$ is not continuous, we get a jump in the derivative at $x=0$:

$$
\Delta \left( \frac{\mathrm{d}\psi}{\mathrm{d}x} \right) = \lim_{\epsilon \to 0} \left[ \left.\frac{\mathrm{d}\psi}{\mathrm{d}x} \right|_{x=\epsilon} - \left.\frac{\mathrm{d}\psi}{\mathrm{d}x}\right|_{x=-\epsilon} \right] = \frac{2m}{\hbar^2} \lim_{\epsilon \to 0} \int_{-\epsilon}^\epsilon V(x) \psi(x) \mathrm{d}x.
$$ (wavefunctionderivativejump)

Now for our choice of potential, $V(x) = \varepsilon \delta(x)$, the integral on the right-hand side of&nbsp;{eq}`wavefunctionderivativejump` evaluates to $(2m\varepsilon/\hbar^2) \psi(0)$, so we get a jump in the derivative. Going back to our solutions, we can calculate the derivatives left and right of the barrier from equations&nbsp;{eq}`DiracdeltaSEgensolleft` and&nbsp;{eq}`DiracdeltaSEgensolright`:

```{math}
:label: wavefunctioncontinuitybc
\begin{align*}
\lim_{x \uparrow 0} \frac{\mathrm{d}\psi}{\mathrm{d}x} &= \lim_{x \uparrow 0} ik \left(A e^{ikx} - B e^{-ikx}\right) = ik(A-B), \\
\lim_{x \downarrow 0} \frac{\mathrm{d}\psi}{\mathrm{d}x} &= \lim_{x \downarrow 0} ik \left(F e^{ikx} - G e^{-ikx}\right) = ik(F-G),
\end{align*}
```

and from equation&nbsp;{eq}`wavefunctionderivativejump` we then get

$$
\Delta \left( \frac{\mathrm{d}\psi}{\mathrm{d}x} \right) = ik\left(F-G-A+B\right) = \frac{2m\varepsilon}{\hbar^2} \psi(0) = \frac{2m\varepsilon}{\hbar^2} (A+B).
$$ (wavefunctionderivativebc)

Using equations&nbsp;{eq}`wavefunctioncontinuitybc` and&nbsp;{eq}`wavefunctionderivativebc`, we can now relate the wave functions left and right of the barrier to each other. We still have two unknowns (typically the amplitudes $A$ and $G$ of the incoming waves), but given those, we can calculate the amplitudes $B$ and $F$ of the outgoing waves. In particular, we'll consider what happens to a plane wave coming in from the left, i.e., $A e^{ikx}$, with $G=0$. We can then solve for $B$ and $F$ in terms of $A$ as

```{math}
:label: Diracdeltawavefunctionamplituderelations
\begin{align*}
B &= \frac{i \beta}{1-i \beta} A \\
F &= \frac{1}{1-i\beta} A,
\end{align*}
```

where $\beta = m \varepsilon / \hbar^2 k$ is the (rescaled) inverse of the square root of the energy&nbsp;$E$ of the particle. $B$ and $F$ are the amplitude of the reflected and the transmitted wave, respectively. As the probability density of the particle goes with the square of the wave function, we can calculate the reflection and transmission coefficients ($R$ and $T$) by squaring the ratio of the reflected or transmitted amplitude to the incoming amplitude:

```{math}
:label: reflectiontransmissioncoefficients
\begin{align*}
R &= \frac{|B|^2}{|A|^2} = \frac{\beta^2}{1+\beta^2} = \frac{1}{1+2 \hbar^2 E / m \varepsilon^2},\\
T &= \frac{|F|^2}{|A|^2} = \frac{1}{1+\beta^2} = \frac{1}{1+ m \varepsilon^2 / 2 \hbar^2 E}.
\end{align*}
```

Note that we get $R + T = 1$, as we should. As we might expect, for a stronger well (i.e., larger value of $\varepsilon$), the reflection coefficient gets larger and the transmission coefficient smaller; inversely, for a more energetic particle (i.e, larger value of $E$), the transmission coefficient increases and the reflection coefficient decreases. However, as both $R$ and $T$ depend on the square of $\varepsilon$, the sign of the potential doesn't matter. Therefore, we *always* get both scattering and transmission. A quantum particle thus has a finite chance to scatter off a cliff (where a classical particle always would fall in), and a finite chance to tunnel through a barrier (where a classical particle would always reflect).

```{figure} images/SE/tunnelingexamples.png
:name: fig:tunnelingexamples
Two applications of tunneling. (a/b) In a scanning tunneling microscope, surfaces can be resolved at atomic length scales by bringing a sharp tip close to the surface, applying a potential difference between the surface and tip, and measuring the resulting tunneling current. (a) Illustration of the method <sup>[^5]</sup>. (b) An atomically resolved image of a carbon nanotube <sup>[^6]</sup>. (c) Biological enzymes catalyze many reactions in the cell. They do so highly specifically by only binding to the exact reagants. The binding typically induces a conformational change in the enzyme, bringing the reagants closer together. In many enzymatic reactions, the reagants exchange either an electron (redox reaction) or a proton (acid-base reaction). The exchange of the electron or proton can be highly accelerated through quantum tunneling, facilitated by a metal ion (which reduces the barrier) in the enzyme. The tunneling rate is measured to be a factor 1000 higher than the reaction rate would be in the absence of quantum effects, making tunneling crucial to many biological processes. The illustration shows how the hexokinase enzyme facilitates the reaction between xylose and adenosine triphosphate (ATP); the yellow spot is a magnesium ion ($\mathrm{Mg}^{2+}$) <sup>[^7]</sup>.
```

```{index} scanning tunneling microscope
```
Quantum tunneling has many applications in research, technology, and biology. Two examples are illustrated in {numref}`fig:tunnelingexamples`. Tunneling is the basis of the *scanning tunneling microscope* (STM). In an STM, a (very!) sharp tip is brought in close proximity to a conducting surface. By applying an electric potential across the surface and the tip, a charge difference builds up between them, as in a classical capacitor. Electrons can then tunnel from the tip to the surface, generating a measurable current. Because the distance between the tip and the surface depends on the structure of the surface (it is smaller when the tip is directly above an atom, and larger when it is in between atoms), the current will change as we move the tip, allowing us to re-construct the shape of the surface. An example of such a reconstruction of the surface of a carbon nanotube is shown in {numref}`fig:tunnelingexamples`(b). Tunneling also plays a crucial role in enzymatic reactions in biology, as illustrated in {numref}`fig:tunnelingexamples`(c). Reagants binding to an enzyme commonly induce a conformational change in the enzyme, which not only brings the reagants themselves closer together, but also puts them close to a catalyst in the enzyme, usually one or two metal ions. These metal ions significantly lower the tunneling barrier for the exchange of electrons&nbsp;{cite}`Devault1980,Marcus1985` or (to a lesser degree, because they are heavier) protons&nbsp;{cite}`Sutcliffe2002,Klinman2013` between the reagants. Without the tunneling effect, enzymatic reactions would proceed much more slowly, if at all. 

(sec:harmonicpotential)=
### Harmonic potential

The examples we discussed so far are extremes that couple (relative) mathematical simplicity to lack of physical realism. There are no infinite deep wells or infinitely thin but infinitely high barriers. Even a completely free particle does not exist, and if it would, it would be infinitely boring (as it would be the only thing in the universe). Of course, in many cases you can approximate the actual physics with the crude models of the previous sections, but as soon as you start dealing with actual forces, they fail utterly.

The simplest nontrivial force law in classical mechanics is Hooke's law, which states that the force exerted by a spring is proportional to the extension of the spring. Consequently, a particle with mass $m$ suspended on a spring with spring constant&nbsp;$k$ will oscillate with frequency $\omega = \sqrt{k/m}$. Spring forces are conservative (i.e., don't dissipate mechanical energy) and can thus be written as (minus) the derivative of a potential; for a Hookean spring, the potential is given by

$$
V_\mathrm{harmonic}(x) = \frac12 k x^2 = \frac12 m \omega^2 x^2.
$$ (harmonicpotential)

In everyday life, many things oscillate, often annoyingly; take, for example, the lid of a pan that you leave upside-down on the kitchen counter. There is no spring attached to the lid; its oscillation can however be understood through the same physics, as close to the minimum of the lid's potential energy, it can be expanded in a Taylor series, of which the first nontrivial term has the same mathematical form as the harmonic potential in equation&nbsp;{eq}`harmonicpotential`. The same concept holds for quantum potentials: close to a minimum, the Taylor expansion locally closely matches a harmonic potential, and therefore local solutions to the Schr&ouml;dinger equation will also closely match those of the harmonic potential, at least for the lowest eigenvalues. Note that these solutions will not be oscillations in the classical sense, but rather in the same sense as those we found in the infinite square well: we get standing-wave solutions for definite energies (though with very different shapes than the simple sines in the infinite well) and oscillating solutions for states that are linear combinations of eigenstates.

There are two ways to solve the Schr&ouml;dinger equation for the harmonic potential&nbsp;{eq}`harmonicpotential`. One is substituting a series solution (see {numref}`app:odepowerseries`), a technique we will also use in {numref}`sec:hydrogenatom` and you can try for yourself in {numref}`pb:harmonicoscillatorseriessolution`. The other technique is algebraic, which is at the same time simpler (the mathematical operations are easier) and harder (as it involves new concepts). We will also use this technique when discussing angular momentum, and if you continue studying the quantum world, you will run into it again for the creation and annihilation of particles.

To set the stage, we write the time-independent Schr&ouml;dinger equation with a harmonic potential as

$$
\hat{H} \psi = \frac{1}{2m} \left[ \hat{p}^2 + (m \omega \hat{x})^2 \right] \psi = E \psi.
$$ (SEHP)

If $\hat{p}$ and $\hat{x}$ were numbers (or their actions both were to multiply with a number), we could factor the Hamiltonian, writing the term in square brackets as $(p + i m \omega x)(p - i m \omega x)$. Unfortunately, life isn't that easy with operators, because, as we've seen in {numref}`sec:Heisenberguncertainty`, the order in which you apply them generally matters, in particular for the position and momentum operators. However, nobody stops us from defining new operators that are combinations of the momentum and position operator, so, inspired by the possibility of factorization, we define<sup>[^8]</sup>

$$
\hat{a}_\pm \equiv \frac{1}{\sqrt{2m\hbar\omega}} \left(\mp i \hat{p} + m \omega \hat{x}\right).
$$ (defraisingloweringoperators)

Our two new operators don't commute; using $[\hat{x}, \hat{p}] = i \hbar$ from equation&nbsp;{eq}`positionmomentumcommutator`, we find

$$
[\hat{a}_+, \hat{a}_-] = \frac{1}{2m\hbar\omega} [i\hat{p} + m \omega \hat{x}, -i\hat{p} + m \omega \hat{x}] = \frac{1}{2\hbar} \left( i [\hat{p}, \hat{x}] - i [\hat{x}, \hat{p}] \right) = -1,
$$ (raisingloweringcommutator)

where we used that any operator commutes with itself. By equation&nbsp;{eq}`raisingloweringcommutator`, the operators don't commute, but the price for switching them is relatively mild, a numerical factor, which comes out at&nbsp;$-1$ by our choice of the prefactor in the definition of $\hat{a}_\pm$. Therefore, we can hope to express the Hamiltonian in terms of our new operators, which indeed we can do. To see how, simply apply both operators to the wave function $\psi(x)$, which gives:

```{math}
:label: ladderoperators
\begin{align*}
\hat{a}_+ \hat{a}_- \psi(x) &= \frac{1}{2m\hbar\omega} \left(-i\hat{p} + m \omega \hat{x} \right) \left(i\hat{p} + m \omega \hat{x}\right) \psi(x)  \\
&= \frac{1}{2m\hbar\omega} \left( \hat{p}^2 - i m \omega \hat{p} \hat{x} + i m \omega \hat{x} \hat{p} + m^2 \omega^2 \hat{x}^2 \right) \psi(x)  \\
&= \frac{1}{2m\hbar\omega} \left(\hat{p}^2 + (m \omega \hat{x})^2 \right) \psi(x) + \frac{i }{2\hbar} [\hat{x}, \hat{p}] \psi(x)  \\
&= \frac{1}{\hbar\omega}\hat{H} \psi(x) - \frac12 \psi(x), \\
\hat{a}_- \hat{a}_+ \psi(x) &= \frac{1}{\hbar\omega}\hat{H} \psi(x) + \frac12  \psi(x).
\end{align*}
```

For the Hamiltonian, we can thus write

$$
\hat{H} = \hbar \omega \left(\hat{a}_+ \hat{a}_- + \frac12\right) = \hbar \omega\left(\hat{a}_- \hat{a}_+ - \frac12 \right).
$$ (Hamiltonianraisinglowering)

As it turns out, the 'almost-factorization' of equation&nbsp;{eq}`Hamiltonianraisinglowering` is good enough, because we can use it to construct all the solutions of the Schr&ouml;dinger equation&nbsp;{eq}`SEHP`. To see how this works, first assume that we have a solution in the form of an eigenfunction $\psi(x)$ with eigenvalue $E$. Then the function $\hat{a}_+ \psi(x)$ is also a solution, with eigenvalue $E + \hbar \omega$:
```{math}
\begin{align*}
\hat{H} (\hat{a}_+ \psi) &= \hbar \omega \left(\hat{a}_+ \hat{a}_- + \frac12\right) (\hat{a}_+ \psi)  \\
&= \hbar\omega \hat{a}_+ \hat{a}_- \hat{a}_+ \psi + \frac12 (\hat{a}_+ \psi)  \\
&= \hbar \omega \hat{a}_+ \left( \hat{a}_- \hat{a}_+ + \frac12 \right) \psi  \\
&= \hat{a}_+ \left( \hat{H} + \frac12 \hbar\omega + \frac12 \hbar\omega \right) \psi  \\
&= \hat{a}_+ ( E + \hbar \omega ) \psi = (E + \hbar \omega) (\hat{a}_+ \psi).
\end{align*}
```
Similarly, we find that $\hat{a}_- \psi(x)$ is a solution with eigenvalue $E - \hbar \omega$. From a solution $\psi(x)$ we can thus construct a whole family of solutions by repeatedly applying the operators $\hat{a}_\pm$. Because these operators effectively increase and decrease the eigenvalue of the solution, they are known as the raising and lowering operators; the family of solutions is sometimes called a 'ladder' because they are evenly spaced by $\hbar \omega$.

We're not done yet; notwithstanding our nice creation of a whole family of solutions, we do not yet know if any solutions exist, nor if all possible solutions can be constructed from any given one (there could be multiple independent starting points). As it turns out, there is only one family. To see why, consider what happens when we repeatedly apply the lowering operator $\hat{a}_-$: the energy of the new state is $\hbar \omega$ lower than that of the previous state. Mathematically that's fine, but physically we run into trouble at some point: the energy of the particle cannot be less than the minimum of the potential energy. Our potential nicely has its minimum at zero, so the energies always need to be non-negative. Let $\psi_0(x)$ be the solution with the lowest non-negative solution. Applying $\hat{a}_-$ would give us a solution with negative energy, unless it returns zero, which is a solution of the Schr&ouml;dinger equation but not normalizable (and thus not a physical solution). We can thus determine $\psi_0(x)$ from the condition that $\hat{a}_- \psi_0(x) = 0$, which gives

$$
0 = \hat{a}_- \psi_0(x) = \frac{1}{\sqrt{2m\hbar\omega}} \left( i\hat{p} + m \omega \hat{x}\right) \psi_0(x) = \frac{1}{\sqrt{2m\hbar\omega}} \left[ \hbar \frac{\partial \psi_0}{\partial x} + m \omega x \psi_0(x) \right].
$$ (harmonicoscillatorlowestrungdiffeq)

Equation&nbsp;{eq}`harmonicoscillatorlowestrungdiffeq` is a first-order differential equation, which we can solve by separating variables and integrating; the (normalized) solution (see {numref}`pb:harmonicoscillatorgroundstate`) is

$$
\psi_0(x) = \left( \frac{m \omega}{\pi \hbar} \right)^{1/4} \exp\left( - \frac{m \omega x^2}{2\hbar} \right) \qquad \text{with eigenvalue} \quad E_0 = \frac12 \hbar \omega.
$$ (harmonicoscillatorgroundstate)

Note that, like for the infinite square well, the lowest-energy (ground state) solution has an energy larger than the minimum of the potential: any quantum particle in a harmonic potential will always be moving. Now that we have one solution (which we know to be the unique lowest-energy solution), we can simply construct the whole family of solutions by repeatedly applying the raising operator $\hat{a}_+$, which gives

$$
\psi_n(x) = A_n \left( \hat{a}_+ \right)^n \psi_0(x) \qquad \text{with eigenvalue} \quad E_n = \left( n + \frac12\right) \hbar \omega.
$$ (harmonicoscillatorsolutions)

Unlike in the case of the infinite well (or the hydrogen atom in {numref}`sec:hydrogenatom`), we label the ground state with $n=0$ for the harmonic oscillator (an unfortunate historical convention). The excited states have positive integer values of $n$. Expressions for these states can in principle be found using equation&nbsp;{eq}`harmonicoscillatorsolutions`, though going up to $\psi_{10}$ this way would be rather tedious. Moreover, we would still need to normalize each state (though, as we'll see, we can get the normalization constant algebraically). In contrast, we get all eigenvalues 'for free' without the need of additional calculation. What's more, we can find the expectation value of many observables without having to obtain explicit expressions for the states $\psi_n(x)$.

The first step to finding the normalization constants and expectation values is to observe that we can find the value of $n$ by applying the first the lowering and then the raising operator to any state&nbsp;$\psi_n(x)$:

$$
\hat{a}_+ \hat{a}_- \psi_n(x) = \frac{1}{\hbar\omega} \hat{H} \psi_n(x) - \frac12  \psi_n(x) = \left(\frac{E_n}{\hbar \omega} - \frac12 \right) \psi_n(x) = n \psi_n(x),
$$ (numberoperator)

where we used equation&nbsp;{eq}`ladderoperators`a to re-write $\hat{a}_+ \hat{a}_-$ in terms of the Hamiltonian, and equation&nbsp;{eq}`harmonicoscillatorsolutions` to evaluate $\hat{H} \psi_n(x)$. We can thus define a new operator, the *number operator* $\hat{n} = \hat{a}_+ \hat{a}_-$, whose action on $\psi_n(x)$ returns the value of $n$: $\hat{n} \psi_n(x) = n \psi_n(x)$. Likewise, we can show that $\hat{a}_- \hat{a}_+ \psi_n(x) = (n+1) \psi_n(x)$. Step two is to observe that $\hat{a}_+$ and $\hat{a}_-$ are each other's Hermitian conjugate (see {numref}`sec:Hermitianopertors` and {numref}`pb:harmonicoscillatoroperators`), i.e.

$$
\Braket{f | \hat{a}_+ g} = \Braket{\hat{a}_- f | g},
$$ (raisingloweringopertorsHermitianconjugates)

for any two functions $f(x)$ and $g(x)$ in our Hilbert space. Combining equations&nbsp;{eq}`numberoperator` and&nbsp;{eq}`raisingloweringopertorsHermitianconjugates`, we get

$$
\Braket{\hat{a}_+ \psi_n(x) | \hat{a}_+ \psi_n(x)} = \Braket{\hat{a}_- \left(\hat{a}_+ \psi_n(x)\right) |  \psi_n(x) } = (n+1) \Braket{\psi_n(x) | \psi_n(x)}.
$$

Therefore, we get $\psi_{n+1}(x) = \frac{1}{\sqrt{(n+1)}} \hat{a}_+ \psi_n(x)$. The normalization constant $A_n$ of the $n$-th wave function is thus $A_n = 1/\sqrt{n!}$. Third, we have that the eigenfunctions of the harmonic oscillator Hamiltonian are orthonormal:

$$
n \Braket{\psi_m | \psi_n} = \Braket{ \psi_m | \hat{n} \psi_n } = \Braket{\psi_m | \hat{a}_+ \hat{a}_- \psi_n} = \Braket{ \hat{a}_- \psi_m | \hat{a}_- \psi_n} = \Braket{ \hat{a}_+ \hat{a}_- \psi_m | \psi_n} = m \Braket{\psi_m | \psi_n},
$$

so either $m=n$ or $\Braket{\psi_m | \psi_n} = 0$. 

To find the expectation value of an observable, remember that all observables are functions of $\hat{x}$ and $\hat{p}$. We can express both in terms of our operators $\hat{a}_\pm$ by inverting equation&nbsp;{eq}`defraisingloweringoperators`:

$$
\hat{x} = \sqrt{\frac{\hbar}{2 m \omega}} \left( \hat{a}_+ + \hat{a}_- \right) \quad \text{and} \quad \hat{p} = i\sqrt{\frac{\hbar m \omega}{2}}\left( \hat{a}_+ - \hat{a}_- \right).
$$ (positionmomentumraisingloweringoperators)

As an example on how to use these expressions to efficiently calculate the expectation value of an observable for a particle in a state $\psi_n(x)$, let us consider the potential energy $V(x)$, which we can express in terms of $\hat{a}_\pm$ as

$$
\hat{V} = \frac12 m \omega^2 \hat{x}^2 = \frac{\hbar \omega}{4} \left( \hat{a}_+ + \hat{a}_- \right)^2.
$$

For the expectation value of $V$ we then get
```{math}
\begin{align*}
\Braket{V} &= \Braket{\psi_n | \hat{V} \psi_n} = \frac{\hbar \omega}{4} \Braket{\psi_n | \left( \hat{a}_+ - \hat{a}_- \right)^2 \psi_n} \\
&= \frac{\hbar \omega}{4} \Braket{\psi_n | \hat{a}_+^2 \psi_n  + \hat{a}_+\hat{a}_-\psi_n + \hat{a}_-\hat{a}_+\psi_n + \hat{a}_-^2 \psi_n} \\
&= \frac{\hbar \omega}{4} \left(0 + n + (n+1) + 0\right) = \frac12 \left(n+\frac12\right) \hbar \omega.
\end{align*}
```
In the third line, we used that $\hat{a}_+^2 \psi_n \propto \psi_{n+2}$, $\hat{a}_-^2 \psi_n \propto \psi_{n-2}$, and the fact that the eigenfunctions are orthogonal; we also used equation&nbsp;{eq}`numberoperator` to evaluate the remaining two terms. We find that the expectation value of the potential energy in the $n$th state is exactly half the total energy in that state; that's a specific result for the harmonic oscillator potential that does not generalize to other cases.

(sec:hydrogenatom)=
## The hydrogen atom

### The Schr&ouml;dinger equation in three dimensions
In {numref}`sec:onedimsolutions`, we solved the Schr&ouml;dinger equation for some one-dimensional examples. These examples have the advantage of mathematical simplicity, with solutions of the form of simple sines or Gaussians, but the distinct downside of being physically unrealistic. In classical mechanics, we may, on occasion, be able to restrict a system to one or two spatial dimensions, but in quantum mechanics this turns out to be very complicated. The examples in {numref}`sec:onedimsolutions` thus mostly serve to develop some physical intuition about the quantum world, rather than provide testable predictions. In this section, we'll study the simplest nontrivial three-dimensional case: that of the wave function and energies of an electron in a hydrogen atom (we'll take the nucleus, a single proton, which is about 2000 times heavier than the electron, as stationary). This case is realistic and leads to directly testable predictions, but at the same time is at the very limit of our analytical capabilities. Even the next-simplest cases, either the helium atom or molecular hydrogen, have not been solved analytically in closed form, though of course we have techniques of approximating the solutions (we'll study those in {numref}`ch:varprinciple`), and we can find them numerically.

To study hydrogen, the first thing we need to do is to generalize the Schr&ouml;dinger equation to three dimensions. Fortunately, doing so is straightforward: the second derivative in the kinetic energy term becomes a Laplacian, and the potential simply becomes a (still scalar) function of three coordinates:

$$
i \hbar \frac{\partial \Psi}{\partial t} = \hat{H} \Psi(\bm{r}, t) = -\frac{\hbar^2}{2m} \nabla^2 \Psi(\bm{r}, t) + V(\bm{r}) \Psi(\bm{r}, t).
$$ (SE3D)

As long as the potential remains independent of time, the time dependence in the three-dimensional equation is exactly the same as in the one-dimensional case, so we also retrieve the same time-independent form of the Schr&ouml;dinger equation, $\hat{H} \psi(\bm{r}) = E \psi(\bm{r})$.

### Spherical harmonics
In our example of interest, the hydrogen atom, the potential is a function of the radial coordinate only<sup>[^9]</sup>, so $V(\bm{r}) = V(r)$. We can then solve equation&nbsp;{eq}`SE3D` by separation of variables again, splitting it into a radial and an angular part. In spherical coordinates, we write $\psi(\bm{r}) = R(r) Y(\theta, \phi)$, and (looking up the correct expression for the Laplacian in spherical coordinates), we have

$$
\hat{H} R(r) Y(\theta, \phi) = -\frac{\hbar^2}{2m} \left[ \frac{1}{r^2} \frac{\partial }{\partial r} \left( r^2 \frac{\partial R}{\partial r} \right) Y + \frac{1}{r^2 \sin\theta} \frac{\partial }{\partial \theta} \left( \sin\theta \frac{\partial Y}{\partial \theta} \right) R + \frac{1}{r^2 \sin^2\theta} \frac{\partial^2 Y}{\partial \phi^2} R \right] + V(r) R Y = E R Y.
$$ (SE3Dspherical)

If we now multiply by $2mr^2/\hbar^2$ and divide through by $RY$, we find

$$
\frac{1}{R} \frac{\partial }{\partial r} \left( r^2 \frac{\partial R}{\partial r}\right) - \frac{2mr^2}{\hbar^2} \left[ V(r) - E \right] = -\frac{1}{Y} \left[ \frac{1}{\sin\theta} \frac{\partial }{\partial \theta} \left( \sin\theta \frac{\partial Y}{\partial \theta} \right) + \frac{1}{\sin^2\theta} \frac{\partial^2 Y}{\partial \phi^2} \right]
$$ (SE3Dsphericalseparated)

By the usual argument in separation of variables, as the left-hand side of equation&nbsp;{eq}`SE3Dsphericalseparated` depends only on&nbsp;$r$ (and is thus the same for any value of $\theta$ or $\phi$), and the right-hand side is a function of $\theta$ and $\phi$ alone (and thus invariant when changing&nbsp;$r$), the only option is that both sides are equal to a (presently arbitrary) constant. For later purposes, we write that constant as $l(l+1)$.

The angular part of the equation is now independent of the potential, and equal to the angular part of the Poisson equation from electrostatics, $\nabla^2 \psi = \rho_\mathrm{e}/\varepsilon_0$ (where $\psi$ is the electrical potential, $\rho_\mathrm{e}$ the electric charge, and $\varepsilon_0$ the permittivity of vacuum). Unsurprisingly, the solutions are also the same. They are known as the *spherical harmonics*, in essence the Fourier components of an expansion in spherical modes, similar to the sines and cosines for a linear expansion. To find the functional form of these spherical harmonics, we split the angular equation again, writing $Y(\theta, \phi) = \Theta(\theta) \Phi(\phi)$, and substitute into the condition that the right-hand side of equation&nbsp;{eq}`SE3Dsphericalseparated` equals $l(l+1)$. Separating $\Theta$ and $\Phi$ then gives:

$$
\frac{1}{\Theta} \left[ \sin\theta \frac{\partial }{\partial \theta} \left( \sin\theta \frac{\partial \Theta}{\partial \theta} \right) + l(l+1) \sin^2 \theta \right] = - \frac{1}{\Phi} \frac{\partial^2 \Phi}{\partial \phi^2} = m_l^2.
$$ (sphericalharmonicseparated)

In equation&nbsp;{eq}`sphericalharmonicseparated`, we multiplied both sides with $\sin^2\theta$ and divided through by $\Theta \Phi$ to separate the variables. Repeating the same argument as above, both sides have to be equal to a constant, which (again for future purposes) we write as<sup>[^10]</sup> $m_l^2$. The equation for $\Phi$ is now easy:

$$
\frac{\mathrm{d}^2 \Phi}{\mathrm{d}\phi^2} = - m_l^2 \Phi,
$$ (sphericalharmonicphieq)

with solutions

$$
\Phi(\phi) = e^{i m_l \phi}.
$$ (sphericalharmonicphisln)

Here $m_l$ can be either positive or negative. As the variable $\phi$ 'wraps around the sphere', $\Phi(\phi)$ has to be periodic: $\Phi(\phi) = \Phi(\phi + 2\pi)$, which restricts the possible values of $m_l$ to integers, and we have $m_l \in \mathbb{Z}$. These solutions will perhaps not surprise you: we stated above that the spherical harmonics are like the Fourier modes of the sphere, and in the azimuthal (i.e., $\phi$) direction, they turn out to be the familiar sines and cosines.

In the polar (i.e., $\theta$) direction, things are less easy, as the polar angle runs only from $0$ to $\pi$. Our equation for $\Theta$ reads:

$$
\sin\theta \frac{\mathrm{d}}{\mathrm{d}\theta} \left( \sin\theta \frac{\mathrm{d}\Theta}{\mathrm{d}\theta}\right) + \left[ l(l+1)\sin^2\theta - m_l^2 \right] \Theta(\theta) = 0.
$$ (sphericalharmonicthetaeq)

```{index} Legendre polynomials
```
For $m_l = 0$, the solutions to equation&nbsp;{eq}`sphericalharmonicthetaeq` can be found through series substitution, which can be done most easily if you first make a coordinate transform from $\theta$ to $x = \cos\theta$. The series substitution then gives solutions in the form of the Legendre polynomials $P_l(x)$, defined as

$$
P_l(x) = \frac{1}{2^l l!} \left( \frac{\mathrm{d}}{\mathrm{d}x}\right)^l (x^2-1)^l,
$$ (Legendrepolynomialdef)

for any non-negative integer value of $l$ (i.e., $l= 0, 1, 2, 3, \ldots$). Solutions for nonzero $m_l$ are then derivatives of the Legendre polynomials, known as the associated Legendre polynomials:

$$
P_l^m(x) = (1-x^2)^{|m|/2} \left( \frac{\mathrm{d}}{\mathrm{d}x}\right)^{|m|} P_l(x).
$$ (assocLegendrepolynomialdef)

Although $P_l^m(x)$ is defined for any integer value of $m$, it is identically zero if $|m|>l$, restricting the number of allowed azimuthal modes. For the polar functions we thus find $\Theta(\theta) = A P_l^m(\cos\theta)$, and for the spherical harmonics, after normalization<sup>[^11]</sup>,

$$
Y_l^m(\theta, \phi) = \varepsilon \sqrt{\frac{2l+1}{4\pi} \frac{(l-|m|)!}{(l+|m|)!}} P_l^m(\cos\theta) e^{im\phi},
$$ (sphericalharmonics)

where $\varepsilon = 1$ if $m \leq 0$ and $\varepsilon = (-1)^m$ if $m>0$. We can rewrite the complex spherical harmonics given here as real functions by making linear combinations, giving sine and cosine dependencies on&nbsp;$\phi$ rather than complex exponentials. The resulting functions for the first four values of $l$ are plotted in {numref}`fig:sphericalharmonicplots`.

```{figure} images/SE/Spherical_Harmonics.png
:name: fig:sphericalharmonicplots
Visual representation of the (real) spherical harmonics for $l=0, 1, 2, 3$ (top to bottom) <sup>[^12]</sup>. The real harmonics are obtained from the complex ones by linear combinations of $P_l^m$ and $P_l^{-m}$, which give sine and cosine functions of $\phi$, instead of complex exponentials. Blue and yellow shaded regions have opposite sign. The single $l=0$ harmonic corresponds to uniform expansion / contraction, the three $l=1$ harmonics to translations in the $x$, $y$ and $z$ directions. Note that the $m_l = 0$ harmonics (central axis) are always axisymmetric (i.e., invariant under rotations about the $z$-axis), and that you can always go from a solution with a positive value of $m_l$ to one with a negative value by a rotation about the $z$-axis over an angle $\pi / 2m_l$.
```

(sec:hydrogenradialpart)=
### The radial equation

The spherical harmonics are the solutions of the angular half of equation&nbsp;{eq}`SE3Dsphericalseparated`. The other half is a function of the radial coordinate&nbsp;$r$ alone:

$$
\frac{\mathrm{d}}{\mathrm{d}r} \left(r^2 \frac{\mathrm{d}R}{\mathrm{d}r} \right) - \frac{2 m r^2}{\hbar^2} \left[ V(r) - E \right] R(r) = l(l+1) R(r).
$$ (centralpotentialSEradialpart)

In this form, the radial equation is a rather tough nut to crack, as its coefficients are functions of&nbsp;$r$. Moreover, it contains both a first and a second derivative of $R(r)$. The term with the first derivative can be removed by introducing a new function, $u(r) = r R(r)$, which is simply a re-scaling of $R(r)$. In terms of $u(r)$, the equation reads

$$
-\frac{\hbar^2}{2m} \frac{\mathrm{d}^2u}{\mathrm{d}r^2} + \left[ V(r) + \frac{\hbar^2}{2m} \frac{l(l+1)}{r^2} \right] u(r) = E u(r),
$$ (centralpotentialSEcentrifugal)

which is similar in form to the one-dimensional Schr&ouml;dinger equation. The price of the rescaling is that we pick up an extra term in the potential, of the form $1/r^2$. Something similar happens in classical mechanics when you make a transformation from a fixed to a co-rotating coordinate system: you pick up terms that act like forces on your system. These additional forces are known as 'fictitious forces' (as they are not due to a potential but to your choice of coordinates), but their effects are very real {cite}`Idema2018`. Examples include the centrifugal force and the Coriolis force. The $1/r^2$ term we have here is a centrifugal term. In a classical picture, the corresponding outward force would compensate the inward Coulomb force on the electron, keeping it in a closed orbit.

Equation&nbsp;{eq}`centralpotentialSEcentrifugal` may look somewhat nicer than&nbsp;{eq}`centralpotentialSEradialpart`, but it still is a nontrivial eigenvalue problem. We can solve it in two ways: by series substitution and by the use of raising and lowering operators. We'll work out both, but first we need to 'rephrase' the normalization condition in terms of $u(r)$, as our final solutions will have to be normalized. We have
```{math}
:label: centrifugalfunctionnormalization
\begin{align*}
1 &= \Braket{\psi(\bm{r}) | \psi(\bm{r}} = \Braket{R(r) Y(\theta, \phi)| R(r) Y(\theta, \phi)}  \\
&= \int |R(r)|^2 |Y(\theta, \phi)|^2 \mathrm{d}^3\bm{r} = \int_0^\infty \int_0^\pi \int_0^{2\pi} |R(r)|^2 |Y(\theta, \phi)|^2 r^2 \sin\theta \mathrm{d}r \mathrm{d}\theta \mathrm{d}\phi  \\
&= \int_0^\infty |r R(r)|^2 \mathrm{d}r = \int_0^\infty |u(r)|^2 \mathrm{d}r,
\end{align*}
```
where we used in the third line that the spherical harmonics are normalized independently. The normalization condition on $u(r)$ is thus that its square integrates to&nbsp;$1$ over the range of positive real numbers&nbsp;$r$.

For the hydrogen atom, we have

$$
V(r) = - \frac{e^2}{4 \pi \varepsilon_0} \frac{1}{r}.
$$ (hydrogenpotential)

We have a continuum of scattering states with $E>0$, and a discrete spectrum of bound states with $E<0$. We will solve here for the latter. To simplify the notation, we rescale the energy by dividing equation&nbsp;{eq}`centralpotentialSEcentrifugal` through by $\hbar^2/2m_\mathrm{e}$, like we did before for the particle in the infinite well, and define:

$$
\kappa \equiv \frac{\sqrt{-2 m_\mathrm{e} E}}{\hbar}.
$$ (hydrogendefkappa)

Note that for bound states, $\kappa$ is real as $E$ is negative. I've added a subscript 'e' to the mass to indicate that we are dealing with the electron mass, and to distinguish from the number $m_l$. Note that $\kappa$ has dimensions of length, while $u$ is dimensionless. If we now divide equation&nbsp;{eq}`centralpotentialSEcentrifugal` by $\kappa^2$, we get terms that are all dimensionless:

$$
\frac{1}{\kappa^2} \frac{\mathrm{d}^2 u}{\mathrm{d}r^2} - \frac{1}{\kappa^2} \left[ -\frac{2m_\mathrm{e}}{\hbar^2} \frac{e^2}{4 \pi \varepsilon_0} \frac{1}{r} + \frac{l(l+1)}{r^2} \right] u(r) = u(r).
$$ (hydrogenSErescaling)

```{index} Bohr radius
```
The combination of factors in front of the $1/r$ term in the square brackets must have dimensions of inverse length, so we can define a characteristic length scale of the hydrogen atom, known as the *Bohr radius*:

$$
a_0 \equiv \frac{4 \pi \varepsilon_0 \hbar^2}{m_\mathrm{e} e^2},
$$ (defBohrradius)

where the choice of taking a numerical factor $4$ is historical (see equation&nbsp;{eq}`Bohratomradii`), but will make this length close to (the best definition of) the actual radius, as we will see. The numerical value of the Bohr radius is $5.29 \cdot 10^{-11}\;\mathrm{m}$, or about half an Ångstr&ouml;m. We'll see that $\kappa$ and $a_0$ are closely related.

Finally, we can express the radial coordinate in terms of the length scale&nbsp;$\kappa$, defining $\rho = \kappa r$ and $\rho_0 = 2 / \kappa a_0$. In terms of these rescaled variables, equation&nbsp;{eq}`hydrogenSErescaling` becomes

$$
\frac{\mathrm{d}^2u}{\mathrm{d}\rho^2} = \left[ 1 - \frac{\rho_0}{\rho} + \frac{l(l+1)}{\rho^2} \right] u(\rho).
$$ (hydrogenSEdimless)

We could try a series solution for equation&nbsp;{eq}`hydrogenSEdimless`, but the smarter approach is to check the asymptotic behavior first. In the limit of large $\rho$, only the first term in parentheses on the right-hand side of equation&nbsp;{eq}`hydrogenSEdimless` remains, and we are left with an equation that we can easily solve exactly, giving $u(\rho) = A e^{-\rho} + B e^\rho$. The second term of this solution diverges, and thus cannot be normalized, so we must set $B=0$; unsurprisingly the radial wave function will drop off like $e^{-\rho}$ at large values. In the limit that $\rho \to 0$, we find that the $1/\rho^2$ term dominates equation&nbsp;{eq}`hydrogenSEdimless`, and again we can solve the equation exactly if we ignore the other two terms, which gives $u(\rho) = C \rho^{l+1} + D \rho^{-l}$. As negative powers diverge when we approach zero, the second term again cannot be normalized, and we find that $u(\rho)$ scales as $\rho^{l+1}$ close to zero. An educated guess for the solution of equation&nbsp;{eq}`hydrogenSEdimless` would therefore be a function that interpolates between the two limit solutions. Introducing a new function $v(\rho)$, we write our trial solution as $u(\rho) = e^{-\rho} \rho^{l+1} v(\rho)$. We then find
```{math}
:label: hydrogenSEasymptotics
\begin{align*}
\frac{\mathrm{d}^2u}{\mathrm{d}\rho^2} &= e^{-\rho} \rho^l \left\lbrace \left[ \rho - 2(l+1) + \frac{l(l+1)}{\rho} \right] v(\rho) + 2 (l+1-\rho) \frac{\mathrm{d}v}{\mathrm{d}\rho} + \rho \frac{\mathrm{d}^2v}{\mathrm{d}\rho^2} \right\rbrace \\
&= \left[ 1 - \frac{\rho_0}{\rho} + \frac{l(l+1)}{\rho^2} \right] e^{-\rho} \rho^{l+1} v(\rho).
\end{align*}
```
At first glance, it seems that we made matters significantly worse: while in equation&nbsp;{eq}`hydrogenSEdimless` we had gotten rid of the first derivative term through re-scaling, we got it back in&nbsp;{eq}`hydrogenSEasymptotics`. On the other hand, the asymptotic factors $e^{-\rho}$ and $\rho^l$ appear on both sides of the equation and thus drop out, and we can therefore expect $v(\rho)$ to be well-behaved enough to allow for a series solution. Rearranging terms, our equation now reads:

$$
\rho \frac{\mathrm{d}^2v}{\mathrm{d}\rho^2} + 2 (l+1-\rho) \frac{\mathrm{d}v}{\mathrm{d}\rho} + \left[ \rho_0 - 2(l+1) \right] v(\rho) = 0,
$$ (hydrogenSEinterpolating)

for which we try a series expansion

```{math}
:label: hydrogenseriesexpansion
\begin{align*}
v(\rho) &= \sum_{j=0}^\infty c_j \rho^j, \\
\frac{\mathrm{d}v}{\mathrm{d}\rho} &= \sum_{j=0}^\infty j c_j \rho^{j-1} = \sum_{j=0}^\infty (j+1) c_{j+1} \rho^j, \\
\frac{\mathrm{d}^2 v}{\mathrm{d}\rho^2} &= \sum_{j=0}^\infty j (j+1) c_{j+1} \rho^{j-1},
\end{align*}
```

where in the second equality of&nbsp;{eq}`hydrogenseriesexpansion`b we shifted the summation by one (which we can do as the $j=0$ term was identically zero). We do not need such a shift in&nbsp;{eq}`hydrogenseriesexpansion`c as the second derivative of $v(\rho)$ is multiplied by $\rho$ in equation&nbsp;{eq}`hydrogenSEinterpolating`. Substituting back and collecting like terms, we get
```{math}
:label: centralpotentialSEseries
\begin{align*}
0 &= \sum_{j=0}^\infty \left\lbrace (j(j+1) c_{j+1} \rho^j 2(l+1) (j+1) c_{j+1} \rho^j - 2 j c_j \rho^j + \left[\rho_0 - 2(l+1)\right] c_j \rho^j \right\rbrace  \\
&= \sum_{j=0}^\infty \left\lbrace (j+1)(j + 2l + 2) c_{j+1} + \left[\rho_0 - 2(j+l+1)\right] c_j \right\rbrace \rho^j.
\end{align*}
```
Now for the series to vanish, each individual term must vanish, which gives us a recursion relation between the coefficients:

$$
c_{j+1} = \frac{2(j+l+1) - \rho_0}{(j+1)(j + 2l + 2)} c_j.
$$ (hydrogenSErecursion)

Given a starting value $c_0$, we can then find all the $c_j$'s. Typically, series solutions come in two flavors: those for which the series terminates at some point (with polynomials as solutions) and those which go on with nonzero terms indefinitely. In this case, the non-terminating series cannot be normalized. To see why, consider the limit in which $j$ gets large, then&nbsp;{eq}`hydrogenSErecursion` goes to

$$
c_{j+1} = \frac{2}{j+1} c_j = \frac{2^{j+1}}{(j+1)!} c_0.
$$

The sum of the series with these coefficients is well known: it's $e^{2\rho}$. Even with the $e^{-\rho}$ factor that we will multiply $v(\rho)$ with to get the full solution, this series diverges at large $\rho$, and thus will not give us normalizable solutions. The only allowable solutions are thus the ones for which the series terminates at some point, i.e., $c_{j+1} = 0$ for some value of $j$. If $j_\mathrm{max}$ is the largest value of $j$ for which $c_j$ is nonzero, we have

$$
\rho_0 = 2(j_\mathrm{max}+l+1) = 2n,
$$ (hydrogenseriestermination)

so we find that the values of $\rho_0$ are quantized. The smallest possible value of $n$ is $1$ (for $j_\mathrm{max} = l = 0$), and $n$ can take any integer value greater than zero. However, for a given value of $n$, $l$ is now restricted from above to $n-1$, as for a larger value equation&nbsp;{eq}`hydrogenseriestermination` cannot be satisfied for any nonnegative $j_\mathrm{max}$.

To get the actual energies, we simply re-substitute the definitions of $\rho_0$ and $\kappa$, which gives:

$$
E_n = - \frac{\hbar^2}{2 m_\mathrm{e} a_0^2} \frac{1}{n^2} = -\frac{R_\mathrm{E}}{n^2}.
$$ (hydrogenenergies)

We have retrieved Bohr's result from {numref}`sec:matterquantization` (equation&nbsp;{eq}`Bohratomenergies`). That means that we also retrieve the predictions for the absorption and emission spectra of the hydrogen atom. However, the classical orbits Bohr assumed are replaced with wave functions (sometimes referred to as 'orbitals'), the square of which represents a probability distribution, not a trajectory through space. A measurement of an electron in a state with $n=2$ might give a position closer to the nucleus than the measurement of an election in the $n=1$ state. Moreover, the positions are not restricted to a plane, but rather have a spherical symmetry around the nucleus, making them very different from the Keplerian orbits of planets around the sun.

```{index} Laguerre polynomials
```
The actual polynomial functions&nbsp;$v(\rho)$ that are the solutions of equation&nbsp;{eq}`hydrogenSEinterpolating` are given in terms of the (associated) Laguerre polynomials $L_q^p(x)$, defined by

```{math}
:label: Laguerrepolynomials
\begin{align*}
L_q^p(x) &= (-1)^p \left(\frac{\mathrm{d}}{\mathrm{d}x}\right)^p L_{p+q}(x) = \frac{x^{-p} e^x}{q!} \left(\frac{\mathrm{d}}{\mathrm{d}x}\right)^q \left( e^{-x} x^{p+q} \right), \\
L_q(x) &= \frac{e^x}{q!} \left(\frac{\mathrm{d}}{\mathrm{d}x}\right)^q \left( e^{-x} x^q \right).
\end{align*}
```

Given values of $n$ and $l$, the function $v(\rho)$ is given by

$$
v(\rho) = L_{n-l-1}^{2l+1}(2\rho).
$$

Substituting back, we can eventually work out the radial function $R(r)$, which we characterize by $n$ and $l$. The (normalized) first six are given by

```{math}
:label: hydrogenradialwavefunctions
\begin{align*}
R_{10}(r) &= \frac{2}{\sqrt{a_0^3}} e^{-r/a_0}, \\
R_{20}(r) &= \frac{1}{\sqrt{2 a_0^3}} \left(1-\frac{r}{2 a_0} \right) e^{-r/2a_0}, \\
R_{21}(r) &= \frac{1}{\sqrt{6 a_0^3}} \left(\frac{r}{2 a_0} \right) e^{-r/2a_0}, \\
R_{30}(r) &= \frac{2}{3\sqrt{3 a_0^3}} \left(1 - \frac{2r}{3 a_0} + \frac23 \left(\frac{r}{3 a_0}\right)^2 \right) e^{-r/3a_0}, \\
R_{31}(r) &= \frac{8}{9\sqrt{6 a_0^3}} \left(1 - \frac12 \frac{r}{3 a_0} \right) \left( \frac{r}{3 a_0} \right)  e^{-r/3a_0}, \\
R_{32}(r) &= \frac{4}{9\sqrt{30 a_0^3}} \left( \frac{r}{3 a_0} \right)^2  e^{-r/3a_0}.
\end{align*}
```

```{index} quantum number, quantum number ; principal, quantum number ; orbital, quantum number ; magnetic
```
The radial wave functions are plotted in {numref}`fig:hydrogenradialfunctions`. Note the patterns: the $l=0$ functions contain a exponential decay with $l$ crossings of the horizontal axis, while the $l>0$ functions start at $0$ and have a well-defined local maximum. The product of these radial functions with the spherical harmonic functions $Y_l^m(\theta, \phi)$ for the angular part gives the full solution $\psi_{nlm}(r, \theta, \phi)$. The full wave functions are thus characterized by the three *quantum numbers* $n$, $l$ and $m_l$, known as the principal, orbital, and magnetic quantum number, respectively<sup>[^13]</sup>. It is an easy exercise to show that for a given value of the principal quantum number $n$, there are $n^2$ states. As the energy depends on $n$ alone, these states will thus be $n^2$-fold degenerate.

```{code-cell} ipython3
:tags: [hide-input, remove-output]

%config InlineBackend.figure_formats = ['svg']
import numpy as np
import matplotlib.pyplot as plt
from myst_nb import glue

def R10(r):
    # Returns R_{10}(r), with r in units of the Bohr radius.
    return 2 * np.exp(-r)

def R20(r):
    # Returns R_{20}(r), with r in units of the Bohr radius.
    return (1 / np.sqrt(2)) * (1 - r/2) * np.exp(-r/2)

def R21(r):
    # Returns R_{21}(r), with r in units of the Bohr radius.
    return (1/np.sqrt(6)) * (r/2) * np.exp(-r/2)

def R30(r):
    # Returns R_{30}(r), with r in units of the Bohr radius.
    return (2 / np.sqrt(27)) * (1 - (2*r/3) + (2/3) * pow(r/3, 2)) * np.exp(-r/3)

def R31(r):
    # Returns R_{31}(r), with r in units of the Bohr radius.
    return (8/np.sqrt(486)) * (1- r/6) * (r/3) * np.exp(-r/3)

def R32(r):
    # Returns R_{32}(r), with r in units of the Bohr radius.
    return (4/np.sqrt(2430)) * pow(r/3,2) * np.exp(-r/3)

r = np.linspace(0, 10, 1000)

fig, ax = plt.subplots(figsize=(6,4))

line10 = ax.plot(r, R10(r), label='$R_{10}(r)$')
line20 = ax.plot(r, R20(r), label='$R_{20}(r)$')
line21 = ax.plot(r, R21(r), label='$R_{21}(r)$')

line30 = ax.plot(r, R30(r), label='$R_{30}(r)$')
line31 = ax.plot(r, R31(r), label='$R_{31}(r)$')
line32 = ax.plot(r, R32(r), label='$R_{32}(r)$')

ax.axhline(y = 0, color = 'k', linestyle = ':')

ax.set_xlim(0,10)
ax.set_ylim(-0.2,1.0)
ax.set_xlabel('$r/a_0$')
ax.set_ylabel('$R_{nl}$')
ax.legend()
# Save graph to load in figure later (special Jupyter Book feature)
glue("hydrogenradialfunctions", fig, display=False)
```

```{glue:figure} hydrogenradialfunctions
:name: fig:hydrogenradialfunctions
Radial part of the first six eigenfunctions of the hydrogen Hamiltonian.
```


## Problems
````{exercise} Linear combinations of stationary states
:label: pb:stationarystates
:class: dropdown
Consider the stationary wave functions $\psi_1(x)$ and $\psi_2(x)$. Assume that they are normalized solutions to the stationary Schr&ouml;dinger equation with energies $E_1$ and $E_2$ respectively. Now let
```{math}
:label: linearcombinationstate
\Psi(x,t) = c_1 \psi_1(x) e^{-iE_1 t/\hbar} + c_2 \psi_2(x) e^{-iE_2 t/\hbar}
```
be a linear combination of both solutions, with $c_1$ and $c_2$ (complex and nonzero) constants. Furthermore assume that $c_1$ and $c_2$ are chosen such that $\Psi$ is normalized.
1. Show that $\Psi(x,t)$ is a solution to the time-dependent Schr&ouml;dinger equation.
1. Compute the probability density $|\Psi(x,t)|^2$. Does it depend on $t$? In which case is $\psi = c_1 \psi_1(x) + c_2 \psi_2(x)$ also a solution to the stationary Schr&ouml;dinger equation?
1. Show that $E_1$ (and by similarity also $E_2$) must be real. Hint: Write $E_1 = E + i \Gamma$ with $E$ and $\Gamma$ real and show that for $\Psi_1(x,t) = \psi_1(x)e^{-iE_1t/\hbar}$ to be normalizable for all $t$ we must have $\Gamma = 0$.
1. Show that $\frac12 (\psi_1 + \psi_1^*)$ is also a solution to the stationary Schr&ouml;dinger equation. What can you conclude from this observation?
````

````{exercise} A particle in an infinite square well
:label: pb:infiniteSquareWell1
:class: dropdown
We consider a particle in the infinite square well, i.e. the potential is given by
```{math}
V(x) = \begin{cases}
0, & \text{if } 0 \leq x \leq a, \\
\infty, & \text{else,} \\
\end{cases}
```
where $a>0$ is a real constant. Let the initial state of the particle (with mass $m$) be given by
```{math}
\Psi(x,0) = \frac{1}{\sqrt{3}} \psi_2 + A \psi_4.
```
1. Find $A$ such that $\Psi(x,0)$ is normalized. Hint: this can be done without explicitly computing any integrals.
1. Find $\Psi(x,t)$ and compute $|\Psi(x,t)|^2$ explicitly. Simplify your answer by setting $\omega = \frac{\pi^2 \hbar }{2ma^2}$.
1. Compute $\Braket{\hat{x}}$. Does it depend on time? Why (not)?
1. Compute $\Braket{\hat{H}}$.
1. If an energy measurement is performed on $\Psi(x,t)$ at time $t$, what outcomes can we get and with what probability? Is $\Braket{\hat{H}}$ a possible outcome? Why (not)?
````

````{exercise} Particle phase in the infinite square well
:label: pb:infiniteSquareWell3
:class: dropdown
We consider a particle in the infinite square well, i.e. the potential is given by
```{math}
V(x) = \begin{cases}
0, & \text{if } 0 \leq x \leq a, \\
\infty, & \text{else,} \\
\end{cases}
```
where $a>0$ is a real constant. Let the initial state of the particle (with mass $m$) be given by
```{math}
\Psi(x,0) = \frac{1}{\sqrt{2}}  ( \psi_1 + e^{i \phi} \psi_2),
```
where $0 \leq \phi < 2 \pi$ is an arbitrary phase and $\psi_1$ and $\psi_2$ are the first two eigenstates of the Hamiltonian.
1. Find $\Psi(x,t)$ and $|\Psi(x,t)|^2$ in terms of $\psi_1$, $\psi_2$, $E_1$, $E_2$ and $\phi$.
1. Find $\Braket{\hat{x}}$ and $\Braket{\hat{p}}$. Do they depend on time? What is the role of $\phi$? Use $\omega = \frac{\pi^2 \hbar}{2ma^2}$ to simplify the result. Hint: Use Ehrenfest's theorem to relate $\Braket{\hat{x}}$ and $\Braket{\hat{p}}$ through a classical law.
1. What energies can be obtained if a measurement is performed? With what probabilities? Also, find $\braket{\hat{H}}$.
	\setcounter{problemcounter}{\value{enumii}}
	Suppose now that at $t=0$ the well suddenly expands and the right wall is moved to $x = 2a$. The initial wave function is unchanged by this process.
	\setcounter{enumii}{\value{problemcounter}}
1. What are now the four smallest outcomes an energy measurement could yield? What are the probabilities of finding them? To simplify the calculation you may set $\phi = 0$ here.
1. Find $\braket{\hat{H}}$ again.
````

````{exercise} A free particle
:label: pb:freeparticle
:class: dropdown
Suppose we have a particle of mass $m$ in the initial state
```{math}
\Psi(x,0) = \begin{cases}
A \sin\left(\frac{\pi x}{a}\right) & \text{if } -a \leq x \leq a, \\
0 & \text{otherwise,}
\end{cases},
```
where $A$ and $a$ are real and positive constants.
1. Normalize the wave function.
1. For what potential is the initial state a stationary state? Is it the ground state of this potential? What is the corresponding energy?
1. Find $\Braket{\hat{H}}$ at $t=0$, using the general method $\Braket{\hat{Q}(x,p)} = \int \Psi^* Q(x, -i\hbar \frac{\mathrm{d}}{\mathrm{d}x}) \Psi \mathrm{d}{x}$. Is this in agreement with your previous answer?
1. Now we assume that we have the initial state $\Psi(x,0)$ in a zero potential. Find $\phi(k)$, the Fourier transform of the initial wave function. Hint: You can solve the integral by hand if you use integration by parts twice.
1. Find $\Psi(x,t)$ in terms of an integral over the wavenumber&nbsp;$k$.
````

````{exercise} Gaussian wave packet
:label: pb:Gaussianwavepacket
:class: dropdown
We consider a free particle with a wave function initialized as
```{math}
:label: Gaussianwavepacket
\Psi(x, 0) = A e^{- a x^2},
```
where $a$ and $A$ are real, positive constants.
1. Normalize $\Psi(x, 0)$, i.e., find $A$.
1. Find $\phi(k)$, i.e. the weight of the eigenstate with wavenumber $k$ (cf. equation&nbsp;{eq}`freeparticleSEwavepacket`).
1. Show that the time evolution of the particle's wave function is given by
	```{math}
	:label: Gaussianwavepackettimeevolution
	\Psi(x, t) = \frac{A}{\sqrt{\gamma}} e^{-a x^2 / \gamma},
	```
	where $\gamma$ is a number that depends on time. Find an expression for $\gamma$.
1. Find $|\Psi(x, t)|^2$ and sketch it for $t=0$ and some large value of $t$. *Hint*: it will help to group some numbers together.
````

````{exercise} Bound state of the Dirac delta potential
:label: pb:Diracdeltaboundstate
:class: dropdown
In this problem, we'll find the bound state of the Dirac delta potential well (and prove that there is only one).  Our potential is the same as in equation&nbsp;{eq}`Diracdeltapotential`, with the restriction that $\varepsilon < 0$ (as for positive values of $\varepsilon$ we have a barrier, not a well, and hence no bound states). The time-independent Schr&ouml;dinger equation then reads:
```{math}
:label: SEdeltapot
-\frac{\hbar^2}{2m} \frac{\mathrm{d}^2 \psi}{\mathrm{d}x^2} + \varepsilon \delta(x) \psi = E \psi.
```
As we're looking for bound states, we want solutions with $E<0$.
1. As $E<0$, we can define the (real, positive) number $\kappa = \sqrt{-2mE}/\hbar$. Re-write equation&nbsp;{eq}`SEdeltapot` in terms of $\kappa$.
1. Find the general solution to&nbsp;{eq}`SEdeltapot` for $x<0$.
1. Also find the general solution to&nbsp;{eq}`SEdeltapot` for $x>0$.
1. Of the solutions in (b) and (c), cross out the ones that diverge.
1. Apply the condition that $\psi(x)$ be continuous at $x=0$ to eliminate one of the constants in your solutions.
1.
	Apply the condition&nbsp;{eq}`wavefunctionderivativejump` on the derivative of $\psi(x)$ to find the allowed value of $\kappa$, and hence of $E$.
1. Normalize the solution.
1. Plot the wavefunction and the probability density.
````

````{exercise} The double delta potential
:label: pb:doubledeltapotential
:class: dropdown
\label{prob:doubleDeltaPotential}
In this problem we will consider the symmetric double delta potential well, given by
```{math}
V(x) = -\alpha [\delta(x-a) + \delta(x+a)],
```
where $\alpha$ is a positive constant of appropriate dimension (Joule $\times$ meter, such that the potential is in Joule).
This potential function is even (i.e. $V(x) = V(-x)$) and therefore the bound state solutions are either even ($\psi_\mathrm{even}(x) = \psi_\mathrm{even}(-x)$) or odd ($\psi_\mathrm{odd}(x) = - \psi_\mathrm{odd}(-x)$). Since we are looking for a bound state, we have $E < 0$. Define $\kappa = \frac{\sqrt{-2mE}}{\hbar} > 0$.
1. Write out the general form of the even solution using constants complex $A$ and $B$, $\kappa$ and exponentials. Exploit the symmetry of the solution.
1. Impose the continuity of $\psi$ and the appropriate discontinuity of its derivative at $x=a$. Use these two equations to eliminate the complex constants $A$ and $B$. Find the equation that relates the unknown $\kappa$ to other known physical constants (including $\alpha$ and $a$).
1. How many solutions does this equation have? What does this mean?
1. Proceed in the same way (questions b-d) for finding the odd solution(s).
````

```{exercise} Ground state of the harmonic oscillator
:label: pb:harmonicoscillatorgroundstate
:class: dropdown
1. Through separation of variables and integrating (see {numref}`app:folode`), solve the first-order differential equation&nbsp;{eq}`harmonicoscillatorlowestrungdiffeq` for the ground state of the harmonic oscillator potential.
1. Find the normalization constant of the harmonic oscillator ground state.
1. Find the energy of the harmonic oscillator ground state. Hint: use equation&nbsp;{eq}`Hamiltonianraisinglowering` for the Hamiltonian in combination with the defining property of the ground state $\psi_0(x)$.
```

```{exercise} Harmonic oscillator operators
:label: pb:harmonicoscillatoroperators
:class: dropdown
Prove that the raising and lowering operators $\hat{a}_\pm$ of the harmonic oscillator potential are each other's Hermitian conjugates, i.e., that equation&nbsp;{eq}`raisingloweringopertorsHermitianconjugates` holds.
```

````{exercise} Combining harmonic oscillator states
:label: pb:harmonicoscillatorlinearcombination
:class: dropdown
As we've seen in {numref}`sec:harmonicpotential`, we can find the eigenstates of the one-dimensional harmonic potential, $V(x) = \frac12 m \omega^2 x^2$, through repeated application of the raising operator $\hat{a}_{+}$ on the ground state&nbsp;$\psi_0(x)$, and we get the ground state itself from the condition that acting on it with the lowering operator $\hat{a}_{+}$ should give zero (i.e., $\hat{a}_- \psi_0 = 0$).
1. Construct and normalize the first excited state, $\psi_1(x)$, which is proportional to $\hat{a}_+ \psi_0(x)$. Verify that your result satisfies the general rule that
	```{math}
	:label: HOhighereigenfunctions
	\psi_n = \frac{1}{\sqrt{n!}} \left(\hat{a}_+\right)^n \psi_0(x).
	```
1. Construct a new, normalized state $\psi$, which is a linear combination of $\psi_0$ and $\psi_1$, i.e., $\psi = c_0 \psi_0 + c_1 \psi_1$, such that $\Braket{\hat{x}}$ is as large as possible. The answer, as you might guess, is given by
	```{math}
	:label: HOlincombinationmaxX
	\psi = \frac{1}{\sqrt{2}} \left( \psi_0(x) + \psi_1(x) \right).
	```
	For a full solution, you'd have to allow the coefficients $c_0$ and $c_1$ to take complex values, but for here, it suffices to take them to be real. You may use without needing to prove it that the eigenstates of the harmonic potential Hamiltonian are orthogonal.
1. A particle is initialized at $t=0$ in the state&nbsp;$\psi$ you constructed at (b). Give its time evolution, i.e., give the $\Psi(x, t)$ corresponding to this wave function $\Psi$.
1. If you were to measure the energy of the state, would it matter at which point in time you do so?
````

````{exercise} Eigenfunctions of the harmonic oscillator potential
:label: pb:harmonicoscillatorseriessolution
:class: dropdown
In {numref}`sec:harmonicpotential` we found the eigenvalues of the Hamiltonian with the harmonic oscillator potential. We also found a method to construct the eigenfunctions, by repeatedly acting with the raising operator $\hat{a}_+$ on the ground state wave function $\psi_0(x)$. While in many cases we'll encounter later we'd already be glad to have numbers for the eigenvalues, in this case, we can also solve for the eigenfunctions directly, using a series expansion approach (see {numref}`app:odepowerseries`). The steps we follow here will also be the ones we'll employ to find the radial part of the eigenfunctions of hydrogen atom in {numref}`sec:hydrogenradialpart`.
1. First, to avoid mistakes due to factors floating around, we'll rescale our variables to get the equation in the simplest form possible. Introduce $\xi = \sqrt{m\omega/\hbar} x$ and $K = 2E/\hbar\omega$, and show that in these variables, the time-independent Schr&ouml;dinger equation with the harmonic oscillator potential (equation&nbsp;{eq}`SEHP`) can be written as
	```{math}
	:label: TISEHarmonicRescaled
	\frac{\mathrm{d}^2 \psi}{\mathrm{d}\xi^2} = \left(\xi^2 - K \right) \psi.
	```
1. Before attempting a series solution, we should look at the asymptotic behavior of the solution. If that turns out to be exponential, we need to factorize the solution first, as otherwise the series will not converge. In the present case, for very large values of $\xi$ (and equivalently very large values of&nbsp;$x$), we have $\xi^2 \gg K$, so we can approximate equation&nbsp;{eq}`TISEHarmonicRescaled` as
	```{math}
	:label: TISEHarmonicRescaledApprox
	\frac{\mathrm{d}^2 \psi}{\mathrm{d}\xi^2} = \xi^2 \psi.
	```
	Equation&nbsp;{eq}`TISEHarmonicRescaledApprox` suggests an Ansatz of the form $\psi = \exp(\lambda \xi^2)$. Substitute this Ansatz to get possible values for $\lambda$. NB: you will not get an exact solution - there will be a remainder term. However, we're not after the exact solution here, but the asymptotic behavior, which will scale like the Ansatz.
1. Of the two solutions you found in (b), one is not normalizable (as it diverges as $x \to \pm \infty$), so we drop it. Retaining the other term, we try a solution of the form
	```{math}
	\psi(\xi) = u(\xi) e^{-\frac12 \xi^2}.
	```
	Find the differential equation for $u(\xi)$.
1. We now try a power series solution for&nbsp;$u(\xi)$, i.e., we write $u(\xi)$ as
	```{math}
	:label: harmoscpowerseries
	u(\xi) = \sum_{k=1}^\infty a_k \xi^k.
	```
	Substitute equation&nbsp;{eq}`harmoscpowerseries` in your differential equation for $u(\xi)$, then rearrange terms such that the equation is of the form of a power series (i.e., find the coefficients of $\xi^k$, see equation&nbsp;{eq}`Legendreodepowerseries`).
1. From the power series solution, get a recurrence relation for the coefficients $a_k$.
1. There are now two options: either the power series goes on forever, or it truncates at some finite value of $k$. We'll first show that the first option gives a non-normalizable solution. From your recurrence relation, you should be able to read off that for large values of $k$, we approximately have $a_{k+2} = (2/k)a_k$, with (approximate) solution
	```{math}
	:label: harmoscpowerseriesinfiniteapprox
	a_k \approx \frac{A}{(k/2)!},
	```
	with $A$ a constant. Find the function $u(\xi)$ for the 'solution'&nbsp;{eq}`harmoscpowerseriesinfiniteapprox`, and argue it won't be an acceptable solution.
1. Having ruled out the possibility that the series continue forever, it has to terminate at some point. From that condition, get the possible values of $K$, and thus of the energy eigenvalue $E$.
1. As the recurrence relation only contains $a_k$ and $a_{k+2}$, we get two qualitatively distinct sets of solutions, one with all 'odd-$k$' $a_k$'s equal to zero (giving an even function) and one with all 'even-$k$' $a_k$'s equal to zero (giving an odd function). Argue why only one of the two sets is permissable at a given time.
1. We get the two solutions from initial conditions $a_1=0$ and $a_0 = 0$, respectively. The resulting polynomials for $u(\xi)$ are multiples of the *Hermite polynomials*, denoted $H_n(\xi)$. Find $u(\xi)$ for $n=0, 1, 2, 3$.
1. Find $\psi_0(x)$ and $\psi_1(x)$.
````

```{exercise} The shifted harmonic oscillator
:label: pb:shiftedHarmonicOscillator
:class: dropdown
Consider the one-dimensional harmonic oscillator with a uniform electric field $\mathcal{E}$ (we use $\mathcal{E}$ for electric field and reserve $E$ for energy). The potential energy then becomes $V(x) = \frac{1}{2} m \omega^2 x^2 - e \mathcal{E} x$, where $e$ is the electron charge.
1. Sketch the potential.
1. Write down the Schr&ouml;dinger equation for this problem.
1. Use a coordinate transformation of the form $z = x - C$, where $C$ is a constant (possibly depending on $e$, $\mathcal{E}$, $m$, $\hbar$ or $\omega$), to transform the Schr&ouml;dinger equation of this problem into the equation for the unperturbed harmonic oscillator. What is $C$?
1. What are the eigenfunctions and energies of this problems (let us denote them $\tilde{\psi}_n$ and $\tilde{E}_n$) in terms of $\psi_n$ and $E_n$, the eigenfunctions and energies of the unperturbed harmonic oscillator.
1. For what field strength $\mathcal{E}$ can we find a ground state with zero energy?
```

```{exercise} Spherical harmonics
:label: pb:sphericalharmonicsseriessolution
:class: dropdown
Find the solution to the $\theta$-dependent part of the equation for spherical harmonics, {eq}`sphericalharmonicthetaeq`, using a series expansion (steps to be added).
```

````{exercise} An infinite square well with an internal barrier
:label: pb:wellwithinternalbarrier
:class: dropdown
In this problem, we consider the infinite square well potential with an internal barrier. To be able to exploit the symmetry of the system, we'll shift the well such that it is centered around zero, and the potential reads
```{math}
:label: wellwithbarrierpotential
V(x) = \left\lbrace \begin{array}{ll}
V_0 &\; \text{for}\; |x|<a,\\
0 & \;\text{for}\; a<|x|<b,\\
\infty &\;\text{otherwise}.
\end{array} \right.
```
For energies much larger than $V_0$, the eigenstates of the Hamiltonian corresponding to this potential will essentially be the same as those for an infinite well. For energies smaller than $V_0$ however, the region $-a < x < a$ will be classically forbidden. We did not solve the Schr&ouml;dinger equation for such a finite barrier in class, but the solution method is straightforward, as the potential is piecewise constant. The pieces can then be glued together by the boundary condition that the wavefunction and its derivative must be continuous at $x=a$. Unfortunately, these conditions give us equations that we cannot solve analytically, and thus we are reduced to numerical methods. Those methods tell us that there are two solutions with energies very close together: the ground state, which is symmetric (or 'even', like the cosine) and the first excited state which is antisymmetric (or 'odd', like the sine). We call the ground state $\psi_\mathrm{e}$ with energy $E_\mathrm{e}$, and the excited state $\psi_\mathrm{o}$ with energy $E_\mathrm{o}$. They are plotted in {numref}`fig:wellwithbarrier` on top of  a sketch of the potential itself.
```{figure} images/SE/problems/wellwithbarrier.svg
:name: fig:wellwithbarrier
The infinite square well potential with an internal barrier (equation&nbsp;{eq}`wellwithbarrierpotential`) in dashed gray, with the (even) ground state&nbsp;$\psi_\mathrm{e}$ and (odd) first excited state&nbsp;$\psi_\mathrm{o}$ plotted on top in blue and (dashed) red.
```
As you can see from the plot, for the finite barrier there is a small but nonzero probability of finding the particle inside the classically forbidden region. Moreover, in both states, the particle will, over time, oscillate between being on the left and being on the right of the potential. To see how that comes about, suppose that we prepare a particle such that it is initially in a linear combination of the two states, given by
```{math}
:label: wellwithbarrierevenoddsolutions
\psi_\pm(x) = \frac{1}{\sqrt{2}} \left[ \psi_\mathrm{e}(x) \pm \psi_\mathrm{o}(x) \right].
```
A particle in the $\psi_+$ linear combination will almost certainly be on the right of the barrier, and a particle in the $\psi_-$ combination will almost certainly be on the left. Suppose at time $0$ we prepare a particle in the state $\psi_+$.
1. Write down the time-dependent wavefunction $\Psi(x, t)$ for the particle in the initial state $\psi_+(x)$. Give your answer in terms of $\psi_\mathrm{e}(x)$, $\psi_\mathrm{o}(x)$, $E_\mathrm{e}$ and $E_\mathrm{o}$.
1. Show / argue that after a time interval $\Delta T = \pi \hbar / (E_\mathrm{o}-E_\mathrm{e})$ the particle, initially right of the barrier, has tunneled through it to the left.
	As an application of the well-with-barrier potential, we consider the ammonium molecule $\mathrm{NH}_3$, which consists of one nitrogen and three hydrogen atoms. Because ammonia contains four nuclei and ten electrons, despite its relative simplicity it is already a highly complicated dynamical system. However, we can get quite far by some approximations. One such approximation is treating the covalent bonds formed by the electrons is as if they were springs keeping the nuclei together (this works because the harmonic potential is quadratic, and the expansion of any potential around any minimum will at lowest order have a quadratic term); the nuclei then just oscillate around the equilibrium positions defined by the potential energies of the 'electron springs'. For ammonia, the equilibrium configuration is attained when the three hydrogen nuclei form an equilateral triangle, with the nitrogen molecule some distance $z$ above or below the plane of the triangle, defining two possible minima with a barrier in between. To simplify notation, we define $\ket{\mathrm{e}}$ and $\ket{\mathrm{o}}$ as the even and odd eigenstates of the corresponding Hamiltonian, with energies $E_\mathrm{e}$ and $E_\mathrm{o}$ as before. The $\ket{+}$ state is then the linear combination in which the nitrogen atom lies (almost certainly) above the plane of the hydrogen triangle, and $\ket{-}$ the linear combination in which it lies below the plane:
	```{math}
	\ket{\pm} = \frac{1}{\sqrt{2}} \left( \ket{\mathrm{e}} \pm \ket{\mathrm{o}}\right).
	```
1. Find the matrix elements $\braket{+|\hat{H}|+}$, $\braket{+|\hat{H}|-}$, and $\braket{-|\hat{H}|-}$ in terms of the energies $E_\mathrm{e}$ and $E_\mathrm{o}$. *Hint*: use what you know of the eigenstates of the Hamiltonian, but indicate which properties you use.
1. Argue that we can represent the Hamiltonian with the following matrix
	```{math}
	\hat{H} = \begin{pmatrix}
	\bar{E} & -A \\ -A & \bar{E},
	\end{pmatrix}
	```
	and find expressions for $\bar{E}$ and $A$ in terms of $E_\mathrm{e}$ and $E_\mathrm{o}$.
	Due to the electronic structure of ammonia, the nitrogen atom carries a small negative charge $-q$, with a corresponding positive charge $+q$ distributed among the hydrogen atoms. If the molecule is in either the $\ket{+}$ or $\ket{-}$ state, there is a separation of charge, and the molecule therefore become an electric dipole. Because (as you argued in part b), the molecule will oscillate between these two states, it is an oscillating dipole, and thus will emit electromagnetic radiation; it indeed does so at a frequency of $150\;\mathrm{GHz}$ (i.e., in the microwave range).
1. From the given frequency of the dipole radiation, find the difference in energy between the ground state ($E_\mathrm{e}$) and the first excited state ($E_\mathrm{o}$) of ammonia. Express your answer in electronvolts ($\mathrm{eV}$, one electronvolt is the energy an electron gets when passing through a potential difference of one volt).
````

````{exercise} The first excited state of the hydrogen atom
:label: pb:hydrogenexcitedstate
:class: dropdown
The radial part of the Hamiltonian of a hydrogen atom (or, strictly speaking, the electron in that atom) is given by
```{math}
:label: HydrogenHamiltonianRadial
\hat{H} = -\frac{\hbar^2}{2 m_\mathrm{e} r^2} \frac{\mathrm{d}}{\mathrm{d}r} \left( r^2 \frac{\mathrm{d}}{\mathrm{d}r} \right) - \frac{k e^2}{r},
```
where $\hbar$ is Planck's constant, $m_\mathrm{e}$ and $e$ the mass and charge of the electron, and $k$ the proportionality constant from Coulomb's law. The wave function of hydrogen in the ground state (the $100$ or $1s$ state) is radially symmetric and given by
```{math}
:label: hydrogengroundstate
\psi_{100}(\bm{r}) = \frac{1}{\sqrt{\pi a^3}} e^{-r/a},
```
where $a$ is the Bohr radius.
1. Prove that $\psi_{100}(r)$ is indeed an eigenfunction of the Hamiltonian {eq}`HydrogenHamiltonianRadial`, and find both the associated energy and the value of the Bohr radius in terms of $\hbar$, $m_\mathrm{e}$, $e$, and $k$.
	\setcounter{problemcounter}{\value{enumii}}
	The wave function for hydrogen in the $200$ (or $2s$) state is given by
	```{math}
	:label: hydrogen2sstate
	\psi_{200}(\bm{r}) = \frac{1}{\sqrt{32\pi a^3}} \left(2 - \frac{r}{a}\right) e^{-r/2a}.
	```
	To calculate probabilities and expectation values of a particle in the $\psi_{200}$ state requires integrating over three-dimensional space. Fortunately, this state, like the ground state, is radially symmetric: it is independent of the angles $\theta$ and $\phi$. For any such radially symmetric function $f(r)$, we can simplify the volume integral to a one-dimensional integral by switching to spherical coordinates:
	```{math}
	:label: radiallysymmetricvolumeintegral
	\int_{\text{all space}} f(r) \,\mathrm{d}V = \int_0^\infty f(r) 4 \pi r^2 \,\mathrm{d}r.
	```
	In this problem, you may use the following integrals:
	```{math}
	\int_0^1 r^2 e^{-r} \,\mathrm{d}r = 2 - \frac{5}{e}, \qquad \int_0^1 r^3 e^{-r} \,\mathrm{d}r = 6 - \frac{16}{e}, \qquad \int_0^1 r^4 e^{-r} \,\mathrm{d}r = 24 - \frac{65}{e}.
	```
	\setcounter{enumii}{\value{problemcounter}}
1. Find the probability that an electron in the $200$ state of hydrogen will be found a distance less than $a$ from the nucleus.
1. Find the expectation values of $r$ and $r^2$ for an electron in the $200$ state of hydrogen. Express your answers in terms of the Bohr radius $a$.
1. An electron in the $200$ state of hydrogen can transition to the $100$ state under the emission of a photon. Find the frequency of that photon in terms of $\hbar$, $m_\mathrm{e}$, $e$, and $k$. Hint: combine your answer to part (a) with what you know about the energies of the eigenstates of hydrogen - this will save you a lengthy calculation!
````

````{exercise} Quantum dots
:label: pb:quantumdots
:class: dropdown
From [Wikipedia](https://en.wikipedia.org/wiki/Quantum_dot): ''Quantum dots are tiny semiconductor particles a few nanometres in size, having optical and electronic properties that differ from larger particles due to quantum mechanics. \ldots Quantum dots are sometimes referred to as artificial atoms, emphasizing their singularity, having bound, discrete electronic states, like naturally occurring atoms or molecules.''
Quantum dots are used frequently in experiments (including biological ones) as probes; there also exist biocomposite quantum dots that can be fabricated by viruses. In this problem, we'll treat a quantum dot as a spherical cavity: electrons can move around freely inside, but cannot escape. The solutions of the Schr&ouml;dinger equation then separate like they do for the hydrogen atom: $\psi_{nlm}(r, \theta, \phi) = R_{nl}(r) Y_l^m (\theta, \phi)$. The angular part is still given by the spherical harmonics, and for the radial part we have
```{math}
:label: sphericalwellradialwavefunctionode
\frac{1}{r} \frac{\mathrm{d}^2}{\mathrm{d}r^2} (r R) - \frac{l(l+1)}{r^2} R(r) = \frac{-2 \me E_{nl}}{\hbar^2} R(r),
```
where $E_{nl}$ is the (energy) eigenvalue of the full function $\psi_{nlm}$. NB: Note that the $\psi_{nlm}$ here are not the eigenfunctions of the hydrogen atom.
The first step to solving equation&nbsp;{eq}`sphericalwellradialwavefunctionode` is to re-scale the energy and position variable, writing
$k = \sqrt{2 \me E_{nl}}/\hbar$ and $z = kr$.
1. Show that you can re-write equation&nbsp;{eq}`sphericalwellradialwavefunctionode` in terms of $k$ and the variable $z$ as
	```{math}
	:label: sphericalwellradialwavefunctionoderescaled
	\frac{\mathrm{d}^2 R}{\mathrm{d}z^2} + \frac{2}{z} \frac{\mathrm{d}R}{\mathrm{d}z} + \left( 1 - \frac{l(l+1)}{z^2} \right) R = 0.
	```
	The solutions to equation&nbsp;{eq}`sphericalwellradialwavefunctionoderescaled` are the spherical Bessel functions $j_l(z)$ and spherical Neumann functions $n_l(z)$. The first two of each are given by
	```{math}
	\begin{alignat*}{3}
	j_0(z) &= \frac{\sin(z)}{z}, & j_1(z) &= \frac{\sin(z)}{z^2} - \frac{\cos(z)}{z}, \\
	n_0(z) &= -\frac{\cos(z)}{z}, \qquad & n_1(z) &= -\frac{\cos(z)}{z^2} - \frac{\sin(z)}{z}.
	\end{alignat*}
	```
1. Verify that both $j_0(z)$ and $n_0(z)$ are indeed solutions to your equation at (a).
1. By expanding the sine and cosine functions around $z=0$ (note that $z$ is a real number here), determine which of the two sets will give physically allowable solutions.
	Let&nbsp;$r=a$ be the radius of the quantum dot, then (obviously) we must set $R(r=a) = R(z=ka) = 0$. Unfortunately, the radial Bessel and Neumann functions do not have nice regularly spaced zeros. Except for the $l=0$ case, we can't even compute the positions of the zeros analytically. Therefore, let $\gamma_{n,l}$ be the $n$th zero of the $l$th spherical Bessel/Neumann (we only need one, as we've dropped the other) function.
1. Express the energy $E_{n,l}$ in terms of $\gamma_{n,l}$.
1. Compute the numerical value (in electron volts) of the ground-state energy $E_{1,0}$ for a quantum dot with a radius of $1.0\;\mathrm{nm}$.
1. Find the wavelength of a photon emitted by an electron when transitioning from the $n=2, l=0$ state tot the $n=1, l=0$ state of a quantum dot.
1. Evaluate the probability that an electron in the ground state of a quantum dot will be found inside a sphere of radius $a/2$.
````

`````{exercise} The virial theorem
:label: pb:virialtheorem
:class: dropdown

```{index} virial theorem
```
The *virial theorem* relates the expectation values of the kinetic energy and the work to each other. In this problem, we'll prove and apply both the one-dimensional and three-dimensional versions of the theorem.
1. Using the generalized Ehrenfest theorem (equation&nbsp;{eq}`operatorexpectationvaluetimeevolution2`), show that for a one-dimensional system
	```{math}
	:label: virial1Dwithtime
	\frac{\mathrm{d}}{\mathrm{d}t} \Braket{\hat{x} \hat{p}} = 2 \Braket{\hat{K}} - \Braket{x \frac{\mathrm{d}V}{\mathrm{d}x}}.
	```
	In a stationary state, the left-hand side of equation&nbsp;{eq}`virial1Dwithtime` vanishes, and therefore
	```{math}
	:label: virial1D
	2 \Braket{\hat{K}} = \Braket{x \frac{\mathrm{d}V}{\mathrm{d}x}} \equiv \Braket{W},
	```
	where $W$ is the work (force times displacement). Equation&nbsp;{eq}`virial1D` is known as the virial theorem; it is the quantum-mechanical equivalent of the [work-energy theorem](https://interactivetextbooks.tudelft.nl/nb1140/content/energy.html\#kinetic-energy) in classical mechanics.
1. Use the virial theorem to show that $\Braket{\hat{K}} = \Braket{\hat{V}}$ for stationary states of the harmonic oscillator potential.
1. Now prove the three-dimensional version of the virial theorem:
	````{prf:theorem} Virial theorem
	:label: thm:virialtheorem
	For a stationary state, the expectation values of the kinetic and potential energies are related through
	```{math}
	:label: virialtheorem
	2 \Braket{\hat{K}} = \Braket{ \bm{r} \cdot \bm{\nabla} V }.
	```
	````
1. Apply the virial theorem to the stationary states of the hydrogen atom. Show that for these states we have $\Braket{\hat{K}} = -E_n$, and therefore $\Braket{\hat{V}} = 2 E_n$.
`````

[^1]: If you skipped ahead from {numref}`sec:generaloperators`, the concept of an eigenfunction of an operator may be new to you; eigenfunctions and associated eigenvalues of the Hamiltonian satisfy equation&nbsp;{eq}`TISE`, and are thus similar to the eigenvectors and associated eigenvalues of matrices, see {numref}`sec:matrixeigenvalues`.

```{index} eigenstates
```
[^2]: The eigenfunctions of the Hamiltonian are often referred to as the *eigenstates*.

[^3]: If the energy at infinity is not zero but some other finite value&nbsp;$E(\infty)$, a scattering state is a state with an energy larger than $E(\infty)$, while a stat with an energy smaller than $E(\infty)$ is a bound state.

[^4]: Be careful not to confuse $\epsilon$, the small number describing the size of our interval (of which we take the limit to zero) and $\varepsilon$, the strength of the potential in equation&nbsp;{eq}`Diracdeltapotential`.

[^5]: Image by [Michael Schmid](https://commons.wikimedia.org/wiki/User:Schmid) and [Grzegorz Pietrzak](https://commons.wikimedia.org/wiki/User:Vindicator), obtained from [Wikimedia commons](https://commons.wikimedia.org/wiki/File:Scanning_Tunneling_Microscope_schematic.svg), CC BY-SA 2.0 Austria.

[^6]: Image by Taner Yildirim, National Institute of Standards and Technology (NIST), obtained from [Wikimedia commons](https://commons.wikimedia.org/wiki/File:Chiraltube.png), originally from [NIST](https://www.ncnr.nist.gov/staff/taner/nanotube/types.html), public domain.

[^7]: Image by [Thomas Shafee](https://en.wikipedia.org/wiki/User:Evolution_and_evolvability), obtained from [Wikimedia commons](https://commons.wikimedia.org/wiki/File:Hexokinase_induced_fit.svg), CC BY 4.0.

[^8]: The somewhat strange form of the $\hat{a}_\pm$ operators is just to make the results come out nicely; we could also just have taken $\hat{p} \pm i m \omega \hat{x}$; the price would be some more factors $m$, $\omega$ and $\hbar$ flying around.

[^9]: We call such potentials 'central', and the corresponding forces 'central forces', as they act from a center. Another example (in classical mechanics) is Newtonian gravity.

[^10]: The subscript is to distinguish this new number $m_l$ from the mass of the electron, which we'll now write as $m_\mathrm{e}$. It is unfortunate that the two numbers have the same symbol, but at some point symbol clashes are unavoidable, and both are standard use. You'll often find $m$ without subscript, as we do later when there is no possibility of confusion.

[^11]: Naturally, nobody bothers to memorize the normalization factor, though it is useful to have some inkling about the functional form of the spherical harmonics for low values of $l$ and $m$.

[^12]: Image by [Inigo Quilez](https://commons.wikimedia.org/wiki/User:Inigo.quilez), obtained from [Wikimedia commons](https://commons.wikimedia.org/wiki/File:Spherical_Harmonics.png), CC BY-SA 3.0.

[^13]: For historical reasons, $l$ is sometimes also referred to as the azimuthal quantum number, a name which we'll avoid as it will only cause confusion.

