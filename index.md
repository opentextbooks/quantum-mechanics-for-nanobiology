Introduction to quantum mechanics
================================================

```{figure} content/images/IntroQMcover.jpg
---
width: 200
align: right
alt: Introduction to quantum mechanics
---
```
In this book, you'll find an introduction quantum mecahnics. The material in the book has evolved from lecture notes on courses I have taught at TU Delft since 2015. In most cases, not all of the material covered in the book was discussed in the lectures. The chapters also do not necessarily be taught or read in the order I have presented them. For example, it can be quite useful to have concrete examples of systems from {numref}`ch:SEsolutions` in mind when studying the abstract concepts laid out in sections {numref}`sec:generaloperators`-{numref}`sec:symmetry`. There are thus multiple paths you can take, and I encourage you to look ahead sometimes to see how what is yet to come ties in with what is discussed at a given point in the book. The flowchart in {numref}`fig:flowchart` below shows which alternative paths can be taken. If you need refresher on some of the mathematical techniques, {numref}`app:math` contains some useful background maths. Throughout, I've tried to alternate theory with worked examples, to give you an idea about what you can actually do with the theory just developed.

```{figure} content/images/flowchart.svg
:name: fig:flowchart
Flowchart with connections between sections of the book.
```

---

Version 1.0.0, March 3, 2025.

This book is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>. It is part of the collection of [Interactive Open Textbooks](https://textbooks.open.tudelft.nl/textbooks/catalog/category/interactive) of [TU Delft Open](https://textbooks.open.tudelft.nl/textbooks/index).

This website is a [Jupyter Book](https://jupyterbook.org/intro.html). MarkDown source files are available for download using the button on the top right.

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png"/></a>
