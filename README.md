# Quantum mechanics for Nanobiology

## Purpose of this book

This book evolved from lecture notes I wrote for an introduction course to quantum mechanics, as I taught it at Delft University of Technology since 2015. I mostly follow the line taken in introductory courses in physics programmes, but occasionally expand to include concepts usually taught in chemistry courses, and applications in other fields.

## Author

This book was written by Dr. [Timon Idema](https://idemalab.tudelft.nl), associate professor at Delft University of Technology, The Netherlands.

## License
This work can be redistributed in unmodified form, or in modified form with proper attribution, as specified  by the [Creative Commons Attribution 4.0 License](https://creativecommons.org/licenses/by/4.0/).

## Version
This book was first published on March 3, 2025.
The current version is 1.0.0.