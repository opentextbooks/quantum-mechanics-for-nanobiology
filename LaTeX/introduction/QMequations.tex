\documentclass[12pt,a4paper]{article}
\usepackage[latin1]{inputenc}
\usepackage{amsmath} % Math symbol packages
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{bm} % Bold math
\usepackage{fullpage} % Use full page.

\usepackage{graphicx} % graphics
\usepackage{sidecap}   % Side captions for figures.
\usepackage{cite} % include this package to sort & compress citations in text
\usepackage{hyperref} % make hyperrefs in pdf
\usepackage{enumerate} % For letter-enumerated lists.

\bibliographystyle{../../../Artikelen/idema}

\newcommand{\diff}[2]{\frac{\partial #1}{\partial #2}}
\newcommand{\dd}{\,\mathrm{d}}
\newcommand{\ddt}{\,\mathrm{d}t}
\newcommand{\ddx}{\,\mathrm{d}x}
\newcommand{\ddr}{\,\mathrm{d}r}
\newcommand{\eps}{\varepsilon}
\newcommand{\sign}{\mathrm{sign}}
\newcommand{\erf}{\mathrm{erf}}
%\newcommand{\bvec}[1]{\mathbf{#1}}
\newcommand{\bvec}[1]{\bm{#1}}
\newcommand{\unitvec}[1]{\bm{\hat #1}}
\newcommand{\kB}{k_{\mbox{\tiny B}}}
\newcommand{\order}{\mathcal{O}}

\newcommand{\ev}[1]{\left< #1 \right>}
\newcommand{\bra}[1]{\left< #1 \right|}
\newcommand{\ket}[1]{\left| #1 \right>}

%\hyphenation{Schr\"{o}-din-ger} - doesn't work due to ö character.

\hypersetup{
pdfauthor = {Timon Idema},
pdftitle = {Quantum mechanics equations sheet},
pdfkeywords = {physics, quantum mechanics},
colorlinks = true,	  % links, no boxes
linkcolor = black,        % color of internal links
citecolor = black,        % color of links to bibliography
filecolor = black,         % color of file links
urlcolor = black
}

% For a side figure, use
%\begin{wrapfigure}[8]{r}{0.32 \columnwidth}
%  \includegraphics[width=0.3\columnwidth]{ballonramp.eps}
%  \caption{.}
%\end{wrapfigure}
% her the [8] is the number of lines, and {r} means right. Note that it will block out an empty space next to an item in a list.
%
% For breaking the chain of sub-problems (so you can insert text, or fill out the empty space under a wrapfig), use
%\newcounter{counterone}
%\setcounter{counterone}{\value{enumi}}
%\end{enumerate}
% Text...
%\begin{enumerate}[(a)]
%\setcounter{enumi}{\value{counterone}}



\title{Quantum mechanics equations sheet}
\author{}
\date{}
\begin{document}
\maketitle

%\vspace{-2cm}

\section{Wave-particle duality}
The de Broglie energy and momentum are given by:
\begin{eqnarray}
\label{deBroglieenergy}
E &=& h f = \hbar \omega, \\
\label{deBrogliemomentum}
p &=& h/\lambda = \hbar k.
\end{eqnarray}

\section{Schr\"odinger equation}
The general Schr\"odinger equation in three dimensions is given by:
\begin{equation}
\label{3DSchrodinger}
i \hbar \frac{\partial \Psi(\bvec{x}, t)}{\partial t} = - \frac{\hbar^2}{2m} \nabla^2 \Psi(\bvec{x}, t) + U(\bvec{x}) \Psi(\bvec{x}, t).
\end{equation}
In one dimension, the time-independent version of the Schr\"odinger equation is:
\begin{equation}
\label{1DSchrodinger}
\hat{H} \psi(x) = -\frac{\hbar^2}{2m} \frac{\dd^2 \psi(x)}{\dd x^2} + U(x) \psi(x) = E \psi(x).
\end{equation}

\section{Operators}
%The wavefunction $\Psi(x,t)$ satisfies the normalization condition
%\begin{equation}
%\label{wavefunctionnormalization}
%\int_{-\infty}^\infty |\Psi(x,t)|^2 \dd x = 1.
%\end{equation}
The position and momentum operators are given by
\begin{eqnarray}
\label{positionoperator}
\hat{x} \Psi(x,t) &=& x \Psi(x,t), \\
\label{momentumoperator}
\hat{p} \Psi(x,t) &=& - i \hbar \frac{\partial \Psi(x,t)}{\partial x}.
\end{eqnarray}
The expectation value of any operator $\hat{Q}$ can be calculated through
\begin{equation}
\label{expectationvalues}
\left<\hat{Q}\right> = \int_{-\infty}^\infty \Psi^*(x,t) \hat{Q} \Psi(x,t) \dd x.
\end{equation}
The commutator $\left[ \hat{A}, \hat{B} \right]$ of two operators is defined as
\begin{equation}
\label{defcommutator}
\left[ \hat{A}, \hat{B} \right] = \hat{A}\hat{B} - \hat{B}\hat{A}.
\end{equation}
For any two Hermitian operators $\hat{A}$ and $\hat{B}$, we have the general uncertainty principle
\begin{equation}
\label{generaluncertaintyprinciple}
\sigma_A \sigma_B \geq \left| \frac{1}{2i} \left< \left[ \hat{A}, \hat{B} \right] \right> \right|.
\end{equation}
The time evolution of the expectation value of an operator is given by
\begin{equation}
\label{expectationtimeevolution}
\frac{\mathrm{d}}{\mathrm{d}t} \ev{\hat{Q}} = \frac{i}{\hbar} \left< \left[ \hat{H}, \hat{Q} \right] \right> + \left< \frac{\partial \hat{Q}}{\partial t} \right>,
\end{equation}
where $\hat{H}$ is the Hamiltonian of the system.

\section{Spin}
The operators for the $x$, $y$ and $z$ component of the spin satisfy the commutation relations
\begin{eqnarray}
\label{SxSycommutator}
\left[ \hat{S}_x, \hat{S}_y \right] &=& i \hbar \hat{S}_z, \\
\label{SySzcommutator}
\left[ \hat{S}_y, \hat{S}_z \right] &=& i \hbar \hat{S}_x, \\
\label{SzSxcommutator}
\left[ \hat{S}_z, \hat{S}_x \right] &=& i \hbar \hat{S}_y.
\end{eqnarray}
For a spin-1/2 particle in the basis of the eigenvectors $\chi_{+} = \left(\begin{array}{c}1\\ 0 \end{array}\right)$ and $\chi_{-} = \left(\begin{array}{c}0\\ 1 \end{array}\right)$ of the $\hat{S}_z$ operator, the spin operators are given by the matrix expressions:
\begin{eqnarray}
\label{Sxmatrix}
\hat{S}_x = \frac{\hbar}{2} \hat{\sigma}_x &=& \frac{\hbar}{2} \left( \begin{array}{cc} 0 & 1 \\ 1 & 0 \end{array}\right), \\
\label{Symatrix}
\hat{S}_y = \frac{\hbar}{2} \hat{\sigma}_y &=& \frac{\hbar}{2} \left( \begin{array}{cc} 0 & -i \\ i & 0 \end{array}\right), \\
\label{Szmatrix}
\hat{S}_z = \frac{\hbar}{2} \hat{\sigma}_z &=& \frac{\hbar}{2} \left( \begin{array}{cc} 1 & 0 \\ 0 & -1 \end{array}\right).
\end{eqnarray}

\section{Mathematical identities}
\subsection{Goniometric functions}
\begin{eqnarray}
e^{i\phi} &=& \cos(\phi) + i \sin(\phi) \\
\sin(\phi) &=& \frac{1}{2i} \left( e^{i \phi} - e^{-i \phi} \right) \\
\cos(\phi) &=& \frac12 \left( e^{i \phi} + e^{-i \phi} \right) \\
1 &=& \sin^2(\phi) + \cos^2(\phi) \\
\sin(2 \phi) &=& 2 \sin(\phi) \cos(\phi)\\
\cos(2\phi) &=& \cos^2(\phi)-\sin^2(\phi)
\end{eqnarray}

\subsection{Exponential integrals}
\begin{equation}
\label{expintegral}
\int_0^\infty x^n e^{-x/a} \ddx = n! a^{n+1} \qquad \mbox{for any integer }  n.
\end{equation}

\subsection{Some goniometric integrals}
The following hold for any integers $m$ and $n$.
\begin{eqnarray}
\int_0^L \sin\left(\frac{m \pi x}{L} \right) \sin\left(\frac{n \pi x}{L} \right) \ddx &=& \frac{L}{2} \delta_{mn}.\\
\int_0^L \cos\left(\frac{m \pi x}{L} \right) \cos\left(\frac{n \pi x}{L} \right) \ddx &=& \frac{L}{2} \delta_{mn}. \\
\int_0^L \sin\left(\frac{n \pi x}{L} \right) \cos\left(\frac{n \pi x}{L} \right) \ddx &=& 0. \\
\int_0^L \sin\left(\frac{m \pi x}{L} \right) \cos\left(\frac{n \pi x}{L} \right) \ddx &=& \frac{1-(-1)^{m+n}}{m^2-n^2} \frac{L m}{\pi}. \\
\int_0^L x \sin^2\left(\frac{n \pi x}{L} \right) \ddx = \int_0^L x \cos^2\left(\frac{n \pi x}{L} \right) \ddx &=& \frac{L^2}{4}. \\
\int_0^L x \sin\left(\frac{\pi x}{L} \right) \sin\left(\frac{2 \pi x}{L} \right) \ddx &=& - \frac{8 L^2}{9 \pi^2}. \\
\int_0^L x \cos\left(\frac{\pi x}{L} \right) \cos\left(\frac{2 \pi x}{L} \right) \ddx &=& - \frac{10 L^2}{9 \pi^2}. \\
\int_0^L x \sin\left(\frac{n \pi x}{L} \right) \cos\left(\frac{n \pi x}{L} \right) \ddx &=& -\frac{L^2}{4 n \pi}.\\
\end{eqnarray}

\subsection{Some Gaussian integrals}
Gaussian integrals are integrals over the Gaussian distribution $\exp(-x^2)$ (aka the normal distribution). In general, these cannot be evaluated in closed form, and we define the \emph{error function} as the integral:
\begin{equation}
\label{errorfunction}
\erf(x) = \frac{2}{\sqrt{\pi}} \int_0^x e^{-y^2} \dd y.
\end{equation}
There are however a number of Gaussian integrals that can be evaluated explicitly: those over all of~$\mathbb{R}$. Of course, as the Gaussian distribution is symmetric, we can also find the integrals from plus or minus infinity to the point of symmetry.
\begin{eqnarray}
\label{Gaussintegral}
\int_{-\infty}^\infty e^{-a x^2} \ddx &=& \sqrt{\frac{\pi}{a}}. \\
\int_{-\infty}^\infty e^{-a x^2 + b x + c} \ddx &=& \sqrt{\frac{\pi}{a}} \exp\left(\frac{b^2}{4a}+c\right). \\
\int_{-\infty}^\infty x^n e^{-a x^2} \ddx &=& 0 \qquad \mbox{for any odd value of }  n. \\
\label{Gaussxn}
\int_{-\infty}^\infty x^n e^{-a x^2} \ddx &=& \frac{1 \cdot 3 \cdot 5 \cdots (n-1) \sqrt{\pi}}{2^{n/2} a^{(n+1)/2}} \qquad \mbox{for any even value of }  n.
\end{eqnarray}
Note that we can get~(\ref{Gaussxn}) by repeated differentiation of~(\ref{Gaussintegral}) with respect to $a$.

\section{Physical constants}
\begin{table}[htb]
\begin{tabular}{|l|c|l|}
\hline
\textbf{Name} & \textbf{Symbol} & \textbf{Value} \\
\hline
Speed of light & $c$ & $3.00 \cdot 10^{8}$~m/s \\
Elementary charge & $e$ & $1.60 \cdot 10^{-19}$~C \\
Electron mass & $m_\mathrm{e}$ & $9.11 \cdot 10^{-31}$~kg \\
Proton mass & $m_\mathrm{p}$ & $1.67 \cdot 10^{-27}$~kg \\
Gravitational constant & $G$ & $6.67 \cdot 10^{-11}$~$\mathrm{N}\cdot\mathrm{m}^2/\mathrm{kg}^2$\\
Planck's constant & $h$ & $6.63 \cdot 10^{-34}$~$\mathrm{J}\cdot\mathrm{s}$\\
& $\hbar$ & $1.05 \cdot 10^{-34}$~$\mathrm{J}\cdot\mathrm{s}$\\
Permittivity of space & $\varepsilon_0$ & $8.85419 \cdot 10^{-12}\;\mathrm{C}^2/\mathrm{J}\cdot\mathrm{m}$\\
Boltzmann constant & $\kB$ & $1.38065 \cdot 10^{-23}\;\mathrm{J}/\mathrm{K}$\\
Bohr radius & $a$ & $5.29 \cdot 10^{-11}\;\mathrm{m}$ \\
Fine structure constant & $\alpha$ & $\frac{e^2}{4\pi\varepsilon_0 \hbar c} = \frac{1}{137.036}$ \\
\hline
\end{tabular}
\caption{Some physical constants with their values in standard units.}
\end{table}



\end{document}