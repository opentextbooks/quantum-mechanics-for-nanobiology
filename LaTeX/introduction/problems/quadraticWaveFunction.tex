%Problemtitle: The quadratic wave function
Consider a particle that is initially represented by wave function
\begin{equation}
\label{eq:quadraticWaveFunction}
\Psi(x,0) = \begin{cases}
-A (x-a)(x+a), & \mbox{if } -a \leq x \leq a, \\
0, & \mbox{otherwise,}
\end{cases}
\end{equation}
where $a$ is a real and positive constant and $A$ is a nonzero complex constant.

\begin{enumerate}[(a)]
\item Determine $A$ such that the wave function (\ref{eq:quadraticWaveFunction}) is normalized at $t=0$. Is there a unique $A$ that normalizes (\ref{eq:quadraticWaveFunction})?
\item Sketch the wave function and the corresponding probability distribution at $t=0$.
\item Find the expectation values $\ev{\hat{x}}$, $\ev{\hat{x}^2}$ and compute $\sigma_{\hat{x}}$ at $t=0$.
\item What is the probability of finding the particle between $x=-\frac{a}{2}$ and $x = \frac{a}{2}$ at $t=0$?
\item What is the most probable position to find the particle at $t=0$? 
\item Construct a wave function where the most probable position to find the particle is not the same as the expectation value of $x$.
\end{enumerate}

\ifincludesolutions
\Solution
\begin{enumerate}[(a)]
	\item Imposing the normalization condition we find 
	\begin{equation}
	\int_{-\infty}^{\infty}|\Psi(x,0)|^2 dx = |A|^2 \int_{a}^{b} (x-a)^2(x+a)^2 dx = \frac{16}{15} a^5 |A|^2 = 1,
	\end{equation}
	from which we find that the wave function is normalized if $|A| = \frac{\sqrt{15}}{4} a^{-5/2}$. Since $A$ can be complex, any complex number with modulus $|A|$ suffices, and thus $A = \frac{\sqrt{15}}{4} a^{-5/2} e^{i\phi}$ normalizes the wave function for any phase $\phi$ between 0 and $2\pi$. $A$ is thus not unique (usually we just choose $\phi=0$ such that the normalization constant is real and positive) \score{1 pt}.
	\item See below (for $a=1$) \score{1 pt}.
	\begin{center}
	\includegraphics[width=0.8\columnwidth]{introduction/problems/quadraticwavefunctionplot}
	\end{center}

	\item 
	\begin{equation*}
	\ev{x} = \int_{-\infty}^{\infty} \Psi(x,0)^* x \Psi(x,0) dx = \frac{15}{16} a^{-5} \int_{-a}^{a} x (x-a)^2(x+a)^2 dx = 0,
	\end{equation*}
	since the integrand is odd over a symmetric domain.
	\begin{equation*}
	\ev{x^2} = \int_{-\infty}^{\infty} \Psi(x,0)^* x^2 \Psi(x,0) dx = \frac{15}{16} a^{-5} \int_{-a}^{a} x^2 (x-a)^2(x+a)^2 dx = \frac{a^2}{7}.	
	\end{equation*}
	\begin{equation*}
	\sigma_x = \sqrt{\ev{x^2} - \ev{x}^2} = \frac{a}{\sqrt{7}}.
	\end{equation*} 
	\score{1 pt}
	
	\item \begin{align*}
	P\left(-\frac{a}{2} \leq x \leq \frac{a}{2}\right) &= \int_{-a/2}^{a/2} |\Psi(x,0)|^2 \ddx \\
	&= \frac{15}{16} a^{-5} \int_{-a/2}^{a/2} (x-a)^2(x+a)^2 \ddx = \frac{203}{256} \approx 0.793.
	\end{align*}
	\score{1 pt}
	
	\item The most probable position to find the particle is where $|\Psi|^2$ attains a maximum. In general this can be found by solving $\frac{\partial}{\partial x} |\Psi(x,t)|^2 = 0$ and checking that the solutions are indeed maxima. In our case this yields the maximum at $x = 0$ \score{1 pt}.
	
	\item For example 
	\begin{equation*}
	\psi(x) = \begin{cases}
	\frac{1}{\sqrt{\pi}} \sin(x), & \mbox{if } -\pi \leq x \leq \pi, \\
	0, & \mbox{otherwise.}
	\end{cases}
	\end{equation*}
	Then $|\psi|^2 = \frac{1}{\pi} \sin^2(x)$. Note that this wave function is normalized: $\int_{-\pi}^{\pi} \frac{1}{\pi} \sin^2(x) dx = 1$. The expectation value $\ev{x} = 0$, but the most probable position to find the particle is at $x = \pm \frac{\pi}{2}$, which are the maxima of $|\psi|^2$.
	
	\score{1 pt for any correct example}
	
\end{enumerate}

\fi