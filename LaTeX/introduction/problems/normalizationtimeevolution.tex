%Problemtitle: The time evolution of normalization
%Source: Griffiths section 1.4 and problem 1.17
Our first axiom of quantum mechanics, on the statistical interpretation of the wave function, implies that all quantum mechanical wave functions must be normalized. In one dimension, we thus impose on each wave function~$\Psi(x, t)$ the condition that
\begin{equation}
\label{normalization}
1 = \Braket{\Psi|\Psi} = \int_{-\infty}^\infty \left| \Psi(x, t) \right|^2 \,\dd{x}.
\end{equation}
Naturally, this condition should be satisfied for all time. Fortunately, as we'll prove in this problem, the evolution of the wave function as described by the Schr\"odinger equation (our second axiom) ensures that, if a wave function is normalized at some time $t$, this normalization is conserved, meaning that it doesn't change over time. We'll use the (time-dependent!) one-dimensional version of the Schr\"odinger equation in this problem:
\begin{equation}
i \hbar \diff{\Psi}{t} = \hat{H} \Psi = - \frac{\hbar^2}{2m} \frac{\partial^2 \Psi}{\partial x^2} + V(x) \Psi.
\end{equation}
To show that the normalization is conserved, suppose we have a wavefunction that is normalized at some time (let's say $t=0$), so $\Psi(x, 0)$ satisfies equation~(\ref{normalization}). If the normalization is conserved, the value of the normalization should not change, so its (time) derivative should vanish. We therefore need to calculate the time derivative of the integral in the right-hand term of equation~(\ref{normalization}). As the integral is over space, we can swap integration and differentiation to time, and get:
\begin{equation}
\frac{\dd}{\ddt} \int_{-\infty}^\infty \left| \Psi(x, t) \right|^2 \,\dd{x} = \int_{-\infty}^\infty \frac{\partial}{\partial t}\left| \Psi(x, t) \right|^2 \,\dd{x},
\end{equation}
where the total derivative became a partial derivative because~$\Psi$ is a function of both $t$ and $x$.
\begin{enumerate}[(a)]
\item Using that $\left| \Psi(x, t) \right|^2 = \Psi^* \Psi$, find the time derivative of $\left| \Psi(x, t) \right|^2$, and use the Schr\"odinger equation to express it in terms of $\Psi$, $\Psi^*$, and their (second) derivatives to $x$.
\item In order to calculate the integral of your answer to (a), it would be helpful to be able to write it as a total derivative. You can do so by adding and subtracting the following product:
\[ \diff{\Psi^*}{x} \diff{\Psi}{x} \]
times an appropriate prefactor. Do so, and show that you get
\begin{equation}
\label{normalizationintegrandtimederivative}
\frac{\partial}{\partial t}\left| \Psi(x, t) \right|^2 = \frac{\partial}{\partial x} \left[ \frac{i \hbar}{2m} \left( \Psi^* \diff{\Psi}{x} - \diff{\Psi^*}{x} \Psi \right) \right].
\end{equation}
\item Now integrate equation~(\ref{normalizationintegrandtimederivative}) over all space and argue why the resulting terms should vanish.
\ifproblemset\setcounter{problemcounter}{\value{enumi}}\else\setcounter{problemcounter}{\value{enumii}}\fi
\end{enumerate}
We've now proven that the time derivative of the normalization vanishes, and therefore that normalization is conserved. However, you will have made an implicit assumption: that the potential energy function $V(x)$ is real. We could extend our description of quantum particles to unstable particles, with a finite lifetime, meaning a decreasing probability that the particle is still around after some time $t$. The simplest such example is an exponential decay, for which we'd have
\begin{equation}
\label{unstableparticleprob}
P(t) = \int_{-\infty}^\infty \left| \Psi(x, t) \right|^2 \,\ddx = e^{-t/\tau},
\end{equation}
with $\tau$ the (characteristic) lifetime of the particle. We can get such an exponential decay by giving our potential energy an imaginary part: we write $V = V_0 - i \Gamma$, with $V_0$ the actual (real) potential, and $\Gamma$ a positive (real) constant.
\begin{enumerate}[(a)]
\ifproblemset\setcounter{enumi}{\value{problemcounter}}\else\setcounter{enumii}{\value{problemcounter}}\fi
\item Show that with the new potential we get
\begin{equation}
\label{particledecay}
\frac{\dd P}{\ddt} = - \frac{2 \Gamma}{\hbar} P.
\end{equation}
\item Solve equation~(\ref{particledecay}) for $P(t)$, and find the characteristic lifetime (i.e., the value of $\tau$ in equation~(\ref{unstableparticleprob})) of the particle in terms of~$\Gamma$.
\end{enumerate}


\ifincludesolutions
\Solution
\begin{enumerate}[(a)]
\item We need both the Schr\"odinger equation and its complex conjugate:
\begin{align*}
i \hbar \diff{\Psi}{t} &= - \frac{\hbar^2}{2m} \frac{\partial^2 \Psi}{\partial x^2} + V(x) \Psi \\
-i \hbar \diff{\Psi^*}{t} &= - \frac{\hbar^2}{2m} \frac{\partial^2 \Psi^*}{\partial x^2} + V(x) \Psi^*,
\end{align*}
i.e., the time derivative of the complex conjugate of $\Psi$ gets an extra minus sign \score{1 pt, also give if correctly used but not explicitly stated}. Now:
\begin{align*}
\frac{\partial}{\partial t}\left| \Psi(x, t) \right|^2 &= \diff{\Psi^*}{t} \Psi + \Psi^* \diff{\Psi}{t} \\
&= \frac{i}{\hbar}\left[- \frac{\hbar^2}{2m} \frac{\partial^2 \Psi^*}{\partial x^2} + V(x) \Psi^*\right] \Psi - \frac{i}{\hbar}\left[ - \frac{\hbar^2}{2m} \frac{\partial^2 \Psi}{\partial x^2} + V(x) \Psi \right] \Psi^* \\
&= - \frac{i \hbar}{2m} \Psi \frac{\partial^2 \Psi^*}{\partial x^2} + \frac{i \hbar}{2m} \Psi^* \frac{\partial^2 \Psi}{\partial x^2}
\end{align*}
\score{2 pt}.
\item Adding and subtracting the given product of derivatives, we can write for our answer from (a):
\begin{align*}
\frac{\partial}{\partial t}\left| \Psi(x, t) \right|^2 &= \frac{i \hbar}{2m} \left[ \Psi^* \frac{\partial^2 \Psi}{\partial x^2} + \diff{\Psi^*}{x} \diff{\Psi}{x} - \Psi \frac{\partial^2 \Psi^*}{\partial x^2} - \diff{\Psi^*}{x} \diff{\Psi}{x} \right] \\
&= \frac{i \hbar}{2m} \left[ \frac{\partial}{\partial x} \left( \Psi^* \frac{\partial \Psi}{\partial x} \right) - \frac{\partial}{\partial x} \left( \Psi \frac{\partial \Psi^*}{\partial x}\right) \right],
\end{align*}
which is the given equation~(\ref{normalizationintegrandtimederivative}). Note that we used the product rule in the second line \score{2 pt}.
\item Integrating a total derivative is trivial, and we get:
\begin{align*}
\frac{\dd}{\ddt} \int_{-\infty}^\infty \left| \Psi(x, t) \right|^2 \,\ddx &= \int_{-\infty}^\infty \frac{\partial}{\partial x} \left[ \frac{i \hbar}{2m} \left( \Psi^* \diff{\Psi}{x} - \diff{\Psi^*}{x} \Psi \right) \right] \, \ddx \\
&= \frac{i \hbar}{2m} \left[ \Psi^* \diff{\Psi}{x} - \diff{\Psi^*}{x} \Psi \right]_{-\infty}^\infty.
\end{align*}
Now to be normalizable (or square-integrable), $\Psi(x, t)$ must vanish for $x \to \pm \infty$, and therefore both terms in the last line are identically zero \score{2 pt}.
\item We need to revisit the answer to (a). In the complex conjugate of the Schr\"odinger equation, we now also get the complex conjugate of the potential. Therefore, in the second part, we need to replace $V \Psi^*$ with $V^* \Psi^*$, and the terms with the potential no longer vanish. As $V = V_0 - i \Gamma$, we have $V^* = V_0 + i \Gamma$, so the only parts that don't vanish are the ones with $\Gamma$, and we get an extra term in the time derivative of $\left| \Psi(x, t) \right|^2$:
\begin{align*}
\frac{\partial}{\partial t}\left| \Psi(x, t) \right|^2 &= - \frac{i \hbar}{2m} \Psi \frac{\partial^2 \Psi^*}{\partial x^2} + \frac{i \hbar}{2m} \Psi^* \frac{\partial^2 \Psi}{\partial x^2} + \frac{i}{\hbar} \left[ i \Gamma \Psi^* \Psi + i \Gamma \Psi \Psi^* \right] \\
&=  \frac{i \hbar}{2m} \left[ \frac{\partial}{\partial x} \left( \Psi^* \frac{\partial \Psi}{\partial x} \right) - \frac{\partial}{\partial x} \left( \Psi \frac{\partial \Psi^*}{\partial x}\right) \right] - \frac{2\Gamma}{\hbar} \Psi^* \Psi,
\end{align*}
where we simply copied the re-writing of the first part from (b). Integrating is easy:
\begin{align*}
\frac{\dd P}{\ddt} &= \frac{\dd}{\ddt} \int_{-\infty}^\infty \left| \Psi(x, t) \right|^2 \,\ddx \\
&= \int_{-\infty}^\infty \frac{\partial}{\partial x} \left[ \frac{i \hbar}{2m} \left( \Psi^* \diff{\Psi}{x} - \diff{\Psi^*}{x} \Psi \right) \right] \, \ddx - \frac{2\Gamma}{\hbar} \int_{-\infty}^\infty \left| \Psi(x, t) \right|^2 \,\ddx \\
&= \frac{i \hbar}{2m} \left[ \Psi^* \diff{\Psi}{x} - \diff{\Psi^*}{x} \Psi \right]_{-\infty}^\infty - \frac{2\Gamma}{\hbar} P  = - \frac{2\Gamma}{\hbar} P,
\end{align*}
using again the argument that $\Psi(x, t)$ has to be square-integrable, and thus has to vanish for $x \to \pm \infty$. We thus indeed get equation~(\ref{particledecay}) \score{3 pt}.
\item We can easily solve equation~(\ref{particledecay}) by separation of variables:
\begin{align*}
\frac{1}{P} \, \dd P &= - \frac{2\Gamma}{\hbar} \, \ddt \\
\int \frac{1}{P} \, \dd P &= - \frac{2\Gamma}{\hbar} \int \, \ddt \\
\log(P) &= - \frac{2\Gamma}{\hbar} t + C \\
P(t) &= A \exp\left(- \frac{2 \Gamma t}{\hbar} \right) = A e^{-t / \tau},
\end{align*}
where $C$ is an integration constant, $A = e^C$ (which will have to be one if the normalization is satisfied at $t=0$), and $\tau = 2 \Gamma / \hbar$ is the characteristic lifetime of the particle \score{2 pt}.
\end{enumerate}
\fi