%Problemtitle: Properties of the Dirac delta function
The Dirac delta `function' $\delta(x)$ (technically a distribution) is defined as an infinite sharp peak with area one. In particular
\begin{equation}
\label{defDiracdeltafunction}
\int_{-\infty}^\infty \delta(x) \,\ddx = 1 \quad \mbox{and} \quad \int_{-\infty}^\infty f(x) \delta(x-a) \,\ddx = f(a) \quad \mbox{for any function}\;f(x). 
\end{equation}

\begin{enumerate}[(a)]
\item Evaluate the following integrals:
\begin{align*}
&\int_{-\infty}^\infty (x-3)^2 \delta(x - 1) \,\ddx,\\
&\int_{0}^{10} (x+3)^4 \delta(x + 5) \,\ddx,\\
&\int_{0}^\infty \cos(\pi x) \delta(x-1) \,\ddx.
\end{align*}
\ifproblemset\setcounter{problemcounter}{\value{enumi}}\else\setcounter{problemcounter}{\value{enumii}}\fi
\end{enumerate}

Two expressions involving functions $g(x)$ and $h(x)$ are considered equal if
\begin{equation}
\int_{-\infty}^{\infty} f(x) g(x) \,\ddx = \int_{-\infty}^{\infty} f(x) h(x) \,\ddx
\label{eq:deltaEquality}
\end{equation}
for every suitable (say continuous, although a larger class of functions might be suitable) function $f(x)$.

\begin{enumerate}[(a)]
\ifproblemset\setcounter{enumi}{\value{problemcounter}}\else\setcounter{enumii}{\value{problemcounter}}\fi
	\item Show that $\delta(x)$ can be interpreted as the limit of smaller and smaller rectangles with area one. To do so, define 
	\begin{equation}
	g_\epsilon(x) = \begin{cases}
	\frac{1}{2 \epsilon} & \mbox{if } -\epsilon \leq x \leq \epsilon, \\
	0 & \mbox{otherwise,}
	\end{cases}
	\end{equation}
	and show that 
	\begin{equation}
	\lim_{\epsilon\rightarrow 0} g_\epsilon(x) = \delta(x).
	\end{equation}
	
	\item Show that $\delta(cx) = \frac{1}{|c|}\delta(x)$ for any real and nonzero number $c$.
	
	\item Define the step function
	\begin{equation}
	\Theta(x) = \begin{cases}
	1 & \mbox{if } x \geq 0, \\
	0 & \mbox{if } x < 0, 
	\end{cases}
	\end{equation}
	and show that $\dv{\Theta}{x} = \delta(x)$.
	
	\item Let $g(x)$ be a continuously differentiable and strictly monotonic function with root $x_0$. This means $g(x_0) = 0$ and either $g'(x) > 0$ or $g'(x) < 0$ for all $x$. Show that 
	\begin{equation}
	\delta(g(x)) = \frac{\delta(x-x_0)}{|g'(x_0)|}.
	\end{equation}
	
	\item (Bonus) Argue that for any continuously differentiable function $g(x)$ with simple roots $x_i$ for $i=1,...,n$, i.e. $g(x_i) = 0$ and $g'(x_i) \neq 0$ for all $i$, we have
	\begin{equation}
	\delta(g(x)) = \sum_{i=1}^{n} \frac{\delta(x-x_i)}{|g'(x_i)|}
	\end{equation}
\end{enumerate}

\ifincludesolutions
\Solution
\begin{enumerate}[(a)]
\item We have
\begin{align*}
\int_{-\infty}^\infty (x-3)^2 \delta(x - 1) \ddx &= \left. (x-3)^2 \right|_{x=1} = 4.\\
\int_{0}^{10} (x+3)^4 \delta(x + 5) \ddx &= 0,\\
\int_{0}^\infty \cos(\pi x) \delta(x-1) \ddx &= \left. \cos(\pi x)\right|_{x=1} = \cos(\pi) = -1.
\end{align*}

	\item \begin{equation*}
	\int_{-\infty}^{\infty} f(x) \lim_{\epsilon\rightarrow 0} g_\epsilon(x) dx = \lim_{\epsilon\rightarrow 0} \frac{1}{2 \epsilon} \int_{-\epsilon}^{\epsilon} f(x) \dd{x} = f(0) = \int_{-\infty}^{\infty} f(x) \delta(x) \dd{x}
	\end{equation*}
	
	\item Changing variables $y = cx$ we find
	\begin{multline*}
	\int_{-\infty}^{\infty} f(x) \delta(cx) \dd{x} = \int_{-\sign(c)\infty}^{\sign(c)\infty} f(y/c) \delta(y) \frac{dy}{c} = \\ \int_{-\infty}^{\infty} f(y/c) \delta(y) \frac{\sign(c)}{c} \dd{y} =
	 \frac{1}{|c|} f(0) = \int_{-\infty}^{\infty} f(x) \frac{1}{|c|}\delta(x) \dd{x}
	\end{multline*}
	
	\item 
	\begin{multline*}
	\int_{-\infty}^{\infty} f(x) \dv{\Theta}{x} dx = \Big[f(x)\theta(x) \Big]^\infty_{-\infty} - \int_{-\infty}^{\infty} \dv{f}{x} \Theta(x) \dd{x} = f(\infty) - 0 - \int_0^\infty \dv{f}{x} \dd{x} = \\
	 f(\infty) - f(\infty) + f(0) = f(0) = \int_{-\infty}^{\infty} f(x) \delta(x) \dd{x}
	\end{multline*}
	
	\item We note that $g'(x) > 0$ for all $x$ and that $g(x)$ is invertible by the given properties. Changing variables $u = g(x)$, which yields $\dd{x} = \frac{\dd{u}}{g'(x)}$, we find
	\begin{multline*}
	\int_{-\infty}^{\infty} f(x) \delta(g(x)) \dd{x} = \int_{g(-\infty)}^{g(\infty)} f(g^{-1}(u)) \delta(u) \frac{\dd{u}}{g'(g^{-1}(u))} = \\ 
	 \frac{f(x_0)}{|g'(x_0)|} = \int_{-\infty}^{\infty} f(x) \frac{\delta(x - x_0)}{|g'(x_0)|} \dd{x},
	\end{multline*}
	where we used that $g^{-1}(0) = x_0$, since $g(x_0) = 0$. The absolute value sign is obtained analogous to question b). If $g'(x) < 0$ for all $x$, we find $g(\infty) < g(\infty)$ and interchanging the limits of integration yields the minus sign which is absorbed into the absolute value.
	
	\item Let us cut the interval of integration at each value of $x$ where $g'(x) = 0$. Note that if $g'(x) = 0$ at a certain interval, then $g(x) \neq 0$ on this interval, since we assumed all roots of $g$ are simple. Such an interval we can then disregard since $\delta(g(x))$ is zero on this entire interval. We are thus left with pieces of the integral where (on each separate piece) $g'(x) > 0$ or $g'(x) < 0$. A piece can at most contain one root (but may also contain none, in which case again $\delta(g(x)) = 0$). Now on each separate piece, the function is strictly monotonic and therefore invertible and we can use the result from above. Summing over all intervals containing the roots, yields the result.
	

	
	
	
\end{enumerate}
\fi