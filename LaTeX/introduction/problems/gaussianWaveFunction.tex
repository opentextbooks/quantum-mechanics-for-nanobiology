%Problemtitle: The Gaussian wave function
Consider the wave function of a particle of mass $m$
\begin{equation}
\label{eq:gaussianWaveFunc}
\Psi(x,t) = A e^{-\lambda(x-a)^2} e^{-i \frac{\hbar \lambda}{m} t},
\end{equation}
where $\lambda$ and $a$ are real and positive constants and $A$ is a nonzero complex constant.

\begin{enumerate}[(a)]
\item Determine $A$ such that the wave function is normalized.
\item Find the expectation values $\ev{\hat{x}}$ and $\ev{\hat{x}^2}$, and compute $\sigma_{\hat{x}}$. Do these quantities depend on time? Why (not)?
\item What is the probability to find the particle (represented by the given wave function) between $x = a-2\sigma_x$ and $x = a+2\sigma_x$?
\item Find the expectation values $\ev{\hat{p}}$ and $\ev{\hat{p}^2}$, and compute $\sigma_{\hat{p}}$. Check that the uncertainty principle holds for this example.
\item Find the potential $V(x)$ such that (\ref{eq:gaussianWaveFunc}) is a solution to the Schr\"odinger equation.
\end{enumerate}

\ifincludesolutions
\Solution
\begin{enumerate}[(a)]
	\item \begin{equation*}
	\int_{-\infty}^{\infty} |\Psi|^2 \,\dd{x} = |A|^2 \int_{-\infty}^{\infty} e^{-2\lambda(x-a)^2} \,\dd{x} = |A|^2 \sqrt{\frac{\pi}{2 \lambda}} = 1,
	\end{equation*}
	from which we find $A = \left(\frac{2\lambda}{\pi}\right)^{\frac{1}{4}}$ \score{1 pt}.
	
	\item \begin{equation*}
	\ev{x} = \int_{-\infty}^{\infty} \Psi^* x \Psi \dd{x} = \sqrt{\frac{2\lambda}{\pi}} \int_{-\infty}^{\infty} x e^{-2\lambda(x-a)^2} \dd{x} = a.
	\end{equation*}
	
	\begin{equation*}
	\ev{x^2} = \int_{-\infty}^{\infty} \Psi^* x^2 \Psi \,\ddx = \sqrt{\frac{2\lambda}{\pi}} \int_{-\infty}^{\infty} x^2 e^{-2\lambda(x-a)^2} \dd{x} = a^2 + \frac{1}{4 \lambda}.
	\end{equation*}
	
	\begin{equation*}
	\sigma_x = \sqrt{\ev{x^2} - \ev{x}^2} = \frac{1}{2\sqrt{\lambda}}.
	\end{equation*}
	
	These quantities are independent of time since the probability distribution function $|\Psi(x,t)|^2$ happens to be independent of time \score{2 pt}.
	
	\item	
	Writing $\lambda = \frac{1}{4\sigma_x^2}$ we find
	\begin{equation*}
	P(a-2\sigma_x \leq x \leq a+2\sigma_x) = \int_{a-2\sigma_x}^{a+2\sigma_x} |\Psi|^2 \ddx = \frac{1}{\sigma \sqrt{2\pi}} \int_{a-2\sigma_x}^{a+2\sigma_x} e^{-\frac{(x-a)^2}{2\sigma_x^2}} \ddx \approx 0.9545,
	\end{equation*} 
	where we recognize that the integral represents the probability of drawing a value out of a normal distribution within 2 standard deviations ($2 \sigma$) from the mean ($a$). The value may be looked up in a table or computed numerically \score{1 pt}.
	
	\item $	\ev{p} = m\frac{\dd\ev{x}}{\ddt} = 0$ and using $\frac{\partial^2}{\partial x^2} \Psi = \left(4 \lambda^2 (x-a)^2 - 2 \lambda\right) \Psi$ we find
	\begin{equation*}
	\begin{split}
	\ev{p^2} &= \int_{-\infty}^{\infty} \Psi^* \left(\frac{\hbar}{i} \frac{\partial }{\partial x}\right)^2 \Psi \ddx \\
	&= -\hbar^2 \int_{-\infty}^{\infty} \Psi^* \left(4 \lambda^2 (x-a)^2 - 2 \lambda\right) \Psi \ddx \\
	&= -\hbar^2 \left(4 \lambda^2 \ev{x^2} - 8 \lambda^2 a \ev{x} + [4 \lambda^2 a^2 - 2 \lambda] \ev{I}\right) \\
	&= \hbar^2 \lambda,
	\end{split}
	\end{equation*}
	using the expressions computed above and $\ev{I} = \int_{-\infty}^{\infty} \Psi^* \Psi dx = 1$. So
	\begin{equation*}
	\sigma_p = \sqrt{\ev{p^2} - \ev{p}^2} = \hbar \sqrt{\lambda}.
	\end{equation*}
	Thus $\sigma_x \sigma_p = \frac{1}{2\sqrt{\lambda}} \hbar \sqrt{\lambda} = \frac{\hbar}{2} \geq \frac{\hbar}{2}$. The uncertainty principle is satisfied (with equality!) \score{2 pt}.
	
	\item We insert \eqref{eq:gaussianWaveFunc} into the Schr\"odinger equation to obtain
	\begin{equation*}
	i\hbar \frac{\partial \Psi}{\partial t} = \frac{\hbar^2 \lambda}{m} \Psi = - \frac{\hbar^2}{2m} \frac{\partial^2 \Psi}{\partial x^2} + V \Psi = - \frac{\hbar^2}{2m} \left(4 \lambda^2 (x-a)^2 - 2 \lambda\right) \Psi + V \Psi.
	\end{equation*}
	Therefore $V(x) = 2 \frac{\hbar^2 \lambda^2}{m}   (x-a)^2$ \score{1 pt}.
\end{enumerate}
\fi