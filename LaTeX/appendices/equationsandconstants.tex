\chapter{Some equations and constants}
\label{app:eqsconstants}

\section{Physical constants}
\begin{table}[htb]
\begin{tabular}{|l|c|l|}
\hline
\textbf{Name} & \textbf{Symbol} & \textbf{Value} \\
\hline
Speed of light & $c$ & $3.00 \cdot 10^{8}\;\mathrm{m}/\mathrm{s}$ \\
Elementary charge & $e$ & $1.60 \cdot 10^{-19}\;\mathrm{C}$ \\
Electron mass & $m_e$ & $9.11 \cdot 10^{-31}\;\mathrm{kg} = 0.511\;\mathrm{MeV}/c^2$ \\
Proton mass & $m_p$ & $1.67 \cdot 10^{-27}\;\mathrm{kg} = 938\;\mathrm{MeV}/c^2$ \\
Gravitational constant & $G$ & $6.67 \cdot 10^{-11}\;\mathrm{N}\cdot\mathrm{m}^2/\mathrm{kg}^2$\\
%Gravitational acceleration & $g$ & $9.81\;\mathrm{m}/\mathrm{s}^2$\\
%Gas constant & $R$ & $8.31 \; \mathrm{J}/\mathrm{K} \cdot \mathrm{mol}$\\
%Stefan-Boltzmann constant & $\sigma$ & $5.67 \cdot 10^{-8} \; \mathrm{W}/\mathrm{m}^2 \cdot \mathrm{K}^4$\\
%Avogadro's number & $N_\mathrm{A}$ & $6.02 \cdot 10^{23} \; \mathrm{mol}^{-1}$\\
Planck's constant & $h$ & $6.63 \cdot 10^{-34}\;\mathrm{J}\cdot\mathrm{s}$\\
& $\hbar = h / 2 \pi$ & $1.05 \cdot 10^{-34}\;\mathrm{J}\cdot\mathrm{s}$\\
Permittivity of space & $\varepsilon_0$ & $8.85419 \cdot 10^{-12}\;\mathrm{C}^2/\mathrm{J}\cdot\mathrm{m}$\\
Boltzmann's constant & $\kB$ & $1.38 \cdot 10^{-23}\;\mathrm{J}/\mathrm{K}$\\
Bohr radius & $a$ & $\frac{4\pi \varepsilon_0 \hbar^2}{e^2 m_\mathrm{e}} = 5.29177 \cdot 10^{-11}\;\mathrm{m}$ \\
Fine structure constant & $\alpha$ & $\frac{e^2}{4\pi\varepsilon_0 \hbar c} = \frac{1}{137.036}$ \\
Rydberg energy & $R_\mathrm{E}$ & $\frac{e^4 m_\mathrm{e}}{2(4\pi\varepsilon_0 \hbar)^2} = \frac12 \left(m_\mathrm{e} c^2\right) \alpha^2 = 13.6057\;\mathrm{eV}$.\\
\hline
\end{tabular}
\caption{Some physical constants with their values in standard units.}
\label{table:physicalconstants}
\end{table}

\section{Wave-particle duality}
The de Broglie energy and momentum are given by:
\begin{align}
E &= h f = \hbar \omega, \\
p &= h/\lambda = \hbar k.
\end{align}

\section{Schr\"odinger equation}
The general Schr\"odinger equation in three dimensions is given by:
\begin{equation}
\label{3DSchrodinger}
i \hbar \frac{\partial \Psi(\bvec{x}, t)}{\partial t} = - \frac{\hbar^2}{2m} \nabla^2 \Psi(\bvec{x}, t) + V(\bvec{x}) \Psi(\bvec{x}, t).
\end{equation}
In one dimension, the time-independent version of the Schr\"odinger equation is:
\begin{equation}
\label{1DSchrodinger}
\hat{H} \psi(x) = -\frac{\hbar^2}{2m} \frac{\dd^2 \psi(x)}{\dd x^2} + V(x) \psi(x) = E \psi(x).
\end{equation}

\clearpage
\section{Operators}
%The wavefunction $\Psi(x,t)$ satisfies the normalization condition
%\begin{equation}
%\label{wavefunctionnormalization}
%\int_{-\infty}^\infty |\Psi(x,t)|^2 \dd x = 1.
%\end{equation}
The position and momentum operators are given by
\begin{align}
\hat{x} \Psi(x,t) &= x \Psi(x,t), \\
\hat{p} \Psi(x,t) &= - i \hbar \frac{\partial \Psi(x,t)}{\partial x}.
\end{align}
The expectation value of any operator $\hat{Q}$ can be calculated through
\begin{equation}
\label{expectationvalues}
\ev{\hat{Q}} = \int_{-\infty}^\infty \Psi^*(x,t) \hat{Q} \Psi(x,t) \dd x.
\end{equation}
The commutator $\left[ \hat{A}, \hat{B} \right]$ of two operators is defined as
\begin{equation}
\left[ \hat{A}, \hat{B} \right] = \hat{A}\hat{B} - \hat{B}\hat{A}.
\end{equation}
For any two Hermitian operators $\hat{A}$ and $\hat{B}$, we have the general uncertainty principle
\begin{equation}
\sigma_A \sigma_B \geq \left| \frac{1}{2i} \ev{\left[ \hat{A}, \hat{B} \right]} \right|.
\end{equation}
The time evolution of the expectation value of an operator is given by
\begin{equation}
\label{expectationtimeevolution}
\frac{\mathrm{d}}{\mathrm{d}t} \ev{\hat{Q}} = \frac{i}{\hbar} \ev{\left[ \hat{H}, \hat{Q} \right]} + \ev{ \frac{\partial \hat{Q}}{\partial t}},
\end{equation}
where $\hat{H}$ is the Hamiltonian of the system.

\section{Spin}
The operators for the $x$, $y$ and $z$ component of the spin satisfy the commutation relations
\begin{subequations}
\label{spincommutators-appendix}
\begin{align}
\label{SxSycommutator}
\left[ \hat{S}_x, \hat{S}_y \right] &= i \hbar \hat{S}_z, \\
\label{SySzcommutator}
\left[ \hat{S}_y, \hat{S}_z \right] &= i \hbar \hat{S}_x, \\
\label{SzSxcommutator}
\left[ \hat{S}_z, \hat{S}_x \right] &= i \hbar \hat{S}_y.
\end{align}
\end{subequations}
For a spin-1/2 particle in the basis of the eigenvectors $\chi_{+} = \left(\begin{array}{c}1\\ 0 \end{array}\right)$ and $\chi_{-} = \left(\begin{array}{c}0\\ 1 \end{array}\right)$ of the $\hat{S}_z$ operator, the spin operators are given by the matrix expressions:
\begin{subequations}
\label{spinmatrices}
\begin{align}
\label{Sxmatrix}
\hat{S}_x &= \frac{\hbar}{2} \hat{\bvec{\sigma}}_x = \frac{\hbar}{2} \begin{pmatrix} 0 & 1 \\ 1 & 0 \end{pmatrix}, \\
\label{Symatrix}
\hat{S}_y &= \frac{\hbar}{2} \hat{\bvec{\sigma}}_y = \frac{\hbar}{2} \begin{pmatrix} 0 & -i \\ i & 0 \end{pmatrix}, \\
\label{Szmatrix}
\hat{S}_z &= \frac{\hbar}{2} \hat{\bvec{\sigma}}_z = \frac{\hbar}{2} \begin{pmatrix} 1 & 0 \\ 0 & -1 \end{pmatrix},
\end{align}
\end{subequations}


\clearpage
\section{Mathematical identities}
\subsection{Goniometric functions}
\begin{align}
e^{i\phi} &= \cos(\phi) + i \sin(\phi) \\
\sin(\phi) &= \frac{1}{2i} \left( e^{i \phi} - e^{-i \phi} \right) \\
\cos(\phi) &= \frac12 \left( e^{i \phi} + e^{-i \phi} \right) \\
1 &= \sin^2(\phi) + \cos^2(\phi) \\
\sin(2 \phi) &= 2 \sin(\phi) \cos(\phi)\\
\cos(2\phi) &= \cos^2(\phi)-\sin^2(\phi) \\
\sin(a \pm b) &= \sin(a) \cos(b) \pm \sin(b) \cos(a) \\
\cos(a \pm b) &= \cos(a) \cos(b) \pm \sin(a) \sin(b)
\end{align}

\subsection{Exponential integrals}
\begin{equation}
\label{expintegral}
\int_0^\infty x^n e^{-x/a} \ddx = n!\, a^{n+1} \qquad \mbox{for any integer }  n.
\end{equation}

\subsection{Some goniometric integrals}
The following hold for any integers $m$ and $n$.
\begin{align}
\int_0^L \sin\left(\frac{m \pi x}{L} \right) \sin\left(\frac{n \pi x}{L} \right) \ddx &= \frac{L}{2} \delta_{mn}.\\
\int_0^L \cos\left(\frac{m \pi x}{L} \right) \cos\left(\frac{n \pi x}{L} \right) \ddx &= \frac{L}{2} \delta_{mn}. \\
\int_0^L \sin\left(\frac{n \pi x}{L} \right) \cos\left(\frac{n \pi x}{L} \right) \ddx &= 0. \\
\int_0^L \sin\left(\frac{m \pi x}{L} \right) \cos\left(\frac{n \pi x}{L} \right) \ddx &= \frac{1-(-1)^{m+n}}{m^2-n^2} \frac{L m}{\pi}. \\
\int_0^L x \sin^2\left(\frac{n \pi x}{L} \right) \ddx = \int_0^L x \cos^2\left(\frac{n \pi x}{L} \right) \ddx &= \frac{L^2}{4}. \\
\int_0^L x \sin\left(\frac{\pi x}{L} \right) \sin\left(\frac{2 \pi x}{L} \right) \ddx &= - \frac{8 L^2}{9 \pi^2}. \\
\int_0^L x \cos\left(\frac{\pi x}{L} \right) \cos\left(\frac{2 \pi x}{L} \right) \ddx &= - \frac{10 L^2}{9 \pi^2}. \\
\int_0^L x \sin\left(\frac{n \pi x}{L} \right) \cos\left(\frac{n \pi x}{L} \right) \ddx &= -\frac{L^2}{4 n \pi}.
\end{align}
For any $a \neq 0$, we have the indefinite integrals
\begin{align}
\int x \sin(ax) \,\ddx &= \frac{1}{a^2} \sin(ax) - \frac{x}{a} \cos(ax), \\
\int x \cos(ax) \,\ddx &= \frac{1}{a^2} \cos(ax) + \frac{x}{a} \sin(ax).
\end{align}

\subsection{Some Gaussian integrals}
Gaussian integrals are integrals over the Gaussian distribution $\exp(-x^2)$ (aka the normal distribution). In general, these cannot be evaluated in closed form, and we define the \emph{error function} as the integral:
\begin{equation}
\label{errorfunction}
\erf(x) = \frac{2}{\sqrt{\pi}} \int_0^x e^{-y^2} \dd y.
\end{equation}
There are however a number of Gaussian integrals that can be evaluated explicitly: those over all of~$\mathbb{R}$. Of course, as the Gaussian distribution is symmetric, we can also find the integrals from plus or minus infinity to the point of symmetry.
\begin{align}
\label{Gaussintegral}
\int_{-\infty}^\infty e^{-a x^2} \ddx &= \sqrt{\frac{\pi}{a}}. \\
\int_{-\infty}^\infty e^{-a x^2 + b x + c} \ddx &= \sqrt{\frac{\pi}{a}} \exp\left(\frac{b^2}{4a}+c\right). \\
\int_{-\infty}^\infty x^n e^{-a x^2} \ddx &= 0 \qquad \mbox{for any odd value of }  n. \\
\label{Gaussxn}
\int_{-\infty}^\infty x^n e^{-a x^2} \ddx &= \frac{1 \cdot 3 \cdot 5 \cdots (n-1) \sqrt{\pi}}{2^{n/2} a^{(n+1)/2}} \qquad \mbox{for any even value of }  n.
\end{align}
Note that we can get~(\ref{Gaussxn}) by repeated differentiation of~(\ref{Gaussintegral}) with respect to $a$.
