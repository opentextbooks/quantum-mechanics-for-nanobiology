---
jupytext:
    formats: md:myst
    text_representation:
        extension: .md
        format_name: myst
kernelspec:
    display_name: Python 3 (ipykernel)
    language: python
    name: python3
---
(ch:angularmomentum)=
# Angular momentum

(sec:angularmomentumoperators)=
## Angular momentum operators in quantum mechanics

```{index} symmetry, angular momentum
```
In {numref}`sec:hydrogenatom`, we widened our scope from one to three dimensions. Consequently, we no longer have one but three position operators (one for each of the cardinal directions), and likewise three momentum operators. Moreover, things can now move in more ways than just back-and-forth; in particular, they can now rotate. As we've seen when solving for the eigenstates of the hydrogen atom, that's also what they do: while the eigenfunctions are not the trajectories of the particles as they would be in classical mechanics, they do describe states that have a clear center and a degree of rotational symmetry. Such symmetries are always associated with conserved quantities. As you may know from classical mechanics, symmetry<sup>[^1]</sup> in time implies conservation of energy, and symmetry in space conservation of momentum. Symmetry under rotations also comes with a conserved quantity: angular momentum. The classical definition of the angular momentum is the cross product of the position (with respect to a pivot about which you rotate) with the linear momentum:

$$
\bm{L} = \bm{r} \times \bm{p}.
$$ (classicalangularmomentum)

We don't have a direct quantum-mechanical analog for the cross product. However, we can define quantum-mechanical operators corresponding to each of the three components of the angular momentum vector&nbsp;$\bm{L}$, simply by writing out the cross product. Doing so, we get

```{math}
:label: defangularmomentumoperators
\begin{align*}
\hat{L}_x &= \hat{y}\hat{p}_z - \hat{z} \hat{p}_y, \\
\hat{L}_y &= \hat{z}\hat{p}_x - \hat{x} \hat{p}_z, \\
\hat{L}_z &= \hat{x}\hat{p}_y - \hat{y} \hat{p}_x,
\end{align*}
```

where the position and momentum operators are given by their familiar recipes<sup>[^2]</sup>. We immediately observe that these operators don't commute, as

```{math}
:label: angularmomentumcommutators
\begin{align*}
[\hat{L}_x, \hat{L}_y] &= [\hat{y}\hat{p}_z - \hat{z} \hat{p}_y, \hat{z}\hat{p}_x - \hat{x} \hat{p}_z] \\
&= [\hat{y}\hat{p}_z, \hat{z}\hat{p}_x] - [\hat{y}\hat{p}_z, \hat{x} \hat{p}_z] - [\hat{z} \hat{p}_y, \hat{z}\hat{p}_x] + [\hat{z} \hat{p}_y, \hat{x} \hat{p}_z] \\
&= \hat{y}\hat{p}_x [\hat{p}_z, \hat{z}] - 0 - 0 + \hat{p}_y\hat{x}[\hat{z}, \hat{p}_z] = -i \hbar \hat{y}\hat{p}_x + i \hbar \hat{p}_y\hat{x} \\
&= i \hbar \hat{L}_z,
\end{align*}
```
where we used that every operator commutes with itself, position and momentum operators with different variables commute, and that the commutator of the position and momentum operator is given by $i\hbar$ (equation&nbsp;{eq}`positionmomentumcommutator`). Through cyclic permutation<sup>[^3]</sup> of $x$, $y$ and $z$, we also find
```{math}
:label: LyLzcommutator
\begin{align*}
[\hat{L}_y, \hat{L}_z] &= i \hbar \hat{L}_x, \
\end{align*}
```

```{math}
:label: LzLxcommutator
\begin{align*}
[\hat{L}_z, \hat{L}_x] &= i \hbar \hat{L}_y.
\end{align*}
```

Following the observations in {numref}`sec:Heisenberguncertainty`, we conclude that because the three operators corresponding to the components of the angular momentum do not commute, we cannot measure their values at the same time. Fortunately however, there is an operator which commutes with all three components: the square of the total angular momentum, defined as

$$
\hat{L}^2 = \hat{L}_x^2 + \hat{L}_y^2 + \hat{L}_z^2.
$$ (deftotalangularmomentumoperator)

The verification that $\hat{L}^2$ commutes with each of the three components is a straightforward exercise in applying the rules of commutators:
```{math}
:label: L2Lxcommutator
\begin{align*}
[\hat{L}^2, \hat{L}_x] &= [\hat{L}_x^2, \hat{L}_x] + [\hat{L}_y^2, \hat{L}_x] + [\hat{L}_z^2, \hat{L}_x] \\
&= \hat{L}_y [\hat{L}_y, \hat{L}_x] + [\hat{L}_y, \hat{L}_x] \hat{L}_y + \hat{L}_z [\hat{L}_z, \hat{L}_x] + [\hat{L}_z, \hat{L}_x] \hat{L}_z \\
&= \hat{L}_y (-i \hbar \hat{L}_z) + (-i \hbar \hat{L}_z) \hat{L}_y + \hat{L}_z (i \hbar \hat{L}_y) + (i \hbar \hat{L}_y) \hat{L}_z = 0,
\end{align*}
```
and of course similarly for the other two components. Therefore, $\hat{L}^2$ will have a common set of eigenfunctions with each of the components $\hat{L}_x$, $\hat{L}_y$ and $\hat{L}_z$. Obviously, the exact form of those eigenfunctions will depend on which of the three components we choose. That is essentially a choice of coordinate system, as we can easily see when we express our angular momentum operator in spherical coordinates (as we'll do in {numref}`sec:hydrogenatomrevisited`). The common choice is to work with $\hat{L}_z$; we then know that we can expect to find a common set of eigenfunctions for $\hat{L}^2$ and $\hat{L}_z$ (which will turn out to be just the spherical harmonics). However, we'll first focus on finding the eigenvalues of the two operators, i.e. the values that we may actually expect to measure. We can find the spectra of these two operators with the same method we found the energies of the harmonic potential, by defining 'raising' and 'lowering' operators that take us from one eigenstate to another. The operators are defined using the other two components of the angular momentum, $\hat{L}_x$ and $\hat{L}_y$:

$$
\hat{L}_\pm = \hat{L}_x \pm i \hat{L}_y.
$$ (defangularmomentumraisinglowering)

By construction, $\hat{L}_+$ and $\hat{L}_-$ are each other's Hermitian conjugate; as the operators $\hat{L}_\pm$ themselves are not Hermitian, they do not correspond to an observable. Moreover, while they commute with $\hat{L}^2$ (as both $\hat{L}_x$ and $\hat{L}_y$ do), they don't commute with $\hat{L}_z$:

$$
[ \hat{L}_z, \hat{L}_\pm ] = [\hat{L}_z, \hat{L}_x] \pm i [\hat{L}_z, \hat{L}_y] = i \hbar \hat{L}_y \pm i (-i \hbar \hat{L}_x) = \pm \hbar (\hat{L}_x \pm i \hat{L}_y) = \pm \hbar \hat{L}_\pm.
$$ (LpmLzcommutator)

Now suppose that $\hat{L}^2$ and $\hat{L}_z$ have a common eigenfunction $f$ with eigenvalues $\lambda$ and $\mu$, respectively, i.e. we have

$$
\hat{L}^2 f = \lambda f \quad \text{and} \quad \hat{L}_z f = \mu f.
$$

In complete analogy to the way we constructed new eigenfunctions for the harmonic potential Hamiltonian given a starting eigenfunction, we also can construct new eigenfunctions for $\hat{L}^2$ and $\hat{L}_z$ from the starting point $f$, by (repeatedly) applying the operators $\hat{L}_\pm$ to the function. To verify this claim, we simply check if the new functions are again eigenfunctions, and in one go find their eigenvalues. For $\hat{L}^2$ this is almost tautological as $\hat{L}^2$ commutes with $\hat{L}_\pm$:

$$
\hat{L}^2 (\hat{L}_\pm f) = \hat{L}_\pm (\hat{L}^2 f) = \hat{L}_\pm (\lambda f) = \lambda (\hat{L}_\pm f),
$$

so if $f$ is an eigenfunction of $\hat{L}^2$ with eigenvalue $\lambda$, $\hat{L}_\pm f$ are also eigenfunctions, with the same eigenvalue. For $\hat{L}_z$ we have to do a bit more work, but it is still pretty straightforward if we apply a simple trick: subtract and then add again the same function<sup>[^4]</sup>

$$
\hat{L}_z (\hat{L}_\pm f) = \left(\hat{L}_z \hat{L}_\pm - \hat{L}_\pm \hat{L}_z\right) f + \hat{L}_\pm \hat{L}_z f = \pm \hbar \hat{L}_\pm f + \hat{L}_\pm (\mu f) = (\mu \pm \hbar) \hat{L}_z f,
$$ (LpmLzeigenfunction)

where we used the commutator&nbsp;{eq}`LpmLzcommutator` in the second equality. We thus find that if $f$ is an eigenfunction of $\hat{L}_z$ with eigenvalue $\mu$, $\hat{L}_\pm f$ are also eigenfunctions with eigenvalues $\mu \pm \hbar$. In summary, while $\hat{L}_\pm$ doesn't change the eigenvalue of $\hat{L}^2$, it raises or lowers that of $\hat{L}_z$ with $\hbar$. This observation allows us to constrain the range of eigenfunctions, just like the observation that the energy had to remain positive constrained the string of eigenfunctions of the harmonic potential Hamiltonian. For the present case, the operator $\hat{L}^2$ represents the square of the total angular momentum, which (being a square) always has to be positive. The $\hat{L}_z$ component could be positive or negative, however, its square can never exceed the value of $\hat{L}^2$, as that would mean that either $\hat{L}_x^2$ or $\hat{L}_y^2$ would have to be negative (which, being squares, they again cannot be). In equation form:

$$
L^2 = L_x^2 + L_y^2 + L_z^2 \geq L_z^2,
$$ (angularmomentumsquareinequality)

or, in terms of the eigenvalues (which are the values that we can actually measure) $\lambda$ has to be larger than the square of the eigenvalue of $\hat{L}_z$. Now every time we act with $\hat{L}_+$ on the eigenfunction of $\hat{L}_z$, the eigenvalue goes up by $\hbar$; no matter how low we start, at some point we will exceed the value $\sqrt{\lambda}$. As that cannot be, the only way out is that at some point we have a maximum eigenvalue, with eigenfunction $f_\mathrm{t}$ (with $\mathrm{t}$ for 'top'), such that $\hat{L}_+ f_\mathrm{t} = 0$. Let the corresponding eigenvalue of $f_\mathrm{t}$ for $\hat{L}_z$ be $\mu_\mathrm{t} = \hbar l$ (with $l$ at present an unknown number), then

$$
\hat{L}^2 f_\mathrm{t} = \lambda	f_\mathrm{t} \quad \text{and} \quad \hat{L}_z f_\mathrm{t} = \mu_\mathrm{t}f_\mathrm{t} \equiv \hbar l f_\mathrm{t}.
$$ (angularmomentumtopeigenvalues)

In addition to a maximum value, the eigenvalues of $\hat{L}_z$ must also have a minimum, because in equation&nbsp;{eq}`angularmomentumsquareinequality` the value gets squared. With a completely analogous argument, we find a minimum eigenfunction $f_\mathrm{b}$ (with $\mathrm{b}$ for 'bottom') satisfying $\hat{L}_- f_\mathrm{b} = 0$ and eigenvalues

$$
\hat{L}^2 f_\mathrm{b} = \lambda	f_\mathrm{b} \quad \text{and} \quad \hat{L}_z f_\mathrm{b} = \mu_\mathrm{b} f_\mathrm{b} \equiv \hbar \bar{l} f_\mathrm{b},
$$ (angularmomentumbottomeigenvalues)

where $\bar{l}$ another unknown number. These results limit the scope of the possible eigenfunctions, but don't tell us yet how the eigenvalues of $\hat{L}^2$ and $\hat{L}_z$ are related. Fortunately, there's another trick we can borrow from the treatment of the harmonic potential. There, we defined a number operator by combining the raising and lowering operators; we can do something similar here. We have:

$$
\hat{L}_+ \hat{L}_- = (\hat{L}_x + i \hat{L}_y) (\hat{L}_x - i \hat{L}_y) = \hat{L}_x^2 - i (\hat{L}_x \hat{L}_y - \hat{L}_y \hat{L}_x) + \hat{L}_y^2 = \hat{L}_x^2 + \hat{L}_y^2 + \hbar \hat{L}_z,
$$

so we don't get a new operator in this case, but can express $\hat{L}^2$ in terms of $ \hat{L}_\pm$:

$$
\hat{L}^2 = \hat{L}_+ \hat{L}_- + \hat{L}_z^2 - \hbar \hat{L}_z = \hat{L}_- \hat{L}_+ + \hat{L}_z^2 + \hbar \hat{L}_z,
$$

where the second equality follows from repeating the calculation with the order swapped. Now because $\hat{L}_+ f_\mathrm{t} = \hat{L}_- f_\mathrm{b} = 0$, we find from equations&nbsp;{eq}`angularmomentumtopeigenvalues` and&nbsp;{eq}`angularmomentumbottomeigenvalues` that

```{math}
:label: angularmomentumeigenvaluerelations
\begin{align*}
\lambda	f_\mathrm{t} &= \hat{L}^2 f_\mathrm{t} = \left(\hat{L}_- \hat{L}_+ + \hat{L}_z^2 + \hbar \hat{L}_z \right) f_\mathrm{t} = 0 + (\hbar l)^2 f_\mathrm{t} + \hbar (\hbar l) f_\mathrm{t} = \hbar^2 l(l+1) f_\mathrm{t}, \\
\lambda	f_\mathrm{b} &= \hat{L}^2 f_\mathrm{b} = \left(\hat{L}_+ \hat{L}_- + \hat{L}_z^2 - \hbar \hat{L}_z \right) f_\mathrm{b} = 0 + (\hbar \bar{l})^2 f_\mathrm{b} - \hbar (\hbar \bar{l}) f_\mathrm{b} = \hbar^2 \bar{l} (\bar{l}-1) f_\mathrm{b},
\end{align*}
```

so

$$
\hbar^2 l(l+1) = \lambda = \hbar^2 \bar{l}(\bar{l}-1).
$$ (angularmomentumeigenvaluerelations2)

Equation&nbsp;{eq}`angularmomentumeigenvaluerelations2` has two solutions for $\bar{l}$: either $\bar{l} = l+1$ (but that would imply that the lowest eigenvalue of $\hat{L}_z$ is larger than the largest one), or $\bar{l} = -l$. Consequently, the eigenvalues of $\hat{L}_z$ run from $-l\hbar$ to $l\hbar$ in steps of $\hbar$, which means that the difference between $l$ and $-l$ must be an integer. There are two possibilities for which this can be the case: either $l$ is itself an integer, or $l$ is a 'half-integer' (e.g. $\frac12$, $\frac32$, $\frac52$). As we'll see below, both of these options occur in nature<sup>[^5]</sup>, though for *orbital* angular momentum (the one we're discussing here), the half-integer solutions will be excluded by a symmetry argument, as we'll see in {numref}`sec:rotations` below. You can also prove that the values of $l$ must be integers  by invoking the definition of the angular momentum operator, see {numref}`pb:orbitalangularmomentumeigenvalues`. Quantum particles however have another type of angular momentum, known as *spin* (see {numref}`sec:spin`), for which the half-integer solutions will come into play.

As you have no doubt noted, choosing the number $l$ to characterize the eigenvalues of the angular momentum suggests a relation with the $l$ quantum number of the hydrogen atom, which we'll confirm in {numref}`sec:hydrogenatomrevisited`. For now, we see that, given an eigenstate, the eigenvalue of $\hat{L}^2$ is given by $\hbar^2l(l+1)$, whereas we can create $2l+1$ eigenstates of $\hat{L}_z$ with eigenvalues $m_l \hbar$ running from $m_l = -l$ to $m_l = +l$. The eigenfunctions of the angular momentum operators are thus classified by two quantum numbers, $l$ and $m$, analogous to the eigenfunctions of the hydrogen atom, which were classified by quantum numbers $n$, $l$ and $m$. Moreover, while a measurement of the energy (corresponding to the Hamiltonian) gave us a value that depends on $n$ alone, measurements on the magnitude and the $z$-component of the angular momentum will give us information about the $l$ and $m$ quantum numbers, respectively.

(sec:hydrogenatomrevisited)=
## The hydrogen atom revisited

(sec:hydrogenangularmomentum)=
### The hydrogen Hamiltonian and the angular momentum operator

In {numref}`sec:matterquantization`, we saw that Bohr originally predicted the emission spectrum of hydrogen by assuming quantization of angular momentum, although he worked with a classical view of an electron orbiting a nucleus like a planet orbits a star. As we've derived in {numref}`sec:angularmomentumoperators`, angular momentum will indeed be quantized, but there are no classical orbits of electrons; instead we have probability distributions that depend on three quantum numbers, $n$, $l$ and $m_l$. To verify that the quantum numbers $l$ and $m_l$ of the hydrogen atom are indeed the ones associated with the angular momentum, we will relate the hydrogen Hamiltonian to the angular momentum operators.

Collecting the three operators for the components of the angular momentum into a 'vector of operators', we can write

$$
\hat{\bm{L}} = \begin{pmatrix} \hat{L}_x \\ \hat{L}_y \\ \hat{L}_z \end{pmatrix} = \hat{\bm{r}} \times \hat{\bm{p}},
$$

where $\hat{\bm{r}}$ and $\hat{\bm{p}}$ are also 'vectors of operators', collecting three operators in one expression. If we express $\hat{\bm{r}}$ and $\hat{\bm{p}}$ in Cartesian components, we retrieve the expressions of {numref}`sec:angularmomentumoperators`. However, we can also express them in different coordinate systems, in particular in cylindrical or spherical coordinates. In spherical coordinates, the position $\bm{r}$ is given by $r \bm{\hat{e}}_r$ (where $\bm{\hat{e}}_r$ is the unit vector in the radial direction, and $r$ the distance to the origin), so the recipe of the position operator becomes 'multiply with $r \bm{\hat{e}}_r$'. For the momentum we can then write $\hat{\bm{p}} = -i \hbar \bm{\nabla}$, for which we can look up<sup>[^6]</sup> the expression in spherical coordinates:

$$
\bm{\nabla} = \bm{\hat{e}}_r \frac{\partial }{\partial r} + \bm{\hat{e}}_\theta \frac{1}{r} \frac{\partial }{\partial \theta} + \bm{\hat{e}}_\phi \frac{1}{r \sin(\theta)} \frac{\partial }{\partial \phi}.
$$ (nablaspherical)

For the vector of angular momentum operators we then get in spherical coordinates
```{math}
:label: Lvecspericalcoordinates
\begin{align*}
\hat{\bm{L}} &= -i \hbar r \bm{\hat{e}}_r \times \left( \bm{\hat{e}}_r \frac{\partial }{\partial r} + \bm{\hat{e}}_\theta \frac{1}{r} \frac{\partial }{\partial \theta} + \bm{\hat{e}}_\phi \frac{1}{r \sin(\theta)} \frac{\partial }{\partial \phi} \right) = - i \hbar \left( \bm{\hat{e}}_\phi \frac{\partial }{\partial \theta} - \bm{\hat{e}}_\theta \frac{1}{\sin(\theta)} \frac{\partial }{\partial \phi} \right),
\end{align*}
```
where we used that the spherical basis vectors $(\bm{\hat{e}}_r, \bm{\hat{e}}_\theta, \bm{\hat{e}}_\phi)$ form a right-handed orthogonal triplet. From equation&nbsp;{eq}`Lvecspericalcoordinates`, we can easily find an expression for $\hat{L}^2$ in spherical coordinates
```{math}
:label: L2spehricalcoordinates
\begin{align*}
\hat{L}^2 &= \hat{\bm{L}} \cdot \hat{\bm{L}} = - \hbar^2 \bm{\hat{e}}_\phi \cdot \frac{\partial }{\partial \theta} \left( \bm{\hat{e}}_\phi \frac{\partial }{\partial \theta} - \bm{\hat{e}}_\theta \frac{1}{\sin(\theta)} \frac{\partial }{\partial \phi} \right) + \hbar^2 \bm{\hat{e}}_\theta \cdot \frac{1}{\sin(\theta)} \frac{\partial }{\partial \phi} \left( \bm{\hat{e}}_\phi \frac{\partial }{\partial \theta} - \bm{\hat{e}}_\theta \frac{1}{\sin(\theta)} \frac{\partial }{\partial \phi} \right)  \\
&= -\hbar^2 \left(\bm{\hat{e}}_\phi \cdot \bm{\hat{e}}_\phi\right) \frac{\partial^2 }{\partial \theta^2}
+ \hbar^2 \left(\bm{\hat{e}}_\phi \cdot \frac{\partial \bm{\hat{e}}_\theta}{\partial \theta}\right) \frac{1}{\sin(\theta)} \frac{\partial }{\partial \phi}
+ \hbar^2 \frac{1}{\sin(\theta)}\left(\bm{\hat{e}}_\theta \cdot \frac{\partial \bm{\hat{e}}_\phi}{\partial \phi}\right) \frac{\partial }{\partial \theta}
- \hbar^2 \left(\bm{\hat{e}}_\theta \cdot \bm{\hat{e}}_\theta \right) \frac{1}{\sin^2(\theta)} \frac{\partial^2 }{\partial \phi^2} \\
&= -\hbar^2 \frac{\partial^2 }{\partial \theta^2} - \hbar^2 \frac{1}{\sin(\theta)} \frac{-1}{\cos(\theta)} \frac{\partial }{\partial \theta} - \hbar^2 \frac{1}{\sin^2(\theta)} \frac{\partial^2 }{\partial \phi^2} \\
&= -\hbar^2 \left[ \frac{1}{\sin(\theta)} \frac{\partial }{\partial \theta} \left( \sin(\theta) \frac{\partial }{\partial \theta} \right) + \frac{1}{\sin^2(\theta)} \frac{\partial^2 }{\partial \phi^2} \right],
\end{align*}
```
where in the second line we dropped two terms that were zero, and another one in the third line. We now read off that $\hat{L}^2$ is nothing but the angular part of the Laplacian in spherical coordinates. This result means that we can express the hydrogen Hamiltonian as

$$
\hat{H} = - \frac{\hbar^2}{2m_\mathrm{e}} \nabla^2 + \hat{V}(\bm{r}) = -\frac{\hbar^2}{2m_\mathrm{e}} \frac{1}{r^2} \frac{\partial }{\partial r} \left( r^2 \frac{\partial }{\partial r} \right) + \frac{\hat{L}^2}{2 m_\mathrm{e} r^2} - \frac{e^2}{4 \pi \varepsilon_0} \frac{1}{r}.
$$ (hydrogenHamiltonianangularmomentumoperator)

The square of the angular momentum thus represents the full angular part of the hydrogen Hamiltonian. The eigenfunctions of&nbsp;$\hat{L}^2$ will then be the spherical harmonics  $Y_l^m(\theta, \phi)$, as given by equation&nbsp;{eq}`sphericalharmonics`, which indeed satisfy

$$
\hat{L}^2 Y_l^m(\theta, \phi) = \hbar^2 l (l+1) Y_l^m(\theta, \phi).
$$

```{index} quantum number ; orbital
```
We thus see that the orbital quantum number $l$ of the hydrogen atom wave function $\psi_{nlm}$ is indeed related to the eigenvalue of $\hat{L}^2$. As $\hat{L}_z$ commutes with $\hat{L}^2$, we expect that the spherical harmonics are also eigenfunctions of $\hat{L}_z$. To verify that explicitly, we need an expression for $\hat{L}_z$ in terms of the spherical coordinates; we can find one from equation&nbsp;{eq}`Lvecspericalcoordinates` by expressing the basis vectors $\bm{\hat{e}}_\theta$ and $\bm{\hat{e}}_\phi$ in Cartesian coordinates, and then read off the $z$-component, which gives

$$
\hat{L}_z = - i \hbar \frac{\partial }{\partial \phi}.
$$ (Lzsphericalcoordinates)

Thus

$$
\hat{L}_z Y_l^m(\theta, \phi) = - i \hbar \frac{\partial }{\partial \phi}Y_l^m(\theta, \phi) = \hbar m Y_l^m(\theta, \phi),
$$

```{index} quantum number ; magnetic
```
and the magnetic quantum number $m_l$ is related to the eigenvalue of $\hat{L}_z$.

As a (perhaps unexpected) bonus, we get a mathematical result from our physics: because the spherical harmonics for fixed $l$ but different $m$ are eigenfunctions of $\hat{L}_z$ for different eigenvalues, and the spherical harmonics for different $l$ are eigenfunctions of $\hat{L}^2$ for different eigenvalues, we get 'for free' that the spherical harmonics are all orthogonal; that's now a consequence of {prf:ref}`lemma:orthogonaleigenfunctions`. 

### Finding hydrogen energies with raising and lowering operators
In {numref}`sec:hydrogenatom`, we solved the radial part $R(r)$ of the (orbital) wave function of hydrogen through first re-writing in terms of a function $u(r) = r R(r)$ (introducing an effective potential containing both the Coulomb attraction and a centrifugal term) and subsequent series expansion. In this section, we'll instead solve for the eigenvalues and lowest eigenfunctions using raising and lowering operators like we did for the one-dimensional harmonic potential and the eigenstates of the angular momentum. You get to work out some of the intermediate steps in {numref}`pb:hydrogenspectrumalgebraic`.

We start from the form of the Hamiltonian given in equation&nbsp;{eq}`hydrogenHamiltonianangularmomentumoperator`. When applied to a function of the form $R(r) Y_l^m(\theta, \phi)$, the $\hat{L}^2$ operator gets replaced by its eigenvalue $l(l+1)\hbar^2$, which gives us a one-dimensional Hamiltionian&nbsp;$\hat{H}_l$ for each value of&nbsp;$l$. Re-writing again in terms of the function $u(r)$, this Hamiltonian is given by:

$$
\hat{H}_l = \frac{\hbar^2}{2 m_\mathrm{e}} \left( \frac{\hat{p}_r^2}{\hbar^2} + \frac{l(l+1)}{r^2} - \frac{2}{a_0 r} \right),
$$ (hydrogenradialHamiltonian)

where $a_0$ is the Bohr radius and

$$
\hat{p}_r = - i \hbar \left( \frac{\partial }{\partial r} + \frac{1}{r} \right)
$$ (radialmomentumoperator)

is the momentum operator acting along the radial direction $r$.

We now introduce the ladder operator $\hat{A}_l$ and its Hermitian conjugate&nbsp;$\hat{A}_l^\dagger$:

```{math}
:label: hydrogenladderoperators
\begin{align*}
\hat{A}_l &= \frac{a_0}{\sqrt{2}} \left(\frac{i}{\hbar} \hat{p}_r - \frac{l+1}{r} + \frac{1}{a_0(l+1)} \right), \\
\hat{A}_l^\dagger &= \frac{a_0}{\sqrt{2}} \left(-\frac{i}{\hbar} \hat{p}_r - \frac{l+1}{r} + \frac{1}{a_0(l+1)} \right)
\end{align*}
```

We can then express the Hamiltonian&nbsp;$\hat{H}_l$ as

$$
\hat{H}_l = \frac{\hbar^2}{a_0^2 m_\mathrm{e}} \left( \hat{A}_l^\dagger \hat{A}_l - \frac{1}{2(l+1)^2} \right).
$$ (hydrogenradialHamiltonianladderoperators)

And (after some algebra), we can show that

$$
\hat{A}_l \hat{H}_l = \hat{H}_{l+1} \hat{A}_l.
$$

As we've seen before, the use of the ladder operators is that, given a solution of the eigenvalue equation, we can generate more solutions. Therefore, suppose the function $u_l(r)$ is an eigenvalue of the Hamiltonian $\hat{H}_l$ with eigenvalue $E_l$, i.e.,

$$
\hat{H}_l u_l(r) = E_l u_l(r).
$$ (Hamiltonianeigenvalueeq)

For the harmonic oscillator, the ladder operator gave us another eigenfunction of the Hamiltonian. Here, from an eigenfunction of $\hat{H}_l$, we get that $\hat{A}_l u_l$ is and eigenfunction of $\hat{H}_{l+1}$, with the same eigenvalue $E_l$. As we go up this ladder, the Hamiltonian itself therefore changes, in particular its potential energy part, of which the minimum goes up as $l$ increases. At some point, the minimum of the potential energy will exceed the energy eigenvalue, which means that there must be a maximum value $l_\mathrm{max}$ for which we have

$$
\hat{A}_{l_\mathrm{max}} u_{l_\mathrm{max}} = 0.
$$ (hydrogenladdertop)

(As we know that $l$ is directly related to the angular momentum, and classically the orbit with the maximal angular momentum is a circular one, the solution with $l = l_\mathrm{max}$ is known as the 'circular orbit'; indeed it is usually radially symmetric).

We can get an expression for the energy of the eigenstate $u_\mathrm{lmax}$ by noticing that if equation&nbsp;{eq}`hydrogenladdertop` holds, the integral of its square must also vanish, and we have:

$$
0 = \int \left| \hat{A}_{l_\mathrm{max}} u_{l_\mathrm{max}} \right|^2 \mathrm{d}r = \Braket{u_{l_\mathrm{max}} | \hat{A}_{l_\mathrm{max}}^\dagger \hat{A}_{l_\mathrm{max}} | u_{l_\mathrm{max}}} = \frac{a_0^2 m_\mathrm{e}}{\hbar^2} E_{l_\mathrm{max}} + \frac{1}{2(l_\mathrm{max}+1)^2}
$$ (hydrogenladdertopselfinnerproduct)

and therefore (writing $n = 1+l_\mathrm{max}$):

$$
E_n = E_{l_\mathrm{max}} =- \frac{\hbar^2}{2 a_0^2 m_\mathrm{e}} \frac{1}{n^2}.
$$

Unlike the case of the one-dimensional harmonic oscillator (but like the angular momentum), there is not one unique ladder of eigenfunctions here. There are in fact infinitely many ones, one for each positive integer&nbsp;$n$ (indeed, the primary quantum number). We will therefore label the functions $u$ now by both $n$ and $l$: $u = u_{nl}(r)$, where the values of $l$ are restricted to the familiar range $l = 0, 1, \ldots, n-1$. We can easily calculate $u_{10}(r)$, and with a bit more effort, $u_{n, n-1}(r)$, which of course give the same results as we found in {numref}`sec:hydrogenatom`.

(sec:rotations)=
## Rotations

In analogy with the translation operator of {numref}`sec:spacetranslations`, we can define an operator&nbsp;$\hat{R}_z$ that represents rotations about the $z$ axis by an angle&nbsp;$\alpha$:

$$
\hat{R}_z(\alpha) \psi(r, \theta, \phi) = \psi(r, \theta, \phi - \alpha).
$$ (defzrotationoperator)

Expanding $\psi$ in a Taylor series in&nbsp;$\phi$, we find that the angular momentum operator $\hat{L}_z$ is the generator of these rotations, i.e., we can also write

$$
\hat{R}_z(\alpha) \psi(r, \theta, \phi) = \exp\left(- \frac{i \alpha}{\hbar} \hat{L}_z \right)  \psi(r, \theta, \phi).
$$ (zrotationoperatorgenerator)

Like the translation operator, this rotation operator is not Hermitian, but it is unitary. Its infinitesimal form is given by

$$
\hat{R}_z(\delta) = \exp\left(-\frac{i \delta}{\hbar} \hat{L}_z\right) \approx 1 - \frac{i \delta}{\hbar} \hat{L}_z.
$$ (zrotationoperatorgeneratorinfenitesimal)

As long as the potential does not depend on the angle&nbsp;$\theta$, the operator $\hat{L}_z$ commutes with the Hamiltonian. Therefore, once again invoking the generalized Ehrenfest theorem (equation&nbsp;{eq}`operatorexpectationvaluetimeevolution2`), we have

$$
\frac{\mathrm{d}}{\mathrm{d}t} \Braket{\hat{L}_z} = \frac{i}{\hbar} \Braket{\left[ \hat{H}, \hat{L}_z \right]} = 0,
$$

and thus (the expectation value of) the angular momentum about the $z$-axis is conserved.

We could of course use a completely analogous argument to show that, if the potential is invariant under rotations about the $x$ or $y$ axis, we find conservation of the angular momenta $\hat{L}_x$ and $\hat{L}_y$ about these axes. We can now easily generalize to rotations about an arbitrary axis defined by the unit vector $\bm{\hat{n}}$. For rotations about this axis, the rotation operator is generated by $\bm{\hat{n}} \cdot \hat{\bm{L}}$:

$$
\hat{R}_{\bm{\hat{n}}}(\alpha) = \exp \left(- \frac{i \alpha}{\hbar} \bm{\hat{n}} \cdot \hat{\bm{L}} \right).
$$ (rotationoperatorgenerator)

The angular momentum is thus the generator of rotations in space, and if the potential is invariant under rotations, the (expectation value of) the angular momentum will be conserved.

(sec:spin)=
## Spin

In {numref}`sec:hydrogenatom`, we found that the eigenfunctions of the hydrogen atom Hamiltonian are characterized by three quantum numbers, $n$, $l$ and $m$. The eigenvalues (i.e., energies) of that Hamiltonian however were determined by the value of $n$ alone, though they did not have to be, because the radial part of the wavefunction depends on both $n$ and $l$. As we'll see in {numref}`ch:perturbationtheory`, there will be corrections to the Hamiltonian which will introduce a dependence of the energy on the orbital quantum number&nbsp;$l$. However, for the ground state we have $l=0$, which means that we expect a single ground-state energy level, also if we account for these corrections. In particular, for the transition between a given excited state and the ground state, we expect to observe a single emission line, as the difference between the two states should correspond to one photon energy (and thus wavelength). However, people quickly found that in alkali metals (elements in the first column of the periodic table, including lithium, sodium, potassium and rubidium) there are in fact two lines, close together but clearly distinct. Wolfgang Pauli suggested in 1924 that therefore electrons should have another, non-classical property (or 'degree of freedom') that could assume two values only. The interpretation of this degree of freedom as a form of angular momentum was suggested independently by Ralph Kronig and by George Uhlenbeck and Samuel Goudsmit, both in 1925. While Pauli dismissed Kronig's suggestion with the argument that for any finite-size particle the rate of spin would have to be so high that it's surface speed would exceed the speed of light, Uhlenbeck and Goudsmit were luckier, gaining support from their advisor Paul Ehrenfest. As we now understand electrons and other elementary particles to be point particles with no measurable finite size, but with intrinsic properties like mass and charge, we can add a quantum mechanical property of 'intrinsic angular momentum' without violating the theory of relativity. In contrast, only a few years later, spin became a central part of Dirac's theory of relativistic quantum mechanics (see {numref}`ch:relativisticcquantummechanics`).

```{index} spin, bosons, fermions
```
If we accept spin as a form of angular momentum, we already know its mathematical description, as we derived it in {numref}`sec:angularmomentumoperators`. However, unlike orbital angular momentum, spin angular momentum will be an intrinsic property of a particle, meaning that it will always have the same *total* angular momentum. We therefore assign a fixed *spin quantum number*&nbsp;$s$ to each particle. There is no reason why the spin quantum number&nbsp;$s$ could not have half-integer values. As proved later, there is however a fundamental difference between particles with integer and those with half-integer spin, which is reason enough to give them different names. Particles with integer spin are known as *bosons*, and those with half-integer spin are called *fermions*. Electrons have $s=\frac12$ and are thus fermions; photons have $s=1$ and are therefore bosons.

Although the magnitude of the spin of a particle is fixed, its direction can change. In particular, a particle with $s=\frac12$ can have two different values for its associated $m_s$ quantum number, namely $m_s = + \frac12$ and $m_s=-\frac12$. These states are commonly referred to as 'spin up' and 'spin down', respectively, although it is wise to remember that these are just colloquialisms, meant to distinguish the two states.

Because a spin state is fully classified by the values of the quantum numbers $s$ and $m_s$, we can write the state as $\ket{s m_s}$. Doing so, the operators from {numref}`sec:angularmomentumoperators` act on these states as (replacing the $\hat{L}$ by a $\hat{S}$ to indicate that the operator acts on the spin instead of the orbital angular momentum):

```{math}
:label: spinoperators
\begin{align*}
\hat{S}^2 \ket{s m_s} &= \hbar^2 s(s+1) \ket{s m_s}, \\
\hat{S}_z \ket{s m_s} &= \hbar m_s \ket{s m_s}, \\
\hat{S}_\pm \ket{s m_s} &= \hbar \sqrt{s(s+1) - m_s(m_s \pm 1)} \ket{s (m_s \pm 1)}.
\end{align*}
```

As for the orbital angular momentum, we defined $\hat{S}^2 = \hat{S}_x^2 + \hat{S}_y^2 + \hat{S}_z^2$ and $\hat{S}_\pm = \hat{S}_x \pm i \hat{S}_y$. The three spin operators also satisfy the same commutation relations as before, in particular

$$
\left[\hat{S}_x, \hat{S}_y\right] = i \hbar \hat{S}_z.
$$ (spincommutators)

Note that the state $\ket{s m_s}$ is now an eigenstate of both $\hat{S}^2$ and $\hat{S}_z$, but not of $\hat{S}_x$ or $\hat{S}_y$; like for the orbital angular momentum, we have chosen a reference direction, which conventionally is the $z$-direction.

Because the number of spin eigenstates is finite, we can write all eigenstates as vectors, and all operators as matrices. We'll illustrate here for the simplest nontrivial case, $s=\frac12$; you'll get to do it yourself for some other cases in the problems. Our two states, 'spin up' and 'spin down', are going to be eigenstates of the $\hat{S}_z$ operator; any other state can be written as a linear combination of these two. We therefore introduce a basis of a two-dimensional vector state as

```{math}
:label: basisspinors
\begin{align*}
\chi_+ &= \Ket{\frac12, \,\frac12} = \Ket{\uparrow} = \begin{pmatrix} 1 \\ 0 \end{pmatrix}, \\
\chi_- &= \Ket{\frac12, \,-\frac12} = \Ket{\downarrow} = \begin{pmatrix} 0 \\ 1 \end{pmatrix}.
\end{align*}
```

```{index} spinor
```
A general spin state $\chi$, known as a *spinor*, can then be written as

$$
\chi = a \chi_+ + b \chi_- = a \Ket{\uparrow} + b \Ket{\downarrow} = \begin{pmatrix} a \\ b \end{pmatrix}.
$$ (defspinor)

Like any quantum state, a spinor must be normalized, which means that we demand that

$$
1 = |\chi|^2 = \chi^\dagger \chi = \left( a^*,  \, b^* \right) \begin{pmatrix} a \\ b \end{pmatrix} = a^*a + b^*b,
$$ (spinornormalization)

where the $^\dagger$ (pronounced 'dagger') stands for the Hermitian conjugate, and the star for the complex conjugate. As in {numref}`sec:Diracnotation`, we can now find a matrix representation for any of the spin operators in the basis&nbsp;{eq}`basisspinors` of eigenstates of the $\hat{S}_z$ operator by simply looking at the action of the operator on the eigenstates. For example

```{math}
\begin{align*}
\hat{S}^2 \chi_+ &= \hat{S}^2 \Ket{\uparrow} = \hat{S}^2 \Ket{\frac12 \frac12} = \hbar^2 \frac12 \left(\frac12 + 1\right) \Ket{\frac12 \frac12} = \frac34 \hbar^2 \chi_+, \\
\hat{S}^2 \chi_- &= \hat{S}^2 \Ket{\downarrow} = \hat{S}^2 \Ket{\frac12 -\frac12} = \hbar^2 \frac12 \left(\frac12 + 1\right) \Ket{\frac12 -\frac12} = \frac34 \hbar^2 \chi_-,
\end{align*}
```

and therefore

```{math}
:label: S2matrixelements
\begin{align*}
\Braket{\uparrow | \hat{S}^2 | \uparrow} = \Braket{\downarrow | \hat{S}^2 | \downarrow} &= \frac34 \hbar^2, \\
\Braket{\uparrow | \hat{S}^2 | \downarrow} = \Braket{\downarrow | \hat{S}^2 | \uparrow} &= 0,
\end{align*}
```

where we used that the eigenstates are orthonormal. From equation&nbsp;{eq}`S2matrixelements` we can read off that we can represent $\hat{S}^2$ as the matrix

$$
\hat{S}^2 = \frac34 \hbar^2 \begin{pmatrix} 1 & 0 \\ 0 & 1 \end{pmatrix}.
$$ (S2matrix)

We readily obtain the matrix representation of $\hat{S}_z$ using the same method (see equation&nbsp;{eq}`Paulispinmatrices`). For $\hat{S}_x$ and $\hat{S}_y$ we have to take an intermediate step though, as we don't (yet) know their action on a general spin state. We do however know the action of the raising and lowering operators $\hat{S}_\pm$, which allows us to find matrix representations for them. We readily obtain

```{math}
\begin{align*}
\Braket{\uparrow | \hat{S}_+ | \downarrow} = \hbar \Braket{\uparrow | \uparrow} &= \hbar, \qquad \text{all others } 0, \\
\Braket{\downarrow | \hat{S}_- | \uparrow} = \hbar \Braket{\downarrow | \downarrow} &= \hbar, \qquad \text{all others } 0.
\end{align*}
```

The matrix representations of $\hat{S}_\pm$ are thus given by

$$
\hat{S}_+ = \hbar \begin{pmatrix} 0 & 1 \\ 0 & 0 \end{pmatrix} \qquad \text{and} \qquad \hat{S}_- = \hbar \begin{pmatrix} 0 & 0 \\ 1 & 0 \end{pmatrix}.
$$ (Spmmatrices)

Writing $\hat{S}_x = \frac12 \left(\hat{S}_+ + \hat{S}_- \right)$ and $\hat{S}_y = \frac{1}{2i} \left(\hat{S}_+ - \hat{S}_- \right)$, we also find matrix expressions for  $\hat{S}_x$ and $\hat{S}_y$:

$$
\hat{S}_x = \frac{\hbar}{2} \begin{pmatrix} 0 & 1 \\ 1 & 0 \end{pmatrix} = \frac{\hbar}{2} \bm{\sigma}_x, \qquad \hat{S}_y = \frac{\hbar}{2} \begin{pmatrix} 0 & -i \\ i & 0 \end{pmatrix} = \frac{\hbar}{2} \bm{\sigma}_y \qquad \text{and} \qquad \hat{S}_z = \frac{\hbar}{2} \begin{pmatrix} 1 & 0 \\ 0 & -1 \end{pmatrix} = \frac{\hbar}{2} \bm{\sigma}_z.
$$ (Paulispinmatrices)

```{index} Pauli spin matrices
```
The matrices $\bm{\sigma}_x$, $\bm{\sigma}_y$ and $\bm{\sigma}_z$ are known as the *Pauli spin matrices*.

### Worked example: measurements of the z and x component of the spin
Suppose we have a spin-$\frac12$ particle in an arbitrary spin state, characterized by a (properly normalized) spinor $\chi = \begin{pmatrix} a \\ b \end{pmatrix}$. A *measurement* of the $z$-component of the spin of the particle will (as always) yield one of the eigenvalues of the $\hat{S}_z$ operator, which are $\pm \hbar/2$. To find the probability of each possible outcome, we write the spinor as $\chi = a \chi_+ + b \chi_-$, from which we can read off that the probability of measuring $+\hbar/2$ is $|a|^2$, and the probability of measuring $-\hbar/2$ is $|b|^2$.
What if we had measured the $x$-component instead? Our physical intuition tells us that the possible outcomes should still be $\pm \hbar/2$, because the choice of the $x$ and $z$ axes is arbitrary. Mathematically we can verify that this is indeed the case, as the eigenvalues of $\hat{S}_x$, as expressed in matrix form in equation&nbsp;{eq}`Paulispinmatrices`, are indeed $\pm \hbar/2$:

$$
0 = \det(\hat{S}_x - \lambda\bm{I}) = \begin{vmatrix} -\lambda & \hbar/2 \\ \hbar/2 & -\lambda \end{vmatrix} = \lambda^2 - \left(\frac{\hbar}{2}\right)^2 \quad \Rightarrow \quad \lambda = \pm \frac{\hbar}{2}.
$$

Determining the probability of each outcome requires a little more work, as now we have to expand the spinor in the eigenstates of $\hat{S}_x$. Finding those eigenstates is a straightforward exercise in linear algebra. Writing the state as a spinor with coefficients $\alpha$ and $\beta$, we have:

$$
\frac{\hbar}{2} \begin{pmatrix} 0 & 1 \\ 1 & 0 \end{pmatrix} \begin{pmatrix} \alpha \\ \beta \end{pmatrix} = \pm \frac{\hbar}{2} \begin{pmatrix} \alpha \\ \beta \end{pmatrix} \quad \Rightarrow \quad \begin{pmatrix} \beta \\ \alpha \end{pmatrix} = \pm \begin{pmatrix} \alpha \\ \beta \end{pmatrix},
$$

so $\beta = \pm \alpha$ and we find for the eigenspinors of $\hat{S}_x$ (which we'll denote with $\xi$'s instead of $\chi$'s):

$$
\xi_+ = \frac{1}{\sqrt{2}} \begin{pmatrix} 1 \\ 1 \end{pmatrix} \quad \text{and} \quad \xi_- = \frac{1}{\sqrt{2}} \begin{pmatrix} 1 \\ -1 \end{pmatrix},
$$ (Sxeigenspinors)

for eigenvalues $\hbar/2$ and $-\hbar/2$, respectively. Now expanding the initial spinor in the eigenspinors of $\hat{S}_x$, we get

$$
\chi = \begin{pmatrix} a \\ b \end{pmatrix} = \frac{a+b}{\sqrt{2}} \xi_+ + \frac{a-b}{\sqrt{2}} \xi_-,
$$

and we can read off that the probability that a measurement of the $x$-component of the spin yields a value $+\hbar/2$ equals $\frac12 |a+b|^2$, and the probability of finding $-\hbar/2$ equals $\frac12 |a-b|^2$.

Putting the above in formal form (and stressing the analogy with what we did in {numref}`sec:generaloperators`), we can write the spinor $\chi$ in terms of the eigenspinors $\chi_n$ of an operator $\hat{Q}$ as

$$
\chi = \sum_n c_n \chi_n,
$$

where $c_n = \Braket{\chi_n | \chi}$ and the probability of measuring eigenvalue $q_n$ associated with state $\chi_n$ is $|c_n|^2$. Naturally, this general form applies to any spin operators, also for particles with spin values other than $\frac12$.

(sec:spinmagneticfield)=
### Spin in a magnetic field: Larmor precession

```{index} magnetic dipole moment
```
An electric current (stream of charged particles) moving through an externally applied magnetic field will experience a Lorentz force, deflecting the direction of the particles. In turn, the moving charges also generate a magnetic field. It doesn't matter if the motion of the charges is linear or circular, a field will be generated in both cases. As spin is a form of angular momentum, and angular momentum is associated with particle motion, we might therefore reasonably expect that a charged particle with spin generates a magnetic field, and indeed it does. For a single particle with spin angular momentum $\bm{S}$, the resulting magnetic field is a magnetic dipole, with a *magnetic dipole moment* $\bm{\mu}$ proportional to $\bm{S}$:

$$
\bm{\mu} = \gamma \bm{S}.
$$ (spinmagneticdipole)

```{index} gyromagnetic ratio
```
The proportionality constant&nbsp;$\gamma$ is known as the *gyromagnetic ratio*. For a classical spinning sphere with charge $q$ and mass $m$, the gyromagenetic ratio is given by $q/2m$; due to relativistic effects however, the gyromagnetic ratio of an electron (as a point particle with spin-$\frac12$) is twice<sup>[^7]</sup> that value, $\gamma = -e / m_\mathrm{e}$. If we place the electron in an external magnetic field&nbsp;$\bm{B}$, the external field will exert a torque $\bm{\mu} \times \bm{B}$ on our electron that tends to align its dipole moment with the external field. We can calculate the work done by this torque if it rotates the electron over an angle&nbsp;$\theta$ through integration, from which we obtain the (potential) energy of the magnetic dipole $\bm{\mu}$ in the external field $\bm{B}$ as $V = - \bm{\mu} \cdot \bm{B}$. Because for a spin-$\frac12$ (or any finite number) particle the components of the angular momentum vector $\bm{S}$ can be expressed in matrix form, we can also express the associated Hamiltonian as a matrix:
```{math}
:label: spinmagneticfieldHamiltonian
\begin{align*}
\hat{H} &= - \gamma \bm{B} \cdot \hat{\bm{S}} = -\gamma \left( B_x \hat{S}_x + B_y \hat{S}_y + B_z \hat{S}_z \right)  \\
&= -\frac{\hbar \gamma}{2} \left[ B_x \begin{pmatrix} 0 & 1 \\ 1 & 0 \end{pmatrix} + B_y \begin{pmatrix} 0 & -i \\ i & 0 \end{pmatrix} + B_z \begin{pmatrix} 1 & 0 \\ 0 & -1 \end{pmatrix} \right].
\end{align*}
```

```{figure} images/angularmomentum/precession.svg
:name: fig:precession
Changes in angular momentum due to external torques. (a) (Classical) precession and nutation of the Earth, under the action of the tidal forces exerted by the sun and the moon. Earth's own rotation axis (black) presently makes an angle of about $23^\circ$ with the vertical (i.e., the line perpendicular to the plane of Earth's orbit around the sun). The rotation axis both *precesses*, meaning that it itself rotates around the vertical (blue line) and *nutates*, meaning that its angle with the vertical changes (orange line). Earth's precession has a period of about 26000 years. The largest component of Earth's nutation has a period of 18.6 years, the same as that of the precession of the Moon's orbital nodes, causing a change in the angle of plus or minus 9.2 arcseconds (1/60th of 1/60th of a degree), or a change of plus or minus 17 arcseconds in the position of lines of latitude&nbsp;{cite}`Lowrie2007`. Image adopted from&nbsp;<sup>[^8]</sup>, public domain. (b) Larmor precession of the avearage of the spin vector of a quantum particle in an external magnetic field of magnitude&nbsp;$B_0$ aligned along the $z$-direction. If the spin vector makes an angle&nbsp;$\alpha$ with the $z$-direction, its expectation value will precess with frequency $\omega = \gamma B_0$, where $\gamma$ is the gyromagnetic ratio of the particle.
```

Torques can have quite counter-intuitive effects, even in classical mechanics, especially on objects that are already rotating. Two of the more baffling ones are precession (rotation of the rotation axis) and nutation (change in angle of the rotation axis). Earth (a spinning sphere with an axis not aligned with the force of gravity exerted by the sun) experiences both, causing the Earth's axis to shift over time (the polar star won't remain the polar star forever) and possibly the occurrence of ice ages (though the jury is still out on that), see {numref}`fig:precession`(a). A similar thing happens with our electron when placed in a uniform external magnetic field. As we're free to pick our axes, we'll put the field along the $z$-direction, so $\bm{B} = B_0 \bm{\hat{z}}$, and the Hamiltonian simplifies to

$$
\hat{H} = - \frac{\gamma B_0 \hbar}{2} \bm{\sigma}_z = - \frac{\gamma B_0 \hbar}{2} \begin{pmatrix} 1 & 0 \\ 0 & -1 \end{pmatrix}.
$$ (LarmorprecessionHamiltonian)

Because $\hat{H}$ is now a multiple of $\hat{S}_z$, its eigenvectors are the same, namely the eigenspinors $\chi_\pm$, with associated eigenvalues (i.e., energies) $E_\pm = \mp \frac12 \gamma B_0 \hbar$. Unsurprisingly, the energy is largest when the spin is oriented opposite to the magnetic field, and lowest when the spin and the field are parallel. If we set up the system in one of those two states, it will remain there forever, but if we put it in a general spinor state $\chi$, the time evolution becomes nontrivial. That time evolution follows from the full (i.e., time-dependent) Schr&ouml;dinger equation, which now takes the form

$$
i \hbar \frac{\partial \chi}{\partial t} = \hat{H} \chi.
$$ (LarmorprecessionSE)

Because the Hamiltonian itself is time-independent, we can proceed as in {numref}`sec:TISE`, and immediately write down the solution of&nbsp;{eq}`LarmorprecessionSE` in terms of the stationary states:

$$
\chi(t) = a \chi_+ \exp\left(- \frac{i E_+}{\hbar} t \right) + b \chi_- \exp\left(- \frac{i E_-}{\hbar} t\right),
$$

where the constants $a$ and $b$ are set by the initial condition, $\chi(0) = \begin{pmatrix} a \\ b \end{pmatrix}$. Naturally, $\chi(0)$ should be normalized, which means that we demand that $a^2 + b^2 = 1$, and we can write $a = \cos(\alpha/2)$ and $b = \sin(\alpha/2)$ for an arbitrary angle&nbsp;$\alpha$. In terms of $\alpha$, the time-dependent spinor then reads

$$
\chi(t) = \begin{pmatrix} \cos(\alpha/2) \exp(i\gamma B_0 t / 2) \\ \sin(\alpha/2) \exp(-i\gamma B_0 t / 2) \end{pmatrix}.
$$

As usual, we can't say anything about the current state of the spin without performing a measurement (which will destroy the state), but we can calculate what will happen with the expectation value of each of its components. We get:

```{math}
\begin{align*}
\Braket{\hat{S}_x} &= \chi^\dagger (t) \bm{S}_x \chi(t) = \left( \cos\left(\frac12 \alpha\right) \exp\left(\frac12 -i\gamma B_0 t\right), \, \sin\left(\frac12 \alpha\right) \exp\left(\frac12 i\gamma B_0 t\right) \right) \frac{\hbar}{2} \begin{pmatrix} 0 & 1 \\ 1 & 0 \end{pmatrix} \begin{pmatrix} \cos(\alpha/2) \exp(i\gamma B_0 t / 2) \\ \sin(\alpha/2) \exp(-i\gamma B_0 t / 2) \end{pmatrix}  \\
&= \frac{\hbar}{2} \sin(\alpha) \cos\left(\gamma B_0 t\right), \\
\Braket{\hat{S}_y} &= \frac{\hbar}{2} \sin(\alpha) \sin\left(\gamma B_0 t\right), \\
\Braket{\hat{S}_z} &= \frac{\hbar}{2} \cos(\alpha).
\end{align*}
```

```{index} Larmor precession
```
Therefore, if the direction of $\bm{S}$ makes an angle $\alpha$ with the direction of the magnetic field, the expectation value of $\bm{S}$ will precess around that direction with the *Larmor frequency*

$$
\omega = \gamma B_0,
$$ (Larmorfrequency)

as illustrated in {numref}`fig:precession`(b). 
(sec:SternGerlach)=
### A proof of the quantization of spin: the Stern-Gerlach experiment

The Stern-Gerlach experiment<sup>[^9]</sup> is famous as the test that first showed that angular momentum is indeed quantized. The basic setup of the experiment is simple (see {numref}`fig:SternGerlachexperiment`): a stream of electrically neutral silver atoms travels through a slightly inhomogeneous magnetic field, causing them both to precess (as discussed above) and be deflected due to the force on the magnetic dipole that results from the inhomogeneity of the external magnetic field. If the atoms would be classical particles with spin (i.e., effectively spinning spheres), we would expect the direction of their angular momentum to be random and continuous, and therefore also their dipoles to have a continuous distribution. Because the magnetic force is proportional to the magnetic dipole moment, we'd expect a continuous range of deflection angles, which can be visualized by collecting the particles on a screen once they leave the magnet; classically, we'd therefore expect a line of particles. If the angular momenta on the other hand are quantized, we expect to see discrete spots only, which is indeed the case.

```{code-cell} ipython3
:tags: [hide-input, remove-output]

%config InlineBackend.figure_formats = ['svg']
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatch
import matplotlib.path as mpath
from myst_nb import glue

fig,ax = plt.subplots(figsize=(10,2.5))

ax.set_xlim(-0.5,1.1)
ax.set_ylim(-0.15,0.18)

#experiment 

x = np.linspace(0,1,1000)

lineformatting = {"linewidth":2, "color":"black"}
path = (1/2*x)**4

codes = [
    mpath.Path.MOVETO,
    mpath.Path.LINETO,
    mpath.Path.CURVE4,
    mpath.Path.LINETO,
    mpath.Path.LINETO
    
]

style = mpatch.ArrowStyle("-|>", head_length=10, head_width=5)
a1 = mpatch.FancyArrowPatch(path=mpath.Path([(0,0),(0.1,0),(0.6,0),(0.7,0.03),(1,0.08)],
codes=codes), arrowstyle = style, **lineformatting)
a2 = mpatch.FancyArrowPatch(path=mpath.Path([(0,0),(0.1,0),(0.6,0),(0.7,-0.03),(1,-0.08)],
codes=codes), arrowstyle = style, **lineformatting)


for a in [a1,a2]:
    ax.add_patch(a)
    
props = {"s":"     magnet     ","fontsize":15,"fontfamily":"serif"}
boxprops = {"boxstyle":'square', "facecolor":'lightblue',"edgecolor":"black","lw":1.5}

ax.text(0.22,0.1,**props,bbox = boxprops)
ax.text(0.22,-0.12,**props,bbox = boxprops)
    
#coordinate set
arrowprops = {"width":0.003,"head_width":0.02,"head_length":0.02, "color":"black","edgecolor":None}

ax.arrow(-0.32,0,0,0.15,**arrowprops)
ax.arrow(-0.32,0,0.2,0,**arrowprops)
ax.arrow(-0.32,0,-0.1,-0.1,**arrowprops)

textprops = {"fontsize":16,"fontfamily":"serif","fontstyle":"italic"}
ax.text(-0.44,-0.06,"x",**textprops)
ax.text(-0.15,-0.05,"y",**textprops)
ax.text(-0.37,0.14,"z",**textprops)
    

ax.set_xticks([])
ax.set_yticks([])
ax.axis('off')
# Save graph to load in figure later (special Jupyter Book feature)
glue("SternGerlach", fig, display=False)
```

```{glue:figure} SternGerlach
:name: fig:SternGerlachexperiment
Setup of the Stern-Gerlach experiment
```


The force exerted on a magnetic dipole in an inhomogeneous magnetic field&nbsp;$\bm{B}$ can be found as minus the gradient of the associated potential energy, which is simply the Hamiltonian of equation&nbsp;{eq}`spinmagneticfieldHamiltonian`, $\hat{H} = - \bm{\mu} \cdot \bm{B}$, so we find

$$
\bm{F} = \nabla \left(\bm{\mu} \cdot \bm{B}\right).
$$ (magneticdipoleforce)

If we send a beam of (electrically) neutral atoms through this field, there will be no Lorentz force, but if the atoms have nonzero spin, they do have a magnetic dipole and thus a force of the form of equation&nbsp;{eq}`magneticdipoleforce`. Suppose the particles travel in the positive $y$ direction, and the magnetic field is still mostly in the $z$-direction, but with a slight inhomogeneity<sup>[^10]</sup> of magnitude $\varepsilon$:

$$
\bm{B} = - \varepsilon x \bm{\hat{x}} + (B_0 + \varepsilon z) \bm{\hat{z}},
$$

where $\varepsilon \ll B_0/L$ with $L$ the length of the path of the particles. For the force we then get

$$
\bm{F} = \gamma \varepsilon \left( - S_x \bm{\hat{x}} + S_z \bm{\hat{k}} \right).
$$

Because the main part of the field is still in the $z$-direction, the particle will precess around this direction. Consequently, the expectation value of $S_x$ averages to zero, while that of $S_z$ becomes a constant; effectively therefore, the particles will feel a net force in the $z$ direction, proportional to the magnitude of the $z$-component of their spin. By measuring where the particles end up, we force them to choose one of the allowed values, and for a spin-$\frac12$ particle, we therefore end up with two dots, one for particles that had spin-up and were deflected upwards, one for particles that picked spin-down and were deflected downwards. The same approach works for particles with other values of the quantum number $s$; we then expect to see $2s+1$ separate dots on our screen.

(sec:nmr)=
### Nuclear magnetic resonance

```{index} nuclear magnetic resonance, magnetic resonance imaging
```
As we've seen above, a particle with spin placed in a magnetic field can minimize its energy by aligning it's spin with the direction of the field. In general, the alignment will not be perfect, and the component of the spin perpendicular to the field will precess around the field's direction. This precession can be influenced by a second, weaker field, in particular if this second field is also oscillating. Just like with classical harmonic oscillators, the response will be largest if the driving frequency is close to the natural frequency, and we get resonance. The resonance frequency of a particular particle depends on the magnetic properties of the particle, its chemical environment, and the strength of the applied field. Measuring the resonance frequency is therefore a non-invasive method of distinguishing different particles, in particular (isotopes of) atomic nuclei. The technique is therefore known as *nuclear magnetic resonance* or NMR; both NMR spectroscopy and *magnetic resonance imaging* (MRI) are NMR techniques.

To increase spatial resolution, in NMR imaging the strong applied field is usually not uniform, but has a gradient; as the resonance frequency depends on the (local) external field strength, the resolution depends on the steepness of the gradient. The nucleus needs to have a nonzero net spin to be affected by the field. The most common isotopes of carbon and oxygen, with 12 and 16 nucleons respectively, both have spin&nbsp;$0$; therefore, in organic compounds, the major contribution to NMR is given by hydrogen. MRI imaging can therefore be used to locate both water and fat in living organisms, as shown in {numref}`fig:MRIimages`. Spin coupling between atoms (see {numref}`sec:angularmomentumaddition` and&nbsp;{numref}`sec:spinorbitcoupling`) can shift the resonance frequencies, and are used in NMR spectroscopy to obtain information about the configuration of atoms in molecules.

```{figure} images/angularmomentum/MRIs.jpg
:name: fig:MRIimages
Examples of images obtained with MRI. (a) MRI angiography, view of the arteries in the upper body&nbsp;<sup>[^11]</sup>. (b) MRI of the brain, visualized with different techniques: T1-weighted contrast, where the magnetization is in the same direction as the applied magnetic field (left) and T2-weighted contrast, where the magnetization is perpendicular to the applied magnetic field (right). There is a large tumor in the brain, which is darker in the left image, and much brighter in the right, clearly showing the different tissue composition <sup>[^12]</sup>.
```

(sec:angularmomentumaddition)=
## Addition of angular momenta

Angular momenta are vector quantities; when adding and subtracting them, we need to account for this vector nature. To illustrate the complications and the benefits we get from these vector additions, we consider the simplest possible example: the total spin of a composite particle that consists of two spin-$\frac12$ particles (e.g. a hydrogen atom in the ground state, with angular momentum zero ($l=0$), consisting of a proton and an electron, both spin-$\frac12$ particles). Each of the particles has two possible states, which we'll call 'spin up' and 'spin down' as before. For the combined state, we then expect to find four options:

$$
\uparrow \uparrow, \quad \uparrow \downarrow, \quad \downarrow \uparrow, \quad \text{and} \quad \downarrow\downarrow.
$$ (twospincombinations)

Although these two particles are in a composite state, they are still quantum particles, and therefore the composite state should be a quantum state itself, characterized by quantum numbers. However, we quickly run into trouble if we try to calculate the $m_s$ quantum number of the combined states given above. For the 'double up' and 'double down' states we find $m_s = \frac12 + \frac12 = 1$ and $m_s = -\frac12 - \frac12 = -1$, respectively, but the middle two options both give $m_s = 0$. As two different quantum states cannot have the same quantum numbers, we have arrived at a paradox. To resolve it, we need to realize that the spin operators acting on these composite states are themselves composites as well. We found the value of the $m_s$ quantum number through addition, which is correct, because the composite $\hat{S}_z$ operator is simply the sum of the two spin-$\frac12$ $\hat{S}_z$ operators, or

$$
\hat{S}_z = \hat{S}_z^{(1)} + \hat{S}_z^{(2)},
$$ (Szcomposite)

where $\hat{S}_z^{(1)}$ acts only on the first particle (say the electron) and is indifferent to the second, while the opposite holds for $\hat{S}_z^{(2)}$. Now, to go from a 'spin up' to a 'spin down' eigenstate, we used the lowering operator $\hat{S}_-$. This operator too becomes a composite, similar to $\hat{S}_z$ in equation&nbsp;{eq}`Szcomposite`. Applying it to the 'double up' combined state, we do not find either of the middle states, but a linear combination of them:

```{math}
:label: Sminuscomposite
\begin{align*}
\hat{S}_- \Ket{\uparrow \uparrow} &=  \hat{S}_-^{(1)} + \hat{S}_-^{(2)} \Ket{\uparrow \uparrow} = \Ket{\left(\hat{S}_-^{(1)} \uparrow \right) \uparrow} + \Ket{\uparrow \left(\hat{S}_-^{(2)} \uparrow \right)} = \hbar \Ket{\downarrow \uparrow} + \hbar \Ket{\uparrow \downarrow} = \hbar \left(\Ket{\downarrow \uparrow} + \Ket{\uparrow \downarrow}\right), \\
\hat{S}_- \left(\Ket{\downarrow \uparrow} + \Ket{\uparrow \downarrow}\right) &= \Ket{\left(\hat{S}_-^{(1)} \downarrow \right) \uparrow} + \Ket{\left(\hat{S}_-^{(1)} \uparrow \right) \downarrow} + \Ket{\downarrow \left(\hat{S}_-^{(2)} \uparrow \right)} + \Ket{\uparrow \left(\hat{S}_-^{(2)} \downarrow \right)} = 0 + \hbar \Ket{\downarrow \downarrow} + \hbar \Ket{\downarrow \downarrow} + 0 = 2 \hbar \Ket{\downarrow \downarrow}.
\end{align*}
```

```{index} spin ; triplet
```
Equations&nbsp;{eq}`Sminuscomposite` tell us that there is a *triplet* of composite spin states, which are transferred one into the other by the lowering (and, in the opposite direction, the raising) operators $\hat{S}_\pm$. Properly normalized, this triplet of states consists of

$$
\Ket{\uparrow \uparrow} = \Ket{1 1}, \quad \frac{1}{\sqrt{2}} \left( \Ket{\uparrow \downarrow} + \Ket{\downarrow \uparrow}\right) = \Ket{1 0}, \quad \Ket{\downarrow \downarrow} = \Ket{1 -1}.
$$ (spintriplet)

```{index} spin ; singlet
```
The three states of the triplet have $s=1$, and values of $m_s$ running between $s$ and $-s$, as we would expect for angular momentum states. The triplet kind of solves our paradox, but we are still one state short, as the two 'middle' options in&nbsp;{eq}`twospincombinations` are mutually orthogonal, so we need there to be four states in total. The fourth state is the linear combination of these states that is orthogonal to the $\Ket{1 0}$ combination in the triplet; it is known as the *singlet* spin state

$$
\frac{1}{\sqrt{2}} \left( \Ket{\uparrow \downarrow} - \Ket{\downarrow \uparrow}\right) = \Ket{0 0}.
$$ (spinsinglet)

The singlet state obviously has $m_s = 0$ (simply apply the composite $\hat{S}_z$ operator to it), but also $s=0$, which we can see in one of two ways: either by applying the composite $\hat{S}^2$ operator (which gives $0$), or by trying to 'raise' or 'lower' the state with $\hat{S}_\pm$ (which also give $0$, i.e., there is only one state on the 'ladder'). Unlike for the single particle or for the orbital angular momentum, the combined states thus have multiple ladders; in this example there are two, one in which the spins are in parallel (giving a composite particle with $s=1$) and one where they are antiparallel (resulting in a composite particle with $s=0$).

Composite particles are everywhere. Not only atoms are composites of multiple spin-$\frac12$ particles, so are atomic nuclei (except, obviously, the single-proton hydrogen nucleus), and nuclear particles themselves (protons and neutrons both consist of three spin-$\frac12$ particles known as quarks, see {numref}`ch:relativisticcquantummechanics`). Particles need not be composed of spin-$\frac12$ particles; they could also be composed of particles with different spin. We can however extend the treatment of the two spin-$\frac12$ particles easily to either multiple particles or particles with different spins. By induction, we find that combining particles with spins $s_1$ and $s_2$ yields multiple possibilities for the composed particles, with spin quantum numbers $s$ ranging between $|s_1 - s_2|$ and $s_1 + s_2$ in integer steps, and as always, associated $m_s$ quantum numbers ranging from $-s$ to $+s$ in integer steps.

```{index} total angular momentum
```
As spin and orbital angular momenta are ultimately the same physical quantities, we can also define a *total angular momentum*, usually denoted by $\hat{\bm{J}}$, as the (vector) sum of the orbital and spin angular momentum:

$$
\hat{\bm{J}} = \hat{\bm{L}} + \hat{\bm{S}}.
$$ (deftotalangularmomentum)

The addition rules that give the quantum numbers for the total angular momentum ($j$ and $m_j$) are the same as those for adding spin quantum numbers.

Once we go beyond adding spin-$\frac12$ particles, a general combined state $\Ket{s m_s}$ will be a linear combination of composite states $\Ket{s_1 s_2 m_1 m_2}$:

$$
\Ket{s m_s} = \sum_{m_1 + m_2 = m_s} C_{m_1, m_2, m_s}^{s_1, s_2, s} \Ket{s_1 s_2 m_1 m_2}.
$$ (spincombinationsClebschGordan)

```{index} Clebsch-Gordan coefficients
```
Working out the prefactors in the combinations (known as the *Clebsch-Gordan coefficients*) can be a quite time-consuming and tedious (though straightforward) task. To help us out, they have been tabulated for us by physicists of Christmas past (see e.g. Bohm&nbsp;{cite}`Bohm1986`, Griffiths and Schroeter&nbsp;{cite}`Griffiths2018`, or Hagiwara *et al.*&nbsp;{cite}`Hagiwara2002`; naturally, they can also be found on [Wikipedia](https://en.wikipedia.org/wiki/Table_of_Clebsch–Gordan_coefficients)&nbsp;{cite}`WikipediaClebschGordan`). 

### Mathematical interlude: product state space
In the example of {numref}`sec:angularmomentumaddition`, it probably didn't surprise you that we found four options for the possible combinations of two spin-$\frac12$ particles. After all, each of the particles has two possible states, and $2 \times 2 = 4$. The combinations we got (the singlet and triplet states) might not be what you had expected, but at least there are four of them, and they are mutually orthogonal. To check that last statement, remember that the first of the two spins in each of the kets is the state of particle 1 and the second that of particle 2, so if we for instance calculate the inner product between the 'double up' and the central triplet state, we get
```{math}
:label: tripletorthogonality
\begin{align*}
\Braket{11|10} &= \frac{1}{\sqrt{2}} \left(\Braket{\uparrow \uparrow |\uparrow \downarrow} + \Braket{\uparrow \uparrow | \downarrow \uparrow}\right) = \frac{1}{\sqrt{2}} \left( \Braket{\uparrow | \uparrow}^{(1)}  \Braket{\uparrow | \downarrow}^{(2)} + \Braket{\uparrow | \downarrow}^{(1)} \Braket{\uparrow | \uparrow}^{(2)} \right) = \frac{1}{\sqrt{2}} \left( 1 \cdot 0 + 0 \cdot 1 \right) = 0,
\end{align*}
```
where as before the superscripts indicate which particle we're talking about. The orthogonality between all other pairs follows in the same way.

```{index} product state space
```
For wave functions, we introduced the *Hilbert space* of all allowed quantum states in {numref}`sec:Hilbertspace`, and we found that the eigenstates of the Hamiltonian form a basis for this Hilbert space; we get a similar space of functions for any Hermitian operator, with its eigenfunctions as a basis. For operators with a finite, discrete spectrum, these state spaces can be written as vector spaces, with the operators cast in matrix form, and the eigenstates becoming eigenvectors, as we have done for the spin-$\frac12$ particles in {numref}`sec:spin`. The combined space of eigenstates of two such operators then becomes the *product state space*. Product state spaces are not limited to operators with finite, discrete spectra; you can also create product state spaces of Hilbert spaces (as we'll do when looking at multiple electrons later on) or between a finite-dimensional and an infinite-dimensional state space, for example when considering both the spin and the position state of an electron in a hydrogen atom.

```{index} tensor product
```
To characterize the product state space, it suffices to define its basis; as the product space will again be a vector space, all its elements can then be written as linear combinations of the basis. To make things concrete, let's say we have state spaces<sup>[^13]</sup> $S_A$ and $S_B$. The product space is then formally written as $S_{AB} = S_A \otimes S_B$, where the symbol $\otimes$ is known as the *tensor product*. The tensor product differs from the Cartesian product $S_A \times S_B$, which is simply the collection of all pairs $(a,b)$ with $a \in S_A$ and $b \in S_B$. For the tensor product, we also define the basis, which we construct from the basis vectors of $S_A$ and $S_B$, again using a tensor product. Writing the basis vectors (or states) of $S_A$ and $S_B$ as $\Ket{a}$ and $\Ket{b}$, the basis of $S_{AB}$ then consists of elements of the form

$$
\Ket{ab} = \Ket{a} \otimes \Ket{b}.
$$ (productstatebasis)

To understand what the tensor product in&nbsp;{eq}`productstatebasis` means, we go back to a regular vector space, $V$. An element of $V$ can be written as a vector, $\bm{v}$, which, in Dirac notation, becomes a state, $\Ket{\bm{v}}$. Each vector defines a *linear map* from the vector space to the (real or complex) numbers, through the inner product. In mathematical notation, given a vector $\Ket{\bm{v}}$, we have a function $\phi_{\bm{v}} : V \to \mathbb{C}$ defined as

$$
\phi_{\bm{v}}(\bm{w}) = \Braket{\bm{v} | \bm{w}} = \sum_{i=1}^n v_i^* w_i.
$$

By the properties of inner products, $\phi_{\bm{v}}$ is linear (see {numref}`app:linalg`). The tensor product in&nbsp;{eq}`productstatebasis` now defines a *bilinear* map, taking two arguments, one from $S_A$ and one from $S_B$. The bilinear map is defined as the product of the inner products, as you would expect. In particular, if we have an element $\Ket{ab}$ of $S_{AB}$, and elements $\Ket{c} \in S_A$ and $\Ket{d} \in S_B$, we have

$$
\Ket{ab} \left(\Ket{c}, \Ket{d} \right) = \Braket{a|c}\Braket{b|d}.
$$

While the above definition is very formal, you have already applied it (tacitly) in equation&nbsp;{eq}`tripletorthogonality`; essentially, the elements of the product space act on the elements of the component states.

### Product states and entanglement
A general spin state can be written as a linear combination of a spin-up and a spin-down state (cf. equation&nbsp;{eq}`defspinor`). If we have two spin-$\frac12$ particles (one for Alice and one for Bob), they can both be in such linear combinations:

$$
\chi_A = \alpha_1 \Ket{\uparrow_A} + \alpha_2 \Ket{\downarrow_A} = \begin{pmatrix} \alpha_1 \\ \alpha_2 \end{pmatrix}_A, \qquad \chi_B = \beta_1 \Ket{\uparrow_B} + \beta_2 \Ket{\downarrow_B} = \begin{pmatrix} \beta_1 \\ \beta_2 \end{pmatrix}_B,
$$

```{index} product state
```
where $\chi_A$ and $\chi_B$ should be properly normalized. The *product state* describing the combined system in $S_{AB}$ can now be written as

```{math}
:label: productstate
\begin{align*}
\Ket{\text{product state}} &= \chi_A \otimes \chi_B = \left(\alpha_1 \Ket{\uparrow_A} + \alpha_2 \Ket{\downarrow_A} \right) \otimes \left(\beta_1 \Ket{\uparrow_B} + \beta_2 \Ket{\downarrow_B} \right) \\
&= \alpha_1 \beta_1 \Ket{\uparrow \uparrow} + \alpha_1 \beta_2 \Ket{\uparrow \downarrow} + \alpha_2 \beta_1 \Ket{\downarrow \uparrow} + \alpha_2 \beta_2 \Ket{\downarrow \downarrow}.
\end{align*}
```

As you can check easily, if $\chi_A$ and $\chi_B$ are normalized, the product state in equation&nbsp;{eq}`productstate` is normalized as well.

A key property of product states is that the two constituent parts remain independent of each other. If Alice decides to measure the $z$-component of the spin of her particle, she will force it to choose between up and down, and change the state of her particle accordingly, but Bob's particle will remain unaffected.

At this point, I wouldn't blame you for being a bit confused: in {numref}`sec:angularmomentumaddition` I argued that while the product states $\Ket{\uparrow \uparrow}$ and $\Ket{\downarrow \downarrow}$ are perfectly acceptable, the states $\Ket{\uparrow \downarrow}$ and $\Ket{\downarrow \uparrow}$ are not properly characterized by unique quantum numbers. However, in equation&nbsp;{eq}`productstate` they seem to have made a comeback, as part of the basis of $S_{AB}$. Of course, the combination of the three triplet and one singlet state also form a basis, just a different one, and while from a physics perspective we have a clear preference for that one, mathematically it doesn't matter which basis we work in. To see why, note that product spaces are more than the sum (or rather, the product) of their parts. *Any* linear combination of the four basis elements is an element of the product space, and so we can write a general element of $S_{AB}$ as

$$
\Ket{\bm{\gamma}} = \gamma_{11} \Ket{\uparrow \uparrow} + \gamma_{12} \Ket{\uparrow \downarrow} + \gamma_{21} \Ket{\downarrow \uparrow} + \gamma_{22} \Ket{\downarrow \downarrow}.
$$ (generalproductspacevector)

The state $\Ket{\bm{\gamma}}$ is thus characterized by four complex numbers $\gamma_{ij}$. Naturally, if it is to represent a quantum mechanical state, it should still be normalized, so we have:

$$
1 = \Braket{\bm{\gamma}|\bm{\gamma}} = \gamma_{11}^* \gamma_{11} + \gamma_{12}^* \gamma_{12} + \gamma_{21}^* \gamma_{21} + \gamma_{22}^* \gamma_{22}.
$$

It is easy to see that there must be more states in $S_{AB}$ than just product states. While the product states are characterized by four (in general complex) numbers of the form $\alpha_i \beta_j$, these numbers have two constraints, because both $\chi_A$ and $\chi_B$ need to be normalized. The four complex numbers $\gamma_{ij}$ only have one constraint though, given by the normalization of the state $\Ket{\bm{\gamma}}$. Therefore, there is 'room' for states that are not product states, and these indeed exist<sup>[^14]</sup>. 

```{index} mixed state, entanglement, spin ; singlet
```
A composite state that cannot be written as a product state is called a *mixed* state. Such mixed states are *entangled*: the underlying composite states cannot be separated anymore. Entanglement comes in degrees; we'll see how we can determine the amount of entanglement. The singlet $\Ket{00}$ and 'intermediate triplet' state $\Ket{10}$ are examples of maximally entangled states. Entanglement has an immediate consequence in physics. If Alice and Bob each hold a spin-$\frac12$ particle that together are in an entangled state, a measurement by Alice on her particle will not only affect that particle, but also Bob's! To see how this works, suppose the particles are in the singlet state, and Alice measures the $z$-component of the spin of her particle. She can get 'up' (really $+\hbar/2$) and 'down' (really $-\hbar/2$) with equal probability, but after the measurement, her particle will for certain be in that state. However, if Alice's spin is in the 'up' state, we *know* that Bob's *has* to be in the 'down' state, and vice versa. Moreover, it doesn't matter if the two spins are sitting side-by-side, or if Alice first took hers on a trip to the far side of the world; once she measures the spin of her particle, Bob's immediately assumes the opposite value. This 'spooky action at a distance' has spooked many physicists and philosophers, not in the least Albert Einstein, who would have none of it, instead claiming that there should be a 'hidden variable' by which the value of the spin of Alice's and Bob's particles should be set from the moment they got entangled. As we'll discuss later, Einstein's own thought experiment to prove the existence of such a hidden variable was later extended to prove that it in fact cannot exist, and the latter result has been experimentally verified. Quantum particles really get entangled, and the entanglement holds no matter how far apart the particles are.

(sec:densitymatrix)=
### The density matrix

```{index} pure state, mixed state
```
If we measure the energy, the magnitude and $z$-component of the orbital angular momentum, and the $z$-component of the spin of the electron in a hydrogen atom, we know all there is to know about that electron. We've found all the relevant quantum numbers describing its state, and we know that after all these measurements, it's in an eigenstate of all corresponding operators. Such a maximally-determined state is known as a *pure state* in quantum mechanics. In contrast, a state in which one or more of the quantum numbers are undetermined is known as a *mixed state*. 
#### Pure states

```{index} density matrix, density operator
```
Both pure and mixed states can be represented by a *density matrix*, though as you can also represent pure states in other forms (namely as eigenfunctions of the corresponding operators), the formalism of the density matrix is mostly useful for mixed states. However, to define it, we'll start from a pure state. Suppose we have a pure state $\Psi$, written in Dirac notation as $\ket{\Psi}$. We can then define the *density operator*&nbsp;$\hat{\rho}$ as the projection on $\Psi$ (c.f. the definition of the projection operator in equation&nbsp;{eq}`Projectionoperator`):

$$
\hat{\rho} = \ket{\Psi}\bra{\Psi}.
$$ (densityoperatorpurestate)

Note that the density operator is simply the projection on $\ket{\Psi}$. If the Hilbert space of our quantum system has a basis $\{\ket{e_j}\}$, we can define the matrix elements of the density operator (see equation&nbsp;{eq}`operatormatrixelements`) as

$$
\rho_{ij} = \Braket{e_i | \hat{\rho} | e_j} = \Braket{e_i | \Psi} \Braket{\Psi | e_j}.
$$ (densityoperatormatrixelementspurestate)

The density operator (denoted $\hat{\rho}$) and the corresponding density matrix (simply denoted $\rho$) of a pure state have three useful properties: they're Hermitian ($\rho^\dagger = \rho$), idempotent (like the projection operator), i.e. $\rho^2 = \rho$, and have unit trace:

$$
\mathrm{Tr}(\rho) = \sum_i \rho_{ii} = 1.
$$ (densitymatrixtrace)

From the density matrix, we can moreover calculate the expectation value of any observable&nbsp;$\hat{A}$. To do so, we use the fact that the identity operator can be written as the sum over the projections on all basis vectors (equation&nbsp;{eq}`Identityoperator`)), and write:
```{math}
:label: expectationvaluepurestate
\begin{align*}
\hat{A} &= \Braket{\Psi | \hat{A} | \Psi} = \Braket{\Psi | \hat{I} \hat{A} \hat{I} | \Psi} \\
&= \sum_{i,j} \Braket{\Psi|e_i} \Braket{e_i | \hat{A} | e_j} \Braket{e_j | \Psi} = \sum_{i,j} \Braket{e_j | \Psi} \Braket{\Psi|e_i} \Braket{e_i | \hat{A} | e_j}  \\
&= \sum_{i,j} \rho_{ij} A_{ji} = \mathrm{Tr}(\rho A).
\end{align*}
```
Finally, the time evolution of the density operator (i.e., the Schr&ouml;dinger equation in terms of $\hat{\rho}$, see {numref}`pb:densityoperatorproperties`) is given by

$$
i \hbar \frac{\mathrm{d}\hat{\rho}}{\mathrm{d}t} = \left[\hat{H}, \hat{\rho}\right].
$$ (densityoperatortimeevolution)

#### Mixed states
A mixed state cannot be written as a single function in the Hilbert space of our quantum system. Note that this property makes a mixed state different from a superposition state. A superposition state is written as a linear combination of other states, but itself is still a single function in the Hilbert space. A mixed state is not a single state, but a mixture of possible states. To represent it, we'll use the density operator and density matrix.

In a mixed state, there are multiple states $\ket{\Psi_k}$ that the system can be found in after a measurement, each with an associated probability $p_k$. If our system is in a mixed state, we can still calculate the expectation value of any observable via its associated operator, using the standard way of calculating the expectation value, by weighing each option by the associated probability. Therefore, for an operator&nbsp;$\hat{A}$, the expected outcome of a measurement if the system is in a mixed state is given by:

$$
\Braket{\hat{A}} = \sum_k p_k \Braket{\Psi_k | \hat{A} | \Psi_k}.
$$ (expectationvaluemixedstate)

Comparing equation&nbsp;{eq}`expectationvaluemixedstate` with its pure state equivalent&nbsp;{eq}`expectationvaluepurestate`, we see that we can again write $\hat{A} = \mathrm{Tr}(\rho A)$ if we generalize the definition of the density operator&nbsp;{eq}`densityoperatorpurestate` to

$$
\hat{\rho} = \sum_k p_k \ket{\Psi_k}\bra{\Psi_k}.
$$ (densityoperator)

For the elements of the corresponding density matrix we then find

$$
\rho_{ij} = \sum_k p_k \Braket{e_i | \Psi_k} \Braket{\Psi_k | e_j}.
$$ (densityoperatormatrixelements)

The density matrix of a mixed state, like that of a pure state, is Hermitian and has unit trace (equation&nbsp;{eq}`densitymatrixtrace`). It can be used to calculate the expectation value of an operator directly through the trace, like in equation&nbsp;{eq}`expectationvaluepurestate`, so $\Braket{\hat{A}} = \mathrm{Tr}(\rho A)$. Its time evolution is moreover still given by equation&nbsp;{eq}`densityoperatortimeevolution`. However, the density matrix of a mixed state is *not* idempotent: $\hat{\rho}^2 \neq \hat{\rho}$, which (especially in the matrix representation), gives us a quick way of determining whether a state is mixed or pure.

#### Example: Singlet state
The singlet combination state of two spin-$\frac12$ particles is a pure state in the Hilbert space of the joint particles. To be precise, it can be written as a linear combination of two of the states that together form a basis, namely $\ket{\uparrow \downarrow}$ and $\ket{\downarrow \uparrow}$. However, each of the individual particles is not in a pure state, as we do not know whether it's spin is up or down. We can describe both the pure state and the mixed state with a density matrix.

For the pure state, the full basis is $\left\lbrace \ket{\uparrow \uparrow}, \ket{\uparrow \downarrow}, \ket{\downarrow \uparrow}, \ket{\downarrow \downarrow}\right\rbrace$, and the state<sup>[^15]</sup> can be written in this basis as $\ket{\chi} = \frac{1}{\sqrt{2}} \left(\ket{\uparrow \downarrow} - \ket{\downarrow \uparrow}\right)$. We could calculate the components of the corresponding density matrix with equation&nbsp;{eq}`densityoperatormatrixelementspurestate`, but it's more efficient to use the &nbsp;{eq}`densityoperatorpurestate`, which gives

$$
\hat{\rho} = \Ket{\chi} \Bra{\chi} = \frac{1}{\sqrt{2}} \begin{pmatrix} 0 \\ 1 \\ -1 \\ 0 \end{pmatrix} \frac{1}{\sqrt{2}} \begin{pmatrix} 0 & 1 & -1 & 0 \end{pmatrix} = \frac12 \begin{pmatrix} 0 & 0 & 0 & 0 \\ 0 & 1 & -1 & 0  \\ 0 & -1 & 1 & 0 \\ 0 & 0 & 0 & 0 \end{pmatrix}.
$$ (singlettotaldensitymatrix)

It's an easy exercise to check that this density matrix is Hermitian, has unit trace, and is idempotent ($\hat{\rho}^2 = \hat{\rho}$), as it should be for a pure state.

For either of the spin&nbsp;$\frac12$ particles, the probability of being in the spin up and spin down states are $\frac12$. Therefore, taking (as usual) $\ket{\uparrow}$ and $\ket{\downarrow}$ as our basis, we get for their individual density matrices

$$
\hat{\rho} = \sum_k p_k \Ket{\chi_k} \Bra{\chi_k} = \frac12 \begin{pmatrix} 1 \\ 0 \end{pmatrix} \begin{pmatrix} 1 & 0 \end{pmatrix} + \frac12 \begin{pmatrix} 0 \\ 1 \end{pmatrix} \begin{pmatrix} 0 & 1 \end{pmatrix} = \frac12 \begin{pmatrix} 1 & 0 \\ 0 & 1\end{pmatrix}.
$$ (singletparticledensitymatrix)

These matrices are again Hermitian and have unit trace, but are not idempotent, as $\hat{\rho}^2 = \frac12 \hat{\rho}$.

(sec:EPR)=
### The EPR paradox and Bell's inequality

```{index} hidden variables, realist view, Copenhagen interpretation
```
From a philosophical point of view, people have objected to the concept of entanglement. The most famous opponent no doubt was Albert Einstein himself, who (supposedly) said that 'God does not throw dice': Einstein believed that the universe is ultimately deterministic, in the sense that, although the wavefunction of a particle can only tell us something about the probability of the outcome of a certain measurement, the outcome is already determined by the particle's initial conditions. According to Einstein, the fact that the wavefunction does not contain this information, does not mean that quantum mechanics is wrong (Einstein did not dispute the outcome of either the quantum mechanical calculations nor the associated experiments), but that it is incomplete: there should be a larger theory, of which quantum mechanics would somehow be a subset, which would allow us to calculate all properties of a particle from its initial conditions. As quantum mechanics clearly does not contain all necessary information about the particle to do so, there should be additional variables that contain the missing information. These variables have become known as *hidden variables*, and the philosophical point of view that the properties of a particle are determined by their initial condition the *realist* position. In contrast, the (now generally accepted) view that the outcome of the measurement of a physical property of a particle is determined only at the moment of measurement, corresponding to a collapse of the wavefunction, is known as the *Copenhagen interpretation*, after the city in which Niels Bohr lived and worked.

```{figure} images/portraits/BohrEinsteinsmall.jpg
:name: fig:BohrEinstein
Niels Bohr (left) and Albert Einstein photographed by fellow theoretical physicist Paul Ehrenfest in Ehrenfest's home in Leiden on 11 December 1925&nbsp;<sup>[^16]</sup>. Bohr and Einstein both were friends of Ehrenfest and frequently stayed at the house of Ehrenfest and his wife Tatyana Afanasyeva, discussing physics and playing music together. The house, desgined by Afanasyeva, still stands. Also, Ehrenfest and Afanasyeva organized colloquia for graduate students in their home, where guests like Bohr and Einstein gave talks about the latest developments in physics. Afterwards, the lecturers were invited to sign the wall (initially only if Ehrenfest thought the lecture was good). These colloquia later moved to a lecture hall in Leiden university, and [continue to be organized today](https://www.lorentz.leidenuniv.nl/history/colloquium/colloquium.html), having collected over the years a very impressive list of signatures on the wall that has been moved with the physics department from one location in Leiden to the next.
```

```{index} EPR paradox
```
Together with fellow physicists Podolsky and Rosen, Einstein published a paper in 1935 about a thought experiment that has become known as the *EPR paradox*. The experiment was designed to support their realist position. The argument is simple. Suppose a pair of particles is created in an entangled state, let's say the singlet spin state. Then, according to the Copenhagen interpretation, the spin of each of the particles is undetermined until one of them is measured. Because the joint state is known, a measurement on one particle will also fix the outcome of a measurement on the other particle: if the first measurement yielded spin up, the second must give spin down. On this point, the correlation between the two measurements, everybody agrees, as it is a prediction based on the theory of quantum mechanics, independent of the interpretation. However, Einstein, Podolsky and Rosen argued, if the Copenhagen interpretation is correct, information must travel from one particle to the other at the time of measurement. From (special) relativity we know that no information can travel faster than the speed of light. But there's nothing preventing us from first taking the particles very far apart, and then doing the measurements only after the separation, so close together in time that information, even at the speed of light, could not possibly travel from one location to the other. The measurements on each particle would give a seemingly random outcome, but when compared, they would always anti-correlate. Therefore, the Copenhagen interpretation violates the principle of *locality* (which states that no information can travel faster than light). Instead, the outcome of the measurement should have been determined at the point when the particles were created (or when they became entangled), and therefore one particle already had spin up, and the other spin down, from the outset. The measurement only serves to determine the value, but (again, according to Einstein, Podolsky and Rosen) does not influence the outcome. That quantum mechanics cannot tell us what the outcome will be is therefore a shortcoming of quantum mechanics, and an indication that a hidden variable does exist.

The argument behind the EPR paradox may seem compelling, but ironically, could ultimately be used to do the exact opposite, namely rule out that a hidden variable can exist (or, to be precise, prove that the notion of a hidden variable is incompatible with quantum mechanics). This proof is due to Bell, and now known as Bell's theorem. It proves, that if quantum mechanics is correct, nature is in fact not local; however, even though this means that 'internal' information about the state of the entangled quantum system can be shared instantaneously (with very useful applications in a secure quantum version of the internet), it does not allow us to violate locality in a communications sense: any scheme that pretends to send a message from Alice to Bob faster than the speed of light is doomed to fail. We'll discuss quantum communication in more detail below, but first focus on the underlying principle.

To make things concrete, we'll use a simple thought experiment in the spirit of the EPR paradox that is due to Bohm. A neutral pi meson (also known as a pion, a particle consisting of two quarks (see {numref}`ch:relativisticcquantummechanics`), for the neutral pi meson either up and anti-up or down and anti-down, making it highly unstable) can spontaneously decay into an electron-positron pair. As the pi meson has spin zero, and angular momentum is conserved, the combined electron-positron system also must have spin zero, and they must therefore be created in the singlet configuration. Working in a reference frame in which the pi meson was stationary, if the electron moves to the right, the positron moves with equal speed to the left. We identify two locations, A and B, where we measure the spin of the positron and electron, respectively. However, unlike in the original EPR thought experiment, we do not measure the spin in the same direction, we measure the component along some (independently set) directions $\bm{\hat{a}}$ and $\bm{\hat{b}}$. The outcome of each measurement would still be $\pm \hbar/2$ (as those are the only possible ones), but the probability of getting each outcome would depend on the direction of $\bm{\hat{a}}$ and $\bm{\hat{b}}$. Moreover, if we calculate the product of the two outcomes, we could get either $- \hbar^2/4$ or $+ \hbar^2/4$, or, if we measure the outcome in units of $\hbar/2$, the outcomes can be $\pm 1$, and so can the products. In particular, if we set $\bm{\hat{a}} \parallel \bm{\hat{b}}$, we'll retrieve the original experiment, and always get $-1$ from the product. Likewise, if we point $\bm{\hat{a}}$ and $\bm{\hat{b}}$ in opposite directions (so $\bm{\hat{a}} \parallel -\bm{\hat{b}}$), we always get $+1$. In general, we can define a function $P(\bm{\hat{a}}, \bm{\hat{b}})$ as the average of the product of the outcomes over many measurements. The special cases above are then $P(\bm{\hat{a}}, \bm{\hat{a}}) = -1$ and $P(\bm{\hat{a}}, -\bm{\hat{a}}) = 1$, and in general $-1 \leq P(\bm{\hat{a}}, \bm{\hat{b}}) \leq 1$.

With a bit more work, we can show that $P(\bm{\hat{a}}, \bm{\hat{b}}) = - \bm{\hat{a}} \cdot \bm{\hat{b}}$. We define $S_{\bm{\hat{a}}}^{(1)}$ to be the component of the spin of the positron along the direction of $\bm{\hat{a}}$, and likewise $S_{\bm{\hat{b}}}^{(2)}$ as the component of the electron along the direction of $\bm{\hat{b}}$. We now choose our coordinates such that $\bm{\hat{a}}$ lies along the positive $z$ axis (so $\bm{\hat{a}} = \bm{\hat{z}}$ and that $\bm{\hat{b}}$ lies in the $xz$ plane, so $\bm{\hat{b}} = \sin(\theta)\bm{\hat{x}} + \cos(\theta) \bm{\hat{z}}$, where $\theta$ is the angle between $\bm{\hat{a}}$ and $\bm{\hat{b}}$ (and therefore $\cos(\theta) = \bm{\hat{a}} \cdot \bm{\hat{b}}$). In these coordinates, we can calculate the expectation value of product of the measurements of the $z$ components of the two spins as
```{math}
\begin{align*}
\Braket{\hat{S}_z} &= \Braket{00 | S_{\bm{\hat{a}}}^{(1)} S_{\bm{\hat{b}}}^{(2)} | 00} = \frac{1}{\sqrt{2}} \Bra{00} \left[ \left( \hat{S}_z \Ket{\uparrow}\right) \left(\cos(\theta) \hat{S}_z \Ket{\downarrow} + \sin(\theta) \hat{S}_z \Ket{\downarrow} \right) - \left( \hat{S}_z \Ket{\downarrow}\right) \left(\cos(\theta) \hat{S}_z \Ket{\uparrow} + \sin(\theta) \hat{S}_z \Ket{\uparrow} \right) \right]  \\
&= \frac{1}{\sqrt{2}} \frac{\hbar^2}{4} \Bra{00} \left[ \left( \Ket{\uparrow}\right) \left(-\cos(\theta) \Ket{\downarrow} - \sin(\theta) \Ket{\downarrow} \right) - \left( -\Ket{\downarrow}\right) \left(\cos(\theta) \Ket{\uparrow} + \sin(\theta) \Ket{\uparrow} \right) \right] \\
&= \frac{\hbar^2}{4} \Bra{00} \left[ \cos(\theta) \frac{1}{\sqrt{2}} \left( - \Ket{\uparrow\downarrow} + \Ket{\downarrow\uparrow}\right) + \sin(\theta) \frac{1}{\sqrt{2}} \left( - \Ket{\uparrow\uparrow} + \Ket{\downarrow\downarrow}\right) \right] \\
&= \frac{\hbar^2}{4} \left[ - \cos(\theta) \Braket{00|00} + \sin(\theta) \frac{1}{\sqrt{2}} \left( \Braket{00|11} + \Braket{00 | 1-1} \right)  \right] = - \frac{\hbar^2}{4} \cos(\theta) = - \frac{\hbar^2}{4} \bm{\hat{a}} \cdot \bm{\hat{b}},
\end{align*}
```
which proves the claim that $P(\bm{\hat{a}}, \bm{\hat{b}}) = - \bm{\hat{a}} \cdot \bm{\hat{b}}$.

(sec:twoparticlesystems)=
## General two-particle systems

As we've seen in {numref}`sec:angularmomentumaddition`, if you combine the spins of two spin-$\frac12$ particles<sup>[^17]</sup> you will get combinations that are either symmetric or antisymmetric, meaning that if you swap the two particles, you will either get the same state, or minus the state. A similar effect applies to the positional part of the state of the particles. For a single particle, the behavior (over time) is described by a wave function $\Psi(\bm{r}, t)$, which evolves according to the Schr&ouml;dinger equation&nbsp;{eq}`SE3D`. According to our statistical interpretation ({prf:ref}`axiom:qmstatisticalinterpretation`), the probability of finding the particle in some volume&nbsp;$\mathrm{d}^3\bm{r}$ around a point $\bm{r}$ is given by $|\Psi(\bm{r}, t)|^2 \, \mathrm{d}^3\bm{r}$. We can easily generalize this notion to a system of two particles: the wavefunction then has two spatial coordinates, defining the positions of the two particles, so we get $\Psi(\bm{r}_1, \bm{r}_2, t)$, which we interpret to mean that the probability of finding particle&nbsp;1 in a volume $\mathrm{d}^3\bm{r}_1$ around $\bm{r}_1$ *and* particle&nbsp;2 in a volume $\mathrm{d}^3\bm{r}_2$ around $\bm{r}_2$ is given by $|\Psi(\bm{r}_1, \bm{r}_2, t)|^2 \, \mathrm{d}^3\bm{r}_1 \, \mathrm{d}^3 \bm{r}_2$. The Hamiltonian of the system with two particles will have two kinetic energy terms (one for each particle), and a potential energy which will depend on the positions of the two particles (and, in general, could depend on time):

$$
\hat{H} = - \frac{\hbar^2}{2m_1} \nabla_1^2 - \frac{\hbar^2}{2m_2} \nabla_2^2 + V(\bm{r}_1, \bm{r}_2, t),
$$

where the subscript on each Laplace operator $\nabla^2$ indicates which of the two particles it acts on (leaving the other one alone). As long as the potential is time-independent, we can separate the temporal and spatial variables, yielding the same time dependence of $\Psi$ as before, allowing us to write

$$
\Psi(\bm{r}_1, \bm{r}_2, t) = \psi(\bm{r}_1, \bm{r}_2) \exp(-i E t/\hbar),
$$

and leaving us with the time-independent Schr&ouml;dinger equation to solve.

Some two-particle solutions are direct generalizations of single-particle ones. If we have two different particles (labeled $1$ and $2$) in different states (labeled $a$ and $b$), the combined wave function is simply a product of the single-particle ones:

$$
\psi(\bm{r}_1, \bm{r}_2) = \psi_a(\bm{r}_1) \psi_b(\bm{r}_2).
$$ (wavefunctionproductstate)

We've already seen an example of what can happen if different particles can potentially be in the same state: they can get entangled. For example, the proton and the electron in a hydrogen atom both have spin-$\frac12$, and their spin states could get entangled in a spin singlet with total spin zero. However, we will always be able to tell which particle is which; in the example, we could measure either the mass or the charge of one of them to be sure.

Things get really interesting when we consider *identical* particles. Quantum-mechanical particles can be identical in the way classical ones can never be. Even if you could produce two perfectly spherical billiard balls with the same mass, radius, moment of inertia, surface roughness, and so on, you could always paint one white and one red, and thus tell them apart. In quantum mechanics, two electrons are truly identical; there's no way of telling which is which. Therefore, no quantum observable should be affected if we swap them; in more mathematical terms, we can say that the quantum observables should be invariant under such swapping operations. The wavefunction however is not an observable. As we've just re-asserted above, it's square is though, and so we must have that for any pair of identical particles, the wave function satisfies

$$
|\psi(\bm{r}_1, \bm{r}_2)|^2 = |\psi(\bm{r}_2, \bm{r}_1)|^2.
$$ (quantumidenticalparticleswavefunction)

We can take a square root of both sides of equation&nbsp;{eq}`quantumidenticalparticleswavefunction`, which gives

$$
\psi(\bm{r}_1, \bm{r}_2) = \alpha \psi(\bm{r}_2, \bm{r}_1),
$$

```{index} bosons, fermions
```
where $|\alpha|^2 = 1$. Mathematically, $\alpha$ can be any complex number with norm&nbsp;$1$. As it turns out, in three dimensions, physics allows for only two options: $\alpha = \pm 1$. Particles with $\alpha = 1$ are known as *bosons*, while particles with $\alpha = -1$ are called *fermions*. Bosons and fermions can be distinguished by their spin: bosons always have integer spin, while fermions have half-integer spin (this result is known as the spin-statistics theorem, from which the limitation of $\alpha$ to $\pm 1$ also follows; the proof requires relativistic quantum mechanics). In two dimensions, other values for $\alpha$ are also possible; in general they are of the form $\alpha = e^{i \theta}$ with $\theta \in [0, 2\pi]$. Such (quasi-)particles are known as *anions*. They occur in the [quantum Hall effect](https://en.wikipedia.org/wiki/Quantum_Hall_effect), in which conductance is quantified for two-dimensional electron systems in a strong magnetic field and at low temperature.

For identical particles, we cannot write the combined wave function as a simple product state like we did in equation&nbsp;{eq}`wavefunctionproductstate`, but we can write it in general as a linear combination of product states, which obeys the symmetrization requirement:

$$
\psi_\pm(\bm{r}_1, \bm{r}_2) = \frac{1}{\sqrt{2}} \left[\psi_a(\bm{r}_1) \psi_b(\bm{r}_2) \pm \psi_a(\bm{r}_2) \psi_b(\bm{r}_1) \right],
$$ (wavefunctionfermionsbosons)

where we take the $+$ sign for bosons and the $-$ sign for fermions. The prefactor $1/\sqrt{2}$ is to ensure that the state $\psi_\pm(\bm{r}_1, \bm{r}_2)$ is normalized if $\psi_a$ and $\psi_b$ both are.

For bosons, there is no reason why we could not take $a=b$ in equation&nbsp;{eq}`wavefunctionfermionsbosons`. Multiple bosons can be in the same state, and we need not stop at two. A collection of a large number of identical bosons all in the same ground state is a distinct state of matter, known as a [Bose-Einstein condensate](https://en.wikipedia.org/wiki/Bose–Einstein_condensate). The possible existence of this state was predicted by Bose and Einstein in 1924, but it wasn't until 1995 before it was first realized in the lab, using ultracold helium nuclei, earning the experimenters the 2001 Nobel prize.

```{index} Pauli exclusion principle
```
For fermions, the situation is quite different. If two identical fermions (for example two electrons) would be in the same state, equation&nbsp;{eq}`wavefunctionfermionsbosons` tells us that this state would have a wavefunction which is identically zero. Such a state cannot exist (the probability of finding the particles cannot be zero everywhere). Therefore, two identical fermions *cannot* occupy the same state. This purely quantum-mechanical result is known as the *Pauli exclusion principle*. As we will see below, without this effect, chemistry (and therefore life) would not exist.

### The helium atom
A direct application of the the theory of multiple particles is the electrons in any atom that is not hydrogen: they all contain multiple electrons, which are fermions, so the wavefunction that describes their (collective) state should be antisymmetric under swapping any pair of them. The easiest case is helium, with two electrons orbiting a nucleus that contains two protons and (usually) two neutrons. As for hydrogen, the nucleus is much heavier than the electrons, so we'll work in the approximation that it is fixed. We can then write down the Hamiltonian for the helium atom (i.e., for the wavefunction of the two electrons):
```{math}
:label: HeliumHamiltonian
\begin{align*}
\hat{H}_{\mathrm{He}} &= - \frac{\hbar^2}{2m_{\mathrm{e}}} \nabla_1^2 - \frac{\hbar^2}{2m_{\mathrm{e}}} \nabla_2^2 - \frac{e^2}{4 \pi \varepsilon_0} \left( \frac{2}{r_1} + \frac{2}{r_2} - \frac{1}{|\bm{r}_1-\bm{r}_2|} \right)  \\
&= \hat{H}_{\mathrm{H}_1} + \hat{H}_{\mathrm{H}_2} + \hat{H}_{\mathrm{e}-\mathrm{e}},
\end{align*}
```
where $\bm{r}_1$ and $\bm{r}_2$ are the positions of the two electrons (with the nucleus at the origin), and $r_1$ and $r_2$ their respective norms. As shown in the second line, we can write the helium Hamiltonian as the sum of two hydrogen Hamiltonians (one for each electron), plus an extra term representing the electron-electron repulsion. The hydrogen Hamiltonians are slightly different from regular hydrogen, because the nucleus has twice the charge, and therefore the ground state energy (in which we get the square of this charge), becomes four times that of hydrogen itself.

No exact solutions are known of the Schr&ouml;dinger equation with the helium Hamiltonian. We can however make approximations, some of which will be quite good, as we'll see in {numref}`ch:varprinciple`. For now, we'll start with the crudest possible approximation: ignoring the electron-electron repulsion altogether. In that case, the wave function separates, as $\hat{H}_{\mathrm{H}_1}$ doesn't affect particle 2, and vice versa. Therefore, the eigenfunctions $\psi(\bm{r}_1, \bm{r}_2)$ of the helium Hamiltonian without the electron-electron term can be written as the product of hydrogen eigenfunctions $\psi_{nlm}(\bm{r})$:
```{math}
:label: heliumeigenfunctionroughapproximation
\begin{align*}
\left(\hat{H}_{\mathrm{H}_1} + \hat{H}_{\mathrm{H}_2}\right) \psi(\bm{r}_1, \bm{r}_2) &= \left(\hat{H}_{\mathrm{H}_1} + \hat{H}_{\mathrm{H}_2}\right) \psi_{n_1 l_1 m_1}(\bm{r}_1) \psi_{n_2 l_2 m_2}(\bm{r}_2) \\
&= \psi_{n_2 l_2 m_2}(\bm{r}_2) \hat{H}_{\mathrm{H}_1}\psi_{n_1 l_1 m_1}(\bm{r}_1) + \psi_{n_1 l_1 m_1}(\bm{r}_1) \hat{H}_{\mathrm{H}_2} \psi_{n_2 l_2 m_2}(\bm{r}_2) \\
&= 4 E_1 \left(\frac{1}{n_1^2} + \frac{1}{n_2^2} \right),
\end{align*}
```
where $E_1$ is the ground state energy of hydrogen, and the factor $4$ due to the double charge in the helium nucleus. For the ground state energy ($n_1 = n_2 = 1$), we get $-109\;\mathrm{eV}$, which is pretty far from the actual value of $-79\;\mathrm{eV}$. The difference of course is not surprising: we ignored electron-electron repulsion, so we expect our estimate to be off significantly.

At this point, you should be protesting: we just asserted above that, because electrons are fermions, they cannot exist in the same state, so surely there should be limitations to equation&nbsp;{eq}`heliumeigenfunctionroughapproximation`. If two electrons are in the same state, they have the same quantum numbers. Orbital wavefunctions are defined by the three quantum numbers $n$, $l$, and $m_l$, so as long as one of them is different between the two, we're good. However, for the ground state, we have $n=1$, $l=0$, $m_l=0$, so we would be in trouble, except that electrons have a fourth quantum number<sup>[^18]</sup>, the spin state $m_s$. Therefore, the two electrons of the hydrogen atom could both be in a hydrogen-like orbital ground state with $n=1$, $l=0$ and $m_l = 0$, as long as their spins are different. As we've seen in {numref}`sec:angularmomentumaddition`, we can't take one spin up and the other down, but instead will have to combine them into a singlet or a triplet state. Now if the orbital part of the wavefunction (the part described by $n$, $l$, and $m_l$) is symmetric, the spin part has to be antisymmetric, and so only the singlet will do. Therefore, our approximation of the ground state of helium is the wavefunction $\psi_{100}(\bm{r}_1) \psi_{100}(\bm{r}_2) \sigma_{-}(1,2)$, with the $\psi$'s indicating the orbital part of the wavefunction, and the $\sigma_{-}$ the singlet spin state.

Of course, we need not put both electrons in the $\psi_{100}$ state. We could put one of them in an excited state (putting both in an excited state would require more energy than ionizing one of the electrons). Because the orbital states are then different, we can use them to make an antisymmetric combination, allowing for a symmetric spin state, which would be the triplet, as we still need to satisfy the rules for adding angular momenta. We could also make a symmetric combination of the ground state and excited state orbital, made antisymmetric by putting the spins in the singlet. Therefore, there are two qualitatively different ways of getting excited states in the helium atom:

```{math}
:label: heliumexcitedstates
\begin{align*}
\psi_+(\bm{r}_1, \bm{r}_2) &= \frac{1}{\sqrt{2}} \left[\psi_{100}(\bm{r}_1) \psi_{nlm}(\bm{r}_2) + \psi_{nlm}(\bm{r}_1) \psi_{100}(\bm{r}_2) \right] \sigma_{-}(1, 2) \\
\psi_-(\bm{r}_1, \bm{r}_2) &= \frac{1}{\sqrt{2}} \left[\psi_{100}(\bm{r}_1) \psi_{nlm}(\bm{r}_2) - \psi_{nlm}(\bm{r}_1) \psi_{100}(\bm{r}_2) \right] \sigma_{+}(1, 2)
\end{align*}
```

```{index} parahelium, orthohelium
```
The symmetric orbital (and antisymmetric spin) combination $\psi_+$ is known as *parahelium*, and the antisymmetric orbital (symmetric spin) combination $\psi_-$ as *orthohelium*. As we'll see in {numref}`sec:covalentbonds`, a symmetric combination of orbitals will on average put identical particles closer together, and an antisymmetric combination will drive them apart, we can predict that for parahelium the interaction term in the Hamiltonian will be higher than for orthohelium, which is indeed the experimentally obtained result.

(sec:periodictable)=
## The periodic table of elements

```{index} hydrogenic orbitals
```
In the previous section, we approximated the combined wavefunction of the two electrons in a helium atom using the solutions of the hydrogen Hamiltonian. We call such hydrogen-orbital based approximate solutions *hydrogenic orbitals*. We need to complement them with spin states to give the full wavefunction, with the proper antisymmetry property for the fermionic electrons.

When moving from helium to lithium, the third element with three electrons, we cannot construct an approximate ground state from hydrogenic orbitals in the same way we did for helium. There are only two possible spin states for our spin-$\frac12$ electrons, so adding a third electron with orbital $\psi_{100}$ is not possible. The two electrons in the helium atom therefore together occupy all possible states with $n=1$. To add a third electron, we'll need to put one in an $n=2$ state. As we'll see in {numref}`ch:perturbationtheory`, higher-order terms make the energy of hydrogen orbitals dependent on not just the principal quantum number&nbsp;$n$ but also the orbital quantum number&nbsp;$l$; the state $\psi_{200}$ then has lower energy than $\psi_{210}$ (or either of the others with $l=1$, as the energy won't depend on $m_l$ unless we apply a magnetic field). Therefore, our best guess for the ground state of lithium is the ground state of helium together with a third electron in a $\psi_{200}$ orbital. Naturally, the combined wavefunction will still need to be antisymmetric under exchange of any pair of electrons, but as the orbital state of the added electron is different from the other two, doing so is straightforward. Extending to the fourth element, beryllium, is also not hard: we put the fourth electron in a singlet combination with the third, both in $\psi_{200}$ orbitals.

By putting two electrons in a $\psi_{100}$ orbital and two in a $\psi_{200}$ orbital, we've exhausted the $l=0$ options for $n=1$ and $n=2$. The next electron will therefore need to be put in an orbital with $l=1$. There are three such orbitals, as we now have three choices for $m_l$. Because we now have $l=1$, the orbitals have nonzero orbital angular momentum, and the rules for adding angular momenta (of the orbitals and the spin) apply. Therefore, the wavefunction of the fifth electron in boron, the fifth and sixth in carbon, and so on till the tenth electron in neon, will be linear combinations of the possible wavefunctions with $l=1$ and $s=\frac12$.

Once we have exhausted all hydrogenic orbitals with $n=2$, we move on to $n=3$ for natrium, magnesium, and on till argon with orbitals with $l=0$ and $l=1$. However, the nineteenth electron of potassium will not go into a $n=3, l=2$ orbit, but instead into a $n=4, l=0$ orbit, as that one turns out to have a lower energy. Only once those two slots are filled, the twenty-first electron of scandium uses an $n=3, l=2$ orbitals. A similar thing happens with the $n=4, l=3$ orbitals, which have a higher energy than not only the $n=5, l=0$ but also the $n=6, l=0$ orbitals.

Arranging the elements by the highest-energy orbitals their electrons occupy in the ground state, the periodic table emerges. The rows (or 'period') correspond to the primary quantum number (at least for the $l=0$ and $l=1$ orbitals), whereas the 'groups' are set by the orbital quantum number, see {numref}`fig:periodictable`. The block with $l=2$ is shifted down one row, and the block with $l=3$ two rows (these are the lanthanides and actinides, which are usually shown below the table to not make it overly wide). Elements with $n=8$ have already been found (or rather made in the lab); to date, no elements are known for which the ground state configuration contains an orbital with $l=4$, but there is no fundamental reason why they could not exist.

```{index} valence electrons, covalent bond
```
As you probably know, chemists refer to the rows in the periodic table as 'shells', labeling them with either the principal quantum number, or a corresponding capital letter (the 'X-ray notation'), where the $n=1$ shell has a K, $n=2$ an L, $n=3$ M, and so on. Shells are divided in subshells, corresponding to the orbital quantum number $l$. Electrons in the outermost (i.e., highest $n$) shell are called *valence electrons*; unsurprisingly, as their energy is highest, they are the ones that have the weakest binding with the nucleus. Some atoms (predominantly non-metals like carbon, nitrogen and oxygen) can make covalent bonds with other non-metals. In such a covalent bond, valence electrons from the two atoms are entangled into a common orbital, in such a way that each atom effectively has just enough electrons to 'fill its shell' to capacity. Therefore, even though oxygen has six electrons in $n=2$ orbitals, it only can form two 'covalent links' to other atoms, while carbon, with four valence electrons, can form four covalent bonds. Helium, neon, and the other elements in the rightmost column of the table already have a full highest shell, and are therefore chemically inert; they are the noble gases. Going down to less filled shells, we run out of valence electrons to completely fill up a shell, and atoms instead arrange in a periodic lattice, sharing electrons between many atoms; these materials therefore form electrically conducting metals. We will discuss the quantum mechanics behind both covalent bonds and metals in much more detail in {numref}`ch:varprinciple`.

Next to naming the shells, chemists have also come up with a way to describe the ground-state configuration of the electrons in any atom. This configuration contains two pieces of information. The first we've already seen: which hydrogenic orbitals the electrons occupy. These orbitals are characterized by a combination of the principal and orbital quantum numbers, $n$ and $l$. For $n$, the notation simply gives the number, but the $l$ orbitals historically have been labeled with letters - and not in a alphabetical order. We have $s$ for $l=0$, $p$ for $l=1$, $d$ for $l=2$ and $f$ for $l=3$. The combination $nl$ is written in parentheses, with a superscript of how many electrons it applies to; therefore, for oxygen, we have $(1s)^2 (2s)^2 (2p)^4$, and sulphur gets $(1s)^2 (2s)^2 (2p)^6 (3s)^2 (3p)^4$. 'Reading off' this configuration from the periodic table is straightforward. The other piece of information however is harder to obtain: the *total* orbital, spin, and combined angular momentum of an atom. Writing $L$ for the total orbital angular momentum, $S$ for the total spin, and $J$ for the total angular momentum (which could be anything between $L+S$ and $|L-S|$), the combination is written in a somewhat cryptic way as

$$
{}^{2S+1}L_J.
$$ (atomangularmomentum)

For $L$, letters again replace numbers, now using capitals (the same letters though), and the superscript number doesn't give you the total spin directly, but the *multiplicity*, $2S+1$. To illustrate, the ground state of oxygen has $S=1, L=1, J=2$, so the corresponding symbol is ${}^3P_2$, while the ${}^2S_{1/2}$ of hydrogen tells you that its ground state (unsurprisingly) has $S=1/2, L=0, J=1/2$. Elements in the same group (i.e., column) in the periodic table will have the same angular momentum ground state, with the noble gases on the right having the simplest one (${}^1S_0$), while iron in group&nbsp;8 gets something as complicated as ${}^5D_4$.

```{figure} images/angularmomentum/periodictable_withblocks.svg
:name: fig:periodictable
The periodic table, with principal and orbital quantum numbers of the valence electrons. The principal quantum number $n$ corresponds to the row number; the orbital quantum number $l$ depends on the group. Due to electron-electron repulsion and higher-order effects, the $n=4, l=0$ orbital has lower energy than the $n=3, l=2$ one, resulting the $l=2$ orbitals to be 'bumped down' one row. A similar thing happens to the lanthanides and actinides with $l=3$ orbitals. Elements with valence electrons with $n=8, l=0$ have been made in the lab (elements 119 and 120), and no doubt others will follow in the future. Classification of the elements based on the classification by the American Chemical Society&nbsp;{cite}`ACSwebsite`, figure adapted from&nbsp;<sup>[^19]</sup>.
```

```{index} Hund's rules
```
To figure out what the ground state configuration of an atom is, would in principle require detailed calculations of the energies of all possibilities, from which you can then pick the lowest one. Fortunately, some common principles can be extracted, which allow us to skip all the math and write down the ground state configuration directly. These principles are known as *Hund's rules*, and at least the first two can be understood qualitatively quite easily:
1. If the electrons have the choice of more than one orbital with equal energy, they will sit in different orbitals, in a configuration that maximizes their multiplicity (and therefore the total spin).
1. For a given value of the spin, the state with the largest possible value of the total orbital angular momentum (satisfying the total antisymmetry condition) will have lowest energy.
1. Given $S$ and $L$, if the outermost shell is less than or equal to half filled, the term with lowest possible $J$ will have lowest energy, whereas if the outermost shell is more than half filled, the term with the highest possible $J$ will have the lowest energy

Naturally, the valence electrons in any atom can also occupy excited states, and transition between states by the adsorption and emission of photons. Therefore, each atom has its own emission spectrum, similar to the one of hydrogen in {numref}`fig:hydrogenemissionspectrum`, which acts as an atomic fingerprint. As we'll see in {numref}`ch:varprinciple`, the same holds true for molecules.

```{index} Slater-type orbitals
```
In contrast to the preceding sections, here we've taken a quite rough approximation for the eigenfunctions of the electrons in various atoms. You might wonder how good this approximation is, and if we can do any better. The unsurprising answer to the first question is 'quite bad', but the fortunate answer to the second question is 'yes'. Unfortunately, we're already out of our depth with regards to analytical methods<sup>[^20]</sup>, but there are many approximation methods that can get us much closer to the actual states. One result of those approximation methods is a correction to the hydrogenic orbitals we've used for our atoms, in particular for the ground state. These more accurate orbitals are known as *Slater-type orbitals*, or STOs, given by

$$
\psi_{nlm}(r, \theta, \phi) = A r^{n_\mathrm{eff}-1} \exp\left(-\frac{Z_\mathrm{eff} r}{n_\mathrm{eff} a} \right) Y_l^m(\theta, \phi),
$$ (Slaterorbitals)

where the $Y_l^m(\theta, \phi)$ are (still) the spherical harmonics, $a$ is the Bohr radius, and $A$ a normalization constant. The principal quantum number&nbsp;$n$ gets replaced by an effective value $n_\mathrm{eff}$, which is equal to $n$ for $n=1, 2, 3$, but reduced for higher values, with $n_\mathrm{eff} = 3.7$ for $n=4$, $n_\mathrm{eff} = 4.0$ for $n=5$, and $n_\mathrm{eff} = 4.2$ for $n=6$. Likewise, the effective charge of the nucleus is reduced due to screening; they differ not just per atom but also per atomic orbital (with lower orbitals having higher values as there are fewer screening electrons). STOs are sometimes used in quantum chemistry software. They have their shortcomings, but are a first step to a more accurate approximation of the actual atomic wave functions.

```{figure} images/angularmomentum/xkcd_2913_periodic_table_regions.png
:name: fig:periodictablexkcd
The periodic table with division into regions according to how and where one might encounter the elements. Cartoon by Randall Munroe, CC-BY-NC 2.5. Source: [xkcd.com/2913/](https://xkcd.com/2913/) <sup>[^21]</sup>.
```

## Problems
````{exercise} Eigenvalues of the orbital angular momentum operator
:label: pb:orbitalangularmomentumeigenvalues
:class: dropdown
From the algebraic calculations in {numref}`sec:angularmomentumoperators`, we got both integer and half-integer values for the quantum number&nbsp;$l$. From the solution of the eigenstates of the hydrogen atom, we know however that for orbital angular momentum, we only get the integer values. Thus, in contrast to spin, where, with the same commutation relations, the half-integer values do occur, there must be an extra piece of physics in the orbital angular momentum operator that excludes the half-integer options. That in itself is perhaps not surprising: while we define the spin operators through the commutation relations, for the orbital angular momentum operators we derived the commutation relations from the 'quantization' of the classical definition $\bm{L} = \bm{r} \times \bm{p}$ of the angular momentum. In this problem, we'll 'disentangle' the cross product, ending up with two mathematical equivalents of harmonic oscillators, from which we can prove that we can only get integer values of $l$.
To start, we define the operators
```{math}
\hat{q}_\pm \equiv \frac{1}{\sqrt{2}} \left( \hat{x} \pm \alpha \hat{p}_y \right); \qquad \hat{p}_\pm \equiv \frac{1}{\sqrt{2}} \left( \hat{p}_x \pm \frac{1}{\alpha} \hat{y} \right),
```
where $\alpha = a^2/\hbar$, with $a$ the Bohr radius.
1. Verify that $[\hat{q}_{+}, \hat{q}_{-}] = [\hat{p}_{+}, \hat{p}_{-}] = 0$ and $[\hat{q}_{+}, \hat{p}_{-}] = [\hat{q}_{-}, \hat{p}_{+}] = i \hbar$.
1. The result of (a) shows that the $\hat{q}$'s and $\hat{p}$'s have the same commutation relations as the position and momentum operators. How would you pair the $\hat{q}$'s and $\hat{p}$'s such that they correspond to 'orthogonal directions'?
1. Show that
	```{math}
	\hat{L}_z = \frac{1}{2 \alpha} \left( \hat{q}_{+}^2 - \hat{q}_{-}^2\right) - \frac{\alpha}{2} \left( \hat{p}_{+}^2 - \hat{p}_{-}^2\right).
	```
1. Now show that we can write $\hat{L}_z$ as the difference between two Hamiltonians describing one-dimensional harmonic oscillators with frequency&nbsp;$1$ and mass $m = 1/\alpha$. Hint: Make use of the sets of $\hat{q}$ and $\hat{p}$ operators you grouped in (b).
1. Combine what you know about the eigenvalues of the harmonic oscillator with the result of (d) to prove that the eigenvalues of $\hat{L}_z$ must be integer multiples of $\hbar$.
````

````{exercise} Solving the hydrogen Hamiltonian with raising and lowering operators
:label: pb:hydrogenspectrumalgebraic
:class: dropdown
1. Derive equation&nbsp;{eq}`hydrogenradialHamiltonianladderoperators` for $\hat{H}_l$ through explicit calculation of $\hat{A}_l^\dagger \hat{A}_l$.
1. Show that the commutator of the ladder operator and its Hermitian conjugate is given by
	```{math}
	:label: hydrogenradialHamiltonianladderoperatorscommutator
	\left[ \hat{A}_l, \hat{A}_l^\dagger \right] = \frac{a_0^2 m_\mathrm{e}}{\hbar^2} \left( \hat{H}_{l+1} - \hat{H}_l \right).
	```
1. Find the commutator $\left[ \hat{A}_l, \hat{H}_l \right]$, and use it to prove that $\hat{A}_l \hat{H}_l = \hat{H}_{l+1} \hat{A}_l$. *Hint*: you will only need to combine your results of (a) and (b) here.
1. Prove that if $u_l(r)$ satisfies equation&nbsp;{eq}`Hamiltonianeigenvalueeq` with eigenvalue $E_l$, then $\hat{A}_l u_l$ is an eigenfunction of $\hat{H}_{l+1}$ with eigenvalue&nbsp;$E_l$.
1. Show that the last equality in equation&nbsp;{eq}`hydrogenladdertopselfinnerproduct` holds. You may assume that $u_{l_\mathrm{max}}$ is properly normalized.
1. Argue why the values of $l$ in $u_{nl}$ are restricted to the familiar range $l = 0, 1, \ldots, n-1$.
1. By solving the first-order differential equation&nbsp;{eq}`hydrogenladdertop` for $n=1, l=0$, find an expression for $u_{10}(r)$. You may leave the normalization constant in the expression.
1. By solving the first-order differential equation&nbsp;{eq}`hydrogenladdertop` for $l=n-1$ and arbitrary $n$, explicitly find the expression for $u_{n,n-1}(r)$. Again, you may leave the normalization constant in the expression.
````

````{exercise} Orbital angular momentum and kinetic energy
:label: pb:orbitalangularmomentum
:class: dropdown
The operators representing the square and the $z$-component of the angular momentum can be expressed in spherical coordinates as
```{math}
\begin{align*}
\hat{L}^2 &= - \hbar^2 \frac{1}{\sin\theta} \frac{\partial }{\partial \theta} \left(\sin\theta \frac{\partial }{\partial \theta}\right) - \frac{1}{\sin^2\theta} \frac{\partial^2}{\partial \phi^2}, \\
\hat{L}_z &= -i \hbar \frac{\partial }{\partial \phi}
\end{align*}
```
As we derived in class, these operators commute; their joint eigenfunctions are the spherical harmonics $Y_l^m(\theta, \phi)$, with eigenvalues $\hbar^2 l (l+1)$ for $\hat{L}^2$ and $\hbar m$ for $\hat{L}_z$ (note that the $m$ here is the quantum number $m_l$, not the mass). The fact that the spherical harmonics are the eigenfunctions is not surprising if you compare the expression for $\hat{L}^2$ with the Laplacian in spherical coordinates; you'll find that the angular part of the Laplacian is exactly $-\hat{L}^2 / (\hbar^2 r^2)$. This observation allows us to split the kinetic energy into a radial and an angular part. As the kinetic energy is proportional to the square of the momentum, we need an expression for the radial momentum. Defining<sup>[^22]</sup> 'operator vectors' $\hat{\bm{r}} = (\hat{x}, \hat{y}, \hat{z})$ and $\hat{\bm{p}} = (\hat{p}_x, \hat{p}_y, \hat{p}_z)$, we might define the radial component of the momentum by the projection of $\hat{\bm{p}}$ on the unit vector in the radial direction ($\hat{\bm{r}}/r$, where as usual $r = \sqrt{x^2 + y^2 + z^2}$). Unfortunately, this won't do, as the resulting operator is not Hermitian.
1. Show that $\hat{\bm{r}} \cdot \hat{\bm{p}}$ is not Hermitian.
1. Show that $\hat{p}_r = \frac{1}{2r} \left(\hat{\bm{r}} \cdot \hat{\bm{p}} + \hat{\bm{p}} \cdot \hat{\bm{r}} \right)$ is Hermitian.
	Substituting the recipes for $\hat{\bm{r}}$ and $\hat{\bm{p}}$, we find the recipe for $\hat{p}_r$:
	```{math}
	p_r = - \frac{i\hbar}{2} \left[ \frac{1}{r} \bm{r} \cdot \bm{\nabla} + \bm{\nabla} \cdot \left( \frac{\bm{r}}{r} \right) \right].
	```
	Note that here $\bm{r}$ is just the position vector. We can write $\bm{\nabla}$ in both Cartesian and spherical coordinates, from which we find that $\bm{\nabla} \cdot \bm{r} = 3$ and
	```{math}
	\bm{r} \cdot \bm{\nabla} = x \frac{\partial }{\partial x} + y \frac{\partial }{\partial y} + z \frac{\partial }{\partial z} = r \frac{\partial }{\partial r}.
	```
1. Use the above expressions to find an expression for $\hat{p}_r$ in terms of $r$ and derivatives to $r$. *NB: remember that an operator always acts on a function!*
1. Show that the commutator of $\hat{r}$ (recipe: multiply with $r$) and $\hat{p}_r$ is the same as the one-dimensional version, i.e., $\left[ \hat{r}, \hat{p}_r \right] = i\hbar$.
1. Show that
	```{math}
	:label: radialmomentumsquared
	\hat{p}_r^2 = - \frac{\hbar^2}{r^2} \frac{\partial }{\partial r} \left( r^2 \frac{\partial }{\partial r} \right).
	```
	\setcounter{problemcounter}{\value{enumi}}
	Recognizing equation&nbsp;{eq}`radialmomentumsquared` as the radial part of the Laplacian in spherical coordinates, we can write
	```{math}
	:label: radialLaplacian
	\nabla^2 = - \frac{1}{\hbar^2} \hat{p}_r^2 - \frac{\hat{L}^2}{\hbar^2 r^2}.
	```
````

````{exercise} Three-dimensional harmonic oscillator
:label: pb:threedimensionalharmonicoscillator
:class: dropdown
The Hamiltonian for the three-dimensional harmonic oscillator is given by:
```{math}
:label: 3DharmoscHamiltonian
\hat{H} = \frac{\hat{\bm{p}^2}}{2m} + \frac12 m \omega^2 \hat{\bm{r}}^2.
```
If we write this Hamiltonian in Cartesian coordinates, we see that it separates into three one-dimensional harmonic oscillator Hamiltonians:
```{math}
\begin{align*}
\hat{H} &= \hat{H}_x + \hat{H_y} + \hat{H}_z,
\end{align*}
```
where $\hat{H}_x = (\hat{p}_x^2/2m) + \frac12 m \omega^2 \hat{x}^2$, and similar for $y$ and $z$. As these three Hamiltonians commute with each other, the solutions of the Schr&ouml;dinger equation with the three-dimensional Hamiltonian&nbsp;{eq}`3DharmoscHamiltonian` factorize, i.e. they can be written as
```{math}
\psi_{n_x n_y n_z}(\bm{r}) = \psi_{n_x}(x) \psi_{n_y}(y) \psi_{n_z}(z),
```
where the three functions of $x$, $y$ and $z$ are the solutions to the one-dimensional Schr&ouml;dinger equation with harmonic potentials.
1. Argue that the energies of the three-dimensional Schr&ouml;dinger equation with a harmonic potential are given by
	```{math}
	E = \left(n_x + n_y + n_z + \frac32 \right) \hbar \omega.
	```
	With equation&nbsp;{eq}`radialLaplacian`, we can also write the three-dimensional Hamiltonian&nbsp;{eq}`3DharmoscHamiltonian` in terms of the radial and angular momentum operators:
	```{math}
	\hat{H} = \frac{\hat{p}_r^2}{2m} + \frac{\hbar^2}{2m r} \hat{L}^2 + \frac12 m \omega^2 \hat{r}^2.
	```
	The angular part of this Hamiltonian is completely covered by the angular momentum operator $\hat{L}^2$, so we may also expect the solutions to separate into an angular part (which will be the spherical harmonics) and a radial part, for which we can write an effective radial Hamiltonian:
	```{math}
	:label: radialHamiltonian
	\hat{H}_l = \frac{\hat{p}_r^2}{2m} + \frac{\hbar^2 l (l+1)}{2m r} + \frac12 m \omega^2 \hat{r}^2
	```
	The eigenfunctions of this radial Hamiltonian will be functions of $r$, and be characterized by quantum numbers $n$ and $l$. We shall write them as $R_{nl}(r)$.
1. Relate the (radial) quantum number $n$ to the Cartesian quantum numbers $n_x$, $n_y$ and $n_z$.
1. Find the degeneracy of $E_n$.
	Like for the one-dimensional harmonic oscillator, the three-dimensional one has associated raising and lowering operators, defined as
	```{math}
	\hat{A}_{l, \pm} = \frac{1}{\sqrt{2m\hbar\omega}} \left( \mp i \hat{p}_r - \frac{(l+1)\hbar}{r} + m \omega r \right).
	```
	Unlike for the one-dimensional harmonic oscillator, $\hat{A}_{l, \pm}$ don't just change the energy, they also change the angular momentum. In particular, $\hat{A}_{l, -}$ lowers the energy by $\hbar \omega$ but raises the angular momentum quantum number $l$ by $1$:
	```{math}
	\hat{A}_{l, -} R_{n, l} = \alpha_- R_{n-1, l+1},
	```
	where $\alpha_-$ is a normalization constant. Now the eigenstates have two kinds of kinetic energy: radial and tangential. If the angular momentum goes up, so does the tangential velocity and thus kinetic energy; consequently, the $\hat{A}_{l, -}$ operator must decrease the radial kinetic energy. Therefore, the new orbital is more circular than the previous one. At some point, there will be no radial kinetic energy left, and the angular momentum is maximal; applying the lowering operator once more should then give a zero (as otherwise the radial kinetic energy becomes negative). This 'lowest rung' solution, sometimes referred to as a 'circular orbit', therefore satisfies (with $\mathcal{L}$ representing the maximum value of $l$):
	```{math}
	:label: circularorbitode
	0 = \hat{A}_{\mathcal{L},-} R_{0, \mathcal{L}} = \left( \frac{\partial }{\partial r} + \frac{1}{r} - \frac{\mathcal{L}+1}{r} + \frac{m \omega}{\hbar} r \right) R_{0, \mathcal{L}}
	```
1. Find and sketch the function describing the circular orbit (i.e., the solution of the ordinary differential equation&nbsp;{eq}`circularorbitode`).
````

````{exercise} An electron in an oscillating magnetic field
:label: pb:spininoscillatingmagneticfield
:class: dropdown
In class, we studied what happened when an electron (with spin&nbsp;$\frac12$) is placed in a constant magnetic field. If the electron is at rest, we get a simple Hamiltonian, coupling the (externally applied) field&nbsp;$\bm{B}$ to the spin vector&nbsp;$\bm{S}$ of the electron, through the electron's magnetic dipole moment $\bm{\mu} = \gamma \bm{S}$:
```{math}
:label: magneticfieldHamiltonian
\hat{H} = - \bm{\mu} \cdot \bm{B} = - \gamma \bm{B} \cdot \bm{S}.
```
In particular, if we define the direction of the external field as the $z$-direction (so $\bm{B} = B_0 \bm{\hat{z}}$), the field will couple to the $z$ component of the electron's spin, and the Hamiltoinan becomes
```{math}
:label: magneticfieldHamiltonianZ
\hat{H} = - \frac{\gamma B_0 \hbar}{2} \begin{pmatrix}
1 & 0 \\ 0 & -1
\end{pmatrix}.
```
We found that in this setup, the expectation value of the particle's spin, $\Braket{\bm{S}}$, will precess around the $z$ axis with Larmor frequency $\omega_\mathrm{L} = \gamma B_0$. In this problem, we'll generalize this system to the case that the magnetic field is oscillating, i.e., we have
```{math}
:label: oscillatingmagneticfield
\bm{B}(t) = B_0 \cos(\omega t) \bm{\hat{z}}.
```
NB: the oscillation frequency $\omega$ is not to be confused with the precession frequency $\omega_\mathrm{L}$. Suppose we initialize the system such that the electron is in the spin-up state with respect to the $x$ (not $z$!) axis, i.e., the initial spinor is
```{math}
:label: oscillatingmagneticfieldinitialcondition
\chi(0) = \xi_+ = \frac{1}{\sqrt{2}} \begin{pmatrix} 1 \\ 1 \end{pmatrix}.
```
1. Solve the (time-dependent!) Schr&ouml;dinger equation for the evolution of the spinor over time, i.e., solve
	```{math}
	i \hbar \frac{\partial \chi}{\partial t} = \hat{H}(t) \chi,
	```
	where $\hat{H}$ is given by equation&nbsp;{eq}`magneticfieldHamiltonianZ` and the initial condition of the spinor is equation&nbsp;{eq}`oscillatingmagneticfieldinitialcondition`.
1. Show that the (time-dependent) probability of getting $-\hbar/2$ if you measure $\hat{S}_x$ is given by
	```{math}
	\sin^2\left(\frac{\gamma B_0}{2 \omega} \sin(\omega t) \right).
	```
1. Find the minimum field strength $B_0$ necessary to force a complete flip in $\hat{S}_x$.
````

````{exercise} Properties of the density operator
:label: pb:densityoperatorproperties
:class: dropdown
In this problem we'll prove some of the properties of the density operator given in {numref}`sec:densitymatrix`.
1. Show that for a pure state, the density operator is idempotent, i.e., $\hat{\rho}^2 = \hat{\rho}$.
1. Through explicit calculation, show that the trace of the density matrix is one for any state, i.e., $\mathrm{Tr}(\rho) = 1$.
1. Prove that the time evolution of the density operator is given by equation&nbsp;{eq}`densityoperatortimeevolution` for any state, as long as the probabilities associated with the state are independent of time. *Hint*: Use the product rule to calculate
	```{math}
	i \hbar \frac{\mathrm{d}\hat{\rho}}{\mathrm{d}t} = i \hbar \frac{\mathrm{d}}{\mathrm{d}t} \left(\sum_k p_k \Ket{\Psi_k}\Bra{\Psi_k}\right)
	```
	and the Schr&ouml;dinger equation to evaluate the time derivative of $\Psi_k$.
````

[^1]: 'Symmetry' under a certain parameter here means that the system doesn't change if you change the parameter; symmetry in time then means that the system won't change if you change all times $t$ to $t+t'$ with $t'$ an arbitrary number; symmetry in space means that the same hold for changes in $x$, $y$, and $z$.

[^2]: Given the three operators $\hat{L}_x$, $\hat{L}_y$ and $\hat{L}_z$, we can now define an 'angular momentum vector' $\hat{\bm{L}}$ consisting of the three operators, which represents the total angular momentum.

[^3]: Meaning that we replace every $x$ with $y$, every $y$ with $z$, and every $z$ with $x$.

[^4]: If you find the various $\pm$ and $\mp$ dazzling, first write out equation&nbsp;{eq}`LpmLzeigenfunction` for one choice of sign; then, with another color, add the opposite sign. That's also the approach I take when explaining this in class.

[^5]: As a general rule, when something is allowed by the basic laws of physics, nature will find a way to use it.

[^6]: Or, if you are so inclined, re-derive.

[^7]: Strictly speaking, the factor two is also an approximation; there are additional corrections, of which the lowest is $\alpha/\pi$, with $\alpha$ the fine structure constant (see {numref}`sec:matterquantization` and&nbsp;{numref}`sec:hydrogenfinestructure`). The actual factor is $2.002319...$.

[^8]: Image adapted from an image made by [Mysid](https://en.wikipedia.org/wiki/User:Mysid), working for NASA, obtained from [Wikimedia commons](https://en.wikipedia.org/wiki/File:Earth_precession.svg), public domain.

[^9]: The idea for the experiment was proposed by Stern in 1921, and successfully executed by Gerlach in 1922. Stern received the Nobel prize in physics in 1943; Gerlach was not included because he was a resident of Nazi Germany.

[^10]: Note that we cannot just add an inhomogeneity in the $z$-direction, as that would violate the Maxwell equation for the divergence of the field, $\nabla \cdot \bm{B} = 0$, which always holds because magnetic monopoles don't exist.

[^11]: Magnetic resonance angiography; view from the front, by [Ofirglazer](https://en.wikipedia.org/wiki/User:Ofirglazer), obtained from [Wikimedia commons](https://commons.wikimedia.org/wiki/File:Mra1.jpg), CC-BY SA 3.0.

[^12]: Axial magnetic resonance images of the brain, by Jpoolzer, obtained from [Wikimedia commons](https://commons.wikimedia.org/wiki/File:Idh1mut_gbm.jpg), CC-BY SA 4.0.

[^13]: To juice things up a bit, rather than calling the systems 'system A' and 'system B', people often refer to them as Alice's and Bob's system, especially when using these states for quantum communication.

[^14]: Note that because the coefficients are complex, we get to pick four real numbers for each of the states $\chi_A$ and $\chi_B$ (the real and imaginary parts of the $\alpha_i$ and $\beta_i$). The normalization fixes two of these. Two others represent phases, which don't show up in the probability density, as they are squared out, so there are four (two per state) relevant numbers we can pick freely. For the eight real values in the $\gamma_{ij}$'s, one is fixed by normalization, and one sets the overall phase, which gives us six relevant values we can freely choose.

[^15]: I'm using $\chi$ instead of $\Psi$ as we're working with spin states here.

[^16]: Photograph of [Bohr](https://en.wikipedia.org/wiki/Niels_Bohr) and [Einstein](https://en.wikipedia.org/wiki/Albert_Einstein) by [Paul Ehrenfest](https://en.wikipedia.org/wiki/Paul_Ehrenfest), taken in Leiden on 11 December 1925, obtained from [Wikimedia commons](https://commons.wikimedia.org/wiki/File:Niels_Bohr_Albert_Einstein_by_Ehrenfest.jpg), public domain.

[^17]: These could be electrons fixed in place, but also other realizations, as people aiming to build quantum computers can tell you all about.

[^18]: The total spin $s$ is also a quantum number, but as it is identical for all electrons in all states, it won't help us here.

[^19]: Image adapted from [Wikimedia commons](https://commons.wikimedia.org/wiki/File:Simple_Periodic_Table_Chart-en.svg), public domain.

[^20]: That shouldn't stop you from trying for yourself, of course.

[^21]: Cartoon by Randall Munroe, from [xkcd.com/2913/](https://xkcd.com/2913/), CC-BY-NC 2.5.

[^22]: While we write the vectors in boldface here, on paper that's hard; you can drop the 'operator hat' there if you like, and simply write $\vec{r}$ and $\vec{p}$, or, if you prefer, write $\hat{\vec{x}}$ and $\hat{\vec{p}}$.

