%Problemtitle: Solving the hydrogen Hamiltonian with raising and lowering operators
% See sect. 8.1 of Binney & Skinner; 2020 final.
\begin{enumerate}[(a)]
\item Derive equation~(\bookref{hydrogenradialHamiltonianladderoperators}) for $\hat{H}_l$ through explicit calculation of $\hat{A}_l^\dagger \hat{A}_l$.
\item Show that the commutator of the ladder operator and its Hermitian conjugate is given by
\begin{equation}
\label{hydrogenradialHamiltonianladderoperatorscommutator}
\left[ \hat{A}_l, \hat{A}_l^\dagger \right] = \frac{a_0^2 m_\mathrm{e}}{\hbar^2} \left( \hat{H}_{l+1} - \hat{H}_l \right).
\end{equation}
\item Find the commutator $\left[ \hat{A}_l, \hat{H}_l \right]$, and use it to prove that $\hat{A}_l \hat{H}_l = \hat{H}_{l+1} \hat{A}_l$. \textit{Hint}: you will only need to combine your results of (a) and (b) here.
\item Prove that if $u_l(r)$ satisfies equation~(\bookref{Hamiltonianeigenvalueeq}) with eigenvalue $E_l$, then $\hat{A}_l u_l$ is an eigenfunction of $\hat{H}_{l+1}$ with eigenvalue~$E_l$.
\item Show that the last equality in equation~(\bookref{hydrogenladdertopselfinnerproduct}) holds. You may assume that $u_{l_\mathrm{max}}$ is properly normalized.
\item Argue why the values of $l$ in $u_{nl}$ are restricted to the familiar range $l = 0, 1, \ldots, n-1$.
\item By solving the first-order differential equation~(\bookref{hydrogenladdertop}) for $n=1, l=0$, find an expression for $u_{10}(r)$. You may leave the normalization constant in the expression.
\item By solving the first-order differential equation~(\bookref{hydrogenladdertop}) for $l=n-1$ and arbitrary $n$, explicitly find the expression for $u_{n,n-1}(r)$. Again, you may leave the normalization constant in the expression.
\end{enumerate}

\ifincludesolutions
\Solutions
\begin{enumerate}[(a)]
\item As instructed, we calculate $\hat{A}_l^\dagger \hat{A}_l$:
\begin{equation}
\label{AdaggerA}
\hat{A}_l^\dagger \hat{A}_l = \frac{a_0^2}{2} \left\lbrace \frac{\hat{p}_r^2}{\hbar^2} + \left( \frac{1}{a_0 (l+1)} - \frac{l+1}{r} \right)^2 + \frac{i}{\hbar} \left[ \hat{p}_r, \frac{l+1}{r} \right] \right\rbrace,
\end{equation}
so we'll need the commutator of $\hat{p}_r$ and $1/r$, for which we get:
\begin{align*}
\left[ \hat{p}_r, \frac{1}{r} \right] \psi(r) &= - i \hbar \left(\diff{}{r} + \frac{1}{r}\right) \left( \frac{\psi(r)}{r} \right) - \frac{1}{r} \left[ -i \hbar \left(\diff{\psi(r)}{r} + \frac{\psi(r)}{r} \right) \right] \\
&= - i \hbar \psi(r) \diff{(1/r)}{r} = \frac{i \hbar}{r^2} \psi(r).
\end{align*}
\score{1 pt}. Substituting back into~(\ref{AdaggerA}) we get
\begin{align*}
\hat{A}_l^\dagger \hat{A}_l &= \frac{a_0^2}{2} \left\lbrace \frac{\hat{p}_r^2}{\hbar^2} + \frac{(l+1)^2}{r^2} + \frac{1}{a_0^2 (l+1)^2} - \frac{2}{a_0 r} - \frac{l+1}{r^2} \right\rbrace \\
&= \frac{a_0^2}{2} \left\lbrace \frac{\hat{p}_r^2}{\hbar^2} + \frac{l(l+1)}{r^2} - \frac{2}{a_0 r} \right\rbrace + \frac{1}{2(l+1)^2} \\
&= \frac{a_0^2 m_\mathrm{e}}{\hbar^2} \hat{H}_l + \frac{1}{2(l+1)^2}
\end{align*}
\score{1 pt}. For the Hamiltonian itself we thus find equation~(\bookref{hydrogenradialHamiltonianladderoperators}) \score{0.5 pt}.
\item We can easily repeat the calculation of part (a) with the order of the operators in~(\ref{AdaggerA}) reversed, which gives
\[ \hat{A}_l \hat{A}_l^\dagger = \frac{a_0^2 m_\mathrm{e}}{\hbar^2} \hat{H}_{l+1} + \frac{1}{2(l+1)^2}, \]
\score{1 pt} from which we read off that their commutator is given by equation~(\ref{hydrogenradialHamiltonianladderoperatorscommutator}) \score{0.5 pt}.
\item We have
\begin{align*}
\left[ \hat{A}_l, \hat{H}_l \right] &= \left[ \hat{A}_l, \frac{\hbar^2}{a_0^2 m_\mathrm{e}} \left( \hat{A}_l^\dagger \hat{A}_l \right) \right] = \frac{\hbar^2}{a_0^2 m_\mathrm{e}} \left[ \hat{A}_l, \hat{A}_l^\dagger \right] \hat{A}_l \\
&= \frac{\hbar^2}{a_0^2 m_\mathrm{e}} \frac{a_0^2 m_\mathrm{e}}{\hbar^2} \left( \hat{H}_{l+1} - \hat{H}_l \right) \hat{A}_l = \left( \hat{H}_{l+1} - \hat{H}_l \right) \hat{A}_l,
\end{align*}
where we used that $[\hat{A}, \hat{B}\hat{A}] = \hat{A}\hat{B}\hat{A} - \hat{B}\hat{A}\hat{A} = [\hat{A},\hat{B}] \hat{A}$ \score{1 pt}. Writing out the commutator explicitly, we also have
\[ \hat{A}_l \hat{H}_l - \hat{H}_l, \hat{A}_l = \hat{H}_{l+1} \hat{A}_l - \hat{H}_l \hat{A}_l, \]
from which the required relation $\hat{A}_l \hat{H}_l = \hat{H}_{l+1} \hat{A}_l$ directly follows.
\item Using the result of (c), we have:
\[ E_l \hat{A}_l u_l = \hat{A}_l E_l u_l = \hat{A}_l \hat{H}_l u_l = \hat{H}_{l+1} \hat{A}_l u_l, \]
so $\hat{A}_l u_l$ is an eigenfunction of $\hat{H}_{l+1}$ with eigenvalue $E_l$ (the same as the eigenfunction $u_l$ of $\hat{H}_l$) \score{1 pt}.
\item We re-write the combination $\hat{A}_{l_\mathrm{max}}^\dagger \hat{A}_{l_\mathrm{max}}$ in terms of the Hamiltonian using our result from (a), then evaluate the inner product:
\begin{align*}
\Braket{u_{l_\mathrm{max}} | \hat{A}_{l_\mathrm{max}}^\dagger \hat{A}_{l_\mathrm{max}} | u_{l_\mathrm{max}}} &= \Braket{u_{l_\mathrm{max}} | \frac{a_0^2 m_\mathrm{e}}{\hbar^2} \hat{H}_{l_\mathrm{max}} + \frac{1}{2(l_\mathrm{max}+1)^2} | u_{l_\mathrm{max}}} \\
&= \frac{a_0^2 m_\mathrm{e}}{\hbar^2} E_{l_\mathrm{max}} + \frac{1}{2(l_\mathrm{max}+1)^2}
\end{align*}
\score{1 pt}.
%\item By equation~(\ref{hydrogenladdertopselfinnerproduct}), the result from (e) equals zero, which gives for the energy eigenvalue (writing $n = 1+l_\mathrm{max}$):
%\[ E_n = - \frac{\hbar^2}{2 a_0^2 m_\mathrm{e}} \frac{1}{n^2} \]
%\score{1 pt}, which, unsurprisingly, is the same result as we got from the analytical solution.
\item $l$ was already a non-negative integer; for any value $l>n-1$ equation~(\bookref{hydrogenladdertop}) gives us an eigenfunction which is identically zero, so the range of $l$ is now from $0$ to $n-1$ \score{1 pt}.
\item We have
\begin{equation}
0 = \hat{A}_0 u_{10}(r) = \frac{a_0}{\sqrt{2}} \left( \frac{i}{\hbar} \hat{p}_r - \frac{1}{r} + \frac{1}{a_0} \right) u_{10}(r),
\end{equation}
or, by substituting the expression~(\bookref{radialmomentumoperator}) for $\hat{p}_r$:
\[ \frac{\dd u_{10}(r)}{\ddr} = -\frac{1}{a_0} u_{10}(r). \]
We can easily solve this equation through separation of variables and integration:
\[ \int \frac{1}{u_{10}} \dd u_{10} = \log(u_{10}) + C = -\int \frac{1}{a_0} \ddr = -\frac{r}{a_0}, \]
so we find
\[ u_{10}(r) = A \exp(-r/a_0) \]
\score{1 pt}.
\item We now have
\begin{align*}
0 = \hat{A}_0 u_{n,n-1}(r) &= \frac{a_0}{\sqrt{2}} \left( \frac{i}{\hbar} \hat{p}_r - \frac{n}{r} + \frac{1}{n a_0} \right) u_{n,n-1}(r) \\
&= \frac{a_0}{\sqrt{2}} \left[ \diff{u_{n,n-1}}{r} + \left( \frac{1}{n a_0} - \frac{n-1}{r} \right) u_{n,n-1}(r) \right],
\end{align*}
to which we can again apply separation of constants to give
\begin{align*}
u_{n,n-1}(r) &= A \exp\left[ -\int \left( \frac{1}{n a_0} - \frac{n-1}{r} \right) \ddr \right] \\
&= A \exp\left[ - \frac{r}{n a_0} + (n-1) \log(r) \right] = A r^{n-1} \exp(-r/na_0)
\end{align*}
\score{1 pt}.
\end{enumerate}
\fi