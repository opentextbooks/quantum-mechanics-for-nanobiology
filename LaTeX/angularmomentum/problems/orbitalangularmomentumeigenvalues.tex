%Problemtitle: Eigenvalues of the orbital angular momentum operator
%Source: Griffiths 4.57 (only 2nd ed), exam 2017
From the algebraic calculations in section~\bookref{sec:angularmomentumoperators}, we got both integer and half-integer values for the quantum number~$l$. From the solution of the eigenstates of the hydrogen atom, we know however that for orbital angular momentum, we only get the integer values. Thus, in contrast to spin, where, with the same commutation relations, the half-integer values do occur, there must be an extra piece of physics in the orbital angular momentum operator that excludes the half-integer options. That in itself is perhaps not surprising: while we define the spin operators through the commutation relations, for the orbital angular momentum operators we derived the commutation relations from the `quantization' of the classical definition $\bvec{L} = \bvec{r} \times \bvec{p}$ of the angular momentum. In this problem, we'll `disentangle' the cross product, ending up with two mathematical equivalents of harmonic oscillators, from which we can prove that we can only get integer values of $l$.

To start, we define the operators
\begin{equation}
\hat{q}_\pm \equiv \frac{1}{\sqrt{2}} \left( \hat{x} \pm \alpha \hat{p}_y \right); \qquad \hat{p}_\pm \equiv \frac{1}{\sqrt{2}} \left( \hat{p}_x \pm \frac{1}{\alpha} \hat{y} \right),
\end{equation}
where $\alpha = a^2/\hbar$, with $a$ the Bohr radius. 
\begin{enumerate}[(a)]
\item Verify that $[\hat{q}_{+}, \hat{q}_{-}] = [\hat{p}_{+}, \hat{p}_{-}] = 0$ and $[\hat{q}_{+}, \hat{p}_{-}] = [\hat{q}_{-}, \hat{p}_{+}] = i \hbar$. 
\item The result of (a) shows that the $\hat{q}$'s and $\hat{p}$'s have the same commutation relations as the position and momentum operators. How would you pair the $\hat{q}$'s and $\hat{p}$'s such that they correspond to `orthogonal directions'?
\item Show that
\[ \hat{L}_z = \frac{1}{2 \alpha} \left( \hat{q}_{+}^2 - \hat{q}_{-}^2\right) - \frac{\alpha}{2} \left( \hat{p}_{+}^2 - \hat{p}_{-}^2\right). \]
\item Now show that we can write $\hat{L}_z$ as the difference between two Hamiltonians describing one-dimen\-sional harmonic oscillators with frequency~$1$ and mass $m = 1/\alpha$. Hint: Make use of the sets of $\hat{q}$ and $\hat{p}$ operators you grouped in (b).
\item Combine what you know about the eigenvalues of the harmonic oscillator with the result of (d) to prove that the eigenvalues of $\hat{L}_z$ must be integer multiples of $\hbar$.
\end{enumerate}

\ifincludesolutions
\Solution
\begin{enumerate}[(a)]
\item We have
\begin{align*}
[\hat{q}_{+}, \hat{q}_{-}] &= \frac12 [\hat{x} + \alpha \hat{p}_y, \hat{x} - \alpha \hat{p}_y] = \frac12 \left( [\hat{x}, \hat{x}] - \alpha [\hat{x}, \hat{p}_y] + \alpha [\hat{p}_y, \hat{x}] - \alpha^2 [\hat{p}_y, \hat{p}_y] \right) = 0 \\
[\hat{p}_{+}, \hat{p}_{-}] &= \frac12 \left( [\hat{p}_x, \hat{p}_x] - \frac{1}{\alpha} [\hat{p}_x, \hat{y}] + \frac{1}{\alpha} [\hat{y}, \hat{p}_x] - \frac{1}{\alpha^2} [\hat{y}, \hat{y}] \right) = 0 \\
[\hat{q}_{+}, \hat{p}_{-}] &= \frac12 \left( [\hat{x}, \hat{p}_x] - \frac{1}{\alpha} [\hat{x}, \hat{y}] + \alpha [\hat{p}_y, \hat{p}_x] - [\hat{p}_y, \hat{y}] \right) = \frac12 \left(i\hbar - 0 + 0 + i\hbar\right) = i\hbar\\
[\hat{q}_{-}, \hat{p}_{+}] &= \frac12 \left( [\hat{x}, \hat{p}_x] + \frac{1}{\alpha} [\hat{x}, \hat{y}] - \alpha [\hat{p}_y, \hat{p}_x] - [\hat{p}_y, \hat{y}] \right) = \frac12 \left(i\hbar - 0 + 0 + i\hbar\right) = i\hbar
\end{align*}
\score{0.25 pt per item}.
\item `1' direction: $\hat{q}_{+}$ with $\hat{p}_{-}$; `2' direction: $\hat{q}_{-}$ with $\hat{p}_{+}$ \score{0.5 pt}.
\item We have
\begin{align*}
\hat{x} &= \frac{1}{\sqrt{2}} (\hat{q}_{+} + \hat{q}_{-}) \\
\hat{y} &= \frac{\alpha}{\sqrt{2}} (\hat{p}_{+} - \hat{p}_{-}) \\
\hat{p}_x &= \frac{1}{\sqrt{2}} (\hat{p}_{+} + \hat{p}_{-}) \\
\hat{p}_y &= \frac{1}{\alpha\sqrt{2}} (\hat{q}_{+} - \hat{q}_{-})
\end{align*}
\score{0.5 pt} and thus
\begin{align*}
\hat{L}_z &= \hat{x}\hat{p}_y - \hat{y}\hat{p}_x \\
&= \frac{1}{2\alpha} (\hat{q}_{+} + \hat{q}_{-})(\hat{q}_{+} - \hat{q}_{-}) - \frac{\alpha}{2} (\hat{p}_{+} - \hat{p}_{-})(\hat{p}_{+} + \hat{p}_{-}) \\
&= \frac{1}{2\alpha} (\hat{q}_{+}^2 - \hat{q}_{-}^2) - \frac{\alpha}{2} (\hat{p}_{+}^2 - \hat{p}_{-}^2)
\end{align*}
\score{0.5 pt}.
\item The Hamiltonian of a harmonic oscillator with position $\hat{x}$ and momentum $\hat{p}$ is given by
\[ \hat{H} = \frac{\hat{p}^2}{2m} + \frac12 m \omega^2 \hat{x}^2 \]
\score{0.25 pt}. Then, following the hint, we define
\begin{align*}
\hat{H}_1 &= \frac{\alpha}{2} \hat{p}_{-}^2 + \frac{1}{2\alpha} \hat{q}_{+}^2 \\
\hat{H}_2 &= \frac{\alpha}{2} \hat{p}_{+}^2 + \frac{1}{2\alpha} \hat{q}_{-}^2 \\
\hat{H}_1 - \hat{H}_2 &= \frac{1}{2\alpha} \left( \hat{q}_{+}^2 - \hat{q}_{-}^2 \right) - \frac{\alpha}{2} \left(\hat{p}_{+}^2 - \hat{p}_{-}^2 \right) = \hat{L}_z
\end{align*}
\score{0.25 pt per line}.
\item The eigenvalues of $\hat{H}_1$ are $(n_1 + 1/2)\hbar$, and those of $\hat{H}_2$ are $(n_2+1/2)\hbar$, so the eigenvalues of $\hat{L}_z$ are $(n_1 + 1/2)\hbar - (n_2 + 1/2)\hbar = (n_1-n_2)\hbar = m_l\hbar$ \score{0.25 pt}, where $m_l$ must be an integer because both $n_1$ and $n_2$ are integers \score{0.25 pt}.
\end{enumerate}
\fi