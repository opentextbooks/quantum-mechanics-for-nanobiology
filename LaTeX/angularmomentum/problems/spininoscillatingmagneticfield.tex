%Problemtitle: An electron in an oscillating magnetic field
%Source: Griffiths 4.36
In class, we studied what happened when an electron (with spin~$\frac12$) is placed in a constant magnetic field. If the electron is at rest, we get a simple Hamiltonian, coupling the (externally applied) field~$\bvec{B}$ to the spin vector~$\bvec{S}$ of the electron, through the electron's magnetic dipole moment $\bvec{\mu} = \gamma \bvec{S}$:
\begin{equation}
\label{magneticfieldHamiltonian}
\hat{H} = - \bvec{\mu} \cdot \bvec{B} = - \gamma \bvec{B} \cdot \bvec{S}.
\end{equation}
In particular, if we define the direction of the external field as the $z$-direction (so $\bvec{B} = B_0 \unitvec{z}$), the field will couple to the $z$ component of the electron's spin, and the Hamiltoinan becomes
\begin{equation}
\label{magneticfieldHamiltonianZ}
\hat{H} = - \frac{\gamma B_0 \hbar}{2} \begin{pmatrix}
1 & 0 \\ 0 & -1
\end{pmatrix}.
\end{equation}
We found that in this setup, the expectation value of the particle's spin, $\ev{\bvec{S}}$, will precess around the $z$ axis with Larmor frequency $\omega_\mathrm{L} = \gamma B_0$. In this problem, we'll generalize this system to the case that the magnetic field is oscillating, i.e., we have
\begin{equation}
\label{oscillatingmagneticfield}
\bvec{B}(t) = B_0 \cos(\omega t) \unitvec{z}.
\end{equation}
NB: the oscillation frequency $\omega$ is not to be confused with the precession frequency $\omega_\mathrm{L}$. Suppose we initialize the system such that the electron is in the spin-up state with respect to the $x$ (not $z$!) axis, i.e., the initial spinor is
\begin{equation}
\label{oscillatingmagneticfieldinitialcondition}
\chi(0) = \xi_+ = \frac{1}{\sqrt{2}} \spinor{1}{1}.
\end{equation}
\begin{enumerate}[(a)]
\item Solve the (time-dependent!) Schr\"odinger equation for the evolution of the spinor over time, i.e., solve
\begin{equation}
i \hbar \diff{\chi}{t} = \hat{H}(t) \chi,
\end{equation}
where $\hat{H}$ is given by equation~(\ref{magneticfieldHamiltonianZ}) and the initial condition of the spinor is equation~(\ref{oscillatingmagneticfieldinitialcondition}).
\item Show that the (time-dependent) probability of getting $-\hbar/2$ if you measure $\hat{S}_x$ is given by
\begin{equation}
\sin^2\left(\frac{\gamma B_0}{2 \omega} \sin(\omega t) \right).
\end{equation}
\item Find the minimum field strength $B_0$ necessary to force a complete flip in $\hat{S}_x$.
\end{enumerate}

\ifincludesolutions
\Solution
\begin{enumerate}[(a)]
\item Because the Hamiltonian matrix is diagonal, our system of differential equations nicely splits in two first-order differential equations. Labeling the two components of $\chi$ as $\chi_1$ and $\chi_2$, we have:
\begin{align*}
i \hbar \dot{\chi}_1 &= - \frac{\gamma B_0 \hbar}{2} \cos(\omega t) \chi_1 \\
i \hbar \dot{\chi}_2 &= \frac{\gamma B_0 \hbar}{2} \cos(\omega t) \chi_2,
\end{align*}
or
\begin{align*}
\frac{\dd\chi_i}{\ddt} &= \pm i \frac{\gamma B_0}{2} \cos(\omega t) \chi_i \\
\int \frac{1}{\chi_i} \dd\chi_i &= \pm i \frac{\gamma B_0}{2} \int \cos(\omega t) \,\ddt \\
\log \left(\chi_i\right) &= \pm i \frac{\gamma B_0}{2 \omega} \sin(\omega t) + C \\
\chi_i &= A \exp \left[ \pm i \frac{\gamma B_0}{2 \omega} \sin(\omega t) \right] \\
\chi_i(0) &= \frac{1}{\sqrt{2}} \quad \Rightarrow \quad \chi_i(t) = \frac{1}{\sqrt{2}} \exp \left[ \pm i \frac{\gamma B_0}{2 \omega} \sin(\omega t) \right]
\end{align*}
so
\[ \chi(t) = \frac{1}{\sqrt{2}} \spinor{e^{i (\gamma B_0 / 2 \omega) \sin(\omega t)}}{e^{-i (\gamma B_0 / 2 \omega) \sin(\omega t)}} \]
\score{3 pt}.
\item To get the probability of either of the eigenvalues of $\hat{S}_x$, we need to expand the state $\chi(t)$ in terms of the eigenstates of $\hat{S}_x$, i.e., $\xi_\pm$ \score{1 pt; also give if correctly used but not explicitly stated}. The coefficients are given by the inner products of $\xi_\pm$ with $\chi$; in particular
\begin{align*}
c_- &= \Braket{\xi_- | \chi(t)} = \frac{1}{\sqrt{2}} \begin{pmatrix} 1 & -1 \end{pmatrix} \spinor{\chi_1(t)}{\chi_2(t)} \\
&= \frac12 \left[ e^{i (\gamma B_0 / 2 \omega) \sin(\omega t)} - e^{-i (\gamma B_0 / 2 \omega) \sin(\omega t)} \right] \\
&= i \sin\left( \frac{\gamma B_0}{2 \omega} \sin(\omega t) \right].
\end{align*}
\score{1 pt}. The probability of getting the $-\hbar/2$ eigenvalue is simply the square of $c_-$:
\begin{align*}
P\left(\mbox{measuremet of } \hat{S}_x \mbox{ gives } -\frac{\hbar}{2} \right) = |c_-|^2 = \sin^2\left( \frac{\gamma B_0}{2 \omega} \sin(\omega t) \right]
\end{align*}
\score{1 pt}.
\item We have a complete flip if the probability of getting `spin down', i.e., eigenvalue $-\hbar/2$, equals~$1$. For that to happen, the argument of the $\sin^2$ in the answer of (b) must equal a multiple of $\pi/2$. The maximum of the $\sin(\omega t)$ is one, so the minimum value of $B_0$ is $B_0 = \omega \pi / \gamma$ \score{2 pt}.
\end{enumerate}
\fi

