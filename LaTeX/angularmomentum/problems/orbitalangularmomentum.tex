%Problemtitle: Orbital angular momentum and kinetic energy
%Source: Inspired by Binney & Skinner section 7.4; final 2021
The operators representing the square and the $z$-component of the angular momentum can be expressed in spherical coordinates as
\begin{subequations}
\begin{align}
\hat{L}^2 &= - \hbar^2 \frac{1}{\sin\theta} \diff{}{\theta} \left(\sin\theta \diff{}{\theta}\right) - \frac{1}{\sin^2\theta} \frac{\partial^2}{\partial \phi^2}, \\
\hat{L}_z &= -i \hbar \diff{}{\phi}
\end{align}
\end{subequations}
As we derived in class, these operators commute; their joint eigenfunctions are the spherical harmonics $Y_l^m(\theta, \phi)$, with eigenvalues $\hbar^2 l (l+1)$ for $\hat{L}^2$ and $\hbar m$ for $\hat{L}_z$ (note that the $m$ here is the quantum number $m_l$, not the mass). The fact that the spherical harmonics are the eigenfunctions is not surprising if you compare the expression for $\hat{L}^2$ with the Laplacian in spherical coordinates; you'll find that the angular part of the Laplacian is exactly $-\hat{L}^2 / (\hbar^2 r^2)$. This observation allows us to split the kinetic energy into a radial and an angular part. As the kinetic energy is proportional to the square of the momentum, we need an expression for the radial momentum. Defining\footnote{While we write the vectors in boldface here, on paper that's hard; you can drop the `operator hat' there if you like, and simply write $\vec{r}$ and $\vec{p}$, or, if you prefer, write $\hat{\vec{x}}$ and $\hat{\vec{p}}$.} `operator vectors' $\hat{\bvec{r}} = (\hat{x}, \hat{y}, \hat{z})$ and $\hat{\bvec{p}} = (\hat{p}_x, \hat{p}_y, \hat{p}_z)$, we might define the radial component of the momentum by the projection of $\hat{\bvec{p}}$ on the unit vector in the radial direction ($\hat{\bvec{r}}/r$, where as usual $r = \sqrt{x^2 + y^2 + z^2}$). Unfortunately, this won't do, as the resulting operator is not Hermitian.
\begin{enumerate}[(a)]
\item Show that $\hat{\bvec{r}} \cdot \hat{\bvec{p}}$ is not Hermitian.
\item Show that $\hat{p}_r = \frac{1}{2r} \left(\hat{\bvec{r}} \cdot \hat{\bvec{p}} + \hat{\bvec{p}} \cdot \hat{\bvec{r}} \right)$ is Hermitian.
\setcounter{problemcounter}{\value{enumi}}
\end{enumerate}
Substituting the recipes for $\hat{\bvec{r}}$ and $\hat{\bvec{p}}$, we find the recipe for $\hat{p}_r$:
\begin{equation}
p_r = - \frac{i\hbar}{2} \left[ \frac{1}{r} \bvec{r} \cdot \bvec{\nabla} + \bvec{\nabla} \cdot \left( \frac{\bvec{r}}{r} \right) \right].
\end{equation}
Note that here $\bvec{r}$ is just the position vector. We can write $\bvec{\nabla}$ in both Cartesian and spherical coordinates, from which we find that $\bvec{\nabla} \cdot \bvec{r} = 3$ and
\begin{equation}
\bvec{r} \cdot \bvec{\nabla} = x \diff{}{x} + y \diff{}{y} + z \diff{}{z} = r \diff{}{r}.
\end{equation}
\begin{enumerate}[(a)]
\setcounter{enumi}{\value{problemcounter}}
\item Use the above expressions to find an expression for $\hat{p}_r$ in terms of $r$ and derivatives to $r$. \textit{NB: remember that an operator always acts on a function!}
\item Show that the commutator of $\hat{r}$ (recipe: multiply with $r$) and $\hat{p}_r$ is the same as the one-dimensional version, i.e., $\left[ \hat{r}, \hat{p}_r \right] = i\hbar$.
\item Show that
\begin{equation}
\label{radialmomentumsquared}
\hat{p}_r^2 = - \frac{\hbar^2}{r^2} \diff{}{r} \left( r^2 \diff{}{r} \right).
\end{equation}
\setcounter{problemcounter}{\value{enumi}}
\end{enumerate}
Recognizing equation~(\ref{radialmomentumsquared}) as the radial part of the Laplacian in spherical coordinates, we can write
\begin{equation}
\label{radialLaplacian}
\nabla^2 = - \frac{1}{\hbar^2} \hat{p}_r^2 - \frac{\hat{L}^2}{\hbar^2 r^2}.
\end{equation}





