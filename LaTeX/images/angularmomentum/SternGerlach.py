import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatch
import matplotlib.path as mpath
from myst_nb import glue

fig,ax = plt.subplots(figsize=(10,2.5))

ax.set_xlim(-0.5,1.1)
ax.set_ylim(-0.15,0.18)

#experiment 

x = np.linspace(0,1,1000)

lineformatting = {"linewidth":2, "color":"black"}
path = (1/2*x)**4

codes = [
    mpath.Path.MOVETO,
    mpath.Path.LINETO,
    mpath.Path.CURVE4,
    mpath.Path.LINETO,
    mpath.Path.LINETO
    
]

style = mpatch.ArrowStyle("-|>", head_length=10, head_width=5)
a1 = mpatch.FancyArrowPatch(path=mpath.Path([(0,0),(0.1,0),(0.6,0),(0.7,0.03),(1,0.08)],
codes=codes), arrowstyle = style, **lineformatting)
a2 = mpatch.FancyArrowPatch(path=mpath.Path([(0,0),(0.1,0),(0.6,0),(0.7,-0.03),(1,-0.08)],
codes=codes), arrowstyle = style, **lineformatting)


for a in [a1,a2]:
    ax.add_patch(a)
    
props = {"s":"     magnet     ","fontsize":15,"fontfamily":"serif"}
boxprops = {"boxstyle":'square', "facecolor":'lightblue',"edgecolor":"black","lw":1.5}

ax.text(0.22,0.1,**props,bbox = boxprops)
ax.text(0.22,-0.12,**props,bbox = boxprops)
    
#coordinate set
arrowprops = {"width":0.003,"head_width":0.02,"head_length":0.02, "color":"black","edgecolor":None}

ax.arrow(-0.32,0,0,0.15,**arrowprops)
ax.arrow(-0.32,0,0.2,0,**arrowprops)
ax.arrow(-0.32,0,-0.1,-0.1,**arrowprops)

textprops = {"fontsize":16,"fontfamily":"serif","fontstyle":"italic"}
ax.text(-0.44,-0.06,"x",**textprops)
ax.text(-0.15,-0.05,"y",**textprops)
ax.text(-0.37,0.14,"z",**textprops)
    

ax.set_xticks([])
ax.set_yticks([])
ax.axis('off')