import numpy as np
import matplotlib.pyplot as plt
from myst_nb import glue

def Efs(n, j, alpha):
    # Fine structure correction in terms of prefactor En^2 / 2 m c^2.
    return alpha * alpha * (3- (4 * n)/(j+0.5)) / (n*n*n*n) - 1/(n*n)

fig,ax = plt.subplots(figsize=(10,8))

alpha = np.linspace(0, 1, 100)
lw = 2
fs1 = 18
fs2 = 14

ax.plot(alpha, Efs(1, 0.5, alpha),lw = lw)
ax.text(1.01, Efs(1, 0.5, 1)-0.002, r'$E_{(1, \frac{1}{2})}$', c = 'C0', fontsize = fs2)

ax.plot(alpha, Efs(2, 0.5, alpha),lw = lw)
ax.text(1.01, Efs(2, 0.5, 1)-0.002, r'$E_{(2, \frac{1}{2})}$', c = 'C1', fontsize = fs2)
ax.plot(alpha, Efs(2, 1.5, alpha),lw = lw)
ax.text(1.01, Efs(2, 1.5, 1)-0.002, r'$E_{(2, \frac{3}{2})}$', c = 'C2', fontsize = fs2)

ax.plot(alpha, Efs(3, 0.5, alpha),lw = lw)
ax.text(1.01, Efs(3, 0.5, 1)-0.01, r'$E_{(3, \frac{1}{2})}$', c = 'C3', fontsize = fs2)
ax.plot(alpha, Efs(3, 1.5, alpha),lw = lw)
ax.text(1.01, Efs(3, 1.5, 1)-0.02, r'$E_{(3, \frac{3}{2})}$', c = 'C4', fontsize = fs2)
ax.plot(alpha, Efs(3, 2.5, alpha),lw = lw)
ax.text(1.01, Efs(3, 1.5, 1)+.04, r'$E_{(3, \frac{5}{2})}$', c = 'C5', fontsize = fs2)

hline = {"color":"k","linestyle":":","linewidth":lw}

ax.axhline(y = Efs(1,0,0), **hline)
ax.axhline(y = Efs(2,0,0), **hline)
ax.axhline(y = Efs(3,0,0), **hline)

ax.set_xlim(0,1)

ax.set_yticks([Efs(1,0,0),Efs(2,0,0),Efs(3,0,0), 0])
ax.set_yticklabels([r'$E_1$', r'$E_2$', r'$E_3$', 0],fontsize = fs1)

ax.set_xlabel(r'$\alpha^2$', fontsize=fs1)