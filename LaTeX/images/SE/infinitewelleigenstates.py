import numpy as np
import matplotlib.pyplot as plt
from myst_nb import glue

#define the wavefunction
def psi(x,n,a):
    return np.sqrt(2/a)*np.sin(n*np.pi*x)


a = 1
x = np.linspace(0.0,a,500)

fig,ax = plt.subplots(figsize = (6.5,5))

ax.spines["bottom"].set_linewidth(30)
ax.spines["bottom"].set_color("grey")

ax.spines["right"].set_linewidth(30)
ax.spines["right"].set_color("grey")

ax.spines["left"].set_linewidth(30)
ax.spines["left"].set_color("grey")

ax.spines["top"].set_visible(False)

ax.set_xlim(-0.05,a+0.05)
ax.set_ylim(-2,19)

ax.set_xticks([])
ax.set_yticks([])

ax.hlines(0.0, 0.0, a, linewidth=1, linestyle=(5,(10,3)), color="black")
ax.plot(x, psi(x,1,a), color="slateblue", linewidth=2.8) 
ax.text(0.05,2,r"$\psi_1$",c = "slateblue",fontsize = 20)

ax.hlines(5, 0.0, a, linewidth=1, linestyle=(5,(10,3)), color="black")
ax.plot(x, psi(x,2,a)+5, color="orange", linewidth=2.8) 
ax.text(0.05,7,r"$\psi_2$",c = "orange",fontsize = 20)

ax.hlines(10, 0.0, a, linewidth=1, linestyle=(5,(10,3)), color="black")
ax.plot(x, psi(x,3,a)+10, color="olive", linewidth=2.8) 
ax.text(0.05,12,r"$\psi_3$",c = "olive",fontsize = 20)

ax.hlines(15, 0.0, a, linewidth=1, linestyle=(5,(10,3)), color="black")
ax.plot(x, psi(x,4,a)+15, color="firebrick", linewidth=2.8) 
ax.text(0.05,17,r"$\psi_4$",c = "firebrick",fontsize = 20)