import numpy as np
import matplotlib.pyplot as plt
import scipy.special as sc
from myst_nb import glue

# Helper functions / integrals
def S(s):
    return (1 + s + s*s/3) * np.exp(-s)

def D(s):
    return (1/s) - (1+(1/s)) * np.exp(-2*s)

def X(s):
    return (1+s) * np.exp(-s)

def J(s):
    return (1/s) - (1/s + 11/8 + 3*s/4 + s*s/6) * np.exp(-2*s)

def A(s):
    return (6/s) * ((np.euler_gamma + np.log(s)) * S(s) * S(s) + sc.expi(-4*s) * S(-s) * S(-s) - 2 * sc.expi(-2 * s) * S(s) * S(-s))

def B(s):
    return (-25/8 + 23*s/4 + 3 * s * s + s*s*s/3) * np.exp(-2*s)

def K(s):
    return (A(s) - B(s))/5

def M(s):
    return 5/8

def L(s):
    return (s + 0.125 + 5 / (16*s)) * np.exp(-s) - (0.125 + 5 / (16*s)) * np.exp(-3*s)

# Energy estimates
def EHLplus(s):
    # Energy from Heitler-London approximation.
    # We return the energy minus 2 E1 and with an overall minus sign as the value of E1 is negative.
    return (2 / s) - 2 * (2 * D(s) - J(s) + 2 * X(s) * S(s) - K(s)) / (1 + S(s) * S(s))

def EHLminus(s):
    return (2 / s) - 2 * (2 * D(s) - J(s) - 2 * X(s) * S(s) + K(s)) / (1 - S(s) * S(s))

def Etwoionplus(s):
    # Energy from taking the product of two hydrogen molecule ion states.
    return (2/s) - 4 * (D(s) + X(s)) / (1+S(s)) + (J(s) + 2 * K(s) + M(s) + 4 * L(s)) / ((1+S(s)) * (1+S(s)))

def Etwoionminus(s):
    return (2/s) - 4 * (D(s) - X(s)) / (1-S(s)) + (J(s) + 2 * K(s) + M(s) - 4 * L(s)) / ((1-S(s)) * (1-S(s)))

def H12(s):
    # Additional term in the configuration interaction expectation value.
    return (1/((1-S(s)*S(s)))) * (M(s) - J(s))

def ECIplus(s):
    # Energy from configuration interaction optimization.
    return 0.5* (Etwoionplus(s) + Etwoionminus(s) - np.sqrt((Etwoionplus(s) - Etwoionminus(s))*(Etwoionplus(s) - Etwoionminus(s)) + 4 * H12(s) * H12(s)))

def ECIminus(s):
    return 0.5* (Etwoionplus(s) + Etwoionminus(s) + np.sqrt((Etwoionplus(s) - Etwoionminus(s))*(Etwoionplus(s) - Etwoionminus(s)) + 4 * H12(s) * H12(s)))

# Comparison of five energy estimates.

fig,ax = plt.subplots(figsize=(10,8))
lw = 2
fs2 = 14

s = np.linspace(0.1, 5, 500)

ax.plot(s, EHLplus(s), lw = lw, label=r'$E_{+}^{\mathrm{HL}}$')
ax.plot(s, EHLminus(s), lw = lw, label=r'$E_{-}^{\mathrm{HL}}$')
ax.plot(s, Etwoionplus(s), lw = lw, label=r'$E_{+}^{\mathrm{2ion}}$')
ax.plot(s, Etwoionminus(s), lw = lw, label=r'$E_{-}^{\mathrm{2ion}}$')
ax.plot(s, ECIplus(s), lw = lw, label=r'$E^{\mathrm{CI}}$')
ax.set_ylim(-0.25, 1)
ax.set_xlim(0,5)

ax.set_xlabel(r'$R/a$', fontsize = fs2)
ax.set_ylabel(r'$\frac{E - 2 E_1}{E_1}$', fontsize = fs2)

ax.legend(fontsize = fs2)

hline = {"color":"k","linestyle":":","linewidth":lw}

ax.axhline(y = 0, **hline)
