import numpy as np
import matplotlib.pyplot as plt
from myst_nb import glue

def S(s):
    return (1 + s + s*s/3) * np.exp(-s)

def D(s):
    return (1/s) - (1+(1/s)) * np.exp(-2*s)

def X(s):
    return (1+s) * np.exp(-s)

def Eplus(s):
    return (2/s) - 2 * (D(s) + X(s)) / (1 + S(s))

def Eminus(s):
    return (2/s) - 2 * (D(s) - X(s)) / (1 - S(s))

# Plot energies
s = np.linspace(.1, 5, 500)

fig, ax = plt.subplots(figsize=(6,4))

line1 = ax.plot(s, Eplus(s), label='$E_{+}$')
line2 = ax.plot(s, Eminus(s), label='$E_{-}$')

ax.axhline(y = 0, color = 'k', linestyle = ':')

ax.set_xlim(0,5)
ax.set_ylim(-0.2,1.5)
ax.set_xlabel('$R/a_0$', fontsize=16)
ax.set_ylabel('$\\frac{E-E_1}{E_1}$', fontsize=16)
ax.legend(fontsize=16)