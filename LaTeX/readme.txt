Notes on the book-tudelft-latex-v3-TI template

This LaTeX template is based on the TU Delft PhD thesis template, adapted for formatting ebooks. It consists of the following files:

ebook-tudelft.tex 	Basis TeX file, from which all other files are called.
book-tudelft.cls	Style sheet
book-tudelft.bib	Bibliography
folder/folder.tex	Various dependent files. To keep things organized, it can be useful to create a separate file for each piece, to be called from the main .tex file.


--- PAPER FORMAT ---

The stylesheet has two paper formats: "regular" (540 x 666 pt, or 190.5 x 234.95 mm) and A4 (210 x 297 mm). To get A4paper, use the [A4paper] option in document class.

You can make two versions with the same contents easily - make two main files, one with and one without "A4paper", and in the tex files with the content, whenever you need formatting that is dependent on paper size, include the following conditional:
\ifAfourpaper
	*** A4 specific formatting ***
\else
	*** Book specific formatting ***
\fi
(If you only need the A4, just leave out the \else; if you only need the book, leave the Afourpaper part empty).


--- COLORS ---

The stylesheet defines a number of "TU Delft house style colors" that you can use in the text:
{tudelft-cyan}{cmyk}{1,0,0,0}
{tudelft-black}{cmyk}{0,0,0,1}
{tudelft-white}{cmyk}{0,0,0,0}
{tudelft-sea-green}{cmyk}{0.54,0,0.32,0}
{tudelft-green}{cmyk}{1,0.15,0.4,0}
{tudelft-dark-blue}{cmyk}{1,0.66,0,0.4}
{tudelft-purple}{cmyk}{0.98,1,0,0.35}
{tudelft-turquoise}{cmyk}{0.82,0,0.21,0.08}
{tudelft-sky-blue}{cmyk}{0.45,0,0.06,0.06}
{tudelft-lavendel}{cmyk}{0.45,0.2,0,0.07}
{tudelft-orange}{cmyk}{0.02,0.56,0.84,0}
{tudelft-warm-purple}{cmyk}{0.58,1,0,0.02}
{tudelft-fuchsia}{cmyk}{0.19,1,0,0.19}
{tudelft-bright-green}{cmyk}{0.36,0,1,0}
{tudelft-yellow}{cmyk}{0.02,0,0.54,0}

By default, titles, links, and internal references are set in cyan. For grayscale printing, you can specify the option [print] in the document class, which will also add a bleed to the paper.


--- THUMBS ---

The stylesheet, copying from the TU Delft thesis format, includes thumbs at the side of the page. These thumbs roll over after 12 for the book format, and 15 for the A4paper format. If you want the thumbs to roll over at a different point, find the appropriate place in the style sheet. If you want no thumbs, just leave out the \thumbtrue in the main document. NB: The style sheet can currently handle up to 24 / 30 chapters; if you have more, you need to add in an additional optional yourself.


--- FONTS ---

The stylesheet for the PhD thesis comes with a set of "house style fonts", which are now obsolete. They didn't work under PDFLaTeX anyway. I've kept them in in case you like them, but to employ you'll have to use XeTeX.

The default font set by the stylesheet is Utopia Regular with Fourier math. We use Latin Modern as a fallback, since Utopia lacks support for sans-serif and monospace. Other options include Palatino for both text and math, or Palatino for text and Euler for math. To get these, uncomment the relevant lines in the style sheet.


--- PACKAGES and NEWCOMMANDS ---

The stylesheet and basis .tex file both load a number of packages that are all part of a standard LaTeX installation. I've provided a bunch of additional commands that I find useful, including \bvec{} for a bold vector (rather than one with an arrow) for both Latin and Greek letters.


--- TITLE AND AUTHOR ---

The main .tex file has a \title{} and an \author{}{}, which are used throughout, so you only have to set them once.


--- BIBLIOGRAPHY ---

Same as for TU Delft PhD thesis, except collected at the end (I never understood why the default for theses is per chapter anyway).


--- INDEX ---

The command \printindex in the main document prints the index. To include a term in the index, add \index{keyword} at the appropriate part in the text (note that "keyword" will be the index entry, but not printed in the text at that point). Additional options that are useful:
\index{keyword|textbm}	print the page number in bold in the index (e.g. to indicate definition of a term)
\index{keyword|textit} 	print the page number in italic in the index
\index{keyword@\textbf{keyword}}	print the keyword in bold in the index. Can also be used with \textit or other formatting, and with accents (\index{ecole@\'ecole}.
\index{keyword!subentry}	create a sub-entry "subentry" under the keyword "keyword".
\index{keyword|see {otherkeyword}}	cross-ref: creates entry "keyword, see otherkeyword".
More details at https://en.wikibooks.org/wiki/LaTeX/Indexing








