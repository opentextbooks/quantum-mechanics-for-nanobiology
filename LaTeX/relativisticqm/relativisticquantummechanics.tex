\chapter{Relativistic quantum mechanics}
\label{ch:relativisticcquantummechanics}

So far, our quantum mechanical descriptions have been of massive particles with velocities much lower than the speed of light. This formalism gets us a long way: we can use it to describe all of chemistry, and, by extension, it has many applications in biology. Nonrelativistic quantum mechanics also gives us basic physics insights, like in the emission spectra of atoms, and engineering applications including lasers, NMR, qubits, and quantum computers. However, it intrinsically cannot give us a description of light, while light does have an innate quantum nature, and it was the quantum nature of light that triggered the quantum revolution. Because light, inevitably, travels at the speed of light, we will need to include relativistic effects in our theory if we want it to describe light (and all processes where light interacts with matter), not just as a correction like in our discussion of the fine structure of hydrogen in section~\ref{sec:hydrogenfinestructure}, but at the basis of our theory. In this theory, we will combine quantum mechanics with the special theory of relativity. Its ultimate form, quantum chromodynamics (QCD), is very powerful and extremely accurate, combining three of the four known fundamental forces. It is however not complete: gravity isn't part of the theory, and at present nobody knows how to integrate QCD with the general theory of relativity.

\section{The central equation of relativistic quantum mechanics}
\label{sec:relativisticqmcentraleq}

\subsection{The Klein-Gordon equation}
\label{sec:KleinGordonequation}
A first attempt at constructing a base equation for relativistic quantum mechanics could be to `quantize' the special theory of relativity. This attempt is based on the observation that the Schr\"odinger equation, after a fashion, can be seen as the `quantization' of the classical equation for conservation of energy:
\begin{equation}
\label{classicalenergyconservation}
E = K + V = \frac{p^2}{2m} + V.
\end{equation}
To turn equation~(\ref{classicalenergyconservation}) into a quantum one, we apply the same procedure we used to arrive at quantum-mechanical analogs of the angular momentum (see section~\ref{sec:angularmomentumoperators}): we replace the momentum and energy with quantum operators:
\begin{equation}
\label{momentumenergyquantization}
\bvec{p} \to i \hbar \bvec{\nabla} \qquad \mbox{and} \qquad E \to i \hbar \frac{\partial}{\partial t}.
\end{equation}
We also replace the potential $V$ with the potential energy operator $\hat{V}$. If we make these substitutions in equation~(\ref{classicalenergyconservation}) and then have both sides act on a wave function $\Psi(\bvec{x}, t)$, we indeed arrive at the Schr\"odinger equation:
\begin{equation}
i \hbar \diff{\Psi}{t} = - \frac{\hbar^2}{2m} \nabla^2 \Psi + \hat{V} \Psi.
\end{equation}
Note that this procedure does not give us a true `derivation' of the Schr\"odinger equation (we still need axiom~\ref{axiom:SE}), as the `quantization' recipe in equation~(\ref{momentumenergyquantization}) follows from the Schr\"odinger equation. However, it does give us an idea about how we could extend quantum mechanics to relativistic systems, as we know that in relativity, the energy equation gets an extra term (see equation~(\ref{relativsiticenergymomentumequation})):
\begin{equation}
\label{relativsiticenergymomentumequation2}
E^2 = m^2 c^4 + p^2 c^2.
\end{equation}
Giving equation~(\ref{relativsiticenergymomentumequation2}) the same `quantization treatment' as equation~(\ref{classicalenergyconservation}), we arrive at
\begin{equation}
\label{KleinGordoneq}
-\frac{1}{c^2} \frac{\partial^2 \psi}{\partial t^2} + \nabla^2 \psi = \frac{m^2 c^2}{\hbar^2} \psi.
\end{equation}
Equation~(\ref{KleinGordoneq}) is known as the \index{Klein-Gordon equation}\emph{Klein-Gordon equation}. Unlike the Schr\"odinger equation, it is a proper wave equation, and it is the correct relativistic quantum equation for spin-$0$ particles. Unfortunately however, the particles of interest all have nonzero spin, and we need a more general form.

\subsection{Four-vectors}
In equation~(\ref{relativsiticenergymomentumequation2}), the momentum $p$ is the `three-momentum', the (length of) the classical momentum vector $\bvec{p}$. In relativity theory, we work with four-vectors, which describe relativistic quantities in the four dimensions of spacetime, reflecting the relativistic notion that time is no longer `just' a parameter, but a dimension, and transformations from one (inertial) frame of reference to another affect both the spatial and temporal coordinates. In short, four-vectors get an extra (`zeroth') component, which for the position vector represents the time, and for the momentum vector the energy (times factors of $c$, the speed of light, which is a universal constant):
\begin{equation}
\label{fourvectors}
\fvec{x} = \fourvec{ct}{x}{y}{z} = \fourvec{x^0}{x^1}{x^2}{x^3} \qquad \mbox{and} \qquad \fvec{p} = \fourvec{E/c}{p_x}{p_y}{p_z} = \fourvec{p^0}{p^1}{p^2}{p^3}.
\end{equation}
When making a transformation from one inertial frame to another (e.g. from that of an observer on a platform, to that of an observer on a train moving at constant velocity), the coordinates change according to the Lorentz transformations, $\fvec{x}' = \bvec{L} \fvec{x}$, which can be expressed in matrix form:
\begin{equation}
\label{Lorentztransformations}
\fourvec{x'^0}{x'^1}{x'^2}{x'^3} = \begin{pmatrix}
\gamma(u) & - \gamma(u) \frac{u}{c} & 0 & 0 \\
- \gamma(u) \frac{u}{c} & \gamma(u) & 0 & 0 \\
0 & 0 & 1 & 0 \\
0 & 0 & 0 & 1
\end{pmatrix} \fourvec{x^0}{x^1}{x^2}{x^3}
\end{equation}
for a transformation between a stationary frame $S$ with coordinates $\bvec{x}$ and a frame $S'$ with coordinates $\bvec{x}'$ moving in the positive $x$ direction with speed $u$ with respect to frame $S$. Here $\gamma(u)$ is the contraction factor from special relativity,
\begin{equation}
\gamma(u) = \frac{1}{\sqrt{1-(u/v)^2}}.
\end{equation}

Relativistic four-vectors form a space which is close but not equal to $\mathbb{R}^4$, as they have an inner product\footnote{You might (rightly) protest that equation~(\ref{fourvecinnerproduct}) does not actually define an inner product, as clearly by this definition we can have a nonzero four-vector of which the inner product with itself is zero, or even negative, and thus the vector would have zero or imaginary length. We indeed distinguish three types of four-vectors: those whose inner product with themselves is positive (timelike), zero (lightlike) and negative (spacelike). The trajectory of a ray of light is always described by a lightlike four-vector, while that of a massive particle is described by a timelike four-vector; a spacelike four-vector trajactory would correspond to a particle traveling faster than light, violating causality.} which is defined\footnote{The inner product of equation~(\ref{fourvecinnerproduct}) represents the flat (known as `Minkowsky') spacetime of special relativity. In general relativity, spacetime can be deformed by mass or energy, leading to a generalized form of equation~(\ref{fourvecinnerproduct}), which includes a metric tensor $g_{\mu \nu}$, representing the shape of spacetime.} differently\footnote{An equivalent definition would be to put the minus sign in front of the time components, and have plus signs for the space components. Naturally the interpretation of positive and negative lengths then swaps, but the physics is not affected. Unfortunately, this is one of the cases where neither possible convention has won out, and so you can find both options in books and articles; you'll just have to check which choice the authors made.}:
\begin{equation}
\label{fourvecinnerproduct}
\fvec{x} \cdot \fvec{y} = x^0 y^0 - x^1 y^1 - x^2 y^2 - x^3 y^3.
\end{equation}
An easy calculation shows that the `length' of a four-vector (the quantity $\fvec{x} \cdot \fvec{x}$), and by extension, the inner product between any two four-vectors, is invariant under Lorentz transformations.

To distinguish between three-vectors and four-vectors, components of three-vectors are indicated with Roman indices like $p_i$, while those of four-vectors are indicated with Greek ones, like $p^\mu$. The upper index represents a (standard) column-vector like configuration (known as the contravariant components of the vector). We also have a version with a lower index, corresponding to a row-vector like configuration (known as the covariant components); the inner product can be written as $\fvec{x} \cdot \fvec{y} = x_\mu y^\mu = x^\mu y_\mu$, where the sum over $\mu$ (ranging from $0$ to $3$) is implicit\footnote{This implicit summation over repeated indices is the famous Einstein summation convention, jokingly referred to by Einstein himself as `his biggest contribution to science'.}. Within special relativity, the covariant components of a vector are the same as the contravariant ones, except for a minus sign on the space components: $x_0 = x^0$, but $x_i = - x^i$. We can summarize these relations using the \index{metric tensor}\emph{metric tensor}, which for special relativity is usually written as $\eta^{\mu \nu}$ (and its inverse, $\eta_{\mu \nu}$), to distinguish from the general relativity version $g^{\mu \nu}$. Using the Einstein summation convention, the metric tensor is defined through
\begin{equation}
\label{metricdef}
\fvec{x} \cdot \fvec{y} = x^\mu y_\mu = x_\mu y^\mu = \eta_{\mu \nu} x^\mu y^\nu = \eta^{\mu \nu} x_\mu y_\nu,
\end{equation}
from which we can read off that
\begin{equation}
\label{SRTmetric}
\eta^{00} = 1, \quad \eta^{11} = \eta^{22} = \eta^{33} = -1, \quad \eta^{\mu \nu} = 0 \;\;\mbox{if}\;\; \mu \neq \nu.
\end{equation}
The coefficients of $\eta_{\mu \nu}$ are the same as those of $\eta^{\mu \nu}$.

Derivatives can be taken with respect to any of the four components of the position (or time-position) vector; in short-hand notation, we have
\begin{align}
\label{fourvecderivative}
\partial_\mu f &= \frac{\partial f}{\partial x^\mu} \\
\label{dAlembertian}
\square f = \partial^\mu \partial_\mu f &= \frac{1}{c^2} \frac{\partial^2 f}{\partial t} - \nabla^2 f.
\end{align}
Equation~(\ref{fourvecderivative}) thus generalizes the partial derivative, and~(\ref{dAlembertian}) the Laplacian; the operator $\square$ is known as the \index{d'Alembertian}\emph{d'Alembertian}.

%The momentum (or energy-momentum) four vector follows from the position (or time-position) four-vector in the usual way: it is the mass times the derivative of the position-four vector to time. However, we need to take the derivative with respect to \emph{proper time} (often denoted by $\tau$), meaning time as measured on a clock moving with the particle:
%\begin{equation}
%\fvec{p} = m \frac{\dd \fvec{x}}{\dd \tau}.
%\end{equation}
%A straightforward calculation then shows that the length of the momentum four-vector is fixed, which is a simpler statement 
In terms of four-vectors, we can re-write equation~(\ref{relativsiticenergymomentumequation2}) in (even) more concise form:
\begin{equation}
\label{relativsiticenergymomentumequation3}
\fvec{p} \cdot \fvec{p} = m^2 c^2.
\end{equation}
If we now apply the `quantization recipe' of equation~(\ref{momentumenergyquantization}) to our four-vectors, we get
\begin{subequations}
\label{fourmomentumquantization}
\begin{align}
p_\mu &\to i \hbar \partial_\mu = i \hbar \frac{\partial}{\partial x^\mu}\\
p_0 &\to i \hbar \partial_0 = \frac{i\hbar}{c} \frac{\partial}{\partial t} \qquad \mbox{and} \qquad p_i \to i \hbar \partial_i = i \hbar \frac{\partial}{\partial x^i}.
\end{align}
\end{subequations}
Unsurprisingly, just substituting the `quantization' of the four-momentum in equation~(\ref{relativsiticenergymomentumequation3}) and have it act on a wave function again gives us the Klein-Gordon equation, albeit in more concise form:
\begin{equation}
\label{KleinGordonfourvectorform}
-\hbar^2 \square \psi = -\hbar^2 \partial^\mu \partial_\mu \psi = m^2 c^2 \psi.
\end{equation}
However, you might now guess where things go wrong: rather than `applying' $\fvec{p} \cdot \fvec{p}$, which only gives us the magnitude of the four-momentum, we'll want each individual component, and will therefore have to `factorize' equation~(\ref{relativsiticenergymomentumequation3}) to get a more detailed view.

\subsection{The Dirac equation}
\label{sec:Diracequation}
To motivate why we'd want to factorize equation~(\ref{relativsiticenergymomentumequation3}), let's consider the case of a stationary particle\footnote{Such a particle obviously can't be a light particle, as those always move. But (from a relativistic perspective), as long as the particle is not accelerating, we can always make a transformation to a co-moving frame, in which the particle will indeed be at rest.}, for which the three-momentum is zero, and we only have one nonzero component of the four-momentum, $p^0$, directly related to its energy. In that case, equation~(\ref{relativsiticenergymomentumequation3}) simplifies to
\begin{equation}
0 = p^0 p_0 - m^2 c^2 = (p^0)^2 - m^2 c^2 = (p^0 + mc)(p^0 - mc),
\end{equation}
where we used that $p_0 = p^0$ (i.e., the zeroth component of the covariant and contravariant representations are identical) in special relativity. We find that we have two solutions: either $p^0 = mc$ or $p^0 = -mc$. As $p^0$ is the energy of our particle, classically we'd dismiss the second solution, but as we'll see below, we will in fact always get two solutions in relativistic quantum mechanics.

Unfortunately, the factorization for a moving particle is more involved, because with the extra components we get cross terms, and moreover the covariant and contravariant components are no longer identical. Introducing new components $\beta^\nu$ and $\gamma^\lambda$ (so four each, for $\nu, \lambda = 0, 1, 2, 3$), we can formally proceed with the factorization:
\begin{align}
\label{relativsiticenergymomentumfactorization}
0 &= \fvec{p} \cdot \fvec{p} - m^2 c^2 = p^\mu p_\mu - m^2 c^2 \nonumber \\
&= \left( \beta^\nu p_\nu + mc \right) \left( \gamma^\lambda p_\lambda - mc \right) \nonumber \\
&= \beta^\nu \gamma^\lambda p_\nu p_\lambda - mc \left(\beta^\nu - \gamma^\nu \right) p_\nu - m^2 c^2
\end{align}
Note that the first term in the last line of~(\ref{relativsiticenergymomentumfactorization}) is a sum over sixteen terms, and the second a sum over four terms. The second term however should vanish, as in the original sum in the first line of~(\ref{relativsiticenergymomentumfactorization}) there are no linear terms in the momentum. Therefore, we have $\beta^\nu = \gamma^\nu$, and we're left with four unknowns, the coefficients $\gamma^\nu$, which satisfy $p^\mu p_\mu = \gamma^\nu \gamma^\lambda p_\nu p_\lambda$. By writing out the four terms on the left and sixteen terms on the right of this equation, we get
\begin{subequations}
\label{Diraceqcoefficients1}
\begin{align}
&\left(\gamma^0\right)^2 = 1, \qquad \left(\gamma^1\right)^2 = \left(\gamma^2\right)^2 = \left(\gamma^3\right)^2 = -1, \\
&\gamma^\nu \gamma^\lambda + \gamma^\lambda \gamma^\nu = 0 \qquad \mbox{if}\; \nu \neq \lambda.
\end{align}
\end{subequations}
We can summarize equations~(\ref{Diraceqcoefficients1}) using the \emph{anticommutator}, $\{a, b\} = ab + ba$, and the metric tensor $\eta^{\mu \nu}$:
\begin{equation}
\label{Diraceqcoefficients}
\left\{ \gamma^\mu, \gamma^\nu \right\} = 2 \eta^{\mu \nu}.
\end{equation}
There is no solution of equations~(\ref{Diraceqcoefficients}) in terms of numbers. However, there are solutions in which the coefficients $\gamma^\mu$ are matrices. The smallest solutions are $4 \times 4$ matrices, which can be expressed in terms of the $2 \times 2$ identity matrix $I_2$, the $2 \times 2$ Pauli spin matrices $\sigma^i$, and the $2 \times 2$ zero matrices $0_2$:
\begin{equation}
\label{Diraceqcoefficientmatrices}
\gamma^0 = \begin{pmatrix}
I_2 & 0_2 \\ 0_2 & I_2
\end{pmatrix}, \qquad 
\gamma^i = \begin{pmatrix}
0_2 & \sigma^i \\ -\sigma^i & 0_2
\end{pmatrix}.
\end{equation}
With these matrices as the coefficients, we can finally factorize equation~(\ref{relativsiticenergymomentumfactorization}), `quantize' the momenta $p_\mu$, and have them act on a quantum function $\psi$, which gives us the \index{Dirac equation}\emph{Dirac equation}:
%\footnote{You may sometimes encounter the shorthand $\partialslash \psi$ for $\gamma^\mu \partial_\mu \psi$, but we'll stay clear of that here.}:
\begin{equation}
\label{Diraceq}
i \hbar \gamma^\mu \partial_\mu \psi - m c \psi = 0,
\end{equation}
where
\begin{equation}
\label{defbispinor}
\psi = \fourvec{\psi_1}{\psi_2}{\psi_3}{\psi_4}
\end{equation}
is known as the \index{bispinor}\emph{bispinor} or Dirac spinor. Note that $\psi$ is not a four-vector; it simply contains four pieces of information that can be cast in (regular) vector form.





%In terms of the quantized operators, equation~(\ref{relativsiticenergymomentumequation3}), when acting on a wave function~$\psi(\fvec{x})$, becomes 
%\begin{subequations}
%\label{KleinGordon}
%\begin{align}
%-\hbar^2 \square \psi = -\hbar^2 \partial^\mu \partial_\mu \psi &= m^2 c^2 \psi \\
%-\frac{1}{c^2} \frac{\partial^2 \psi}{\partial t^2} + \nabla^2 \psi &= \frac{m^2 c^2}{\hbar^2} \psi.
%\end{align}
%\end{subequations}
%Equation~(\ref{KleinGordon}) (in either form) is known as the \index{Klein-Gordon equation}\emph{Klein-Gordon equation}. Unlike the Schr\"odinger equation, it is a proper wave equation, and it is the correct relativistic quantum equation for spin-$0$ particles. Unfortunately however, the particles of interest all have nonzero spin, and we need a more general form.

%\section{The Dirac equation}
%\label{sec:Diracequation}
