Figures for QM for NB

Remake in python

Figure 2.1 1D particle in a box solutions
Figure 2.5 Radial part of wave function (already done, see under notebooks)
Figure 2.6 Infinite well with internal barrier (in problem 2.14)
Figure 3.3 Stern-Gerlach (can also be made prettier I think)

Figure 4.2 spin-orbit coupling
Figure 4.3 Energy levels with fine structure splitting - see work-in-progress version in twodimharmoscperturbationenergies.pdf under notebooks
Figure 5.1 Coordinates
Figure 5.2 Energy of LCAO of hydrogen molecule ion
Figure 5.3 Energies of hydrogen molecule (NB: This version is not copyright-proof)
Figure 5.4 Molecular orbital energy levels (NB: This version is not copyright-proof)

Figure 5.7 Fermi surface
Figure 5.8 Conductance bands
Figure 5.9 Elliptical coordinates
Figure 5.10 lines of constant \mu and \nu

Maybe remake (see if possible without losing info, otherwise convert to svg)

Figure 1.1 emission spectrum
Figure 1.3 Young double slit exp
Figure 1.4 Atom models

Figure 2.2 Wave packets

Figure 4.1(a) (see if we can somehow split the image without affecting numbering?)


Create svg image 

Figure 3.2 angular momentum change (get fig a original for remake)

Figure 3.6 (NB: I have an svg version of the periodic table that this image is based on, re-make the svg with that one).

Figure 5.5 Water molecule (Check copyright; I made this image but it's very close to one from Atkins).
Figure 5.6 Butadiene energy levels (Check copyright; I made this image but it's very close to one from Atkins).


Questions

1. Interactive: these would be nice:

NEW: Figure with worked example in section 2.2.1 - show the wave function A x (a-x) (normalized, see text) and the approximation with well functions with steps in n.

2.5 select which ones to show.

5.3b with and without configuration interactions

5.4 fill up, starting from Li2. 

5.8 show / hide the conductor and insulator. Could be also in (a). Note that the black bands in (b) and the gray ones in (a) are identical (probably good to make them same color...)

2. Coordinates indeed perhaps not necessary (4.1a, 5.1, 5.9). 4.2 can also be just converted to svg.  5.8b nice to make interactive with part a. 5.4 needs remake as not copyright-proof, would be nice to make filling it up interactive (see above). 

3. Figure 3.6 is the periodic table (page 76). I have an svg version (see content/images/angularmomentum on gitlab), but somehow that version sometimes puts all texts in italics.

4. Point is moot as I checked and saw I already made an svg version (also in 
content/images/angularmomentum on gitlab). What I meant was that we should work with a 'raw' version of the globe in figure a, not just pixelate the one I have now.

5. It'd be good to be consistent in figure numbers between the notes / pdf version and the interactive textbook. In the book I put 4.1 (a) and (b) together to save space, that is less necessary / useful in the online version. But if we make 4.1(b) a separate figure, all references change. So perhaps best to keep them together for now.

6. Fine to keep these for now.

NB: I have svg versions of self-made figures of chapters 1-3 already (see content/images/introduction, SE, angularmomentum). Some could do with remake in python and/or animation.




