%Problemtitle: Bound state of the Dirac delta potential
%Source: Griffiths section 2.5.2 first part
In this problem, we'll find the bound state of the Dirac delta potential well (and prove that there is only one). \ifproblemset The potential is thus $\alpha \delta(x)$, with the restriction that $\alpha < 0$ (as for positive values of $\alpha$ we have a barrier, not a well, and hence no bound states). The time-independent Schr\"odinger equation then reads:
\begin{equation}
\label{SEdeltapot}
-\frac{\hbar^2}{2m} \dv[2]{\psi}{x} + \alpha \delta(x) \psi = E \psi.
\end{equation}
\else Our potential is the same as in equation~(\bookref{Diracdeltapotential}), with the restriction that $\varepsilon < 0$ (as for positive values of $\varepsilon$ we have a barrier, not a well, and hence no bound states). The time-independent Schr\"odinger equation then reads:
\begin{equation}
\label{SEdeltapot}
-\frac{\hbar^2}{2m} \dv[2]{\psi}{x} + \varepsilon \delta(x) \psi = E \psi.
\end{equation}
\fi
As we're looking for bound states, we want solutions with $E<0$.
\begin{enumerate}[(a)]
\item As $E<0$, we can define the (real, positive) number $\kappa = \sqrt{-2mE}/\hbar$. Re-write equation~(\ref{SEdeltapot}) in terms of $\kappa$.
\item Find the general solution to~(\ref{SEdeltapot}) for $x<0$.
\item Also find the general solution to~(\ref{SEdeltapot}) for $x>0$.
\item Of the solutions in (b) and (c), cross out the ones that diverge.
\item Apply the condition that $\psi(x)$ be continuous at $x=0$ to eliminate one of the constants in your solutions.
\item \ifproblemset Apply the boundary condition on the derivative of $\psi(x)$
\begin{equation}
\Delta \left( \dv{\psi}{x} \right) = \lim_{\epsilon \to 0} \left[ \left.\dv{\psi}{x} \right|_{x=\epsilon} - \left.\dv{\psi}{x}\right|_{x=-\epsilon} \right] = \frac{2m}{\hbar^2} \lim_{\epsilon \to 0} \int_{-\epsilon}^\epsilon V(x) \psi(x) \ddx.
\end{equation}
\else
Apply the condition~(\bookref{wavefunctionderivativejump}) on the derivative of $\psi(x)$\fi to find the allowed value of $\kappa$, and hence of $E$.
\item Normalize the solution.
\item Plot the wavefunction and the probability density.
\end{enumerate}

\ifincludesolutions
\Solution
\begin{enumerate}[(a)]
\item We get
\ifproblemset
\begin{align*}
\dv[2]{\psi}{x} - \frac{2m\alpha}{\hbar^2} \delta(x) \psi = - \frac{2mE}{\hbar^2} \psi = \kappa^2 \psi
\end{align*}
\else
\begin{align*}
\dv[2]{\psi}{x} - \frac{2m\varepsilon}{\hbar^2} \delta(x) \psi = - \frac{2mE}{\hbar^2} \psi = \kappa^2 \psi
\end{align*}
\fi
\score{2 pt}.
\item For $x<0$, the delta function is identically zero, and we can find the solution to~(\ref{SEdeltapot}) through the Ansatz $e^{\lambda x}$. Substituting, we get a characteristic equation for $\lambda$:
\begin{align*}
\lambda^2 e^{\lambda x} &= \kappa^2 e^{\lambda x} \\
\lambda &= \pm \kappa
\end{align*}
\score{1 pt} and hence
\begin{align*}
\psi_-(x) &= A e^{\kappa x} + B e^{-\kappa x}
\end{align*}
\score{1 pt}.
\item The steps are identical to those in (b), giving as solution
\begin{align*}
\psi_+(x) &= C e^{\kappa x} + D e^{-\kappa x}
\end{align*}
\score{1 pt}.
\item For $x \to -\infty$, the second term in $\psi_-$ diverges, so we set $B=0$ \score{1 pt}. For $x \to \infty$, the first term in $\psi_+$ diverges, so we set $C=0$ \score{1 pt}.
\item We get
\begin{align*}
\lim_{\epsilon \uparrow 0} \psi_-(x) = A = \lim_{\epsilon \downarrow 0} \psi_+(x) = D
\end{align*}
\score{1 pt}.
\item We have
\begin{align*}
\lim_{x \uparrow 0} \dv{\psi_-}{x} &= \lim_{x \uparrow 0} \kappa A e^{\kappa x} = \kappa A, \\
\lim_{x \downarrow 0} \dv{\psi_+}{x} &= \lim_{x \downarrow 0} \left(-\kappa A e^{-\kappa x}\right) = -\kappa A
\end{align*}
\score{1 pt}. Therefore
\begin{align*}
\Delta \left( \dv{\psi}{x} \right) &= -2\kappa A = \frac{2m\varepsilon}{\hbar^2} A, \\
\kappa &= -\frac{m\varepsilon}{\hbar^2} \qquad \mbox{(note that } \varepsilon < 0, \;\mbox{so } \kappa > 0 \mbox{)}, \\
E &= - \frac{\hbar^2 \kappa^2}{2m} = - \frac{m \varepsilon^2}{2 \hbar^2}
\end{align*}
\score{2 pt}.
\item We have
\begin{align*}
1 &= \braket{\psi|\psi} = \int_{-\infty}^\infty |\psi(x)|^2 \dd{x} = 2 |A|^2 \int_0^\infty e^{-2\kappa x} \dd{x} = \frac{|A|^2}{\kappa} \\
A &= \sqrt{\kappa} = \frac{\sqrt{-m\varepsilon}}{\hbar}
\end{align*}
\score{2 pt}.
\item See figure below, with $x$ in units of $\sqrt{\kappa}$ \score{2 pt}.
\begin{center}
\includegraphics[scale=1]{SE/problems/deltawellwavefunctionplot}
\end{center}
\end{enumerate}
\fi



