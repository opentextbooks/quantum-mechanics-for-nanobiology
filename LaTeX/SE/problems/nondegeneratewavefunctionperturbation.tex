% Problemtitle: Corrections to the wave function and energy in non-degenerate perturbation theory 
In this problem, we'll derive the expressions~(\bookref{firstorderperturbationwavefunction}) and~(\bookref{secondorderperturbationenergy}) for the first-order correction to the wave function and the second-order correction to the energy of a non-degenerate system. First, we re-write equation~(\bookref{SEfirstorderperturbation}) as 
\begin{equation}
\label{SEfirstorderperturbationaspde}
\left( \hat{H}^0 - E_n^0 \right) \psi_n^1 = - \left( \hat{H}' - E_n^1 \right) \psi_n^0.
\end{equation}
We already have an expression for $E_n^1$, and we assumed we knew~$\psi_n^0$, so the right-hand side of equation~(\ref{SEfirstorderperturbationaspde}) is a known function, and we're left with a differential equation for the unknown function~$\psi_n^1$. As the $\psi_n^0$ are the eigenfunctions of the unperturbed Hamiltonian~$\hat{H}^0$, which is a Hermitian operator, they form a complete set, and we can therefore express $\psi_n^1$ as a linear combination of the $\psi_n^0$:
\begin{equation}
\label{perturbedwavefunctionexpansion}
\psi_n^1 = \sum_{k=0}^\infty c_k \psi_k^0.
\end{equation}
\begin{enumerate}[(a)]
\item Argue why we can leave out the term with $k=n$ in the sum in equation~(\ref{perturbedwavefunctionexpansion}).
\item Substitute~(\ref{perturbedwavefunctionexpansion}), without the $k=n$ term, into equation~(\ref{SEfirstorderperturbationaspde}), then take the inner product with $\psi_m^0$, to show that you get $0$ if $m=n$ and, for $m \neq n$,
\begin{equation}
\label{firstorderperturbationwavefunctioncoefficient}
\left( E_m^0 - E_n^0 \right) c_m = - \Braket{\psi_m^0 | \hat{H}' | \psi_n^0}.
\end{equation}
\item From equations~(\ref{firstorderperturbationwavefunctioncoefficient}) and~(\ref{perturbedwavefunctionexpansion}), derive equation~(\bookref{firstorderperturbationwavefunction}).
\item Repeat the calculation in section~\bookref{sec:nondegenerateperturbationtheory}, now retaining terms to order~$\varepsilon^2$, to derive equation~(\bookref{secondorderperturbationenergy}) for the  second-order correction to the energy. You'll need to use the fact that (by part (a)) $\psi_n^0$ and $\psi_n^1$ are orthogonal.
\end{enumerate}


\ifincludesolutions
\Solution
\begin{enumerate}[(a)]
\item By equation~(\bookref{perturbedwavefunction}), if $\psi_n^1$ contains a multiple of $\psi_n^0$, we can simply absorb it into $\psi_n^0$. (Note that the presence of such a term would require us to re-normalize $\psi_n^0$).
\item Substitution of~(\ref{perturbedwavefunctionexpansion}) for $k \neq n$  into~(\ref{SEfirstorderperturbationaspde}) gives
\begin{align*}
\sum_{k \neq n} \left( E_k^0 - E_n^0 \right) c_k \psi_k^0 = - \left( \hat{H}' - E_n^1 \right) \psi_n^0.
\end{align*}
Taking the inner product with $\psi_m^0$ we get
\begin{align*}
\sum_{k \neq n} \left( E_k^0 - E_n^0 \right) c_k \Braket{\psi_m^0|\psi_k^0} = - \Braket{\psi_m^0|\hat{H}'|\psi_n^0} + E_n^1 \Braket{\psi_m^0|\psi_n^0}.
\end{align*}
If $m=n$, the inner product on the left-hand side vanishes, because we excluded the term with $k=n$ from the expansion (so $c_n = 0$), and the right-hand side reduces to equation~(\bookref{firstorderperturbationenergy}). For all other terms, the second term on the right-hand side is zero, and the inner product on the left hand side reduces to $\delta_{mk}$, so we get
\begin{align*}
\left( E_m^0 - E_n^0 \right) c_m &= - \Braket{\psi_m^0|\hat{H}'|\psi_n^0} \\
c_m &= - \frac{\Braket{\psi_m^0|\hat{H}'|\psi_n^0}}{E_m^0 - E_n^0}.
\end{align*}
\item Using our found expression for $c_m$, and the exclusion of $m = n$, equation~(\bookref{firstorderperturbationwavefunction}) immediately follows.
\item The second-order terms from equation~(\bookref{perturbedSEexpansion}) give
\begin{align*}
\hat{H}^0 \psi_n^2 + \hat{H}' \psi_n^1 &= E_n^0 \psi_n^2 + E_n^1 \psi_n^1 + E_n^2 \psi_n^0.
\end{align*}
We again take the inner product with $\psi_n^0$ to get
\begin{align*}
\Braket{\psi_n^0 | \hat{H}^0 \psi_n^2} + \Braket{\psi_n^0 | \hat{H}' \psi_n^1} &= E_n^0 \Braket{\psi_n^0 | \psi_n^2} + E_n^1 \Braket{\psi_n^0 | \psi_n^1} + E_n^2 \Braket{\psi_n^0 | \psi_n^0}, \\
E_n^0 \Braket{\psi_n^0 | \psi_n^2} + \Braket{\psi_n^0 | \hat{H}' | \psi_n^1} &= E_n^0 \Braket{\psi_n^0 | \psi_n^2} + E_n^1 \Braket{\psi_n^0 | \psi_n^1} + E_n^2 \\
E_n^2 &= \Braket{\psi_n^0 | \hat{H}' | \psi_n^1} - E_n^1 \Braket{\psi_n^0 | \psi_n^1}.
\end{align*}
The second term vanishes, as we absorbed any component in $\psi_n^1$ proportional to $\psi_n^0$ in $\psi_n^0$ itself, and the other components are perpendicular to $\psi_n^0$. We now use our expansion of $\psi_n^1$ in the first term, which gives
\begin{align*}
E_n^2 &= \Braket{\psi_n^0 | \hat{H}' | \psi_n^1} = \sum_{m \neq n} c_m \Braket{\psi_n^0 | \hat{H}' | \psi_m^0} = \sum_{m \neq n} \frac{\Braket{\psi_m^0 | \hat{H}' | \psi_n^0} \Braket{\psi_n^0 | \hat{H}' | \psi_m^0}}{E_n^0 - E_m^0},
\end{align*}
which is equation~(\bookref{secondorderperturbationenergy}).
\end{enumerate}
\fi