%Problemtitle: Three-dimensional harmonic oscillator potential
% Source?
We have already seen an application of the result of problem~\ref{pb:orbitalangularmomentum} in the Hamiltonian for the hydrogen atom, where the radial part got an additional `effective' term in the potential energy. Another application is the three-dimensional harmonic oscillator. The Hamiltonian for that system is given by
\begin{equation}
\label{3DharmoscHamiltonian}
\hat{H} = \frac{\hat{\bvec{p}^2}}{2m} + \frac12 m \omega^2 \hat{\bvec{r}}^2.
\end{equation}
If we write this Hamiltonian in Cartesian coordinates, we see that it separates into three one-dimensional harmonic oscillator Hamiltonians:
\begin{align*}
\hat{H} &= \hat{H}_x + \hat{H_y} + \hat{H}_z,
\end{align*}
where $\hat{H}_x = (\hat{p}_x^2/2m) + \frac12 m \omega^2 \hat{x}^2$, and similar for $y$ and $z$. As these three Hamiltonians commute with each other, the solutions of the Schr\"odinger equation with the three-dimensional Hamiltonian~(\ref{3DharmoscHamiltonian}) factorize, i.e. they can be written as
\begin{equation}
\psi_{n_x n_y n_z}(\bvec{r}) = \psi_{n_x}(x) \psi_{n_y}(y) \psi_{n_z}(z), 
\end{equation}
where the three functions of $x$, $y$ and $z$ are the solutions to the one-dimensional Schr\"o\-ding\-er equation with harmonic potentials.
\begin{enumerate}[(a)]
%\setcounter{enumi}{\value{problemcounter}}
\item Argue that the energies of the three-dimensional Schr\"odinger equation with a harmonic potential are given by
\[ E = \left(n_x + n_y + n_z + \frac32 \right) \hbar \omega. \]
\setcounter{problemcounter}{\value{enumi}}
\end{enumerate}
With equation~(\ref{radialLaplacian}), we can also write the three-dimensional Hamiltonian~(\ref{3DharmoscHamiltonian}) in terms of the radial and angular momentum operators:
\begin{equation}
\hat{H} = \frac{\hat{p}_r^2}{2m} + \frac{\hbar^2}{2m r} \hat{L}^2 + \frac12 m \omega^2 \hat{r}^2.
\end{equation}
The angular part of this Hamiltonian is completely covered by the angular momentum operator $\hat{L}^2$, so we may expect the solutions to separate into an angular part (which will again be the spherical harmonics) and a radial part, for which we can write an effective radial Hamiltonian:
\begin{equation}
\label{radialHamiltonian}
\hat{H}_l = \frac{\hat{p}_r^2}{2m} + \frac{\hbar^2 l (l+1)}{2m r} + \frac12 m \omega^2 \hat{r}^2
\end{equation}
The eigenfunctions of this radial Hamiltonian will be functions of $r$, and be characterized by quantum numbers $n$ and $l$. We shall write them as $R_{nl}(r)$.
\begin{enumerate}[(a)]
\setcounter{enumi}{\value{problemcounter}}
\item Relate the (radial) quantum number $n$ to the Cartesian quantum numbers $n_x$, $n_y$ and $n_z$.
\setcounter{problemcounter}{\value{enumi}}
\end{enumerate}
Like for the one-dimensional harmonic oscillator, the three-dimensional one has associated raising and lowering operators, defined as
\begin{equation}
\hat{A}_{l, \pm} = \frac{1}{\sqrt{2m\hbar\omega}} \left( \mp i \hat{p}_r - \frac{(l+1)\hbar}{r} + m \omega r \right).
\end{equation}
Unlike for the one-dimensional harmonic oscillator, $\hat{A}_{l, \pm}$ don't just change the energy, they also change the angular momentum. In particular, $\hat{A}_{l, -}$ lowers the energy by $\hbar \omega$ but raises the angular momentum quantum number $l$ by $1$:
\begin{equation}
\hat{A}_{l, -} R_{n, l} = \alpha_- R_{n-1, l+1},
\end{equation}
where $\alpha_-$ is a normalization constant. Now the eigenstates have two kinds of kinetic energy: radial and tangential. If the angular momentum goes up, so does the tangential velocity and thus kinetic energy; consequently, the $\hat{A}_{l, -}$ operator must decrease the radial kinetic energy. Therefore, the new orbital is more circular than the previous one. At some point, there will be no radial kinetic energy left, and the angular momentum is maximal; applying the lowering operator once more should then give a zero (as otherwise the radial kinetic energy becomes negative). This `lowest rung' solution, sometimes referred to as a `circular orbit', therefore satisfies (with $\mathcal{L}$ representing the maximum value of $l$):
\begin{equation}
\label{circularorbitode}
0 = \hat{A}_{\mathcal{L},-} R_{0, \mathcal{L}} = \left( \diff{}{r} + \frac{1}{r} - \frac{\mathcal{L}+1}{r} + \frac{m \omega}{\hbar} r \right) R_{0, \mathcal{L}}
\end{equation}
\begin{enumerate}[(a)]
\setcounter{enumi}{\value{problemcounter}}
\item Find and sketch the function describing the circular orbit (i.e., the solution of the ordinary differential equation~(\ref{circularorbitode})).
\end{enumerate}

\ifincludesolutions
\Solutions
\begin{enumerate}[(a)]
\item For each of the three one-dimensional Hamiltonians we have
\[ \hat{H}_\mathrm{(1D)} \psi_n(x) = E_n \psi_n(x) = \left(n+\frac12\right) \hbar \omega \psi_n(x). \]
Simply substituting these solutions in the three-dimensional equation with the factorized wavefunction, we obtain
\begin{align*}
\hat{H} \psi_{n_x n_y n_z}(\bvec{r}) &= \left(\hat{H}_x + \hat{H_y} + \hat{H}_z \right) \psi_{n_x}(x) \psi_{n_y}(y) \psi_{n_z}(z) \\
&= \psi_{n_y}(y) \psi_{n_z}(z) \hat{H}_x \psi_{n_x}(x) + \psi_{n_x}(x) \psi_{n_z}(z) \hat{H_y} \psi_{n_y}(y) \\
&\hspace{5cm} + \psi_{n_x}(x) \psi_{n_y}(y) \hat{H}_z \psi_{n_z}(z) \\
&=  \left( E_{n_x} + E_{n_y} + E_{n_z} \right) \psi_{n_x}(x) \psi_{n_y}(y) \psi_{n_z}(z) \\
&= \left( n_x + n_y + n_z + \frac32 \right) \psi_{n_x n_y n_z}(\bvec{r}),
\end{align*}
so the functions $\psi_{n_x n_y n_z}(\bvec{r})$ are indeed eigenfunctions of the three-dimensional Hamiltonian with the given energies \score{1 pt}.
\item The energy should not depend on the choice of coordinates. As we've seen in part (a), the energy of the three-dimensional harmonic oscillator depends on the sum of $n_x$, $n_y$ and $n_z$; different combinations that sum to the same value will have the same energy. Therefore, we will have different eigenvalues of the radial Hamiltonian only if the sum differs, and we can characterize those eigenvalues (and corresponding eigenfunctions) with the number $n = n_x + n_y + n_z$ \score{1 pt}.
\item We can solve the first-order equation~(\ref{circularorbitode}) by direct integration. First separating variables, we have
\begin{align*}
\frac{\dd R}{\dd r} &= \left(-\frac{1}{r} + \frac{\mathcal{L} + 1}{r} - \frac{m\omega}{\hbar} r \right) R = \left(\frac{\mathcal{L}}{r} - \frac{m\omega}{\hbar} r\right)R \\
\int \frac{1}{R}\,\dd R &= \int \left( \frac{\mathcal{L}}{r} - \frac{m\omega}{\hbar} r \right) \,\dd r \\
\log(R) &= \mathcal{L} \log(r) - \frac{m \omega}{2\hbar} r^2 + C\\
R(r) &= A \left[r^\mathcal{L} \exp\left(-\frac{m \omega}{2\hbar} r^2 \right) \right],
\end{align*}
where $A$ as usual is an integration constant that we can use to normalize the solution \score{1 pt}. Note that we get a typical length scale $b = \sqrt{\hbar/2 m \omega}$. Plots of the solution for $\mathcal{L} = 1$ and $\mathcal{L} = 4$ are shown in figure~\ref{fig:harmonicoscillatorradialwavefunction} \score{1 pt}.
\end{enumerate}
\begin{figure}[hb]
\centering
\includegraphics[scale=1]{problems/harmonicoscillatorradialwavefunction.pdf}
\caption{Normalized versions of the radial part of the circular orbit solution of the three-dimensional harmonic oscillator for $\mathcal{L} = 1$ (blue) and $\mathcal{L} = 4$ (orange). Radial variable in units of $b$, magnitude in units of the normalization~$A$.}
\label{fig:harmonicoscillatorradialwavefunction}
\end{figure}
\fi