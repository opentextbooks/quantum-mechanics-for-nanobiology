%Problemtitle An infinite square well with an internal barrier
%Source: Inspired by section 5.2 of Binney & Skinner.
% Midterm 2021
In this problem, we consider the infinite square well potential with an internal barrier. To be able to exploit the symmetry of the system, we'll shift the well such that it is centered around zero, and the potential reads
\begin{equation}
\label{wellwithbarrierpotential}
V(x) = \left\lbrace \begin{array}{ll}
V_0 &\; \mbox{for}\; |x|<a,\\
0 & \;\mbox{for}\; a<|x|<b,\\
\infty &\;\mbox{otherwise}.
\end{array} \right.
\end{equation}
For energies much larger than $V_0$, the eigenstates of the Hamiltonian corresponding to this potential will essentially be the same as those for an infinite well. For energies smaller than $V_0$ however, the region $-a < x < a$ will be classically forbidden. We did not solve the Schr\"odinger equation for such a finite barrier in class, but the solution method is straightforward, as the potential is piecewise constant. The pieces can then be glued together by the boundary condition that the wavefunction and its derivative must be continuous at $x=a$. Unfortunately, these conditions give us equations that we cannot solve analytically, and thus we are reduced to numerical methods. Those methods tell us that there are two solutions with energies very close together: the ground state, which is symmetric (or `even', like the cosine) and the first excited state which is antisymmetric (or `odd', like the sine). We call the ground state $\psi_\mathrm{e}$ with energy $E_\mathrm{e}$, and the excited state $\psi_\mathrm{o}$ with energy $E_\mathrm{o}$. They are plotted in figure~\ref{fig:wellwithbarrier} on top of  a sketch of the potential itself.

\begin{figure}[ht]
\centering
\includegraphics[scale=1]{SE/problems/wellwithbarrier.pdf}
\caption{The infinite square well potential with an internal barrier (equation~(\ref{wellwithbarrierpotential})) in dashed gray, with the (even) ground state~$\psi_\mathrm{e}$ and (odd) first excited state~$\psi_\mathrm{o}$ plotted on top in blue and (dashed) red.}
\label{fig:wellwithbarrier}
\end{figure}

As you can see from the plot, for the finite barrier there is a small but nonzero probability of finding the particle inside the classically forbidden region. Moreover, in both states, the particle will, over time, oscillate between being on the left and being on the right of the potential. To see how that comes about, suppose that we prepare a particle such that it is initially in a linear combination of the two states, given by
\begin{equation}
\label{wellwithbarrierevenoddsolutions}
\psi_\pm(x) = \frac{1}{\sqrt{2}} \left[ \psi_\mathrm{e}(x) \pm \psi_\mathrm{o}(x) \right].
\end{equation}
A particle in the $\psi_+$ linear combination will almost certainly be on the right of the barrier, and a particle in the $\psi_-$ combination will almost certainly be on the left. Suppose at time $0$ we prepare a particle in the state $\psi_+$.
\begin{enumerate}[(a)]
\item Write down the time-dependent wavefunction $\Psi(x, t)$ for the particle in the initial state $\psi_+(x)$. Give your answer in terms of $\psi_\mathrm{e}(x)$, $\psi_\mathrm{o}(x)$, $E_\mathrm{e}$ and $E_\mathrm{o}$.
\item Show / argue that after a time interval $\Delta T = \pi \hbar / (E_\mathrm{o}-E_\mathrm{e})$ the particle, initially right of the barrier, has tunneled through it to the left.
\setcounter{problemcounter}{\value{enumii}}
\end{enumerate}
As an application of the well-with-barrier potential, we consider the ammonium molecule $\mathrm{NH}_3$, which consists of one nitrogen and three hydrogen atoms. Because ammonia contains four nuclei and ten electrons, despite its relative simplicity it is already a highly complicated dynamical system. However, we can get quite far by some approximations. One such approximation is treating the covalent bonds formed by the electrons is as if they were springs keeping the nuclei together (this works because the harmonic potential is quadratic, and the expansion of any potential around any minimum will at lowest order have a quadratic term); the nuclei then just oscillate around the equilibrium positions defined by the potential energies of the `electron springs'. For ammonia, the equilibrium configuration is attained when the three hydrogen nuclei form an equilateral triangle, with the nitrogen molecule some distance $z$ above or below the plane of the triangle, defining two possible minima with a barrier in between. To simplify notation, we define $\ket{\mathrm{e}}$ and $\ket{\mathrm{o}}$ as the even and odd eigenstates of the corresponding Hamiltonian, with energies $E_\mathrm{e}$ and $E_\mathrm{o}$ as before. The $\ket{+}$ state is then the linear combination in which the nitrogen atom lies (almost certainly) above the plane of the hydrogen triangle, and $\ket{-}$ the linear combination in which it lies below the plane:
\begin{equation}
\ket{\pm} = \frac{1}{\sqrt{2}} \left( \ket{\mathrm{e}} \pm \ket{\mathrm{o}}\right).
\end{equation}
\begin{enumerate}[(a)]
\setcounter{enumii}{\value{problemcounter}}
\item Find the matrix elements $\braket{+|\hat{H}|+}$, $\braket{+|\hat{H}|-}$, and $\braket{-|\hat{H}|-}$ in terms of the energies $E_\mathrm{e}$ and $E_\mathrm{o}$. \textit{Hint}: use what you know of the eigenstates of the Hamiltonian, but indicate which properties you use.
\item Argue that we can represent the Hamiltonian with the following matrix
\begin{equation}
\hat{H} = \begin{pmatrix}
\bar{E} & -A \\ -A & \bar{E},
\end{pmatrix}
\end{equation}
and find expressions for $\bar{E}$ and $A$ in terms of $E_\mathrm{e}$ and $E_\mathrm{o}$.
\setcounter{problemcounter}{\value{enumii}}
\end{enumerate}
Due to the electronic structure of ammonia, the nitrogen atom carries a small negative charge $-q$, with a corresponding positive charge $+q$ distributed among the hydrogen atoms. If the molecule is in either the $\ket{+}$ or $\ket{-}$ state, there is a separation of charge, and the molecule therefore become an electric dipole. Because (as you argued in part b), the molecule will oscillate between these two states, it is an oscillating dipole, and thus will emit electromagnetic radiation; it indeed does so at a frequency of $150\;\mathrm{GHz}$ (i.e., in the microwave range).
\begin{enumerate}[(a)]
\setcounter{enumii}{\value{problemcounter}}
\item From the given frequency of the dipole radiation, find the difference in energy between the ground state ($E_\mathrm{e}$) and the first excited state ($E_\mathrm{o}$) of ammonia. Express your answer in electronvolts ($\mathrm{eV}$, one electronvolt is the energy an electron gets when passing through a potential difference of one volt).
\end{enumerate}

\ifincludesolutions
\Solutions
\begin{enumerate}[(a)]
\item The time dependence for each of the two eigenfunctions is given by $\exp(-i E t / \hbar)$, where $E$ is the eigenfunction's energy. From equation~(\ref{wellwithbarrierevenoddsolutions}), we can then immediately write down the time-de\-pen\-dent wavefunction:
\begin{equation}
\label{wellwithbarrierevensolutiontimedependence}
\Psi(x,t) = \frac{1}{\sqrt{2}} \left[ \psi_\mathrm{e}(x) e^{iE_\mathrm{e}t/\hbar} + \psi_\mathrm{o}(x) e^{iE_\mathrm{o}t/\hbar} \right]
\end{equation}
\score{2 pt}.
\item Pulling out an overall (time-dependent) phase factor, we can re-write the time dependent solution (\ref{wellwithbarrierevensolutiontimedependence}) as
\begin{align*}
\Psi(x,t) &= \frac{1}{\sqrt{2}}  e^{iE_\mathrm{e}t/\hbar} \left[ \psi_\mathrm{e}(x) + \psi_\mathrm{o}(x) e^{i(E_\mathrm{o}-E_\mathrm{e})t/\hbar} \right] \\
&= \frac{1}{\sqrt{2}}  e^{iE_\mathrm{e}t/\hbar} \left[ \psi_\mathrm{e}(x) + \psi_\mathrm{o}(x) e^{i \pi t/(\Delta T)} \right]
\end{align*}
\score{1 pt}. When $t = \Delta T$, the second exponential becomes $e^{i \pi} = -1$, and the solution becomes $\psi_-(x)$ up to a phase factor. Therefore, the particle, which started to the right of the barrier, now will (almost certainly) be found to the left of the barrier, which means that at some point in the interval $\Delta T$ it must have tunneled through \score{1 pt}.
\item We have
\begin{align*}
\braket{+|\hat{H}|+} &= \frac12 \braket{\mathrm{e}+\mathrm{o}|\hat{H}|\mathrm{e}+\mathrm{o}} = \frac12 \braket{\mathrm{e}+\mathrm{o}| E_\mathrm{e}e + E_\mathrm{o}o} \\
&= \frac12 \left( E_\mathrm{e} \braket{\mathrm{e}|\mathrm{e}} + E_\mathrm{e}\braket{\mathrm{o}|\mathrm{e}} + E_\mathrm{o} \braket{\mathrm{e}|\mathrm{o}} + E_\mathrm{o}\braket{\mathrm{o}|\mathrm{o}} \right) = \frac12 \left(E_\mathrm{e} + E_\mathrm{o} \right),
\end{align*}
where we used that the eigenstates $\ket{\mathrm{e}}$ and $\ket{\mathrm{o}}$ are orthonormal (as they are the eigenstates of a Hermitian operator) \score{1 pt}. Using the same properties, we find (completely analogously)
\begin{align*}
\braket{+|\hat{H}|-} &= \frac12 \left(E_\mathrm{e} - E_\mathrm{o} \right), \\
\braket{-|\hat{H}|-} &= \frac12 \left(E_\mathrm{e} + E_\mathrm{o} \right)
\end{align*}
\score{1 pt}.
\item We already found the matrix elements in part (c); as $\hat{H}$ is a Hermitian operator, it can only be represented by a Hermitian matrix. As the matrix elements are all real, that means that the matrix simply has to be symmetric. From (c), we read off that $\bar{E} = \frac12 \left(E_\mathrm{e} + E_\mathrm{o} \right)$ and $A = \frac12 \left(E_\mathrm{o} - E_\mathrm{e} \right)$ \score{2 pt}.
\item The frequency of oscillation is simply one over the time interval, which we already found in (b). Thus:
\[ 150\;\mathrm{Ghz} = \frac{1}{2\Delta T} = \frac{E_\mathrm{o} - E_\mathrm{e}}{2\pi \hbar} = \frac{A}{\pi \hbar}, \]
where the factor $2$ is because for a full oscillation we have to go back-and-forth. The difference between the energy levels is then given by
\[ E_\mathrm{o} - E_\mathrm{e} = 2 \pi \hbar \cdot (150\;\mathrm{GHz}) = 9.94 \cdot 10^{-23}\;\mathrm{J} = 6.2 \cdot 10^{-4} \;\mathrm{eV}\]
\score{2 pt}.
\end{enumerate}
\fi