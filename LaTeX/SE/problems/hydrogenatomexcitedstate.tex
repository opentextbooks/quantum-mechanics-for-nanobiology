%Problemtitle The first excited state of the hydrogen atom
%Source From physics 2 retake / 2019 retake / 2024 midterm.
The radial part of the Hamiltonian of a hydrogen atom (or, strictly speaking, the electron in that atom) is given by
\begin{equation}
\label{HydrogenHamiltonianRadial}
\hat{H} = -\frac{\hbar^2}{2 m_\mathrm{e} r^2} \frac{\mathrm{d}}{\mathrm{d}r} \left( r^2 \frac{\mathrm{d}}{\mathrm{d}r} \right) - \frac{k e^2}{r},
\end{equation}
where $\hbar$ is Planck's constant, $m_\mathrm{e}$ and $e$ the mass and charge of the electron, and $k$ the proportionality constant from Coulomb's law. The wave function of hydrogen in the ground state (the $100$ or $1s$ state) is radially symmetric and given by
\begin{equation}
\label{hydrogengroundstate}
\psi_{100}(\bvec{r}) = \frac{1}{\sqrt{\pi a^3}} e^{-r/a},
\end{equation}
where $a$ is the Bohr radius.
\begin{enumerate}[(a)]
\item Prove that $\psi_{100}(r)$ is indeed an eigenfunction of the Hamiltonian (\ref{HydrogenHamiltonianRadial}), and find both the associated energy and the value of the Bohr radius in terms of $\hbar$, $m_\mathrm{e}$, $e$, and $k$.
\ifproblemset\setcounter{problemcounter}{\value{enumi}}\else\setcounter{problemcounter}{\value{enumii}}\fi
\end{enumerate}
The wave function for hydrogen in the $200$ (or $2s$) state is given by
\begin{equation}
\label{hydrogen2sstate}
\psi_{200}(\bvec{r}) = \frac{1}{\sqrt{32\pi a^3}} \left(2 - \frac{r}{a}\right) e^{-r/2a}.
\end{equation}
To calculate probabilities and expectation values of a particle in the $\psi_{200}$ state requires integrating over three-dimensional space. Fortunately, this state, like the ground state, is radially symmetric: it is independent of the angles $\theta$ and $\phi$. For any such radially symmetric function $f(r)$, we can simplify the volume integral to a one-dimensional integral by switching to spherical coordinates:
\begin{equation}
\label{radiallysymmetricvolumeintegral}
\int_{\mbox{all space}} f(r) \,\dd V = \int_0^\infty f(r) 4 \pi r^2 \,\dd r.
\end{equation}
In this problem, you may use the following integrals:
\begin{equation}
\int_0^1 r^2 e^{-r} \,\dd r = 2 - \frac{5}{e}, \qquad \int_0^1 r^3 e^{-r} \,\dd r = 6 - \frac{16}{e}, \qquad \int_0^1 r^4 e^{-r} \,\dd r = 24 - \frac{65}{e}.
\end{equation}
\begin{enumerate}[(a)]
\ifproblemset\setcounter{enumi}{\value{problemcounter}}\else\setcounter{enumii}{\value{problemcounter}}\fi
\item Find the probability that an electron in the $200$ state of hydrogen will be found a distance less than $a$ from the nucleus.
\item Find the expectation values of $r$ and $r^2$ for an electron in the $200$ state of hydrogen. Express your answers in terms of the Bohr radius $a$.
\item An electron in the $200$ state of hydrogen can transition to the $100$ state under the emission of a photon. Find the frequency of that photon in terms of $\hbar$, $m_\mathrm{e}$, $e$, and $k$. Hint: combine your answer to part (a) with what you know about the energies of the eigenstates of hydrogen - this will save you a lengthy calculation!
\end{enumerate}

\ifincludesolutions
\Solutions
\begin{enumerate}[(a)]
\item We simply apply the given Hamiltonian to the given function. The only thing we actually need to calculate is the double derivative in the first term of the part $\exp{-r/a}$, for which we find:
\begin{equation}
\label{psi100derivative}
\frac{\mathrm{d}}{\mathrm{d}r} \left( r^2 \frac{\mathrm{d}}{\mathrm{d}r} \right) e^{-r/a} = \frac{\mathrm{d}}{\mathrm{d}r} \left( -\frac{r^2}{a} e^{-r/a} \right) = \left(-\frac{2r}{a} + \frac{r^2}{a^2} \right) e^{-r/a}
\end{equation}
\score{1 pt}. Using (\ref{psi100derivative}) we can write down the action of the Hamiltonian on $\psi_{100}$:
\begin{equation}
\label{psi100hamiltionan}
\hat{H} \psi_{100} = \frac{1}{\sqrt{\pi a^3}} \left[\frac{\hbar^2}{2m a^2} \left( \frac{2a}{r} - 1 \right) - \frac{k e^2}{r} \right] e^{-r/a}
\end{equation}
\score{1 pt}. Now if the right hand side of~(\ref{psi100hamiltionan}) is to be a constant times $\psi_{100}$, we must have
\begin{equation}
\label{Bohrradius}
\frac{\hbar^2}{2m a^2} \frac{2a}{r} = \frac{k e^2}{r} \quad \Rightarrow \quad a = \frac{\hbar^2}{m k e^2},
\end{equation}
\score{1 pt} (which is indeed the correct value of the Bohr radius). With this value for $a$, we find that indeed $\hat{H} \psi_{100}(\vec{r}) = E \psi_{100}(\vec{r})$ with
\begin{equation}
E = - \frac{\hbar^2}{2m a^2} = - \frac{m k^2 e^4}{2\hbar^2}
\end{equation}
\score{1 pt (for either version of the energy)}.
\item The probability to find a particle in a range $[a, b]$ is given by the integral from $a$ to $b$ over $\Psi^*(r) \Psi(r)$ \score{1 pt - also assign these points if not explicitly stated but correctly used}. We calculate this integral for the region $[0, a]$ for the given wavefunction:
\begin{align*}
P(x < a) &= \int_0^a \psi_{200}^*(r) \psi_{200}(r) 4 \pi r^2 \dd r \\
&= \frac{1}{32 \pi a^3} \int_0^a \left(2-\frac{r}{a}\right)^2 e^{-r/a} 4 \pi r^2 \dd r \\
&= \frac{1}{8a} \int_0^a \left(4 \frac{r^2}{a^2} - 4 \frac{r^3}{a^3} + \frac{r^4}{a^4} \right) e^{-r/a} \dd r \\
&= \frac{1}{8} \int_0^1 (4 \rho^2 - 4 \rho^3 + \rho^4) e^{-\rho} \dd \rho \\
&= \frac{1}{8} \left( 8 - \frac{20}{e} - 24 + \frac{64}{e} + 24 - \frac{65}{e} \right) \\
&= 1 - \frac{21}{8e} \approx 0.034,
\end{align*}
where we defined $\rho = r/a$ in the fourth line and used the given integrals. \score{1 pt for the right integral to calculate, 1 pt to convert it to a form that can be evaluated using the given integrals, 1 pt for the correct (exact) answer}.
\item We can calculate these expectation values directly using the integral given on the equations sheet:
\begin{align*}
\Braket{r} &= \int_0^\infty \psi_{200}^*(r) r \psi_{200}(r) 4 \pi r^2 \ddr \\ 
&= \frac{1}{8 a^3} \int_0^\infty r^3 \left(2 - \frac{r}{a} \right)^2 e^{-r/a} \dd r \\
&= \frac{1}{8 a^5} \int_0^\infty (4 a^2 r^3 - 4 a r^4 + r^5) e^{-r/a} \dd r \\
&= \frac{1}{8 a^5} (4 a^2 \cdot 6 a^4 - 4 a \cdot 24 a^5 + 120 a^6) = 6 a
\end{align*}
\score{1 pt for the correct integral expression for $\braket{r}$, 1 pt for rewriting the integral to an expression that can be evaluated with the given integral, 1 pt for the correct answer.} The calculation of $\braket{r^2}$ is completely analogous:
\begin{align*}
\Braket{r^2} &= \frac{1}{8 a^3} \int_0^\infty r^4 \left(2 - \frac{r}{a} \right)^2 e^{-r/a} \dd r \\
&= \frac{1}{8 a^5} (4 a^2 \cdot 24 a^5 - 4 a \cdot 120 a^6 + 720 a^5) = 42 a^2
\end{align*}
\score{1 pt}.
\item The energy of the $n = 2$ state of an electron in the hydrogen atom can simply be calculated by $E_n = E_1 / n^2$. Therefore, 
\[ E_2 = \frac14 E_1 = - \frac{\hbar^2}{8 m a^2}, \]
\score{1 pt} and the frequency of the emitted photon is given by
\[ f = \frac{\Delta E}{2 \pi \hbar} = \frac{3\hbar}{16 \pi m a^2} \]
\score{1 pt}.
\end{enumerate}
\fi