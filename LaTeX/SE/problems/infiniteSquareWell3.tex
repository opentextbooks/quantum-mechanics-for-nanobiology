%Problemtitle: Particle phase in the infinite square well
%Source: (a-b) Griffiths 2.6, 
We consider a particle in the infinite square well, i.e. the potential is given by
\begin{equation}
V(x) = \begin{cases} 
0, & \mbox{if } 0 \leq x \leq a, \\
\infty, & \mbox{else,} \\
\end{cases}
\end{equation}
where $a>0$ is a real constant. Let the initial state of the particle (with mass $m$) be given by
\begin{equation}
\Psi(x,0) = \frac{1}{\sqrt{2}}  ( \psi_1 + e^{i \phi} \psi_2),
\end{equation}
where $0 \leq \phi < 2 \pi$ is an arbitrary phase and $\psi_1$ and $\psi_2$ are the first two eigenstates of the Hamiltonian.

\begin{enumerate}[(a)]
\item Find $\Psi(x,t)$ and $|\Psi(x,t)|^2$ in terms of $\psi_1$, $\psi_2$, $E_1$, $E_2$ and $\phi$. 
\item Find $\ev{\hat{x}}$ and $\ev{\hat{p}}$. Do they depend on time? What is the role of $\phi$? Use $\omega = \frac{\pi^2 \hbar}{2ma^2}$ to simplify the result. Hint: Use Ehrenfest's theorem to relate $\ev{\hat{x}}$ and $\ev{\hat{p}}$ through a classical law. 
\item What energies can be obtained if a measurement is performed? With what probabilities? Also, find $\braket{\hat{H}}$.
\ifproblemset\setcounter{problemcounter}{\value{enumi}}\else\setcounter{problemcounter}{\value{enumii}}\fi
\end{enumerate}
Suppose now that at $t=0$ the well suddenly expands and the right wall is moved to $x = 2a$. The initial wave function is unchanged by this process.
\begin{enumerate}[(a)]
\ifproblemset\setcounter{enumi}{\value{problemcounter}}\else\setcounter{enumii}{\value{problemcounter}}\fi
\item What are now the four smallest outcomes an energy measurement could yield? What are the probabilities of finding them? To simplify the calculation you may set $\phi = 0$ here.
\item Find $\braket{\hat{H}}$ again.
\end{enumerate}

\ifincludesolutions
\Solution
\begin{enumerate}[(a)]
	\item \begin{equation*}
	\Psi(x,t) = \frac{1}{\sqrt{2}}  ( \psi_1 e^{-iE_1 t /\hbar} + e^{i \phi} \psi_2 e^{-iE_2 t /\hbar})
	\end{equation*}
	\begin{equation*}
	|\Psi(x,t)|^2 = \frac{1}{2} |\psi_1|^2 + \frac{1}{2} |\psi_2|^2 +  \psi_1\psi_2\cos\left[ \frac{(E_2-E_1)t}{\hbar} - \phi \right],
	\end{equation*}
	using the fact that $\psi_1^* \psi_2 = \psi_1 \psi_2^* = \psi_1 \psi_2$ in this case.
	
	\item Using the above expression and noting that $E_n = n^2 \omega \hbar$, we find
\begin{align*}
\ev{x} &=  \int_{0}^{a} x |\Psi|^2 \dd{x} \\
 &= \frac{1}{a} \int_0^a x \left[\sin^2\left(\frac{\pi x}{a}\right) +   \sin^2\left(\frac{2 \pi x}{a}\right) + 2 \sin\left(\frac{\pi x}{a}\right) \left(\frac{2\pi x}{a}\right) \cos\left( 3 \omega t - \phi \right)  \right] \dd{x} \\
 &= \frac{a}{2} - \frac{16}{9\pi^2} a \cos\left( 3 \omega t - \phi \right)
\end{align*}
	and
	$$\ev{\hat{p}} = m \dv{\ev{\hat{x}}}{t} =  \frac{16}{3\pi^2} m a \omega \sin(3 \omega t - \phi)$$
	Here $\phi$ thus has the role of a time shift.
		
	\item $E_1$ and $E_2$ are obtained with probability $|c_1|^2 = |c_2|^2 = \frac12$. $\ev{\hat{H}} = \frac12 (E_1+E_2) = \frac{5 \pi^2 \hbar^2}{4ma^2}$.
	
	\item The energies $\tilde{E}_n = \frac{n^2 \pi^2 \hbar^2}{8ma^2}$, with corresponding stationary states $\varphi_n = \sqrt{\frac{1}{a}} \sin \left(\frac{n \pi x}{2a}\right)$, of the new infinite square are obtained with probability $|c_n|^2$, where
	\begin{equation*}
	c_n = \int_{0}^{2a} \varphi_n^* \Psi(x,0) \,\ddx.
	\end{equation*}
	
	This yields
	\begin{equation*}
	c_n = \frac{1}{a}  \int_{0}^{a} \sin \left(\frac{n\pi x}{2a}\right) \left[ \sin \left(\frac{\pi x}{a}\right) + \sin \left(\frac{2\pi x}{a}\right) \right] \,\ddx .
	\end{equation*}
	
	We find $c_1 = \frac{4}{5 \pi} \approx 0.255$, $c_2 = c_4 = \frac{1}{2}$, $c_3 = \frac{68}{35 \pi} \approx 0.618$. This yields probabilities of finding the first four energies (in units of $E_1 = \frac{\pi^2 \hbar^2}{2ma^2}$, the ground state energy of the infinite square well of width $a$):
\begin{alignat*}{2}
&\tilde{E}_1 = \frac{1}{4} E_1, &\qquad& P(\tilde{E}_1) = |c_1|^2 = \frac{16}{25 \pi^2} \approx 0.065 \\
&\tilde{E}_2 = E_1, && P(\tilde{E}_2) = |c_2|^2 = \frac14 \\
&\tilde{E}_3 = \frac{9}{4} E_1, && P(\tilde{E}_3) = |c_3|^2 = \frac{4624}{1225 \pi^2} \approx 0.382 \\
&\tilde{E}_4 = 4 E_1 (=E_2), && P(\tilde{E}_4) = |c_4|^2 = \frac14
\end{alignat*}

\item We get
\begin{align*}
\ev{H}_\mathrm{after} &= \int_{0}^{2a} \Psi(x,0)^* \hat{H} \Psi(x,0) \dd{x} = \frac12 \int_{0}^{a} ( \psi_1 + e^{-i \phi} \psi_2) \hat{H} \left( \psi_1 + e^{i \phi} \psi_2 \right) \dd{x} \\
&= \ev{H}_\mathrm{before} = \frac12 (E_1+E_2) = \frac{5 \pi^2 \hbar^2}{4ma^2},
\end{align*}
since the integral is the same integral as before the expansion of the well (The instantaneous expansion of the well did not add energy to the system, but it did alter the possible outcomes of an energy measurement).
\end{enumerate}
\fi