%Problemtitle: The induced dipole perturbation
In section~\bookref{sec:vanderWaalsforce} we studied the perturbation to a hydrogen atom due to the presence of another hydrogen atom nearby, and found that this perturbation leads to an attractive van der Waals force. In this problem we'll derive the dipole-dipole interaction energy, equation~(\bookref{vanderWaalsdipole}), in the limit that the separation~$R$ between the atoms is much larger than the Bohr radius~$a_0$.
\begin{enumerate}[(a)]
\item We choose coordinates such that the two atomic nuclei are on the $z$ axis, separated by a distance~$R$. Let $\bvec{r_1} = (x_1, y_1, z_1)$ be the position of the electron in atom A, in a coordinate system where the nucleus of atom A is at the origin. Likewise, let $\bvec{r_2} = (x_2, y_2, z_2)$ the position of the electron in atom B, in a (shifted) coordinate system where the nucleus of atom B is at the origin. Express the distances~$r_{12}$, $r_{1\mathrm{B}}$ and~$r_{2\mathrm{A}}$ between the electrons and the electrons and opposite nuclei (see figure~\bookref{fig:vanderWaalsforces}(a)) in terms of the coordinates $x_1, x_2, y_1, y_2, z_1, z_2$ and the nuclear separation~$R$.
\item Your expressions from (a) should contain one term that's linear in $R$, and others of order $a_0$ (e.g. $x_1$ and $z_1$). Pull out a factor $R$ from these expressions, and expand $1/r_{1\mathrm{B}}$, $1/r_{2\mathrm{A}}$ and $1/r_{12}$ to second order in terms of magnitude $a_0/R$.
\item Finally, substitute your answers to (b) in the perturbation term $\hat{H}'$ in the van der Waals Hamiltonian~(\bookref{vanderWaalsHamiltonian}) to get the dipole-dipole interaction of equation~(\bookref{vanderWaalsdipole}).
\end{enumerate}

\ifincludesolutions
\Solution
\begin{enumerate}[(a)]
\item We have
\begin{align*}
r_{1\mathrm{B}} &= \begin{vmatrix} x_1 \\ y_1 \\ R-z_1 \end{vmatrix} = \sqrt{x_1^2 + y_1^2 + (R-z_1)^2} = R \sqrt{\left(\frac{x_1}{R}\right)^2 + \left(\frac{y_1}{R}\right)^2 + \left(1-\frac{z_1}{R}\right)^2} \\
r_{2\mathrm{A}} &= \begin{vmatrix} x_2 \\ y_2 \\ R+z_2 \end{vmatrix} = \sqrt{x_2^2 + y_2^2 + (R+z_2)^2} = R \sqrt{\left(\frac{x_2}{R}\right)^2 + \left(\frac{y_2}{R}\right)^2 + \left(1+\frac{z_2}{R}\right)^2} \\
r_{12} &= \begin{vmatrix} x_2-x_1 \\ y_2-y1 \\ R-(z_1-z_2) \end{vmatrix} = \sqrt{(x_2-x_1)^2 + (y_2-y_1)^2 + (R-(z_1-z_2))^2} \\
&= R \sqrt{\left(\frac{x_2-x_1}{R}\right)^2 + \left(\frac{y_2-y_1}{R}\right)^2 + \left(1 - \frac{z_1-z_2}{R}\right)^2}
\end{align*}
\item We have
\begin{align*}
\frac{1}{r_{1\mathrm{B}}} &\approx \frac{1}{R} \left[1 + \frac{z_1}{R} - \frac12 \left(\frac{x_1}{R}\right)^2 - \frac12 \left(\frac{y_1}{R}\right)^2 + \left(\frac{z_1}{R}\right)^2 \right], \\
\frac{1}{r_{2\mathrm{A}}} &\approx \frac{1}{R} \left[1 - \frac{z_2}{R} - \frac12 \left(\frac{x_2}{R}\right)^2 - \frac12 \left(\frac{y_2}{R}\right)^2 + \left(\frac{z_2}{R}\right)^2 \right], \\
\frac{1}{r_{12}} &\approx \frac{1}{R} \left[ 1 + \frac{z_1 - z_2}{R} - \frac12 \left(\frac{x_2-x_1}{R}\right)^2 - \frac12 \left(\frac{y_2-y_1}{R}\right)^2 + \left(\frac{z_1-z_2}{R}\right)^2 \right].
\end{align*}
\item We get
\begin{align*}
\hat{H}' &= e^2 \left[ \frac{1}{R} + \frac{1}{r_{12}} - \frac{1}{r_{1\mathrm{B}}} - \frac{1}{r_{2\mathrm{A}}} \right] \\
&= \frac{e^2}{R} \left[ 1 + 1 - 1 - 1 + \frac{z_1-z_2}{R} - \frac{z_1}{R} + \frac{z_2}{R} - \frac12 \left(\frac{x_2-x_1}{R}\right)^2 - \frac12 \left(\frac{y_2-y_1}{R}\right)^2 + \left(\frac{z_1-z_2}{R}\right)^2 \right. \\ 
&\qquad \qquad + \left. \frac12 \left(\frac{x_1}{R}\right)^2 + \frac12 \left(\frac{y_1}{R}\right)^2 - \left(\frac{z_1}{R}\right)^2 + \frac12 \left(\frac{x_2}{R}\right)^2 + \frac12 \left(\frac{y_2}{R}\right)^2 - \left(\frac{z_2}{R}\right)^2 \right] \\
&= \frac{e^2}{R} \left[ \frac{x_1 x_2}{R^2} + \frac{y_1 y_2}{R^2} - 2 \frac{z_1 z_2}{R^2} \right].
\end{align*}
\end{enumerate}
\fi