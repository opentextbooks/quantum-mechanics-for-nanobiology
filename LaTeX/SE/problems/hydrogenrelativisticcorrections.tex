%Problemtitle: Relativistic corrections to the hydrogen atom
%Source: Griffiths 7.15 (a) and 7.42
In this problem, we'll derive the expressions in equation~(\bookref{hydrogenrelativisticcorrectionexpectationvalues}) for the expectation values of $1/r$ and $1/r^2$ in the unperturbed eigenstates of the hydrogen atom.
\begin{enumerate}[(a)]
\item For the expectation value of $1/r$, use the virial theorem~\bookref{thm:virialtheorem} to derive the expression given in equation~(\bookref{hydrogenrelativisticcorrectionexpectationvalues}).
\setcounter{problemcounter}{\value{enumii}}
\end{enumerate}
We can find the expectation values of both $1/r$ and $1/r^2$ from the Hellmann-Feynman theorem (see problem~\bookref{ch:QMintro}.\bookref{pb:HFtheorem}), which states that if the Hamiltonian depends on a parameter~$u$, then its eigenvalues (i.e., the energies) are also functions of~$u$, and we have
\begin{equation}
\label{HF2}
\dv{E}{u} = \ev{\pdv{\hat{H}}{u}}.
\end{equation}
We'll apply the Hellmann-Feynman theorem to the effective Hamiltonian of the radial part of the wavefunction of a hydrogen atom, see equation~(\bookref{centralpotentialSEcentrifugal}):
\begin{equation}
\label{radialhydrogenHamiltonian}
\hat{H} = -\frac{\hbar^2}{2\me} \frac{\dd^2}{\ddr^2} + \frac{\hbar^2}{2\me} \frac{l(l+1)}{r^2} - \frac{e^2}{4 \pi \varepsilon_0} \frac{1}{r}.
\end{equation}
As the Hamiltonian in equation~(\ref{radialhydrogenHamiltonian}) is given in terms of the quantum number~$l$, for the purposes of this problem, we'd also like to write the (unperturbed!) energies in terms of~$l$. We can do so by invoking equation~(\bookref{hydrogenseriestermination}), which states that a series solution to the Schr\"odinger equation that terminates must satisfy $\rho_0 = 2(j_\mathrm{max}+l+1) = 2n$. We used this equation to define the quantum number $n$, but now we'll use it to convert $n$ to $l$, as $n = l + j_\mathrm{max} + 1 = N + l$ (where, for a given wavefunction, $N$ is a fixed integer). Substituting this expression into the energy~(\bookref{hydrogenenergies}) of the unperturbed state with quantum number $n$, we can write $E_n$ as
\begin{equation}
\label{hydrogenenergiesasfunctionofl}
E_n = - \frac{\me e^4}{2 (4 \pi \varepsilon_0 \hbar)^2 (N+l)^2}.
\end{equation}
\begin{enumerate}[(a)]
\setcounter{enumii}{\value{problemcounter}}
\item Apply the Hellmann-Feynman theorem to the Hamiltonian~(\ref{radialhydrogenHamiltonian}) with energies~(\ref{hydrogenenergiesasfunctionofl}) with $\lambda = e$ to re-derive your expression from (a) for the expectation value of $1/r$.
\item Now apply the Hellmann-Feynman theorem to the same Hamiltonian and energies with $\lambda = l$ to get $\ev{1/r^2}$.
\end{enumerate}


\ifincludesolutions
\Solution
\begin{enumerate}[(a)]
\item Because for the unperturbed hydrogen atom, the potential goes as $1/r$, we can use the virial theorem as in problem~\bookref{ch:SEsolutions}.\bookref{pb:virialtheorem}(d), which gave $\ev{\hat{V}} = 2 E_n$, and therefore
\begin{align*}
\ev{\frac{1}{r}} = - \frac{4 \pi \varepsilon_0}{e^2} \ev{\hat{V}} = \frac{8 \pi \varepsilon_0}{e^2} \frac{\hbar^2}{2 \me a_0^2} \frac{1}{n^2} = \frac{4 \pi \varepsilon_0}{e^2} \frac{\hbar^2 \me e^2}{4 \pi \varepsilon_0 \hbar^2} \frac{1}{a_0} \frac{1}{n^2} = \frac{1}{n^2 a_0},
\end{align*}
where we used one of the factors~$a_0$ to eliminate the other constants.
\item We simply calculate the derivatives of $\hat{H}$ and $E_n$ with respect to $e$, and equate them according to the Hellman-Feynman theorem~(\ref{HF2}):
\begin{align*}
\dv{E_n}{e} &= - 4 \frac{\me e^3}{2 (4 \pi \varepsilon_0 \hbar)^2 (N+l)^2} = - 2 \frac{\hbar^2}{e \me a_0^2 n^2} \\
&= \ev{\pdv{\hat{H}}{e}} = \ev{- \frac{2e}{4 \pi \varepsilon_0} \frac{1}{r}} = - \frac{\hbar^2}{e \me a_0} \ev{\frac{1}{r}},
\end{align*}
and thus
\begin{align*}
\frac{1}{a_0 n^2} = \ev{\frac{1}{r}}.
\end{align*}
\item Again, we simply calculate derivatives, now to $l$, and substitute in the Hellmann-Feynman theorem:
\begin{align*}
\dv{E_n}{l} &= 2 \frac{\me e^4}{2 (4 \pi \varepsilon_0 \hbar)^2 (N+l)^3} = \frac{\hbar^2}{\me a_0^2 n^3} \\
&= \ev{\pdv{\hat{H}}{l}} = \frac{\hbar^2}{2 \me} (2l + 1) \ev{\frac{1}{r^2}},
\end{align*}
and thus
\begin{align*}
\ev{\frac{1}{r^2}} &= \frac{2}{(2l + 1) n^3 a_0^2}.
\end{align*}
\end{enumerate}
\fi