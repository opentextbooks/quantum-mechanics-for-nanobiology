%Problemtitle: The virial theorem
The \index{virial theorem}\emph{virial theorem} relates the expectation values of the kinetic energy and the work to each other. In this problem, we'll prove and apply both the one-dimensional and three-dimensional versions of the theorem.
\begin{enumerate}[(a)]
\item Using the generalized Ehrenfest theorem (equation~\bookref{operatorexpectationvaluetimeevolution2}), show that for a one-dimensional system
\begin{equation}
\label{virial1Dwithtime}
\dv{t} \ev{\hat{x} \hat{p}} = 2 \ev{\hat{K}} - \ev{x \dv{V}{x}}.
\end{equation}
\setcounter{problemcounter}{\value{enumii}}
\end{enumerate}
In a stationary state, the left-hand side of equation~(\ref{virial1Dwithtime}) vanishes, and therefore
\begin{equation}
\label{virial1D}
2 \ev{\hat{K}} = \ev{x \dv{V}{x}} \equiv \ev{W},
\end{equation}
where $W$ is the work (force times displacement). Equation~\ref{virial1D} is known as the virial theorem; it is the quantum-mechanical equivalent of the \bookhref{https://interactivetextbooks.tudelft.nl/nb1140/content/energy.html\#kinetic-energy}{work-energy theorem} in classical mechanics.
\begin{enumerate}[(a)]
\setcounter{enumii}{\value{problemcounter}}
\item Use the virial theorem to show that $\ev{\hat{K}} = \ev{\hat{V}}$ for stationary states of the harmonic oscillator potential.
\item Now prove the three-dimensional version of the virial theorem:
\begin{theorem}[Virial theorem]
\label{thm:virialtheorem}
For a stationary state, the expectation values of the kinetic and potential energies are related through
\begin{equation}
\label{virialtheorem}
2 \ev{\hat{K}} = \ev{ \bvec{r} \cdot \bvec{\nabla} V }.
\end{equation}
\end{theorem}
\item Apply the virial theorem to the stationary states of the hydrogen atom. Show that for these states we have $\ev{\hat{K}} = -E_n$, and therefore $\ev{\hat{V}} = 2 E_n$.
\end{enumerate}

\ifincludesolutions
\Solution
\begin{enumerate}[(a)]
\item We simply apply equation~(\bookref{operatorexpectationvaluetimeevolution2}):
\begin{align*}
\dv{t} \ev{\hat{x} \hat{p}} &= \frac{i}{\hbar} \ev{\commutator{\hat{H}}{\hat{x} \hat{p}}} = \frac{i}{\hbar} \ev{\commutator{\hat{H}}{\hat{x}} \hat{p} + \hat{x} \commutator{\hat{H}}{\hat{p}}} \\
&= \frac{i}{\hbar}\ev{\commutator{\hat{K}}{\hat{x}} \hat{p}} + \frac{i}{\hbar} \ev{\hat{x}\commutator{\hat{V}}{\hat{p}}} \\
\commutator{\hat{p}^2}{\hat{x}} &= \hat{p}^2 \hat{x} - \hat{x}\hat{p}^2 = \hat{p}^2 \hat{x} - \hat{p}\hat{x}\hat{p} + \hat{p}\hat{x}\hat{p} - \hat{x}\hat{p}^2 = \hat{p}\commutator{\hat{p}}{\hat{x}} + \commutator{\hat{p}}{\hat{x}} \hat{p} = -2 i \hbar \hat{p} = \commutator{\hat{K}, \hat{x}}, \\
\commutator{\hat{V}}{\hat{p}} \psi &= -i \hbar V \dv{\psi}{x} + i \hbar \dv{V \psi}{x} = i \hbar \dv{V}{x}, \\
\dv{t} \ev{\hat{x} \hat{p}} &= \frac{i}{\hbar}\ev{ -2 i \hbar \hat{p}^2 + \hat{x} i \hbar \dv{V}{x} } = 2 \ev{\hat{p}^2} - \ev{ \hat{x} \dv{V}{x} }
\end{align*}
\item For the harmonic potential, we have
\begin{align*}
x \dv{V}{x} &= x \dv{x} \left( \frac12 m \omega^2 x^2 \right) = m \omega^2 x^2 = 2 V.
\end{align*}
Therefore, by the virial theorem
\begin{align*}
\ev{\hat{K}} = \frac12 \ev{x \dv{V}{x}} = \ev{\hat{V}}.
\end{align*}
\item Using that the momentum and position operators in different directions commute, i.e., $\commutator{\hat{x}_i}{\hat{p}_j} = i \hbar \delta_{ij}$, the proof goes entirely analogous to the one-dimensional case.
\item We have
\begin{align*}
V(r) &= - \frac{e^2}{4 \pi \varepsilon_0} \frac{1}{r}, \\
\bvec{\nabla} V &= \frac{e^2}{4 \pi \varepsilon_0} \frac{1}{r^2} \unitvec{r}, \\
\bvec{r} \cdot \bvec{\nabla} V &= \frac{e^2}{4 \pi \varepsilon_0} \frac{1}{r} = - V,
\end{align*}
and therefore by the virial theorem
\begin{align*}
2 \ev{\hat{K}} &= - \ev{\hat{V}}.
\end{align*}
Because we also have $\hat{H} = \hat{K} + \hat{V}$, we get $E_n = \ev{\hat{H}} = \ev{\hat{K}} + \ev{\hat{V}} = - \ev{\hat{K}}$ and $\ev{\hat{V}} = 2 E_n$.
\end{enumerate}

\fi