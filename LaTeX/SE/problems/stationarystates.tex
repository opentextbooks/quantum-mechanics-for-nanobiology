%Problemtitle: Linear combinations of stationary states
Consider the stationary wave functions $\psi_1(x)$ and $\psi_2(x)$. Assume that they are normalized solutions to the stationary Schr\"odinger equation with energies $E_1$ and $E_2$ respectively. Now let
\begin{equation}
\label{linearcombinationstate}
\Psi(x,t) = c_1 \psi_1(x) e^{-iE_1 t/\hbar} + c_2 \psi_2(x) e^{-iE_2 t/\hbar}
\end{equation}
be a linear combination of both solutions, with $c_1$ and $c_2$ (complex and nonzero) constants. Furthermore assume that $c_1$ and $c_2$ are chosen such that $\Psi$ is normalized.

\begin{enumerate}[(a)]
\item Show that $\Psi(x,t)$ is a solution to the time-dependent Schr\"odinger equation.
\item Compute the probability density $|\Psi(x,t)|^2$. Does it depend on $t$? In which case is $\psi = c_1 \psi_1(x) + c_2 \psi_2(x)$ also a solution to the stationary Schr\"odinger equation?
\item Show that $E_1$ (and by similarity also $E_2$) must be real. Hint: Write $E_1 = E + i \Gamma$ with $E$ and $\Gamma$ real and show that for $\Psi_1(x,t) = \psi_1(x)e^{-iE_1t/\hbar}$ to be normalizable for all $t$ we must have $\Gamma = 0$.
\item Show that $\frac12 (\psi_1 + \psi_1^*)$ is also a solution to the stationary Schr\"odinger equation. What can you conclude from this observation?
\end{enumerate}

\ifincludesolutions
\Solution
\begin{enumerate}[(a)]
\item We simply substitute $\Psi(x, t)$ in the Schr\"odinger equation:
\begin{align*}
i \hbar \frac{\partial \Psi}{\partial t} &= c_1 E_1 \psi_1 e^{-i E_1 t / \hbar} + c_2 E_2 \psi_1 e^{-i E_2 t / \hbar} \\
&= c_1 \left(-\frac{\hbar^2}{2m} \frac{d^2\psi_1}{dx^2} + V\psi_1\right) e^{-i E_1 t / \hbar} + c_1 \left(-\frac{\hbar^2}{2m} \frac{d^2\psi_2}{dx^2} + V\psi_2\right) e^{-i E_2 t / \hbar} \\
&= -\frac{\hbar^2}{2m} \pdv[2]{(c_1 \psi_1 e^{-iE_1 t/\hbar} + c_2 \psi_2 e^{-iE_2 t/\hbar})}{x} + V(c_1 \psi_1 e^{-iE_1 t/\hbar} + c_2 \psi_2 e^{-iE_2 t/\hbar}) \\
&= -\frac{\hbar^2}{2m} \pdv[2]{\Psi}{x} + V \Psi,
\end{align*}
so~(\ref{linearcombinationstate}) indeed solves the Schr\"odinger equation.
	
\item We find
\begin{align*}
|\Psi|^2 &= \Psi \Psi^* = (c_1 \psi_1 e^{-iE_1 t/\hbar} + c_2 \psi_2 e^{-iE_2 t/\hbar})(c_1 \psi_1 e^{-iE_1 t/\hbar} + c_2 \psi_2 e^{-iE_2 t/\hbar})^* \\
&= |c_1|^2 |\psi_1|^2 + |c_2|^2 |\psi_2|^2 + c_1 c_2^* \psi_1 \psi_2^* e^{-i(E_1-E_2)t/\hbar} + c_1^* c_2 \psi_1^* \psi_2 e^{-i(E_2-E_1)t/\hbar},
\end{align*}
so the probability density depends on $t$ unless $E_1 = E_2$. In that case $\psi$ is also a solution to the stationary Schr\"odinger equation, since it is a linear combination of stationary solutions with the same energy.

\item Writing $E_1 = E + i\Gamma$ and normalizing $\Psi_1$ we find
\begin{align*}
\int |\Psi_1|^2 \dd{x} = \int \psi_1 e^{-i(E+i\Gamma)t/\hbar} \psi_1^* e^{i(E-i\Gamma)t/\hbar} \dd{x} = e^{2\Gamma t / \hbar}  \int |\psi_1|^2 \dd{x} = e^{2\Gamma t / \hbar} = 1,
\end{align*}
hence for the wave function $\Psi_1$ to remain normalized for all $t$ we must have $\Gamma = 0$.

\item Taking the complex conjugate of the stationary equation we find, after noting that $V$, $E$, $\hbar$ and $m$ are real, that
\begin{equation}
-\frac{\hbar^2}{2m} \dv[2]{\psi_1^*}{x} + V\psi_1^* = E_1 \psi_1^*,
\end{equation}
and thus $\psi_1^*$ also satisfies the stationary equation with the same energy. By linearity therefore,
\begin{align*}
\mathrm{Re}\left(\psi_1\right) =  \frac12 \left(\psi_1 + \psi_1^*\right)
\end{align*}
also satisfies it. We conclude that the stationary solutions can therefore always taken to be real.
\end{enumerate}
\fi