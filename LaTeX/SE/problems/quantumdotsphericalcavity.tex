%Problemtitle: Quantum dots
%Source: A&F problem 3.13; 1/2
%Exam 2020.
From \href{https://en.wikipedia.org/wiki/Quantum_dot}{Wikipedia}: ``Quantum dots are tiny semiconductor particles a few nanometres in size, having optical and electronic properties that differ from larger particles due to quantum mechanics. \ldots Quantum dots are sometimes referred to as artificial atoms, emphasizing their singularity, having bound, discrete electronic states, like naturally occurring atoms or molecules.''

Quantum dots are used frequently in experiments (including biological ones) as probes; there also exist biocomposite quantum dots that can be fabricated by viruses. In this problem, we'll treat a quantum dot as a spherical cavity: electrons can move around freely inside, but cannot escape. The solutions of the Schr\"odinger equation then separate like they do for the hydrogen atom: $\psi_{nlm}(r, \theta, \phi) = R_{nl}(r) Y_l^m (\theta, \phi)$. The angular part is still given by the spherical harmonics, and for the radial part we have
\begin{equation}
\label{sphericalwellradialwavefunctionode}
\frac{1}{r} \frac{\dd^2}{\ddr^2} (r R) - \frac{l(l+1)}{r^2} R(r) = \frac{-2 \me E_{nl}}{\hbar^2} R(r),
\end{equation}
where $E_{nl}$ is the (energy) eigenvalue of the full function $\psi_{nlm}$. NB: Note that the $\psi_{nlm}$ here are not the eigenfunctions of the hydrogen atom.

The first step to solving equation~(\ref{sphericalwellradialwavefunctionode}) is to re-scale the energy and position variable, writing\newline
$k = \sqrt{2 \me E_{nl}}/\hbar$ and $z = kr$.
\begin{enumerate}[(a)]
\item Show that you can re-write equation~(\ref{sphericalwellradialwavefunctionode}) in terms of $k$ and the variable $z$ as
\begin{equation}
\label{sphericalwellradialwavefunctionoderescaled}
\frac{\dd^2 R}{\dd z^2} + \frac{2}{z} \frac{\dd R}{\dd z} + \left( 1 - \frac{l(l+1)}{z^2} \right) R = 0.
\end{equation}
\setcounter{problemcounter}{\value{enumii}}
\end{enumerate}
The solutions to equation~(\ref{sphericalwellradialwavefunctionoderescaled}) are the spherical Bessel functions $j_l(z)$ and spherical Neumann functions $n_l(z)$. The first two of each are given by
\begin{alignat*}{3}
j_0(z) &= \frac{\sin(z)}{z}, & j_1(z) &= \frac{\sin(z)}{z^2} - \frac{\cos(z)}{z}, \\
n_0(z) &= -\frac{\cos(z)}{z}, \qquad & n_1(z) &= -\frac{\cos(z)}{z^2} - \frac{\sin(z)}{z}.
\end{alignat*}
\begin{enumerate}[(a)]
\setcounter{enumii}{\value{problemcounter}}
\item Verify that both $j_0(z)$ and $n_0(z)$ are indeed solutions to your equation at (a).
\item By expanding the sine and cosine functions around $z=0$ (note that $z$ is a real number here), determine which of the two sets will give physically allowable solutions.
\setcounter{problemcounter}{\value{enumii}}
\end{enumerate}
Let~$r=a$ be the radius of the quantum dot, then (obviously) we must set $R(r=a) = R(z=ka) = 0$. Unfortunately, the radial Bessel and Neumann functions do not have nice regularly spaced zeros. Except for the $l=0$ case, we can't even compute the positions of the zeros analytically. Therefore, let $\gamma_{n,l}$ be the $n$th zero of the $l$th spherical Bessel/Neumann (we only need one, as we've dropped the other) function.
\begin{enumerate}[(a)]
\setcounter{enumii}{\value{problemcounter}}
\item Express the energy $E_{n,l}$ in terms of $\gamma_{n,l}$.
\item Compute the numerical value (in electron volts) of the ground-state energy $E_{1,0}$ for a quantum dot with a radius of $1.0\;\mathrm{nm}$.
%\item Find the degeneracy of the state with energy $E_{n,l}$ (\textit{Hint}: this does not involve a calculation, but knowledge about the spherical harmonic part).
\item Find the wavelength of a photon emitted by an electron when transitioning from the $n=2, l=0$ state tot the $n=1, l=0$ state of a quantum dot.
\item Evaluate the probability that an electron in the ground state of a quantum dot will be found inside a sphere of radius $a/2$.  % A&F problem 3.13; 1/2.
\end{enumerate}

\ifincludesolutions
\Solution
\begin{enumerate}[(a)]
\item By inverting the expression for $k$, we find $E_{nl} = k^2 \hbar^2 / 2\me$, so the prefactor of the right-hand side of equation~(\ref{sphericalwellradialwavefunctionode}) simply becomes $k^2$. For the derivatives, we get
\begin{equation}
\frac{\dd}{\ddr} = \frac{\dd}{\dd (z/k)} = k \frac{\dd}{\dd z},
\end{equation}
\score{1 pt} so we get for equation~(\ref{sphericalwellradialwavefunctionode}):
\begin{align*}
0 &= \frac{1}{z/k} k^2 \frac{\dd^2}{\dd z^2} \left( \frac{z}{k} R \right) + \left( k^2 - \frac{l(l+1)}{(z/k)^2} \right) R \\
&= \frac{k^2}{z} \frac{\dd^2}{\dd z^2} \left( z R \right) + k^2 \left( 1 - \frac{l(l+1)}{z^2} \right) R \\
0 &= \frac{\dd^2 R}{\dd z^2} + \frac{2}{z} \frac{\dd R}{\dd z} + \left( 1 - \frac{l(l+1)}{z^2} \right) R
\end{align*}
\score{1 pt}.
\item We have
\begin{align*}
\frac{\dd j_0(z)}{\dd z} &= \frac{\cos(z)}{z} - \frac{\sin(z)}{z^2} = -n_0(z) - \frac{j_0(z)}{z}\\
\frac{\dd^2 j_0(z)}{\dd z^2} &= -\frac{\sin(z)}{z} - 2 \frac{\cos(z)}{z^2} + 2 \frac{\sin(z)}{z^3} = - j_0(z) + 2 \frac{n_0(z)}{z} + 2\frac{j_0(z)}{z^2} \\
\frac{\dd n_0(z)}{\dd z} &= \frac{\sin(z)}{z} + \frac{\cos(z)}{z^2} = j_0(z) - \frac{n_0(z)}{z} \\
\frac{\dd^2 j_0(z)}{\dd z^2} &= \frac{\cos(z)}{z} - 2 \frac{\sin(z)}{z^2} - 2 \frac{\cos(z)}{z^3} = - n_0(z) - 2 \frac{j_0(z)}{z} + 2 \frac{n_0(z)}{z^2}
\end{align*}
\score{1 pt}. Substituting into equation~(\ref{sphericalwellradialwavefunctionoderescaled}) we get
\begin{align*}
\frac{\dd^2 j_0(z)}{\dd z^2} + \frac{2}{z} \frac{\dd j_0(z)}{\dd z} + j_0(z) = - j_0(z) + 2 \frac{n_0(z)}{z} + 2\frac{j_0(z)}{z^2} + \frac{2}{z} \left( -n_0(z) - \frac{j_0(z)}{z} \right) + j_0(z) &=0, \\
\frac{\dd^2 n_0(z)}{\dd z^2} + \frac{2}{z} \frac{\dd n_0(z)}{\dd z} + n_0(z) = - n_0(z) - 2 \frac{j_0(z)}{z} + 2 \frac{n_0(z)}{z^2} + \frac{2}{z} \left( j_0(z) - \frac{n_0(z)}{z} \right) + n_0(z) &=0,
\end{align*}
so indeed both are solutions \score{1 pt}.
\item We have (to cubic order): $\sin(z) = z - \frac16 z^3$ and $\cos(z) = 1-\frac12 z^2$, so we can write for our four given functions:
\begin{alignat*}{3}
j_0(z) &\approx 1 - \frac16 z^2, & j_1(z) &\approx \frac{1}{z} - \frac16 z - \frac{1}{z} + \frac12 z = \frac13 z, \\
n_0(z) &\approx -\frac{1}{z} + \frac12 z, \qquad & n_1(z) &\approx - \frac{1}{z^2} + \frac12 - 1 + \frac16 z^2 = - \frac{1}{z^2} - \frac12 + \frac16 z^2, 
\end{alignat*}
and we conclude that (at least as far as we've checked here) the spherical Bessel functions go to zero at $z=0$ and the spherical Neumann functions diverge as we go to $z=0$. For our functions to be square-integrable, they cannot diverge, so the Neumann functions are out \score{1 pt}.
\item From the boundary condition, we get $0 = j_l(\gamma_{n,l}) = j_0(k a)$, so we simply get
\[ E_{nl} = \frac{\hbar^2}{2 \me} \left(\frac{\gamma_{n,l}}{a} \right)^2 = \frac{\hbar^2}{2 \me a^2} \gamma_{n,l}^2 \]
\score{1 pt}.
\item For $l=0$, we must find the first root of $j_l(z) = 0$, which is simply $z=\pi$, so
\[ E_{1,0} = \frac{\hbar^2}{2 \me a^2} \pi^2 = \frac{(1.055 \cdot 10^{-34})^2}{2 \cdot 9.11 \cdot 10^{-31} \cdot (1.0 \cdot 10^{-9})^2} \cdot \pi^2 = 6.029 \cdot 10^{-20}\;\mathrm{J} = 0.38\;\mathrm{eV} \]
\score{1 pt}.
\item For $n=2$, the zero we need is the second one of the sine, at $z = 2\pi$. The energy is then
\[ E_{2,0} = \frac{\hbar^2}{2 \me a^2} (2\pi)^2 = 4 E_{1,0}, \]
and the frequency~$\nu$ of the emitted photon is given by
\[ \nu = \frac{\Delta E}{h} = \frac{\Delta E}{2\pi \hbar} = 3 \frac{\hbar}{4 \me a^2} \pi, \]
so the wavelength is
\[ \lambda = \frac{c}{\nu} = \frac{4 c \me a^2}{3 \pi \hbar} \]
\score{1 pt}.
\item The probability that the particle will be found inside the given sphere is given by the integral over $|\psi|^2$ from $r=0$ to $r=a/2$ and all angles \score{1 pt}. The function $\psi$ is given by the product of the spherical harmonic $Y_0^0$ and the spherical Bessel function $j_0(kr)$, properly normalized. Combining, we have:
\begin{align*}
P\left( r < \frac{a}{2} \right) &= \int_0^{a/2} \int_0^\pi \int_0^{2\pi} |\psi(r, \theta, \phi) |^2 r^2 \sin(\theta) \dd\theta \dd \phi \\
&= \frac{\int_0^{a/2} |j_0(kr)|^2 r^2 \dd r}{\int_0^a |j_0(kr)|^2 r^2 \dd r} \int_0^\pi \int_0^{2\pi} Y_0^0(\theta, \phi) \sin(\theta) \dd\theta \dd \phi \\
&= \frac{\int_0^{a/2} \frac{1}{k^2} \sin^2(kr) \dd r}{\int_0^a \frac{1}{k^2} \sin^2(kr) \dd r} = \frac{\int_0^{a/2} \sin^2\left(\frac{\pi r}{a}\right) \dd r}{\int_0^a \sin^2\left(\frac{\pi r}{a}\right) \dd r} =  \frac12,
\end{align*}
where we used that the spherical harmonics are already normalized, and the integral over $\sin^2(x)$ from the equations sheet \score{1 pt}.
\end{enumerate}
\fi
