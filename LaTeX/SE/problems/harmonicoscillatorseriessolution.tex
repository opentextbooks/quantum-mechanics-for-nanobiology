%Problemtitle: Eigenfunctions of the harmonic oscillator potential
%Source: Griffiths section 2.3.2
In section~\bookref{sec:harmonicpotential} we found the eigenvalues of the Hamiltonian with the harmonic oscillator potential. We also found a method to construct the eigenfunctions, by repeatedly acting with the raising operator $\hat{a}_+$ on the ground state wave function $\psi_0(x)$. While in many cases we'll encounter later we'd already be glad to have numbers for the eigenvalues, in this case, we can also solve for the eigenfunctions directly, using a series expansion approach (see appendix~\bookref{app:odepowerseries}). The steps we follow here will also be the ones we'll employ to find the radial part of the eigenfunctions of hydrogen atom in section~\bookref{sec:hydrogenradialpart}.

\begin{enumerate}[(a)]
\item First, to avoid mistakes due to factors floating around, we'll rescale our variables to get the equation in the simplest form possible. Introduce $\xi = \sqrt{m\omega/\hbar} x$ and $K = 2E/\hbar\omega$, and show that in these variables, the time-independent Schr\"odinger equation with the harmonic oscillator potential (equation~\bookref{SEHP}) can be written as
\begin{equation}
\label{TISEHarmonicRescaled}
\dv[2]{\psi}{\xi} = \left(\xi^2 - K \right) \psi.
\end{equation}
\item Before attempting a series solution, we should look at the asymptotic behavior of the solution. If that turns out to be exponential, we need to factorize the solution first, as otherwise the series will not converge. In the present case, for very large values of $\xi$ (and equivalently very large values of~$x$), we have $\xi^2 \gg K$, so we can approximate equation~(\ref{TISEHarmonicRescaled}) as
\begin{equation}
\label{TISEHarmonicRescaledApprox}
\dv[2]{\psi}{\xi} = \xi^2 \psi.
\end{equation}
Equation~(\ref{TISEHarmonicRescaledApprox}) suggests an Ansatz of the form $\psi = \exp(\lambda \xi^2)$. Substitute this Ansatz to get possible values for $\lambda$. NB: you will not get an exact solution - there will be a remainder term. However, we're not after the exact solution here, but the asymptotic behavior, which will scale like the Ansatz.
\item Of the two solutions you found in (b), one is not normalizable (as it diverges as $x \to \pm \infty$), so we drop it. Retaining the other term, we try a solution of the form
\begin{equation}
\psi(\xi) = u(\xi) e^{-\frac12 \xi^2}.
\end{equation}
Find the differential equation for $u(\xi)$.
\item We now try a power series solution for~$u(\xi)$, i.e., we write $u(\xi)$ as
\begin{equation}
\label{harmoscpowerseries}
u(\xi) = \sum_{k=1}^\infty a_k \xi^k.
\end{equation}
Substitute equation~(\ref{harmoscpowerseries}) in your differential equation for $u(\xi)$, then rearrange terms such that the equation is of the form of a power series (i.e., find the coefficients of $\xi^k$, see equation~(\bookref{Legendreodepowerseries})).
\item From the power series solution, get a recurrence relation for the coefficients $a_k$.
\item There are now two options: either the power series goes on forever, or it truncates at some finite value of $k$. We'll first show that the first option gives a non-normalizable solution. From your recurrence relation, you should be able to read off that for large values of $k$, we approximately have $a_{k+2} = (2/k)a_k$, with (approximate) solution
\begin{equation}
\label{harmoscpowerseriesinfiniteapprox}
a_k \approx \frac{A}{(k/2)!},
\end{equation}
with $A$ a constant. Find the function $u(\xi)$ for the `solution'~(\ref{harmoscpowerseriesinfiniteapprox}), and argue it won't be an acceptable solution.
\item Having ruled out the possibility that the series continue forever, it has to terminate at some point. From that condition, get the possible values of $K$, and thus of the energy eigenvalue $E$.
\item As the recurrence relation only contains $a_k$ and $a_{k+2}$, we get two qualitatively distinct sets of solutions, one with all `odd-$k$' $a_k$'s equal to zero (giving an even function) and one with all `even-$k$' $a_k$'s equal to zero (giving an odd function). Argue why only one of the two sets is permissable at a given time.
\item We get the two solutions from initial conditions $a_1=0$ and $a_0 = 0$, respectively. The resulting polynomials for $u(\xi)$ are multiples of the \emph{Hermite polynomials}, denoted $H_n(\xi)$. Find $u(\xi)$ for $n=0, 1, 2, 3$.
\item Find $\psi_0(x)$ and $\psi_1(x)$.
\end{enumerate}

\ifincludesolutions
\Solution
\begin{enumerate}[(a)]
\item All we need to do is calculate the second derivative of $\psi$ with respect to $\xi$:
\begin{align*}
\dv{\psi}{\xi} &= \dv{\psi}{x} \dv{x}{\xi} = \sqrt{\frac{\hbar}{m\omega}} \dv{\psi}{x}, \\
\dv[2]{\psi}{\xi} &= \dv{x} \left(\dv{\psi}{\xi}\right) \dv{x}{\xi} = \sqrt{\frac{\hbar}{m\omega}} \dv{x} \left(\dv{\psi}{\xi}\right) = \frac{\hbar}{m\omega} \dv[2]{\psi}{x},
\end{align*}
so we can re-write the original equation~(\bookref{SEHP}) in terms of $\xi$ and $K$:
\begin{align*}
\hat{H} \psi = \frac{1}{2m} \left[ \hat{p}^2 + (m \omega \hat{x})^2 \right] \psi = -\frac{\hbar^2}{2m} \dv[2]{\psi}{x} + \frac12 m \omega^2 x^2 \psi &= E \psi \\
-\frac{\hbar}{m\omega} \dv[2]{\psi}{x} + m \omega x^2 \psi &= \frac{2 E}{\hbar \omega} \psi \\
-\dv[2]{\psi}{\xi} + \xi^2 \psi &= K \psi,
\end{align*}
which is equation~(\ref{TISEHarmonicRescaled}).
\item We simply substitute the given Ansatz:
\begin{align*}
\dv[2]{\xi} \exp(\lambda \xi^2) &= \dv{\xi} \left[ 2 \lambda \xi \exp(\lambda \xi^2) \right] = \left[ 4 \lambda^2 \xi^2 + 2 \lambda \right] \exp(\lambda \xi^2) \\
&\approx \xi^2 \exp(\lambda \xi^2).
\end{align*}
As noted, the solution is not exact. However, we readily find that we get an approximate solution if
\begin{align*}
4 \lambda^2 &= 1 \qquad \Rightarrow \qquad \lambda = \pm \frac12.
\end{align*}
\item We have
\begin{align*}
\dv{\psi}{\xi} &= \left[\dv{u}{\xi} - \xi u \right] e^{-\frac12 \xi^2}, \\
\dv[2]{\psi}{\xi} &= \left[\dv[2]{u}{\xi} - 2\xi \dv{u}{\xi} + (\xi^2-1) u \right] e^{-\frac12 \xi^2},
\end{align*}
and thus
\begin{align*}
\dv[2]{u}{\xi} - 2\xi \dv{u}{\xi} + (\xi^2-1) u &= \left(\xi^2 - K \right) u \\
\dv[2]{u}{\xi} - 2\xi \dv{u}{\xi} + (K-1) u &= 0.
\end{align*}
\item We have (shifting $k$ and using that terms with $k=0$ drop out anyway):
\begin{align*}
\dv{u}{\xi} &= \sum_{k=0}^\infty k a_k \xi^{k-1} = \sum_{k=0}^\infty (k+1) a_{k+1} \xi^k, \\
\dv[2]{u}{\xi} &= \sum_{k=0}^\infty k (k+1) a_{k+1} \xi^{k-1} = \sum_{k=0}^\infty (k+1) (k+2) a_{k+2} \xi^k, \\
0 &= \sum_{k=0}^\infty (k+1) (k+2) a_{k+2} \xi^k - 2 \xi \sum_{k=0}^\infty k a_k \xi^{k-1} + (K-1) \sum_{k=1}^\infty a_k \xi^k \\
&= \sum_{k=0}^\infty \left[ (k+1) (k+2) a_{k+2} - (2 k - K + 1) a_k \right] \xi^k,
\end{align*}
where we used the first expansion for the first derivative of $u$ as it gets multiplied by $\xi$.
\item For a power series to vanish, each of its terms must identically vanish (as power series expansions are unique), so we have
\begin{align*}
a_{k+2} &= \frac{2 k - K + 1}{(k+1) (k+2)} a_k.
\end{align*}
\item We get
\begin{align*}
u(x) = A \sum_{k=0}^\infty \frac{1}{(k/2)!} \xi^k \approx A \sum_{k=0}^\infty \frac{1}{k!} \xi^{2k} \approx A e^{\xi^2},
\end{align*}
which diverges for $\xi \to \pm \infty$ (and is actually exactly the opposite of the exponential behavior we split off earlier), so this solution would not be normalizable.
\item For the series to terminate, the recurrence relation must give $0$ at some finite value of $k$ (which we'll call $n$). Then $K$ is given by $2n - K + 1 = 0$, or $K = 2n+1$, which gives
\begin{align*}
E_n &= \left(n + \frac12 \right) \hbar \omega \qquad \mbox{for} \quad n=0, 1, 2, 3, \ldots.
\end{align*}
\item If $n$ is even, we must have that all odd terms vanish, as we never get $a_{k+2} = 0$ for any odd value of $k$, and thus the odd series would be infinite (and not be normalizable). With the same argument the even terms must vanish if $n$ is odd.
\item We have
\begin{alignat*}{3}
n=0 & \quad & a_0 = 1, a_1 = 0, a_2 = 0 & \quad & u_0 = 1 \\
n=1 & \quad & a_0 = 0, a_1 = 1, a_3 = 0 & \quad & u_1 = \xi \\
n=2 & \quad & a_0 = 1, a_1 = 0, a_2 = -2 & \quad & u_2 = 1 - 2 \xi^2 \\
n=3 & \quad & a_0 = 0, a_1 = 1, a_3 = -\frac23 & \quad & u_3 = \xi - \frac23 \xi^3
\end{alignat*}
\item We get
\begin{align*}
\psi_0(x) &= u_0(\xi) e^{-\frac12 \xi^2} = a_0 e^{-\frac12 \xi^2} = \left( \frac{m \omega}{\pi \hbar} \right)^{1/4} \exp\left( - \frac{m \omega x^2}{2\hbar} \right) \\
\psi_1(x) &= u_1(\xi) e^{-\frac12 \xi^2} = a_1 \xi e^{-\frac12 \xi^2} = \left( \frac{m \omega}{\pi \hbar} \right)^{1/4} \sqrt{\frac{2 m \omega}{\hbar}} x \exp\left( - \frac{m \omega x^2}{2\hbar} \right)
\end{align*}
\end{enumerate}
\fi