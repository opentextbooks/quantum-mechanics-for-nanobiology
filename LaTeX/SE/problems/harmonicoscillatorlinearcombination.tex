%Problemtitle: Combining harmonic oscillator states
%Source: Sakurai Modern Quantum Mechanics Solutions 2.6 (except d).
As we've seen in section~\bookref{sec:harmonicpotential}, we can find the eigenstates of the one-dimensional harmonic potential, $V(x) = \frac12 m \omega^2 x^2$, through repeated application of the raising operator $\hat{a}_{+}$ on the ground state~$\psi_0(x)$, and we get the ground state itself from the condition that acting on it with the lowering operator $\hat{a}_{+}$ should give zero (i.e., $\hat{a}_- \psi_0 = 0$).
%\ifproblemset
%The raising and lowering operators are defined as
%\begin{equation}
%\label{HOraisinglowering}
%\hat{a}_\pm = \frac{1}{\sqrt{2 m \hbar \omega}} \left( \mp i \hat{p} + m \omega \hat{x} \right).
%\end{equation}
%For the ground state we found
%\begin{equation}
%\psi_0(x) = \left(\frac{m\omega}{\pi \hbar}\right)^{1/4} \exp\left(- \frac{m \omega}{2 \hbar} x^2 \right).
%\end{equation}
%\fi
\begin{enumerate}[(a)]
\item Construct and normalize the first excited state, $\psi_1(x)$, which is proportional to $\hat{a}_+ \psi_0(x)$. Verify that your result satisfies the general rule that
\begin{equation}
\label{HOhighereigenfunctions}
\psi_n = \frac{1}{\sqrt{n!}} \left(\hat{a}_+\right)^n \psi_0(x).
\end{equation} 
\item Construct a new, normalized state $\psi$, which is a linear combination of $\psi_0$ and $\psi_1$, i.e., $\psi = c_0 \psi_0 + c_1 \psi_1$, such that $\ev{\hat{x}}$ is as large as possible. The answer, as you might guess, is given by
\begin{equation}
\label{HOlincombinationmaxX}
\psi = \frac{1}{\sqrt{2}} \left( \psi_0(x) + \psi_1(x) \right).
\end{equation}
For a full solution, you'd have to allow the coefficients $c_0$ and $c_1$ to take complex values, but for here, it suffices to take them to be real. You may use without needing to prove it that the eigenstates of the harmonic potential Hamiltonian are orthogonal.
%\textit{Hint}: remember that the coefficients in a linear combination of eigenfunctions need not be real, and that any complex number $z$ can be written as $z = |z| e^{i \theta}$, with $|z|$ and $\theta$ real. You may use without needing to prove it that the eigenstates of the harmonic potential Hamiltonian are orthogonal.
\item A particle is initialized at $t=0$ in the state~$\psi$ you constructed at (b). Give its time evolution, i.e., give the $\Psi(x, t)$ corresponding to this wave function $\Psi$.
\item If you were to measure the energy of the state, would it matter at which point in time you do so?
\end{enumerate}


\ifincludesolutions
\Solution
\begin{enumerate}[(a)]
\item We have (lumping all constants in a to-be-determined normalization constant $A$):
\begin{align*}
\hat{a}_+ \psi_0(x) &= A \left(- i \hat{p} + m \omega \hat{x} \right) \exp\left(- \frac{m \omega}{2 \hbar} x^2 \right) = A \left(- \hbar \frac{\dd}{\ddx} + m \omega x \right) \exp\left(- \frac{m \omega}{2 \hbar} x^2 \right) \\
&= A \left(m \omega x + m \omega x \right) \exp\left(- \frac{m \omega}{2 \hbar} x^2 \right) = B x \exp\left(- \frac{m \omega}{2 \hbar} x^2 \right),
\end{align*}
where we absorbed some more constants in $B$ \score{2 pt}. Normalization gives us the value of $B$:
\begin{align*}
1 &= \Braket{\psi_1|\psi_1} = |B|^2 \int_{-\infty}^\infty x^2 \exp\left(- \frac{m \omega}{\hbar} x^2 \right) \, \ddx = \frac{\sqrt{\pi}}{2 (m \omega / \hbar)^{3/2}} \\
B &= \frac{2^{1/2} (m \omega)^{3/4}}{\pi^{1/4} \hbar^{3/4}} = \left[\frac{4}{\pi} \left(\frac{m \omega}{\hbar} \right)^3 \right]^{1/4} = \left(\frac{m\omega}{\pi \hbar}\right)^{1/4} \sqrt{\frac{2 m \omega}{\pi}},
\end{align*}
where we used equation (37) of the equations sheet for the integral \score{1 pt for any correct version}. From the last version, we see that equation~(\ref{HOhighereigenfunctions}) is indeed satisfied for $n=1$.
\item \textit{Simplified answer}: In general, our new state $\psi$ can be written as $c_0 \psi_0 + c_1 \psi_1$. Normalization gives us a relation between these two constants:
\begin{align*}
1 &= \Braket{\psi|\psi} = \Braket{c_0 \psi_0 + c_1 \psi_1|c_0 \psi_0 + c_1 \psi_1} = |c_0|^2 + |c_1|^2, \\
|c_1| &= \sqrt{1 - |c_0|^2},
\end{align*}
where the last equality of the first line holds because the eigenstates are both orthogonal and normalized \score{1 pt}. If $c_0$ and $c_1$ are both real, this simplifies to $c_1 = \sqrt{1-c_0^2}$. Calculating the expectation value of $\hat{x}$ in the state $\psi$ is easy if we express $\hat{x}$ in terms of the operators $\hat{a}_\pm$:
\begin{align*}
\hat{x} &= \sqrt{\frac{\hbar}{2 m \omega}} \left(\hat{a}_+ + \hat{a}_-\right),
\end{align*}
which gives:
\begin{align*}
\ev{\hat{x}} &= \Braket{\psi|\hat{x}|\psi} = c_0^2 \Braket{\psi_0 | \hat{x} | \psi_0} + c_0 c_1 \Braket{\psi_0 | \hat{x} | \psi_1} + c_0 c_1 \Braket{\psi_1 | \hat{x} | \psi_0} + c_1^2 \Braket{\psi_1 | \hat{x} | \psi_1} \\
&= 0 + c_0 c_1 \sqrt{\frac{\hbar}{2 m \omega}} \Braket{\psi_0 | \left(\hat{a}_+ + \hat{a}_-\right) | \psi_1} + c_0 c_1 \sqrt{\frac{\hbar}{2 m \omega}} \Braket{\psi_1 | \left(\hat{a}_+ + \hat{a}_-\right) | \psi_0} + 0 \\
&= \sqrt{\frac{\hbar}{2 m \omega}} \left[ c_0 c_1 \frac{1}{\sqrt{2}}\Braket{\psi_0 | \psi_2} + c_0 c_1 \Braket{\psi_0 | \psi_0} +  c_0 c_1 \Braket{\psi_1 | \psi_1} + c_0 c_1 \cdot 0 \right] \\
&= \sqrt{\frac{\hbar}{2 m \omega}} \left(c_0 c_1 + c_0 c_1 \right) = 2 \sqrt{\frac{\hbar}{2 m \omega}} c_0 \sqrt{1 - |c_0|^2}
\end{align*}
where we used in the second line that the integrals are odd (and thus vanish), in the third line that $\hat{a}_- \psi_0 = 0$, and in the fourth line that the eigenstates are orthogonal and normalized \score{2 pt}. We now need the value of $c_0$ that maximizes $\ev{\hat{x}}$, which is a simple matter of differentiating:
\begin{align*}
0 &= \diff{\ev{\hat{x}}}{c_0} = (\mbox{const}) \left[ \sqrt{1 - c_0^2} - \frac{c_0^2}{\sqrt{1 - c_0^2}} \right] \\
0 &= 1 - c_0^2 - c_0^2 = 1 - 2 c_0^2 \\
c_0 &= \frac{1}{\sqrt{2}},
\end{align*}
which gives equation~(\ref{HOlincombinationmaxX}) \score{1 pt}.
%\bigskip
\textit{Full answer}: In general, our new state $\psi$ can be written as $c_0 \psi_0 + c_1 \psi_1$. Normalization gives us a relation between these two complex constants:
\begin{align*}
1 &= \Braket{\psi|\psi} = \Braket{c_0 \psi_0 + c_1 \psi_1|c_0 \psi_0 + c_1 \psi_1} = |c_0|^2 + |c_1|^2, \\
|c_1| &= \sqrt{1 - |c_0|^2},
\end{align*}
where the last equality of the first line holds because the eigenstates are both orthogonal and normalized \score{1 pt}. Following the hint, we write
\begin{align*}
c_0 &= |c_0| e^{i \theta_0}, \\
c_1 &= |c_1| e^{i \theta_1} = e^{i \theta_1} \sqrt{1 - |c_0|^2}.
\end{align*}
Calculating the expectation value of $\hat{x}$ in the state $\psi$ is easy if we express $\hat{x}$ in terms of the operators $\hat{a}_\pm$:
\begin{align*}
\hat{x} &= \sqrt{\frac{\hbar}{2 m \omega}} \left(\hat{a}_+ + \hat{a}_-\right),
\end{align*}
which gives:
\begin{align*}
\ev{\hat{x}} &= \Braket{\psi|\hat{x}|\psi} = |c_0|^2 \Braket{\psi_0 | \hat{x} | \psi_0} + c_0^* c_1 \Braket{\psi_0 | \hat{x} | \psi_1} + c_0 c_1^* \Braket{\psi_1 | \hat{x} | \psi_0} + |c_1|^2 \Braket{\psi_1 | \hat{x} | \psi_1} \\
&= 0 + c_0^* c_1 \sqrt{\frac{\hbar}{2 m \omega}} \Braket{\psi_0 | \left(\hat{a}_+ + \hat{a}_-\right) | \psi_1} + c_0 c_1^* \sqrt{\frac{\hbar}{2 m \omega}} \Braket{\psi_1 | \left(\hat{a}_+ + \hat{a}_-\right) | \psi_0} + 0 \\
&= \sqrt{\frac{\hbar}{2 m \omega}} \left[ c_0^* c_1 \frac{1}{\sqrt{2}}\Braket{\psi_0 | \psi_2} + c_0^* c_1 \Braket{\psi_0 | \psi_0} +  c_0 c_1^* \Braket{\psi_1 | \psi_1} + c_0 c_1^* \cdot 0 \right] \\
&= \sqrt{\frac{\hbar}{2 m \omega}} \left(c_0^* c_1 + c_0 c_1^* \right) = \sqrt{\frac{\hbar}{2 m \omega}} |c_0| \sqrt{1 - |c_0|^2} \left( e^{-i\theta_0} e^{i \theta_1} + e^{i \theta_0} e^{-i \theta_1} \right) \\
&= 2 \sqrt{\frac{\hbar}{2 m \omega}} |c_0| \sqrt{1 - |c_0|^2} \cos\left(\theta_1 - \theta_0\right),
\end{align*}
where we used in the second line that the integrals are odd (and thus vanish), in the third line that $\hat{a}_- \psi_0 = 0$, and in the fourth line that the eigenstates are orthogonal and normalized \score{2 pt}. We now need values of $|c_0|$ and $\Delta \theta = \theta_1 - \theta_0$ that maximize $\ev{\hat{x}}$, which is a simple matter of differentiating:
\begin{align*}
0 &= \diff{\ev{\hat{x}}}{|c_0|} = (\mbox{const}) \left[ \sqrt{1 - |c_0|^2} - \frac{|c_0|^2}{\sqrt{1 - |c_0|^2}} \right] \\
0 &= 1 - |c_0|^2 - |c_0|^2 = 1 - 2 |c_0|^2 \\
|c_0| &= \frac{1}{\sqrt{2}}
\end{align*}
\score{0.5 pt} and 
\begin{align*}
0 &= \frac{\partial\ev{\hat{x}}}{\partial \Delta \theta} = (\mbox{const}) \sin(\Delta \theta) \\
\theta_1 &= \theta_0 + q \pi, \qquad q \in \mathbb{Z}.
\end{align*}
For a maximum, we also need the second derivative to be negative, so
\begin{align*}
\left. \frac{\partial^2 \ev{\hat{x}}}{\partial \theta_1^2} \right|_{\theta_1 = \theta_{1, \mathrm{max}}} < 0
\end{align*}
which allows only solutions for which $q$ is even. We thus have
\begin{align*}
\psi = \frac{e^{i \theta_0}}{\sqrt{2}} \psi_0 + \frac{e^{i (\theta_1 + 2 q \pi)}}{\sqrt{2}} \psi_1 = \frac{e^{i \theta_0}}{\sqrt{2}} \left( \psi_0 + \psi_1 \right).
\end{align*}
Using the freedom we have to set the overall phase, we pick $\theta_0 = 0$, and get equation~(\ref{HOlincombinationmaxX}).
\item The time evolution of a state that is written as a linear combination of eigenstates at $t=0$ is given by the linear combination of the time evolution of those states \score{1 pt, give point for either correctly stating or correctly using this idea}. The energy of the $n$th eigenstate of the harmonic oscillator is given by $E_n = (n + \frac12) \hbar \omega$, so we get
\begin{align*}
\Psi(x, t) &= \frac{1}{\sqrt{2}} \left( \psi_0(x) e^{-i E_0 t / \hbar} + \psi_1(x) e^{-i E_1 t / \hbar}\right) \\
&= \frac{1}{\sqrt{2}} \left(\frac{m\omega}{\pi \hbar}\right)^{1/4} \exp\left(- \frac{m \omega}{2 \hbar} x^2 \right) e^{-i \omega t / 2} \left[ 1 + \sqrt{\frac{2 m \omega}{\pi}} x e^{-i \omega t} \right]
\end{align*}
\score{1 pt; either form (first or second line) is fine}.
\item No. While the probability distribution of the position of the particle will depend on time, the square of the coefficients of the energy eigenfunctions (i.e., the states $\psi_0$ and $\psi_1$) remain time-independent, and therefore the probabilities of measuring $E_0$ or $E_1$ remain constant over time \score{1 pt}.
\end{enumerate}
\fi