\chapter{Perturbation theory}
\label{ch:perturbationtheory}
The hydrogen atom Hamiltonian with the Coulomb potential term between the nucleus and the electron is the most complicated example for which we know the exact solutions. It is however not complete, as it ignores some physical terms, including the interaction between the spin and the orbital angular momentum and a relativistic correction necessary because the electrons can move quite fast. Fortunately, the additional terms that account for these physical effects are relatively small, and can be treated as perturbations to the zeroth-order Coulomb interaction result. They do however have an important consequence: they cause degenerate energy levels to split up, thus (as it is usually stated) `lifting the degeneracy'. Indeed, we find multiple wavelengths for, say, the transition from the $n=2$ to the $n=1$ state of hydrogen. To see how this works, we will start by the simplest case: a perturbation to a non-degenerate state, then build up to degenerate cases.

\section{Non-degenerate perturbation theory}
\label{sec:nondegenerateperturbationtheory}
Suppose that we have a Hamiltonian that can be written as the sum of two terms, with one much larger than the other, i.e.
\begin{equation}
\label{perturbedHamiltonian}
\hat{H} = \hat{H}^0 + \varepsilon \hat{H}',
\end{equation}
where $\varepsilon \ll 1$. We'll say that $\hat{H}'$ is a \index{perturbation theory}\emph{perturbation} of the unperturbed Hamiltonian $\hat{H}^0$. Suppose moreover that we have a complete set of orthogonal eigenfunctions $\psi_n^0(x)$ of $\hat{H}^0$, with eigenvalues $E_n^0$:
\begin{equation}
\label{unperturbedSE}
\hat{H}^0 \psi_n^0 = E_n^0 \psi_n^0, \qquad \mbox{with} \quad \Braket{\psi_m^0 | \psi_n^0} = \delta_{mn}.
\end{equation}
The Schr\"odinger equation for the full system reads
\begin{equation}
\label{fullSE}
\hat{H} \psi_n = E_n \psi_n,
\end{equation}
which we'd like to solve for the eigenvalues $E_n$ and eigenfunctions $\psi_n$. If doing so exactly is not possible (as will usually be the case), we can approximate the solutions by series expansions in the parameter~$\varepsilon$. We then write
\begin{subequations}
\begin{align}
\label{perturbedenergy}
E_n &= E_n^0 + \varepsilon E_n^1 + \varepsilon^2 E_n^2 + \ldots, \\
\label{perturbedwavefunction}
\psi_n &= \psi_n^0 + \varepsilon \psi_n^1 + \varepsilon^2 \psi_n^2 + \ldots.
\end{align}
\end{subequations}
We call $E_n^1$ and $\psi_n^1$ the first-order corrections to the $n$th energy and wavefunction, respectively. It turns out that finding an expression for $E_n^1$ is pretty easy. We simply substitute our expansion back in the Schr\"odinger equation~(\ref{fullSE}) and collect terms that are of the same power in $\varepsilon$:
\begin{subequations}
\begin{align}
\hat{H} \psi_n = \left( \hat{H}^0 + \varepsilon \hat{H}' \right) \left(\psi_n^0 + \varepsilon \psi_n^1 + \varepsilon^2 \psi_n^2 + \ldots \right) &= E_n \psi_n = \left( E_n^0 + \varepsilon E_n^1 + \varepsilon^2 E_n^2 + \ldots \right) \left(\psi_n^0 + \varepsilon \psi_n^1 + \varepsilon^2 \psi_n^2 + \ldots \right) \\
\label{perturbedSEexpansion}
\hat{H}^0 \psi_n^0 + \varepsilon \left( \hat{H}^0 \psi_n^1 + \hat{H}' \psi_n^0 \right) + \varepsilon^2 \left( \hat{H}^0 \psi_n^2 + \hat{H}' \psi_n^1 \right) + \ldots &= E_n^0 \psi_n^0 + \varepsilon \left( E_n^0 \psi_n^1 + E_n^1 \psi_n^0 \right) \nonumber\\
& \qquad + \varepsilon^2 \left( E_n^0 \psi_n^2 + E_n^1 \psi_n^1 + E_n^2 \psi_n^0 \right) + \ldots
\end{align}
\end{subequations}
To zeroth order, we retrieve the unperturbed equation~(\ref{unperturbedSE}). To first order, we get
\begin{equation}
\label{SEfirstorderperturbation}
\hat{H}^0 \psi_n^1 + \hat{H}' \psi_n^0 = E_n^0 \psi_n^1 + E_n^1 \psi_n^0.
\end{equation}
We now take the inner product of~(\ref{SEfirstorderperturbation}) with $\psi_n^0$, which gives:
\begin{equation}
\label{SEfirstorderperturbationinnerproduct}
\Braket{\psi_n^0 | \hat{H}^0 \psi_n^1} + \Braket{\psi_n^0 | \hat{H}' \psi_n^0} = E_n^0 \Braket{\psi_n^0 | \psi_n^1} + E_n^1 \Braket{\psi_n^0 | \psi_n^0}.
\end{equation}
Because $\hat{H}^0$ is Hermitian, the first term on the left-hand side of equation~(\ref{SEfirstorderperturbationinnerproduct}) can be rewritten as
\begin{equation}
\Braket{\psi_n^0 | \hat{H}^0 \psi_n^1}  = \Braket{\hat{H}^0 \psi_n^0 |  \psi_n^1} = E_n^0 \braket{\psi_n^0 |  \psi_n^1}
\end{equation}
and it thus equals the first term on the right-hand side. Moreover, as $\psi_n^0$ is normalized, the inner product in the second term on the right-hand side of~(\ref{SEfirstorderperturbationinnerproduct}) equals~$1$. We thus find the simple result that the first-order correction to the energy, $E_n^1$, is given by the expectation value of the perturbation~$\hat{H}'$ in the unperturbed eigenstate~$\psi_n^0$:
\begin{equation}
\label{firstorderperturbationenergy}
E_n^1 = \Braket{\psi_n^0 | \hat{H}' | \psi_n^0}.
\end{equation}
Equation~(\ref{firstorderperturbationenergy}) is the central result of this section, and will be used when calculating corrections to the hydrogen energy levels in section~\ref{sec:hydrogenfinestructure}. We can also calculate the corrections to the wavefunction itself. These are not nearly as useful, unless you want to get the second-order corrections to the energies. The results (see problem~\ref{ch:perturbationtheory}.\ref{pb:nondegenerateperturbationsecondorder}) are
\begin{align}
\label{firstorderperturbationwavefunction}
\psi_n^1 &= \sum_{m \neq n} \frac{\Braket{\psi_m^0|\hat{H}'|\psi_n^0}}{E_n^0 - E_m^0} \psi_m^0, \\
\label{secondorderperturbationenergy}
E_n^2 &= \sum_{m \neq n} \frac{\left|\Braket{\psi_m^0|\hat{H}'|\psi_n^0}\right|^2}{E_n^0 - E_m^0}.
\end{align}
One can iterate further and find higher-order corrections, though their usefullness (as values that you could actually measure) quickly declines.

\subsection{Application: Van der Waals interactions}
\label{sec:vanderWaalsforce}
Suppose we have two hydrogen atoms, located a distance~$R$ from each other, with $R$ significantly larger than the Bohr radius~$a_0$. Although both atoms are electrically neutral, the positively charged nucleus of one atom still exerts a Coulomb force on the negatively charged electron of the other, and vice versa. Likewise, the electrons and nuclei exert forces on each other. These forces are much smaller than the force between each nucleus and its corresponding electron. We can therefore split the Hamiltonian of the joint system into a term $\hat{H}^0$ containing the two atoms as isolated parts, and a perturbation term~$\hat{H}'$ for their interaction:
\begin{align}
\label{vanderWaalsHamiltonian}
\hat{H} &= \hat{H}^0 + \hat{H}' = \hat{H}_\mathrm{H}^{(1)} + \hat{H}_\mathrm{H}^{(2)} + \hat{H}' \nonumber\\
&= \left[ \frac{\hbar^2}{2 \me} \nabla_1^2 + \frac{\hbar^2}{2 \me} \nabla_2^2 - \frac{e^2}{r_1} - \frac{e^2}{r_2} \right] + e^2 \left[ \frac{1}{R} + \frac{1}{r_{12}} - \frac{1}{r_{1\mathrm{B}}} - \frac{1}{r_{2\mathrm{A}}} \right],
\end{align}
where A and B label the two nuclei and 1 and 2 the two electrons, see figure~\ref{fig:vanderWaalsforces}(a). The solution to the unperturbed Hamiltonian~$\hat{H}^0$ is simply the product of the eigenstates of the two hydrogen Hamiltonians. As we're only interested in the energy here, we'll consider wavefunctions $\psi_n(\bvec{r})$ with energies $E_n = E_1 / n^2$ (thus ignoring the $l$ and $m$ quantum numbers) and write, with a little abuse of notation
\begin{align}
\label{twohydrogencombistate}
\psi_{nm}(\bvec{r}_1, \bvec{r}_2) &= \psi_n(\bvec{r}_1) \psi_m(\bvec{r}_2), \\
\hat{H}^0 \psi_{nm}(\bvec{r}_1, \bvec{r}_2) &= \left(E_n + E_m\right) \psi_{nm}(\bvec{r}_1, \bvec{r}_2).
\end{align}
As we're in the limit that $R \gg a_0$, the two wavefunctions hardly overlap, and we don't have to worry about exchange terms. Moreover, in this large-separation limit, the perturbation term to the Hamiltonian becomes a dipole-dipole interaction\footnote{The coordinates $x_1$, $x_2$ etc. in equation~(\ref{vanderWaalsdipole}) are with respect to the center of the atom the electron belongs to. See problem~\ref{ch:perturbationtheory}.\ref{pb:vanderWaalsperturbation} for the derivation of~(\ref{vanderWaalsdipole}) from~(\ref{vanderWaalsHamiltonian}).}
\begin{equation}
\label{vanderWaalsdipole}
\hat{H}' = \frac{e^2}{R^3} \left(x_1 x_2 + y_1 y_2 - 2 z_1 z_2 \right).
\end{equation}
If both atoms are in the ground state, the expectation value of this perturbation term is zero, and therefore the first order correction to the energies vanishes. However, the square of the expectation value is not zero, and we thus get a correction to the ground state energy by summing over the excited states:
\begin{align}
\label{vanderWaalsEnergycorrection}
E_1^2 &= \sum_{nm \neq 11} \frac{\left|\braket{\psi_{nm}(\bvec{r}_1, \bvec{r}_2)|\hat{H}'|\psi_{11}(\bvec{r}_1, \bvec{r}_2)}\right|^2}{E_{11} - E_{nm}} \\
&= \frac{e^4}{R^6} \sum_{nm \neq 11} \frac{1}{E_{11}-E_{nm}} \left[ \braket{\psi_n(\bvec{r}_1)|x_1|\psi_1(\bvec{r}_1)} \braket{\psi_m(\bvec{r}_2)|x_2|\psi_1(\bvec{r}_2)} + \ldots \right]^2 \\
&\sim - \frac{\alpha_1 \alpha_2}{R^6},
\end{align}
where
\begin{equation}
\alpha_i = e^2 \sum_{n>1} \frac{\left|\braket{\psi_n(\bvec{r})|x|\psi_1(\bvec{r}}\right|^2}{E_n-E_1}
\end{equation}
is the \index{atomic polarizability}\emph{atomic polarizability} of the atom. The atomic polarizability is a measure for the strength of the \emph{induced dipole} in an otherwise symmetric atom\footnote{The atomic polarizability has a direct relation with the dielectric constant~$\varepsilon$ of a macroscopic sample of the material, where $\varepsilon = 1 + 4 \pi n \alpha$, with $n$ the number density of the atoms in the sample.}. We thus understand the resulting attractive force as the result of a mutually induced dipole in the two hydrogen atoms. It is known as the \index{van der Waals force}\emph{van der Waals force}. Although our calculation here is for two hydrogen atoms, attractive van der Waals forces are present between any two neutral atoms or molecules.

Van der Waals forces are quite weak, as their energy decays as $1/R^6$. Nonetheless, they are key factors in many chemical, biological and (condensed matter) physical systems. One particular striking example often attributed to van der Waals forces is the ability of geckos to climb walls and run upside-down on a ceiling. Specifically, it's the van der Waals forces between the $\beta$-keratin proteins in the lamellar, spatula shaped setae on the gecko's feet (see figure~\ref{fig:vanderWaalsforces}(b)) that are credited with the van der Waals interactions~\cite{Autumn2002}. However, other studies suggests that gecko adhesion could be due to induced electrification of surfaces (and thus electrostatic interactions)~\cite{Izadi2014}, or to the surface chemistry of the gecko feet, specifically the presence of lipids~\cite{Rasmussen2022}. Although the definitive explanation for the gecko's climbing ability is thus still open, the possibility that van der Waals forces are at play has inspired various applications~\cite{Northen2005}, including \bookhref{https://www.reuters.com/article/us-nano-glue-idUSN0942431020081009/}{biologically inspired glue} and \bookhref{https://newatlas.com/bioinspired-adhesive-tape-kiel/20406/}{adhesive tape}.

\begin{figure}[ht]
\centering
\includegraphics[scale=1]{SE/figures/vanderWaalsforces.pdf}
\caption{Van der Waals forces. (a) Coordinates for the long-distance interaction between two hydrogen atoms. (b) Close-up of the foot of a gecko as it climbs a glass wall~[\ref{geckofootimage}]. The lamellar structure of the skin is clearly visible.}
\label{fig:vanderWaalsforces}
\end{figure}


\section{Degenerate perturbation theory}
\label{sec:degenerateperturbationtheory}
In section~\ref{sec:nondegenerateperturbationtheory} we assumed that all the eigenstates of the unperturbed Hamiltonian were non-degenerate. If the states are degenerate, we cannot use equation~(\ref{firstorderperturbationwavefunction}), as the denominator will diverge if $E_m^0 = E_n^0$ for any pair $m \neq n$. Fortunately, the effect of the perturbation will typically be to lift the degeneracy. Based on the perturbed energies, we will even be able to construct `good' eigenstates of the unperturbed Hamiltonian (limit cases of the perturbed state when taking the perturbation parameter $\varepsilon$ to zero) for which we can use equation~(\ref{firstorderperturbationenergy}), as we'll see below.

\subsection{Eigenstates of the perturbation matrix}
\label{sec:perturbationmatrix}
To illustrate how to deal with degenerate eigenstates of an unperturbed Hamiltonian, we start with a system that has two-fold degeneracy (the procedure for higher degeneracies will be analogous): suppose $\hat{H}^0$ has two orthogonal eigenstates $\psi_a^0$ and $\psi_b^0$ that satisfy
\begin{equation}
\label{unperturbedHamiltoniandegenerateeigenstates}
\hat{H}^0 \psi_a^0 = E^0 \psi_a^0, \quad \hat{H}^0 \psi_b^0 = E^0 \psi_b^0, \quad \Braket{\psi_a^0 | \psi_b^0} = 0.
\end{equation}
Note that any linear combination
\begin{equation}
\label{degenerateeigenfunctionslinearcombination}
\psi^0 = \alpha \psi_a^0 + \beta \psi_b^0
\end{equation}
is also an eigenstate of $\hat{H}^0$ with energy $E_0$. Our result will not only be the corrections to the energy, but also which linear combinations correspond to the `good' eigenstates, i.e. which values of $\alpha$ and $\beta$ we should choose for the perturbed system to consist of proper eigenstates.

We calculate the corrections to the energy in much the same way as in non-degenerate perturbation theory. We again write $\hat{H} = \hat{H}^0 + \varepsilon \hat{H}'$ and expand both the energy and the wavefunction of $\hat{H}$ in $\varepsilon$:
\begin{subequations}
\begin{align}
E &= E^0 + \varepsilon E^1 + \varepsilon^2 E^2 + \ldots, \\
\psi &= \psi^0 + \varepsilon \psi^1 + \varepsilon^2 \psi^2 + \ldots.
\end{align}
\end{subequations}
The zeroth-order term of the Schr\"odinger equation with the perturbed Hamiltonian $\hat{H}$ again gives us the unperturbed system, and the first-order term the first variation:
\begin{equation}
\label{SEfirstorderperturbationdegenerate}
\hat{H}^0 \psi^1 + \hat{H}' \psi^0 = E^0 \psi^1 + E^1 \psi^0.
\end{equation}
If we now take the inner product of equation~(\ref{SEfirstorderperturbationdegenerate}) with the original eigenstate~$\psi_a^0$, the first terms on the left and right-hand side are again equal, but the remaining terms are different than in the non-degenerate case. We get
\begin{align}
\Braket{\psi_a^0 | \hat{H}' \psi^0} &= E^1 \Braket{\psi_a^0 | \psi^0} \\
\alpha \Braket{\psi_a^0 | \hat{H}' \psi_a^0} + \beta \Braket{\psi_a^0 | \hat{H}' \psi_b^0} &= \alpha E^1.
\end{align}
Defining $Q_{ij} = \Braket{\psi_i^0 | \hat{H}' \psi_j^0}$ as the `matrix elements' of the perturbation~$\hat{H}'$ in the basis of the unperturbed eigenstates $\left\lbrace\psi_a^0, \psi_b^0\right\rbrace$, we can thus write
\begin{subequations}
\begin{align}
\alpha E^1 &= \alpha Q_{aa} + \beta Q_{ab}, \\
\beta E^1 &= \alpha Q_{ba} + \beta Q_{bb},
\end{align}
\end{subequations}
or, in matrix form
\begin{equation}
\label{degenerateperturbationeigenvalueequation}
Q \twovec{\alpha}{\beta} = E^1 \twovec{\alpha}{\beta}.
\end{equation}
In other words, the first-order corrections $E^1$ to the energy are the eigenvalues of the `perturbation matrix'~$Q$ (the matrix whose elements are obtained from $\hat{H}'$ in the unperturbed state). For a $2 \times 2$ matrix~$Q$, these eigenvalues are given by
\begin{equation}
\label{degeneratefirstorderperturbationenergy}
E^1_\pm = \frac12 \Tr(Q) \pm \frac12 \sqrt{\left(\Tr(Q)\right)^2-4\det(Q)} = \frac12 \left(Q_{aa} + Q_{bb}\right) \pm \frac12 \sqrt{\left(Q_{aa}-Q_{bb}\right)^2 +4 Q_{ab}Q_{ba}}.
\end{equation}
Equation~(\ref{degeneratefirstorderperturbationenergy}) gives us the first order corrections~$E^1_\pm$ to the energy. From equation~(\ref{degenerateperturbationeigenvalueequation}) we can then solve for the coefficients~$\alpha$ and $\beta$, which give us the corresponding linear combinations of the `good' unperturbed states, i.e., the states which are the limit cases of the perturbed ones if we take the magnitude of the perturbation back to zero. Note that for the `good' states, the matrix~$Q$ becomes diagonal, and we retrieve equation~(\ref{firstorderperturbationenergy}) for the energies of the perturbations. Extending to higher-order degeneracies is straightforward: for an $n$-fold degenerate energy, we get an $n \times n$ Hermitian matrix $Q$, with $n$ real eigenvalues and corresponding eigenstates.

In practice, $\hat{H}^0$ will usually have a higher degree of symmetry than $\hat{H}'$. The `lifting of the degeneracy' of the eigenstates of $\hat{H}^0$ then corresponds to a `breaking of the symmetry'. A good example is what happens if you put a hydrogen atom in a magnetic or electric field. While at zero field all directions are equal (full rotational symmetry), in the presence of a constant field, the direction in which the field increases is different from the direction(s) in which it is constant. In three dimensions, for a homogeneous field in the $z$ direction, there is still rotational symmetry in the $xy$ plane, but we have broken the full spherical symmetry. A further reduction of symmetry could lead to a further lifting of a remaining degeneracy. This correspondence between symmetry and degeneracy is a key concept in quantum mechanics; it also suggests a way of picking `good' eigenstates from the start.

\subsection{Joint eigenstates}
\label{sec:degenerateperturbationjointeigenstates}
While we can find the `good' eigenstates for any perturbation with equation~(\ref{degenerateperturbationeigenvalueequation}), there is an easier way if we have a second physical quantity we can measure. For example, for any but the ground state of hydrogen we have degenerate eigenstates of the Hamiltonian, but we can distinguish between states with different values of the orbital quantum number~$l$ by measuring the magnitude of the state's total angular momentum. The corresponding states with different eigenvalues will then correspond to `good' eigenstates, i.e. states for which the perturbation matrix~$Q$ is diagonal.

We can prove the above statement in a more general setting. We'll work out the details for a doubly degenerate state again, as higher degeneracies follow easily. Suppose therefore that we have a perturbed Hamiltionian of the form~(\ref{perturbedHamiltonian}), where the unperturbed part has two degenerate eigenstates as in~(\ref{unperturbedHamiltoniandegenerateeigenstates}). Suppose moreover that we have another Hermitian operator~$\hat{A}$ which commutes with both $\hat{H}^0$ and $\hat{H}'$. Because $\hat{A}$ commutes with $\hat{H}^0$, they have a shared set of eigenfunctions. These eigenfunctions need not be the $\psi_a^0$ and $\psi_b^0$ of equation~(\ref{unperturbedHamiltoniandegenerateeigenstates}), but they can be written as linear combinations of them, as in equation~(\ref{degenerateeigenfunctionslinearcombination}). Moreover, suppose that the joint eigenfunctions are not degenerate as eigenfunctions of $\hat{A}$, i.e., we have
\begin{subequations}
\begin{alignat}{3}
\hat{H}^0 \psi_\lambda^0 &= E_0 \psi_\lambda^0 &\qquad & \hat{H}^0 \psi_\mu^0 &= E_0 \psi_\mu^0 \\
\hat{A} \psi_\lambda^0 &= \lambda \psi_\lambda^0 &\qquad & \hat{A} \psi_\mu^0 &= \mu \psi_\mu^0
\end{alignat}
\end{subequations}
It is now easy to show that the perturbation matrix~$Q$ is diagonal in the basis $\left\lbrace \psi_\lambda^0, \, \psi_\mu^0\right\rbrace$. We simply calculate one of the elements of this matrix, times an eigenvalue of $\hat{A}$:
\begin{align}
\label{jointeigenvalueperturbationmatrixelement}
\lambda Q_{\lambda \mu} &= \lambda \Braket{\psi_\lambda^0 | \hat{H}' \psi_\mu^0} = \Braket{\lambda \psi_\lambda^0 | \hat{H}' \psi_\mu^0} = \Braket{\hat{A} \psi_\lambda^0 | \hat{H}' \psi_\mu^0} = \Braket{\psi_\lambda^0 | \hat{A} \hat{H}' \psi_\mu^0} \nonumber \\
&= \Braket{\psi_\lambda^0 | \hat{H}' \hat{A} \psi_\mu^0} = \mu \Braket{\psi_\lambda^0 | \hat{H}' \psi_\mu^0} = \mu Q_{\lambda \mu},
\end{align}
where we used that $\lambda$ (as the eigenvalue of a Hermitian operator) is real in the second equality, that $\hat{A}$ is Hermitian in the fourth, and that $\hat{A}$ commutes with $\hat{H}'$ in the fifth. From equation~(\ref{jointeigenvalueperturbationmatrixelement}) we find that either $\lambda = \mu$, or $Q_{\lambda \mu} = 0$, and thus the perturbation matrix is diagonal.


\section{The fine structure of hydrogen}
\label{sec:hydrogenfinestructure}
When writing down the Hamiltonian of an electron in a hydrogen atom, we accounted for two effects: the kinetic energy of the electron, and the Coulomb interaction potential. To lowest order, these two terms are indeed dominant, but there are other physical effects that come into play as corrections. The two largest ones, together known as the \index{fine structure of hydrogen}\emph{fine structure}, are a \index{relativistic correction}\emph{relativistic correction} to the kinetic energy, and an extra potential term due to the \index{spin-orbit coupling}coupling between the spin and orbital angular momentum of the electron (known as \emph{spin-orbit coupling}). To assess the magnitude of these effects, we should compare them to the Bohr energy levels, which are proportional to\footnote{To avoid confusion, I'll write $m_\mathrm{e}$ for the mass of the electron throughout this section. Naturally, equation~(\ref{relativsiticenergymomentumequation}) also holds for other relativistic particles if we substitute their mass instead of $m_\mathrm{e}$.} $\alpha^2 m_\mathrm{e} c^2$, where
\begin{equation}
\label{finestructureconstant}
\alpha = \frac{e^2}{4 \pi \varepsilon_0 \hbar c} \approx 	\frac{1}{137}
\end{equation}
is known as the \index{fine structure constant}\emph{fine structure constant}. Both the relativistic and spin-orbit correction have energies of the order $\alpha^4 m_\mathrm{e} c^2$, and are thus an order $\alpha^2$ smaller than the Bohr energies, so perturbation theory will certainly apply. 

\subsection{Relativistic correction to Bohr energies}
Thus far, we've been using quantum-mechanical analogs of classical mechanics expressions in our Hamiltonians. In particular, we've used that the kinetic energy operator is given by $\hat{K} = \hat{p}^2 / 2m_\mathrm{e}$, both in one and in three dimensions. However, when the speed of a particle approaches that of light, this expression for the kinetic energy is no longer correct (it is merely the limit at low velocities). Even if you've never seen special relativity, you will no doubt have seen the most famous equation in physics, Einstein's $E = m_\mathrm{e}c^2$. That equation is also a special case though: it holds only for stationary particles. For moving particles, we instead have a more general version\footnote{There are many texts in which you can find a derivation of equation~(\ref{relativsiticenergymomentumequation}), including my own open textbook \href{https://textbooks.open.tudelft.nl/textbooks/catalog/book/14}{Mechanics and Relativity}.}:
\begin{equation}
\label{relativsiticenergymomentumequation}
E^2 = m_\mathrm{e}^2 c^4 + p^2 c^2.
\end{equation}
The energy in equation~(\ref{relativsiticenergymomentumequation}) is the sum of the kinetic and the rest energy (the $mc^2$ part) of the particle. For the kinetic energy we can then write, in terms of the momentum:
\begin{subequations}
\label{relativsitickineticenergy}
\begin{align}
K &= E - m_\mathrm{e} c^2 = \sqrt{m_\mathrm{e}^2 c^4 + p^2 c^2} - m_\mathrm{e}c^2 = m_\mathrm{e}c^2 \left[ \sqrt{1+\left(\frac{p}{m_\mathrm{e}c}\right)^2} - 1 \right] \\
\label{relativsitickineticenergyseries}
&= m_\mathrm{e} c^2 \left[1 + \frac12 \left(\frac{p}{m_\mathrm{e}c}\right)^2 - \frac18 \left(\frac{p}{m_\mathrm{e}c}\right)^4 + \cdots - 1\right] = \frac{p^2}{2m_\mathrm{e}} - \frac{p^4}{8 m_\mathrm{e}^3 c^2} + \cdots,
\end{align}
\end{subequations}
where we can make the Taylor expansion in the second line if $p \ll m_\mathrm{e}c$, i.e., if the speed of the particle is small compared to that of light. As we see in equation~(\ref{relativsitickineticenergyseries}), the lowest-order term is indeed the classical expression for the kinetic energy, but there are also higher-order corrections. As you can check easily, the rest energy\footnote{The rest energy is simply the energy of a particle at rest, so for the electron, we have $E_\mathrm{rest} = m_\mathrm{e} c^2$.} of an electron (about $0.5\;\mathrm{MeV}$) far exceeds the typical energies of the orbitals (which are of the order of $10\;\mathrm{eV}$), so the expansion holds, and we can suffice with the lowest-order correction. The Hamiltonian then reads:
\begin{equation}
\label{relativsiticHamiltoniancorrection}
\hat{H} = \hat{H}_0 + \hat{H}_\mathrm{r}' = \frac{\hat{p}^2}{2m_\mathrm{e}} - \frac{\hat{p}^4}{8 m_\mathrm{e}^3 c^2} + \hat{V},
\end{equation}
where the relativistic correction (indicated by the subscript $\mathrm{r}$) is given by $\hat{H}_\mathrm{r}' = - \hat{p}^4 / 8 m_\mathrm{e}^3 c^2$. For the unperturbed state, the Schr\"odinger equation gives
\begin{equation}
\label{unperturbedSErel}
\hat{H}_0 \psi^0 = \left(\frac{\hat{p}^2}{2m_\mathrm{e}} + \hat{V} \right)\psi^0 = E^0 \psi^0.
\end{equation}
Now we're in luck: although (as we know) the eigenstates of the unperturbed Hamiltonian of hydrogen are highly degenerate, our perturbation $\hat{H}_\mathrm{r}'$ is radially symmetric, and hence commutes with both $\hat{L}^2$ and $\hat{L}_z$. Therefore, the only quantum number we have to consider here is $n$, and for different values of $n$ we have different energies, also in the unperturbed case. We can therefore find the relativistic correction to the energies through application of non-degenerate perturbation theory. Applying the expression for the first-order correction to the energy, equation~(\ref{firstorderperturbationenergy}), and the fact that $\hat{p}^2$ is Hermitian, we readily find
\begin{align}
\label{relativisticenergycorrection}
E_\mathrm{r}^1 &= \Braket{\psi^0 | \hat{H}_\mathrm{r}' | \psi^0} = - \frac{1}{8 m_\mathrm{e}^3 c^2} \Braket{\psi^0 | \hat{p}^4 \psi^0} = - \frac{1}{8 m_\mathrm{e}^3 c^2} \Braket{\hat{p}^2 \psi^0 | \hat{p}^2 \psi^0} \nonumber\\
&= - \frac{1}{8 m_\mathrm{e}^3 c^2} \Braket{2m_\mathrm{e}(E^0-V)\psi^0 | 2m_\mathrm{e}(E^0-V)\psi^0} = - \frac{1}{2m_\mathrm{e}c^2} \left[(E^0)^2 - 2 E^0 \Braket{V} + \Braket{V^2} \right],
\end{align}
where we used the unperturbed Schr\"odinger equation~(\ref{unperturbedSErel}) to rewrite $\hat{p}^2 \psi^0$ in the second line. Note that equation~(\ref{relativisticenergycorrection}) holds for an arbitrary pair ($\psi_n, E_n$) of eigenfunctions and associated energies of the unperturbed Hamiltonian. Also, equation~(\ref{relativisticenergycorrection}) holds for an arbitrary radially symmetric potential. For the hydrogen atom, we have $V = -e^2/(4\pi\varepsilon_0 r)$, so we have to evaluate the expectation value of $1/r$ and $1/r^2$ in an arbitrary eigenstate $\psi_{nlm}$ of the unperturbed hydrogen Hamiltonian. The calculations are a bit involved (see problem~\ref{ch:perturbationtheory}.\ref{pb:hydrogenrelativisticcorrections}); the results are:
\begin{equation}
\label{hydrogenrelativisticcorrectionexpectationvalues}
\Braket{\frac{1}{r}} = \frac{1}{n^2 a}, \qquad \Braket{\frac{1}{r^2}} = \frac{1}{n^3 \left(l+\frac12\right) a^3},
\end{equation}
where $a$ is the Bohr radius. Substituting these in equation~(\ref{relativisticenergycorrection}), we find for the relativistic correction to the energy $E_n$ of the state $\psi_{nlm}$
\begin{equation}
\label{relativisticenergycorrectionhydrogen}
E_\mathrm{r}^1 = - \frac{(E_n^0)^2}{2 m_\mathrm{e} c^2} \left( \frac{4n}{l + \frac12} - 3 \right).
\end{equation}
Note that $E_\mathrm{r}^1$ depends on the value of the orbital quantum number~$l$ as well as the principal quantum number~$n$, so the relativistic correction does indeed (partially) lift the degeneracy of the eigenstates of the hydrogen atom.

\subsection{Spin-orbit coupling}
\label{sec:spinorbitcoupling}
\begin{figure}[ht]
\centering
\includegraphics[scale=1]{SE/figures/spinorbitcoupling.pdf}
\caption{Spin-orbit coupling in a hydrogen atom. (a) The classical point of view: an electron orbits a single-proton nucleus. (b) From the electron's point of view, the proton is orbiting it. As the proton is a moving electrical charge, it generates a magnetic field~$\bvec{B}$, which couples to the magnetic dipole~$\bvec{\mu}$ originating from the electron's spin.}
\label{fig:spinorbitcoupling}
\end{figure}
As we've seen in section~\ref{sec:spinmagneticfield}, because an electron has a nonzero spin, it has a nonzero magnetic dipole moment~$\bvec{\mu}$ (equation~\ref{spinmagneticdipole}), which can couple to an external magnetic field. As you hopefully remember from electromagnetism, moving electric charges generate magnetic fields. We typically consider electrons in atoms to be moving, and thus they will generate such magnetic fields. That field however cannot interact with the electron's magnetic moment; if it would, the electron would be generating a force on itself from nothing. However, from the point of view of the electron, it's the nucleus that's moving, and as the nucleus also has an electric charge, it does generate a magnetic field that can interact with the electron (cf. figure~\ref{fig:spinorbitcoupling}).

Before we calculate the effect of the interaction between the magnetic field~$\bvec{B}$ due to the moving proton with the magnetic dipole of the electron, we first consider a caveat: because the electron's motion around the proton is an orbit, there's a force acting on the electron, keeping it in orbit, which it can only do by accelerating the electron (i.e., changing its velocity). The same is true for the moon's orbit around the Earth: Earth's gravitational pull exerts a force on the moon, which changes the direction of the moon's velocity such that it stays in its closed orbit. Now while (by Einsteins equivalence postulate) the laws of physics are the same in all inertial frames of reference, we should expect extra terms when transforming between accelerating frames of reference. Again, the same happens in classical mechanics, where going to a rotating frame causes the emergence of fictitious forces like the centrifugal and Coriolis forces (which, among many other things, strongly influence the weather on the spinning sphere on which we live). As it turns out, we're somewhat in luck: there will be no additional fictitious forces here, only a numerical correction factor.

Given the magnetic dipole moment $\bvec{\mu} = \gamma \bvec{S}$, the Hamiltonian for the interaction between the dipole and the magnetic field is simply $\hat{H} = - \bvec{\mu} \cdot \bvec{B}$. For the magnitude of the magnetic field, we invoke the Biot-Savart law from electromagnetism, which states that a current~$I$ in a closed loop of radius $r$ generates a field of magnitude $B = \mu_0 I / 2 r$, with $\mu_0$ the permeability of free space. If the electron takes time $T$ to complete a loop, we have $I = e/T$. The magnitude of the angular momentum of the electron in its circular orbit is $L = r m_\mathrm{e} v = 2 \pi m r^2 / T$. As the magnetic field and the angular momentum moreover point in the same direction, we can thus write
\begin{equation}
\bvec{B} = \frac{1}{4 \pi \varepsilon_0} \frac{e}{m c^2 r^3} \bvec{L},
\end{equation}
where we used $c = 1/\sqrt{\varepsilon_0 \mu_0}$. The Hamiltonian will thus contain a dot product between the spin and the orbital angular momentum; the resulting perturbation to the hydrogen Hamiltonian is therefore known as \index{spin-orbit coupling}\emph{spin-orbit coupling}. As I've stated above, we should expect the actual correction to include an additional factor due to the transition to a non-inertial frame\footnote{The correction is known as Thomas precession, and rather than introducing a factor~$\frac12$, it actually subtracts $1$ from the gyromagnetic ratio's~$g$ factor, which, as we've seen before, is almost exactly~$2$.}; this additional factor turns out to be $\frac12$. Finally, we use again that for a relativistic electron, $\gamma = - e / m_\mathrm{e}$. Putting everything together, the spin-orbit correction Hamiltonian reads:
\begin{equation}
\label{SOHamiltonian}
\hat{H}'_\mathrm{SO} = \left(\frac{e^2}{8 \pi \varepsilon_0} \right)^2 \frac{1}{m_\mathrm{e}^2 c^2 r^3} \hat{\bvec{S}} \cdot \hat{\bvec{L}}.
\end{equation}
Unlike the relativistic correction, the spin-orbit Hamiltonian~(\ref{SOHamiltonian}) is not radially symmetric, and therefore does not commute with either $\bvec{L}$ or $\bvec{S}$ (and specifically, not with either $\hat{L}_z$ or $\hat{S}_z$. However, it does still commute with both $\hat{L}^2$ and $\hat{S}^2$, and it commutes with the sum of the orbital and spin angular momentum, $\bvec{J} = \bvec{L} + \bvec{S}$. Therefore, eigenstates of $\hat{L}_z$ and $\hat{S}_z$ are no longer `good' eigenstates, but eigenstates of $\hat{L}^2$, $\hat{S}^2$, $\hat{J}^2$ and $\hat{J}_z$ are. Moreover, we can relate the first three to $\bvec{L} \cdot \bvec{S}$, making the calculation of the eigenvalues of our perturbed Hamiltonian in these eigenstates easy. We have:
\begin{align}
\hat{J}^2 &= \left(\hat{\bvec{L}} + \hat{\bvec{S}}\right) \cdot \left(\hat{\bvec{L}} + \hat{\bvec{S}}\right) = \hat{L}^2 + \hat{S}^2 + 2 \hat{\bvec{L}} \cdot \hat{\bvec{S}} \nonumber \\
\hat{\bvec{L}} \cdot \hat{\bvec{S}} &= \frac12 \left( \hat{J}^2 - \hat{L}^2 - \hat{S}^2 \right),
\end{align}
and therefore the eigenvalues of $\hat{\bvec{L}} \cdot \hat{\bvec{S}}$ are
\begin{equation}
\label{LSeigenvalues}
\frac12 \hbar^2 \left[ j(j+1) - l(l+1) - s(s+1) \right].
\end{equation}
Calculating the expectation value of the perturbation~(\ref{SOHamiltonian}) in terms of the unperturbed eigenstates takes some work, but is in principle straightforward. From it, we get for the energies (setting $s=\frac12$ for our electron):
\begin{align}
E_\mathrm{SO}^1 &= \ev{\hat{H}'_\mathrm{SO}} = \left(\frac{e^2}{8 \pi \varepsilon_0} \right)^2 \frac{1}{m_\mathrm{e}^2 c^2} \ev{\frac{1}{r^3} \hat{\bvec{S}} \cdot \hat{\bvec{L}}} \nonumber \\
&= \left(\frac{e^2}{8 \pi \varepsilon_0} \right)^2 \frac{1}{m_\mathrm{e}^2 c^2} \ev{\frac{1}{r^3}} \frac12 \hbar^2 \left[ j(j+1) - l(l+1) - s(s+1) \right] \nonumber\\
&= \frac{e^2}{8 \pi \varepsilon_0} \frac{1}{(m_\mathrm{e}c)^2} \frac{\hbar^2}{2} \frac{j(j+1) - l(l+1) - \frac34}{l(l+\frac12)(l+1) n^3 a_0^3} \nonumber \\
&= \frac{\left(E_n^0\right)^2}{m c^2} \frac{j(j+1) - l(l+1) - \frac34}{l(l+\frac12)(l+1)} n.
\end{align}
Remarkably, the relativistic correction and the spin-orbit coupling effect are thus of the same order of magnitude: both scale as $\alpha^2 = (E_n^0)^2/2mc^2$. We therefore need to consider them together, which actually simplifies the final correction energies to
\begin{equation}
\label{hydrogenfinestructurecorrection}
E_{FS}^1 = \frac{\left(E_n^0\right)^2}{2 m c^2} \left( 3 - \frac{4n}{j + \frac12}\right).
\end{equation}
Including fine structure, the energy levels of hydrogen are no longer just functions of the principal quantum number~$n$; they now also contain contributions from the combined angular momentum quantum number $j$:
\begin{equation}
E_{nj} = - \frac{R_\mathrm{E}}{n^2} \left[1 + \frac{\alpha^2}{n^2} \left( \frac{n}{j + \frac12} - \frac34 \right) \right].
\end{equation}
Including fine structure thus breaks some of the symmetry of the excited states of the hydrogen atom: the degeneracy of the $l$ orbitals for $n>1$ is lifted. Some symmetry is preserved though, as states with the same value of $j$ still have the same energy. Moreover, $m_l$ and $m_s$ are no longer `good' quantum numbers; the eigenstates of hydrogen including fine structure are characterized by their values of $n$, $l$, $s$, $j$ and $m_j$. The corrections to energy levels for the first three states are plotted in figure~\ref{fig:hydrogenfinestructureenergylevels}.


%\begin{figure}[ht]
%\centering
%\includegraphics[scale=0.5]{SE/figures/Hydrogen_fine_structure.png}
%\caption{Energy levels of hydrogen, including fine structure, which lifts degeneracy in $l$, but not in $j$. Note that the corrections are not to scale. Image from~[\ref{Hydrogenfinestructureimage}], public domain.}
%\label{fig:hydrogenfinestructureenergylevels}
%\end{figure}

\begin{figure}[ht]
\centering
\includegraphics[scale=0.5]{SE/figures/hydrogenfinestructuresplitting.pdf}
\caption{Fine-structure corrections (equation~(\ref{hydrogenfinestructurecorrection}) to the first three energy levels of the hydrogen atom, as a function of the value of the fine-structure constant~$\alpha$. Note that the actual value of $\alpha$ is very small (approximately $1/137$), so the corrections are also small. The fine-structure correction lifts the degeneracy in the quantum number~$l$, but not in~$j$.}
\label{fig:hydrogenfinestructureenergylevels}
\end{figure}


\newpage
\section{Problems}
\input{SE/problems/perturbationtheoryproblems}
