\chapter{The Schr\"odinger equation}
\label{ch:SE}

\section{Wave functions and Schr\"odinger equation}
\subsection{Wave functions and probability}
In classical mechanics, we tried to find the trajectories $\bvec{x}(t)$ of our particles as a function of time. In a continuous setting, we considered waves, and tried to find the wave function $u(x, t)$ as a function of time and position. In quantum mechanics, every particle is a wave, and thus described by a \emph{wave function}, which we'll denote by $\Psi(x,t)$. We will also refer to $\Psi(x,t)$ as the \emph{state} of our particle. Quantum waves are different from classical ones in several important respects. First, unlike classical waves, the quantum wave function can be a complex number. Second, the quantum wave does not describe a physical property (e.g., in the simplest case of a transverse wave in a guitar string, the distance to the equilibrium position), but a probability. To be precise, if a certain particle's wave function is given by $\Psi(x,t)$, the probability of finding it in the interval $(x, x + \ddx)$ is given by
\begin{equation}
\label{particleprobability}
P(\mbox{particle in }(x, x+\ddx)) = \Psi^*(x, t) \Psi(x,t) \dd x,
\end{equation}
where the star denotes the complex conjugate. Of course, the probability itself is a real number - you can't measure the complex part of the wavefunction, but it is there, and it turns out that it can make our calculations significantly easier to do.

The idea that objects we used to think of as particles are described by a probability distribution which takes the form of a wave can seem absurd - especially since this means that things like interference between particles becomes possible, but that's exactly what we've seen does happen in a Young double-slit experiment with electrons. There's another strange aspect that would never happen in classical physics, which has to do with measurements. When you perform a single measurement on a particle (e.g., you determine its position), you get a single, definite answer, in both quantum and classical physics. The probability distribution only appears when you make multiple measurements on multiple identical particles that all started in the same condition, like in the double-slit experiment. However, if you make multiple subsequent measurements on the same particle, with only short time intervals between these measurements, it seems to behave perfectly normal, following the laws of classical physics. Therefore, something must have tampered with the probability distribution, turning a stochastic (random) process into a deterministic one. That something is you - by performing the measurement, you force the object you're measuring to choose a value from the range of possibilities, and at that point, it gets that value. We say that the wave function \emph{collapses} - whereas before many values were possible, immediately after the measurement, there is only one, the one you measured. If you measure again, right after your first measurement, you will therefore get the same answer. Again, nobody knows how or why a measurement causes the wavefunction to collapse - but we do know that it happens.

The fact that our wavefunction $\Psi(x,t)$ (or rather $\Psi^* \Psi$, technically the square of its norm) represents a probability distribution, has a number of consequences. First, it means that the function $\Psi(x,t)$ is a \emph{scalar} - it may be complex, but at least it is a number, not a vector. It can of course depend on the position in three dimensions, but there too it will remain a scalar itself. Second, since the probability that the particle is somewhere in space is one, the wavefunction must be \emph{normalized}:
\begin{equation}
\label{wavefunctionnormalization}
\int_{-\infty}^\infty \Psi^*(x,t) \Psi(x,t) \dd x = 1.
\end{equation}
Third, even though we can't determine the position of our particle, we can calculate its expectation value\footnote{If you're unfamiliar with probability theory: the expectation value is the average value you'd find when carrying out the same random process many times - so the expectation value of the number you throw with a dice is 3.5.}:
\begin{equation}
\label{wavefunctionpositionexpectation}
\left< x \right> = \int_{-\infty}^\infty \Psi^*(x,t) x \Psi(x,t) \dd x.
\end{equation}
Similarly, we can calculate the expectation value of any function $f(x)$ of $x$, $\left< f(x) \right> = \int_{-\infty}^\infty \Psi^*(x,t) f(x) \Psi(x,t) \dd x$, and of any \emph{operator} (e.g., differentiation to $x$ or $t$) $A$ on $\Psi(x,t)$:
\begin{equation}
\label{operatorexpectation}
\left< A \right> = \int_{-\infty}^\infty \Psi^*(x,t) A \Psi(x,t) \dd x.
\end{equation}
Note that $x$ and $f(x)$ are also operators - they are simply the multiplication of $\Psi(x,t)$ with a number. Expressions like equation~(\ref{operatorexpectation}) occur so often that we use a shorthand notation for them, known as the Dirac bracket notation:
\begin{equation}
\label{Diracbraket}
\left<\Psi | A | \Psi \right> \equiv \int_{-\infty}^\infty \Psi^*(x,t) A \Psi(x,t) \dd x.
\end{equation}
Splitting up the word, and the notation, people refer to $\bra{\Psi}$ as the `bra' and $\ket{\Psi}$ as the `ket'. At present, these simply represent the function $\Psi(x,t)$ (the ket) and its complex conjugate; later we'll use these functions to describe the state of the system under consideration, and refer to $\ket{\Psi}$ as the state function, state vector, or simply state.

Like in any probabilistic event, just the expectation value of your stochastic variable (say the position of your particle) in itself doesn't tell you very much - you also want to know how big the spread around that expectation value is. The standard probabilistic measure for that is the \emph{variance} (or its square root, the standard deviation), usually denoted as $\sigma^2(x)$, and given by the square of the average deviation of the mean:
\begin{equation}
\label{variance}
\sigma^2(x) = \left< (x - \left<x\right>)^2 \right> = \left< x^2 \right> - \left< x \right>^2.
\end{equation}
To see that the two expressions are the same, simply expand the square and use the fact that the expectation value of a number is that number, and that for any number $a$, $\ev{ax} = a \ev{x}$. To calculate the variance, we thus simply need to calculate the expectation value of $x^2$ (known as the second moment of $x$ - the $n$th moment, obviously, is the expectation value of $x^n$), for which we can use equation~(\ref{operatorexpectation}).

Before moving on, it is good to point out what our expectation values mean in an experimental setting. If we have a particle described by the wave function $\Psi(x,t)$, and we measure its position, we get a certain number. If we do this repeatedly, we can of course calculate the average value of that particle's position - but that is not $\ev{x}$ as given by equation~(\ref{wavefunctionpositionexpectation}). The value predicted by equation~(\ref{wavefunctionpositionexpectation}) is the one you will find if you start out with many isolated particles, all described by the same wave function~$\Psi(x,t)$, measure their positions (only once per particle!), and take the average. The reason why the first method (repeated measurements on the same particle) doesn't work is because the first measurement collapsed the particle's wavefunction - and thereby changed it. The new wavefunction is very simple: it gives you a probability of one to find the particle at the spot you found it, and zero everywhere else. If you leave the particle alone, the wavefunction will change again (how it changes is described by the Schr\"odinger equation, which we'll discuss in section~\ref{sec:SE}), but it won't revert to the wavefunction it was before the measurement.

At this point, you may well have stopped believing anything written here - all this is absolutely contrary to our everyday experience. The only reason why we stick with this probabilistic description and particle-wave duality, is that we know, from very precise experiments, that it works - and remarkably well. Nature, it turns out, is probabilistic at the small scale. Measurements cannot be performed in an entirely objective manner, since the measurement changes the object that is measured. Worse, there is absolutely no way to tell where a particle is before you measure its position, thus changing it. If this bothers you, you're in good company - Einstein for one never wanted to accept this vision of reality, and maintained that although quantum mechanics clearly works, there must be some deeper, underlying theory, which for instance would tell you where the particle is at any point in time, given that you know its initial conditions like position and velocity. Together with two other physicists, he came up with a paradox to show that such a deeper theory must exist. The paradox was later resolved, within quantum mechanics, by Bell, leading to the prediction of an even stranger possible event: instantaneous communication between quantum particles over arbitrarily large distances. Such events have since been created in the lab, and form the basis of quantum encryption techniques. They have also convinced most people that quantum mechanics is, despite its absurdity, real.

\subsection{The Schr\"odinger equation}
\label{sec:SE}
In the previous section, we've stated (not proven - as there is no proof, the statement is an axiom) that particles in quantum mechanics are described by a wave function $\Psi(x,t)$. That in itself doesn't tell us much - we'd also like to know how to find this function for a given system. In classical mechanics, we have of course a similar problem: our particles are described by a position function $x(t)$, which itself can be found through Newton's second law, $F = m \ddot{x}$, where $F$ is the sum of all forces acting on the particle. We therefore need the quantum equivalent of Newton's second law. Naturally, there are still forces acting on our particle. Fortunately, in the microscopic quantum world, all these forces are conservative, i.e., we can write the associated force as the derivative of a potential energy $V$: $F = - \partial V / \partial x$. Macroscopic non-conservative forces like friction and drag simply come about through the addition of many microscopic, conservative forces, but there is no such thing as drag on a single electron. Since potentials, as scalars, are much easier to work with than forces, we will work with potentials exclusively. The equation that tells us how a wave function changes due to the action of a potential cannot be derived - like Newton's second law in classical mechanics, it is an axiom of quantum mechanics, known as the Schr\"odinger equation:
\begin{equation}
\label{SE}
i \hbar \frac{\partial \Psi}{\partial t} = - \frac{\hbar^2}{2m} \frac{\partial^2 \Psi}{\partial x^2} + V \Psi.
\end{equation}
The Schr\"odinger equation tells us how $\Psi(x,t)$ evolves over time - the change in time (left hand side) is related to a spatial derivative term and a term with a potential energy on the right hand side. Mathematically, it somewhat resembles the wave equation - with time derivatives on one side, and space derivatives on the other - but not quite: in the wave equation we had a second order time derivative, whereas in the Schr\"odinger equation the time derivative is only first order. Therefore, the Schr\"odinger equation is closer to the diffusion equation, which also has a first order time derivative and second order space derivative. The main difference between the two is the complex number $i$ that multiplies the time derivative in the Schr\"odinger equation. Nonetheless, the effect is similar to diffusion: over time, the evolution described by the Schr\"odinger equation tends to spread out the wave function $\Psi(x,t)$. Therefore, if you initialize a system by putting a particle at some position $x_0$, over time, its probability distribution will spread out around $x_0$ - increasing the probability that if you measure the position again at a later time, you'll find that the particle has moved.

The right hand side of the Schr\"odinger equation has an interpretation in terms of the energy of the system. To see how that comes about, we first need to look at another quantity: the quantum-mechanical momentum.

\subsection{Momentum}
We found the expectation value $\ev{x}$ of the position of a particle, in a state $\Psi(x,t)$ (i.e., described by the wavefunction $\Psi(x,t)$) in equation~(\ref{wavefunctionpositionexpectation}). This expectation value is the closest thing to a quantum version of the classical notion of a particle's trajectory $x(t)$. Since $\Psi(x,t)$ depends on time, so will $\ev{x}$, so we can also introduce a quantum analog of the particle's velocity: simply $\dd\ev{x} / \ddt$. Now rather than with velocity, we prefer to work with momentum - which of course is simply the particle's mass times its velocity. We can thus define the quantum dynamical momentum operator $\hat{p}$ as the operator whose expectation value is given by $\ev{\hat{p}} = m \dd\ev{x} / \ddt$.

Of course we don't only want to know the expectation value of the momentum operator - we also want an expression for the operator itself, so we can do calculations with it. To find that expression, we'll use the definition of the expectation value and the Schr\"odinger equation both, which gives:
\begin{eqnarray}
\label{momentumoperatorderivation}
\ev{\hat{p}} &=& m \frac{\dd\ev{x}}{\ddt} = m \frac{\dd}{\ddt} \int_{-\infty}^\infty \Psi^* x \Psi \dd x \nonumber\\
&=& m \int_{-\infty}^\infty \left( \diff{\Psi^*}{t} x \Psi + \Psi^* x \diff{\Psi}{t} \right) \dd x \nonumber\\
&=& \frac{i \hbar}{2} \int_{-\infty}^\infty \left( -\frac{\partial^2 \Psi^*}{\partial x^2} x \Psi + \Psi^* x \frac{\partial^2 \Psi}{\partial x^2} \right) \dd x + \frac{im}{\hbar} \int_{-\infty}^\infty \Psi^* x \Psi (V-V) \dd x \nonumber\\
&=& \frac{i \hbar}{2} \int_{-\infty}^\infty x \frac{\partial}{\partial x} \left( \Psi^* \diff{\Psi}{x} - \diff{\Psi^*}{x} \Psi \right) \ddx \nonumber\\
&=& - \frac{i\hbar}{2} \int_{-\infty}^\infty \left( \Psi^* \diff{\Psi}{x} - \diff{\Psi^*}{x} \Psi \right) \ddx \nonumber\\
&=& -i \hbar \int_{-\infty}^\infty  \Psi^* \diff{\Psi}{x} \ddx.
\end{eqnarray}
We used the Schr\"odinger equation (and its complex conjugate) in line 3, and integrated by parts in line 5 and 6. The boundary term that you usually get with integration by parts vanishes because $\Psi(x,t)$ goes to zero at infinity - otherwise it could not be normalized. We end up with an operator that `represents' momentum in quantum mechanics:
\begin{equation}
\label{momentumoperator}
\hat{p} = - i \hbar \diff{}{x}.
\end{equation}
Equation~(\ref{momentumoperator}) gives us the recipe for calculating the momentum in quantum mechanics: if we have the wave function~$\Psi(x,t)$, we take its space derivative, and multiply with $-i \hbar$. When we \emph{measure} momentum, what we find is not that number though (that may even be complex), but the expectation value of the momentum operator - given by equation~(\ref{momentumoperatorderivation}).

\subsection{The Hamiltonian}
Now that we know how to calculate the momentum quantum-mechanically, we can also calculate the kinetic energy. Like in classical mechanics, the kinetic energy is given by $K = \frac12 m v^2 = p^2/(2m)$. Because in quantum mechanics the momentum has become an operator, the kinetic energy becomes an operator as well:
\begin{equation}
\label{kinenoperator}
\hat{K} = \frac{1}{2m} (\hat{p})^2 = \frac{1}{2m} (-i \hbar)^2 \left( \diff{}{x} \right)^2 = -\frac{\hbar^2}{2m} \frac{\partial^2}{\partial x^2}.
\end{equation}
Note that the kinetic energy operator is exactly the first term on the right hand side of the Schr\"odinger equation. Perhaps this is no surprise - the second term is the potential energy, so the first must be an energy as well, and which other energy could it be? Together, the kinetic and potential energy make up the `total energy' operator, known as the \emph{Hamiltonian}
\begin{equation}
\label{defhamiltonian}
\hat{H} = \hat{K} + \hat{V} = -\frac{\hbar^2}{2m} \frac{\partial^2}{\partial x^2} + V.
\end{equation}
Using the Hamiltonian, we can write the Schr\"odinger equation in concise form as
\begin{equation}
\label{SEconcise}
i \hbar \frac{\partial \Psi}{\partial t} = \hat{H} \Psi(x,t).
\end{equation}

\section{One-dimensional, time-independent Schr\"odinger equation}
\label{sec:1DTISE}

In this section, we'll consider the simplest case of the Schr\"odinger equation: one dimension, and a time-independent Hamiltonian. Of course, the kinetic energy part of the Hamiltonian is always time-independent, but the potential energy need not be. Fortunately, for many cases it is - and we'll only consider that case here. However, we will go to three dimensions later on, as we need those to study actual particles and angular momentum.

\subsection{Separable solutions}
If the potential is independent of time, we have a differential equation with only time operators on one side, and only space operators on the other. In those cases, a useful strategy is to look for separable solutions, which in our case means that we try a function $\Psi(x,t)$ that can be written as the product of a function of $x$ and another function of $t$:
\begin{equation}
\label{Psiseparation}
\Psi(x,t) = \psi(x) \phi(t).
\end{equation}
Note the difference between capital $\Psi(x,t)$ and lowercase $\psi(x)$. Of course, there is no a-priory reason why equation~(\ref{Psiseparation}) should hold - it is very possible that the actual solution of the Schr\"odinger equation is not separable. However, as we'll see, we will be able to construct all solutions from linear combinations of separable solutions, which is possible because the Schr\"odinger equation is linear. This construction is exactly the same as the linear combination of waves in superposition - that too works because the wave equation is linear, and superposition is applicable to any linear differential equation.

Substituting expression~(\ref{Psiseparation}) into the Schr\"odinger equation~(\ref{SE}), we find:
\begin{equation}
\label{SEseparation}
i \hbar \psi(x) \frac{\dd \phi(t)}{\ddt} = - \frac{\hbar^2}{2m} \frac{\dd^2 \psi(x)}{\dd x^2} \phi(t) + V(x) \psi(x) \phi(t).
\end{equation}
Note that the derivatives have become ordinary derivatives - $\psi$ and $\phi$ both depend on one variable only. In equation~(\ref{SEseparation}), both sides still depend on both $x$ and $t$, but that can be easily remedied by dividing by $\psi(x) \phi(t)$:
\begin{equation}
\label{SEseparated}
\frac{i \hbar}{\phi(t)} \frac{\dd \phi(t)}{\ddt} = - \frac{\hbar^2}{2m} \frac{1}{\psi(x)} \frac{\dd^2 \psi(x)}{\dd x^2} \phi(t) + V(x).
\end{equation}
Equation~(\ref{SEseparated}) is separated - the left hand side only depends on time, the right hand side only on space. Now suppose we know the solution at some point in time and space, say $(t_0, x_0)$. At a later point in time, but the same point in space, the left hand side might have changed, but the right hand side could not - so the left hand side must also have stayed the same! Similarly, at a different point in space but the same point in time, the left hand side is constant, and therefore the right hand side must be constant as well. Consequently, there is only one possibility: both sides are equal to a constant. For reasons that will become obvious a bit later, we'll call this constant $E$. With that, the Schr\"odinger equation, a two-variable partial differential equation, reduces to two ordinary differential equations:
\begin{equation}
\label{SEtime}
\frac{i \hbar}{\phi(t)} \frac{\dd \phi(t)}{\ddt} = E,
\end{equation}
and
\begin{equation}
\label{SEspace}
- \frac{\hbar^2}{2m} \frac{1}{\psi(x)} \frac{\dd^2 \psi(x)}{\dd x^2} + V(x) = E.
\end{equation}
The time equation~(\ref{SEtime}) can be easily solved, essentially again by `separation of variables' (now $\phi$ and $t$), and integrating, which gives:
\begin{equation}
\label{Wavefunctiontimedependence}
\phi(t) = e^{-i E t / \hbar}.
\end{equation}
In general we would include an integration constant (which would then multiply the exponential), but we can absorb that in $\psi(x)$ (as we're ultimately interested in the product $\Psi(x,t)$). The time evolution of a separable solution of the Schr\"odinger equation is thus quite simple. The space part is more complicated (and thus more interesting). Multiplying equation~(\ref{SEspace}) by $\psi(x)$, the space part is known as the time-independent Schr\"odinger equation (often simply referred to as the Schr\"odinger equation), in which we again find the Hamiltonian:
\begin{equation}
\label{tiSE}
\hat{H} \psi(x) = - \frac{\hbar^2}{2m} \frac{\dd^2 \psi(x)}{\dd x^2} + V(x) \psi(x) = E \psi(x).
\end{equation}

