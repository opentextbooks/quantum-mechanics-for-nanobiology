---
jupytext:
    formats: md:myst
    text_representation:
        extension: .md
        format_name: myst
kernelspec:
    display_name: Python 3 (ipykernel)
    language: python
    name: python3
---
(ch:perturbationtheory)=
# Perturbation theory

The hydrogen atom Hamiltonian with the Coulomb potential term between the nucleus and the electron is the most complicated example for which we know the exact solutions. It is however not complete, as it ignores some physical terms, including the interaction between the spin and the orbital angular momentum and a relativistic correction necessary because the electrons can move quite fast. Fortunately, the additional terms that account for these physical effects are relatively small, and can be treated as perturbations to the zeroth-order Coulomb interaction result. They do however have an important consequence: they cause degenerate energy levels to split up, thus (as it is usually stated) 'lifting the degeneracy'. Indeed, we find multiple wavelengths for, say, the transition from the $n=2$ to the $n=1$ state of hydrogen. To see how this works, we will start by the simplest case: a perturbation to a non-degenerate state, then build up to degenerate cases.

(sec:nondegenerateperturbationtheory)=
## Non-degenerate perturbation theory

Suppose that we have a Hamiltonian that can be written as the sum of two terms, with one much larger than the other, i.e.

$$
\hat{H} = \hat{H}^0 + \varepsilon \hat{H}',
$$ (perturbedHamiltonian)

```{index} perturbation theory
```
where $\varepsilon \ll 1$. We'll say that $\hat{H}'$ is a *perturbation* of the unperturbed Hamiltonian $\hat{H}^0$. Suppose moreover that we have a complete set of orthogonal eigenfunctions $\psi_n^0(x)$ of $\hat{H}^0$, with eigenvalues $E_n^0$:

$$
\hat{H}^0 \psi_n^0 = E_n^0 \psi_n^0, \qquad \text{with} \quad \Braket{\psi_m^0 | \psi_n^0} = \delta_{mn}.
$$ (unperturbedSE)

The Schr&ouml;dinger equation for the full system reads

$$
\hat{H} \psi_n = E_n \psi_n,
$$ (fullSE)

which we'd like to solve for the eigenvalues $E_n$ and eigenfunctions $\psi_n$. If doing so exactly is not possible (as will usually be the case), we can approximate the solutions by series expansions in the parameter&nbsp;$\varepsilon$. We then write

```{math}
\begin{align*}
E_n &= E_n^0 + \varepsilon E_n^1 + \varepsilon^2 E_n^2 + \ldots, \\
\psi_n &= \psi_n^0 + \varepsilon \psi_n^1 + \varepsilon^2 \psi_n^2 + \ldots.
\end{align*}
```

We call $E_n^1$ and $\psi_n^1$ the first-order corrections to the $n$th energy and wavefunction, respectively. It turns out that finding an expression for $E_n^1$ is pretty easy. We simply substitute our expansion back in the Schr&ouml;dinger equation&nbsp;{eq}`fullSE` and collect terms that are of the same power in $\varepsilon$:

```{math}
\begin{align*}
\hat{H} \psi_n = \left( \hat{H}^0 + \varepsilon \hat{H}' \right) \left(\psi_n^0 + \varepsilon \psi_n^1 + \varepsilon^2 \psi_n^2 + \ldots \right) &= E_n \psi_n = \left( E_n^0 + \varepsilon E_n^1 + \varepsilon^2 E_n^2 + \ldots \right) \left(\psi_n^0 + \varepsilon \psi_n^1 + \varepsilon^2 \psi_n^2 + \ldots \right) \\
\hat{H}^0 \psi_n^0 + \varepsilon \left( \hat{H}^0 \psi_n^1 + \hat{H}' \psi_n^0 \right) + \varepsilon^2 \left( \hat{H}^0 \psi_n^2 + \hat{H}' \psi_n^1 \right) + \ldots &= E_n^0 \psi_n^0 + \varepsilon \left( E_n^0 \psi_n^1 + E_n^1 \psi_n^0 \right) \\
& \qquad + \varepsilon^2 \left( E_n^0 \psi_n^2 + E_n^1 \psi_n^1 + E_n^2 \psi_n^0 \right) + \ldots
\end{align*}
```

To zeroth order, we retrieve the unperturbed equation&nbsp;{eq}`unperturbedSE`. To first order, we get

$$
\hat{H}^0 \psi_n^1 + \hat{H}' \psi_n^0 = E_n^0 \psi_n^1 + E_n^1 \psi_n^0.
$$ (SEfirstorderperturbation)

We now take the inner product of&nbsp;{eq}`SEfirstorderperturbation` with $\psi_n^0$, which gives:

$$
\Braket{\psi_n^0 | \hat{H}^0 \psi_n^1} + \Braket{\psi_n^0 | \hat{H}' \psi_n^0} = E_n^0 \Braket{\psi_n^0 | \psi_n^1} + E_n^1 \Braket{\psi_n^0 | \psi_n^0}.
$$ (SEfirstorderperturbationinnerproduct)

Because $\hat{H}^0$ is Hermitian, the first term on the left-hand side of equation&nbsp;{eq}`SEfirstorderperturbationinnerproduct` can be rewritten as

$$
\Braket{\psi_n^0 | \hat{H}^0 \psi_n^1}  = \Braket{\hat{H}^0 \psi_n^0 |  \psi_n^1} = E_n^0 \braket{\psi_n^0 |  \psi_n^1}
$$

and it thus equals the first term on the right-hand side. Moreover, as $\psi_n^0$ is normalized, the inner product in the second term on the right-hand side of&nbsp;{eq}`SEfirstorderperturbationinnerproduct` equals&nbsp;$1$. We thus find the simple result that the first-order correction to the energy, $E_n^1$, is given by the expectation value of the perturbation&nbsp;$\hat{H}'$ in the unperturbed eigenstate&nbsp;$\psi_n^0$:

$$
E_n^1 = \Braket{\psi_n^0 | \hat{H}' | \psi_n^0}.
$$ (firstorderperturbationenergy)

Equation&nbsp;{eq}`firstorderperturbationenergy` is the central result of this section, and will be used when calculating corrections to the hydrogen energy levels in {numref}`sec:hydrogenfinestructure`. We can also calculate the corrections to the wavefunction itself. These are not nearly as useful, unless you want to get the second-order corrections to the energies. The results (see {numref}`pb:nondegenerateperturbationsecondorder`) are
```{math}
:label: firstorderperturbationwavefunction
\begin{align*}
\psi_n^1 &= \sum_{m \neq n} \frac{\Braket{\psi_m^0|\hat{H}'|\psi_n^0}}{E_n^0 - E_m^0} \psi_m^0, \
\end{align*}
```

```{math}
:label: secondorderperturbationenergy
\begin{align*}
E_n^2 &= \sum_{m \neq n} \frac{\left|\Braket{\psi_m^0|\hat{H}'|\psi_n^0}\right|^2}{E_n^0 - E_m^0}.
\end{align*}
```
One can iterate further and find higher-order corrections, though their usefullness (as values that you could actually measure) quickly declines.

(sec:vanderWaalsforce)=
### Application: Van der Waals interactions

Suppose we have two hydrogen atoms, located a distance&nbsp;$R$ from each other, with $R$ significantly larger than the Bohr radius&nbsp;$a_0$. Although both atoms are electrically neutral, the positively charged nucleus of one atom still exerts a Coulomb force on the negatively charged electron of the other, and vice versa. Likewise, the electrons and nuclei exert forces on each other. These forces are much smaller than the force between each nucleus and its corresponding electron. We can therefore split the Hamiltonian of the joint system into a term $\hat{H}^0$ containing the two atoms as isolated parts, and a perturbation term&nbsp;$\hat{H}'$ for their interaction:
```{math}
:label: vanderWaalsHamiltonian
\begin{align*}
\hat{H} &= \hat{H}^0 + \hat{H}' = \hat{H}_\mathrm{H}^{(1)} + \hat{H}_\mathrm{H}^{(2)} + \hat{H}' \\
&= \left[ \frac{\hbar^2}{2 \me} \nabla_1^2 + \frac{\hbar^2}{2 \me} \nabla_2^2 - \frac{e^2}{r_1} - \frac{e^2}{r_2} \right] + e^2 \left[ \frac{1}{R} + \frac{1}{r_{12}} - \frac{1}{r_{1\mathrm{B}}} - \frac{1}{r_{2\mathrm{A}}} \right],
\end{align*}
```
where A and B label the two nuclei and 1 and 2 the two electrons, see {numref}`fig:vanderWaalsforces`(a). The solution to the unperturbed Hamiltonian&nbsp;$\hat{H}^0$ is simply the product of the eigenstates of the two hydrogen Hamiltonians. As we're only interested in the energy here, we'll consider wavefunctions $\psi_n(\bm{r})$ with energies $E_n = E_1 / n^2$ (thus ignoring the $l$ and $m$ quantum numbers) and write, with a little abuse of notation
```{math}
:label: twohydrogencombistate
\begin{align*}
\psi_{nm}(\bm{r}_1, \bm{r}_2) &= \psi_n(\bm{r}_1) \psi_m(\bm{r}_2), \\
\hat{H}^0 \psi_{nm}(\bm{r}_1, \bm{r}_2) &= \left(E_n + E_m\right) \psi_{nm}(\bm{r}_1, \bm{r}_2).
\end{align*}
```
As we're in the limit that $R \gg a_0$, the two wavefunctions hardly overlap, and we don't have to worry about exchange terms. Moreover, in this large-separation limit, the perturbation term to the Hamiltonian becomes a dipole-dipole interaction<sup>[^1]</sup>

$$
\hat{H}' = \frac{e^2}{R^3} \left(x_1 x_2 + y_1 y_2 - 2 z_1 z_2 \right).
$$ (vanderWaalsdipole)

If both atoms are in the ground state, the expectation value of this perturbation term is zero, and therefore the first order correction to the energies vanishes. However, the square of the expectation value is not zero, and we thus get a correction to the ground state energy by summing over the excited states:
```{math}
:label: vanderWaalsEnergycorrection
\begin{align*}
E_1^2 &= \sum_{nm \neq 11} \frac{\left|\braket{\psi_{nm}(\bm{r}_1, \bm{r}_2)|\hat{H}'|\psi_{11}(\bm{r}_1, \bm{r}_2)}\right|^2}{E_{11} - E_{nm}} \\
&= \frac{e^4}{R^6} \sum_{nm \neq 11} \frac{1}{E_{11}-E_{nm}} \left[ \braket{\psi_n(\bm{r}_1)|x_1|\psi_1(\bm{r}_1)} \braket{\psi_m(\bm{r}_2)|x_2|\psi_1(\bm{r}_2)} + \ldots \right]^2 \\
&\sim - \frac{\alpha_1 \alpha_2}{R^6},
\end{align*}
```
where

$$
\alpha_i = e^2 \sum_{n>1} \frac{\left|\braket{\psi_n(\bm{r})|x|\psi_1(\bm{r}}\right|^2}{E_n-E_1}
$$

```{index} atomic polarizability, van der Waals force
```
is the *atomic polarizability* of the atom. The atomic polarizability is a measure for the strength of the *induced dipole* in an otherwise symmetric atom<sup>[^2]</sup>. We thus understand the resulting attractive force as the result of a mutually induced dipole in the two hydrogen atoms. It is known as the *van der Waals force*. Although our calculation here is for two hydrogen atoms, attractive van der Waals forces are present between any two neutral atoms or molecules.

Van der Waals forces are quite weak, as their energy decays as $1/R^6$. Nonetheless, they are key factors in many chemical, biological and (condensed matter) physical systems. One particular striking example often attributed to van der Waals forces is the ability of geckos to climb walls and run upside-down on a ceiling. Specifically, it's the van der Waals forces between the $\beta$-keratin proteins in the lamellar, spatula shaped setae on the gecko's feet (see {numref}`fig:vanderWaalsforces`(b)) that are credited with the van der Waals interactions&nbsp;{cite}`Autumn2002`. However, other studies suggests that gecko adhesion could be due to induced electrification of surfaces (and thus electrostatic interactions)&nbsp;{cite}`Izadi2014`, or to the surface chemistry of the gecko feet, specifically the presence of lipids&nbsp;{cite}`Rasmussen2022`. Although the definitive explanation for the gecko's climbing ability is thus still open, the possibility that van der Waals forces are at play has inspired various applications&nbsp;{cite}`Northen2005`, including [biologically inspired glue](https://www.reuters.com/article/us-nano-glue-idUSN0942431020081009/) and [adhesive tape](https://newatlas.com/bioinspired-adhesive-tape-kiel/20406/).

```{figure} images/SE/vanderWaalsforces.svg
:name: fig:vanderWaalsforces
Van der Waals forces. (a) Coordinates for the long-distance interaction between two hydrogen atoms. (b) Close-up of the foot of a gecko as it climbs a glass wall&nbsp;<sup>[^3]</sup>. The lamellar structure of the skin is clearly visible.
```

(sec:degenerateperturbationtheory)=
## Degenerate perturbation theory

In {numref}`sec:nondegenerateperturbationtheory` we assumed that all the eigenstates of the unperturbed Hamiltonian were non-degenerate. If the states are degenerate, we cannot use equation&nbsp;{eq}`firstorderperturbationwavefunction`, as the denominator will diverge if $E_m^0 = E_n^0$ for any pair $m \neq n$. Fortunately, the effect of the perturbation will typically be to lift the degeneracy. Based on the perturbed energies, we will even be able to construct 'good' eigenstates of the unperturbed Hamiltonian (limit cases of the perturbed state when taking the perturbation parameter $\varepsilon$ to zero) for which we can use equation&nbsp;{eq}`firstorderperturbationenergy`, as we'll see below.

(sec:perturbationmatrix)=
### Eigenstates of the perturbation matrix

To illustrate how to deal with degenerate eigenstates of an unperturbed Hamiltonian, we start with a system that has two-fold degeneracy (the procedure for higher degeneracies will be analogous): suppose $\hat{H}^0$ has two orthogonal eigenstates $\psi_a^0$ and $\psi_b^0$ that satisfy

$$
\hat{H}^0 \psi_a^0 = E^0 \psi_a^0, \quad \hat{H}^0 \psi_b^0 = E^0 \psi_b^0, \quad \Braket{\psi_a^0 | \psi_b^0} = 0.
$$ (unperturbedHamiltoniandegenerateeigenstates)

Note that any linear combination

$$
\psi^0 = \alpha \psi_a^0 + \beta \psi_b^0
$$ (degenerateeigenfunctionslinearcombination)

is also an eigenstate of $\hat{H}^0$ with energy $E_0$. Our result will not only be the corrections to the energy, but also which linear combinations correspond to the 'good' eigenstates, i.e. which values of $\alpha$ and $\beta$ we should choose for the perturbed system to consist of proper eigenstates.

We calculate the corrections to the energy in much the same way as in non-degenerate perturbation theory. We again write $\hat{H} = \hat{H}^0 + \varepsilon \hat{H}'$ and expand both the energy and the wavefunction of $\hat{H}$ in $\varepsilon$:

```{math}
\begin{align*}
E &= E^0 + \varepsilon E^1 + \varepsilon^2 E^2 + \ldots, \\
\psi &= \psi^0 + \varepsilon \psi^1 + \varepsilon^2 \psi^2 + \ldots.
\end{align*}
```

The zeroth-order term of the Schr&ouml;dinger equation with the perturbed Hamiltonian $\hat{H}$ again gives us the unperturbed system, and the first-order term the first variation:

$$
\hat{H}^0 \psi^1 + \hat{H}' \psi^0 = E^0 \psi^1 + E^1 \psi^0.
$$ (SEfirstorderperturbationdegenerate)

If we now take the inner product of equation&nbsp;{eq}`SEfirstorderperturbationdegenerate` with the original eigenstate&nbsp;$\psi_a^0$, the first terms on the left and right-hand side are again equal, but the remaining terms are different than in the non-degenerate case. We get
```{math}
\begin{align*}
\Braket{\psi_a^0 | \hat{H}' \psi^0} &= E^1 \Braket{\psi_a^0 | \psi^0} \\
\alpha \Braket{\psi_a^0 | \hat{H}' \psi_a^0} + \beta \Braket{\psi_a^0 | \hat{H}' \psi_b^0} &= \alpha E^1.
\end{align*}
```
Defining $Q_{ij} = \Braket{\psi_i^0 | \hat{H}' \psi_j^0}$ as the 'matrix elements' of the perturbation&nbsp;$\hat{H}'$ in the basis of the unperturbed eigenstates $\left\lbrace\psi_a^0, \psi_b^0\right\rbrace$, we can thus write

```{math}
\begin{align*}
\alpha E^1 &= \alpha Q_{aa} + \beta Q_{ab}, \\
\beta E^1 &= \alpha Q_{ba} + \beta Q_{bb},
\end{align*}
```

or, in matrix form

$$
Q \begin{pmatrix} \alpha \\ \beta \end{pmatrix} = E^1 \begin{pmatrix} \alpha \\ \beta \end{pmatrix}.
$$ (degenerateperturbationeigenvalueequation)

In other words, the first-order corrections $E^1$ to the energy are the eigenvalues of the 'perturbation matrix'&nbsp;$Q$ (the matrix whose elements are obtained from $\hat{H}'$ in the unperturbed state). For a $2 \times 2$ matrix&nbsp;$Q$, these eigenvalues are given by

$$
E^1_\pm = \frac12 \mathrm{Tr}(Q) \pm \frac12 \sqrt{\left(\mathrm{Tr}(Q)\right)^2-4\det(Q)} = \frac12 \left(Q_{aa} + Q_{bb}\right) \pm \frac12 \sqrt{\left(Q_{aa}-Q_{bb}\right)^2 +4 Q_{ab}Q_{ba}}.
$$ (degeneratefirstorderperturbationenergy)

Equation&nbsp;{eq}`degeneratefirstorderperturbationenergy` gives us the first order corrections&nbsp;$E^1_\pm$ to the energy. From equation&nbsp;{eq}`degenerateperturbationeigenvalueequation` we can then solve for the coefficients&nbsp;$\alpha$ and $\beta$, which give us the corresponding linear combinations of the 'good' unperturbed states, i.e., the states which are the limit cases of the perturbed ones if we take the magnitude of the perturbation back to zero. Note that for the 'good' states, the matrix&nbsp;$Q$ becomes diagonal, and we retrieve equation&nbsp;{eq}`firstorderperturbationenergy` for the energies of the perturbations. Extending to higher-order degeneracies is straightforward: for an $n$-fold degenerate energy, we get an $n \times n$ Hermitian matrix $Q$, with $n$ real eigenvalues and corresponding eigenstates.

In practice, $\hat{H}^0$ will usually have a higher degree of symmetry than $\hat{H}'$. The 'lifting of the degeneracy' of the eigenstates of $\hat{H}^0$ then corresponds to a 'breaking of the symmetry'. A good example is what happens if you put a hydrogen atom in a magnetic or electric field. While at zero field all directions are equal (full rotational symmetry), in the presence of a constant field, the direction in which the field increases is different from the direction(s) in which it is constant. In three dimensions, for a homogeneous field in the $z$ direction, there is still rotational symmetry in the $xy$ plane, but we have broken the full spherical symmetry. A further reduction of symmetry could lead to a further lifting of a remaining degeneracy. This correspondence between symmetry and degeneracy is a key concept in quantum mechanics; it also suggests a way of picking 'good' eigenstates from the start.

(sec:degenerateperturbationjointeigenstates)=
### Joint eigenstates

While we can find the 'good' eigenstates for any perturbation with equation&nbsp;{eq}`degenerateperturbationeigenvalueequation`, there is an easier way if we have a second physical quantity we can measure. For example, for any but the ground state of hydrogen we have degenerate eigenstates of the Hamiltonian, but we can distinguish between states with different values of the orbital quantum number&nbsp;$l$ by measuring the magnitude of the state's total angular momentum. The corresponding states with different eigenvalues will then correspond to 'good' eigenstates, i.e. states for which the perturbation matrix&nbsp;$Q$ is diagonal.

We can prove the above statement in a more general setting. We'll work out the details for a doubly degenerate state again, as higher degeneracies follow easily. Suppose therefore that we have a perturbed Hamiltionian of the form&nbsp;{eq}`perturbedHamiltonian`, where the unperturbed part has two degenerate eigenstates as in&nbsp;{eq}`unperturbedHamiltoniandegenerateeigenstates`. Suppose moreover that we have another Hermitian operator&nbsp;$\hat{A}$ which commutes with both $\hat{H}^0$ and $\hat{H}'$. Because $\hat{A}$ commutes with $\hat{H}^0$, they have a shared set of eigenfunctions. These eigenfunctions need not be the $\psi_a^0$ and $\psi_b^0$ of equation&nbsp;{eq}`unperturbedHamiltoniandegenerateeigenstates`, but they can be written as linear combinations of them, as in equation&nbsp;{eq}`degenerateeigenfunctionslinearcombination`. Moreover, suppose that the joint eigenfunctions are not degenerate as eigenfunctions of $\hat{A}$, i.e., we have

```{math}
\begin{alignat*}{3}
\hat{H}^0 \psi_\lambda^0 &= E_0 \psi_\lambda^0 &\qquad & \hat{H}^0 \psi_\mu^0 &= E_0 \psi_\mu^0 \\
\hat{A} \psi_\lambda^0 &= \lambda \psi_\lambda^0 &\qquad & \hat{A} \psi_\mu^0 &= \mu \psi_\mu^0
\end{alignat*}
```

It is now easy to show that the perturbation matrix&nbsp;$Q$ is diagonal in the basis $\left\lbrace \psi_\lambda^0, \, \psi_\mu^0\right\rbrace$. We simply calculate one of the elements of this matrix, times an eigenvalue of $\hat{A}$:
```{math}
:label: jointeigenvalueperturbationmatrixelement
\begin{align*}
\lambda Q_{\lambda \mu} &= \lambda \Braket{\psi_\lambda^0 | \hat{H}' \psi_\mu^0} = \Braket{\lambda \psi_\lambda^0 | \hat{H}' \psi_\mu^0} = \Braket{\hat{A} \psi_\lambda^0 | \hat{H}' \psi_\mu^0} = \Braket{\psi_\lambda^0 | \hat{A} \hat{H}' \psi_\mu^0}  \\
&= \Braket{\psi_\lambda^0 | \hat{H}' \hat{A} \psi_\mu^0} = \mu \Braket{\psi_\lambda^0 | \hat{H}' \psi_\mu^0} = \mu Q_{\lambda \mu},
\end{align*}
```
where we used that $\lambda$ (as the eigenvalue of a Hermitian operator) is real in the second equality, that $\hat{A}$ is Hermitian in the fourth, and that $\hat{A}$ commutes with $\hat{H}'$ in the fifth. From equation&nbsp;{eq}`jointeigenvalueperturbationmatrixelement` we find that either $\lambda = \mu$, or $Q_{\lambda \mu} = 0$, and thus the perturbation matrix is diagonal.

(sec:hydrogenfinestructure)=
## The fine structure of hydrogen

```{index} fine structure of hydrogen, relativistic correction, spin-orbit coupling
```
When writing down the Hamiltonian of an electron in a hydrogen atom, we accounted for two effects: the kinetic energy of the electron, and the Coulomb interaction potential. To lowest order, these two terms are indeed dominant, but there are other physical effects that come into play as corrections. The two largest ones, together known as the *fine structure*, are a *relativistic correction* to the kinetic energy, and an extra potential term due to the coupling between the spin and orbital angular momentum of the electron (known as *spin-orbit coupling*). To assess the magnitude of these effects, we should compare them to the Bohr energy levels, which are proportional to<sup>[^4]</sup> $\alpha^2 m_\mathrm{e} c^2$, where

$$
\alpha = \frac{e^2}{4 \pi \varepsilon_0 \hbar c} \approx 	\frac{1}{137}
$$ (finestructureconstant)

```{index} fine structure constant
```
is known as the *fine structure constant*. Both the relativistic and spin-orbit correction have energies of the order $\alpha^4 m_\mathrm{e} c^2$, and are thus an order $\alpha^2$ smaller than the Bohr energies, so perturbation theory will certainly apply. 

### Relativistic correction to Bohr energies
Thus far, we've been using quantum-mechanical analogs of classical mechanics expressions in our Hamiltonians. In particular, we've used that the kinetic energy operator is given by $\hat{K} = \hat{p}^2 / 2m_\mathrm{e}$, both in one and in three dimensions. However, when the speed of a particle approaches that of light, this expression for the kinetic energy is no longer correct (it is merely the limit at low velocities). Even if you've never seen special relativity, you will no doubt have seen the most famous equation in physics, Einstein's $E = m_\mathrm{e}c^2$. That equation is also a special case though: it holds only for stationary particles. For moving particles, we instead have a more general version<sup>[^5]</sup>:

$$
E^2 = m_\mathrm{e}^2 c^4 + p^2 c^2.
$$ (relativsiticenergymomentumequation)

The energy in equation&nbsp;{eq}`relativsiticenergymomentumequation` is the sum of the kinetic and the rest energy (the $mc^2$ part) of the particle. For the kinetic energy we can then write, in terms of the momentum:

```{math}
:label: relativsitickineticenergy
\begin{align*}
K &= E - m_\mathrm{e} c^2 = \sqrt{m_\mathrm{e}^2 c^4 + p^2 c^2} - m_\mathrm{e}c^2 = m_\mathrm{e}c^2 \left[ \sqrt{1+\left(\frac{p}{m_\mathrm{e}c}\right)^2} - 1 \right] \\
&= m_\mathrm{e} c^2 \left[1 + \frac12 \left(\frac{p}{m_\mathrm{e}c}\right)^2 - \frac18 \left(\frac{p}{m_\mathrm{e}c}\right)^4 + \cdots - 1\right] = \frac{p^2}{2m_\mathrm{e}} - \frac{p^4}{8 m_\mathrm{e}^3 c^2} + \cdots,
\end{align*}
```

where we can make the Taylor expansion in the second line if $p \ll m_\mathrm{e}c$, i.e., if the speed of the particle is small compared to that of light. As we see in equation&nbsp;{eq}`relativsitickineticenergy`a, the lowest-order term is indeed the classical expression for the kinetic energy, but there are also higher-order corrections. As you can check easily, the rest energy<sup>[^6]</sup> of an electron (about $0.5\;\mathrm{MeV}$) far exceeds the typical energies of the orbitals (which are of the order of $10\;\mathrm{eV}$), so the expansion holds, and we can suffice with the lowest-order correction. The Hamiltonian then reads:

$$
\hat{H} = \hat{H}_0 + \hat{H}_\mathrm{r}' = \frac{\hat{p}^2}{2m_\mathrm{e}} - \frac{\hat{p}^4}{8 m_\mathrm{e}^3 c^2} + \hat{V},
$$ (relativsiticHamiltoniancorrection)

where the relativistic correction (indicated by the subscript $\mathrm{r}$) is given by $\hat{H}_\mathrm{r}' = - \hat{p}^4 / 8 m_\mathrm{e}^3 c^2$. For the unperturbed state, the Schr&ouml;dinger equation gives

$$
\hat{H}_0 \psi^0 = \left(\frac{\hat{p}^2}{2m_\mathrm{e}} + \hat{V} \right)\psi^0 = E^0 \psi^0.
$$ (unperturbedSErel)

Now we're in luck: although (as we know) the eigenstates of the unperturbed Hamiltonian of hydrogen are highly degenerate, our perturbation $\hat{H}_\mathrm{r}'$ is radially symmetric, and hence commutes with both $\hat{L}^2$ and $\hat{L}_z$. Therefore, the only quantum number we have to consider here is $n$, and for different values of $n$ we have different energies, also in the unperturbed case. We can therefore find the relativistic correction to the energies through application of non-degenerate perturbation theory. Applying the expression for the first-order correction to the energy, equation&nbsp;{eq}`firstorderperturbationenergy`, and the fact that $\hat{p}^2$ is Hermitian, we readily find
```{math}
:label: relativisticenergycorrection
\begin{align*}
E_\mathrm{r}^1 &= \Braket{\psi^0 | \hat{H}_\mathrm{r}' | \psi^0} = - \frac{1}{8 m_\mathrm{e}^3 c^2} \Braket{\psi^0 | \hat{p}^4 \psi^0} = - \frac{1}{8 m_\mathrm{e}^3 c^2} \Braket{\hat{p}^2 \psi^0 | \hat{p}^2 \psi^0} \\
&= - \frac{1}{8 m_\mathrm{e}^3 c^2} \Braket{2m_\mathrm{e}(E^0-V)\psi^0 | 2m_\mathrm{e}(E^0-V)\psi^0} = - \frac{1}{2m_\mathrm{e}c^2} \left[(E^0)^2 - 2 E^0 \Braket{V} + \Braket{V^2} \right],
\end{align*}
```
where we used the unperturbed Schr&ouml;dinger equation&nbsp;{eq}`unperturbedSErel` to rewrite $\hat{p}^2 \psi^0$ in the second line. Note that equation&nbsp;{eq}`relativisticenergycorrection` holds for an arbitrary pair ($\psi_n, E_n$) of eigenfunctions and associated energies of the unperturbed Hamiltonian. Also, equation&nbsp;{eq}`relativisticenergycorrection` holds for an arbitrary radially symmetric potential. For the hydrogen atom, we have $V = -e^2/(4\pi\varepsilon_0 r)$, so we have to evaluate the expectation value of $1/r$ and $1/r^2$ in an arbitrary eigenstate $\psi_{nlm}$ of the unperturbed hydrogen Hamiltonian. The calculations are a bit involved (see {numref}`pb:hydrogenrelativisticcorrections`); the results are:

$$
\Braket{\frac{1}{r}} = \frac{1}{n^2 a}, \qquad \Braket{\frac{1}{r^2}} = \frac{1}{n^3 \left(l+\frac12\right) a^3},
$$ (hydrogenrelativisticcorrectionexpectationvalues)

where $a$ is the Bohr radius. Substituting these in equation&nbsp;{eq}`relativisticenergycorrection`, we find for the relativistic correction to the energy $E_n$ of the state $\psi_{nlm}$

$$
E_\mathrm{r}^1 = - \frac{(E_n^0)^2}{2 m_\mathrm{e} c^2} \left( \frac{4n}{l + \frac12} - 3 \right).
$$ (relativisticenergycorrectionhydrogen)

Note that $E_\mathrm{r}^1$ depends on the value of the orbital quantum number&nbsp;$l$ as well as the principal quantum number&nbsp;$n$, so the relativistic correction does indeed (partially) lift the degeneracy of the eigenstates of the hydrogen atom.

(sec:spinorbitcoupling)=
### Spin-orbit coupling

```{figure} images/SE/spinorbitcoupling.svg
:name: fig:spinorbitcoupling
Spin-orbit coupling in a hydrogen atom. (a) The classical point of view: an electron orbits a single-proton nucleus. (b) From the electron's point of view, the proton is orbiting it. As the proton is a moving electrical charge, it generates a magnetic field&nbsp;$\bm{B}$, which couples to the magnetic dipole&nbsp;$\bm{\mu}$ originating from the electron's spin.
```

As we've seen in {numref}`sec:spinmagneticfield`, because an electron has a nonzero spin, it has a nonzero magnetic dipole moment&nbsp;$\bm{\mu}$ (equation&nbsp;{eq}`spinmagneticdipole`), which can couple to an external magnetic field. As you hopefully remember from electromagnetism, moving electric charges generate magnetic fields. We typically consider electrons in atoms to be moving, and thus they will generate such magnetic fields. That field however cannot interact with the electron's magnetic moment; if it would, the electron would be generating a force on itself from nothing. However, from the point of view of the electron, it's the nucleus that's moving, and as the nucleus also has an electric charge, it does generate a magnetic field that can interact with the electron (cf. {numref}`fig:spinorbitcoupling`).

Before we calculate the effect of the interaction between the magnetic field&nbsp;$\bm{B}$ due to the moving proton with the magnetic dipole of the electron, we first consider a caveat: because the electron's motion around the proton is an orbit, there's a force acting on the electron, keeping it in orbit, which it can only do by accelerating the electron (i.e., changing its velocity). The same is true for the moon's orbit around the Earth: Earth's gravitational pull exerts a force on the moon, which changes the direction of the moon's velocity such that it stays in its closed orbit. Now while (by Einsteins equivalence postulate) the laws of physics are the same in all inertial frames of reference, we should expect extra terms when transforming between accelerating frames of reference. Again, the same happens in classical mechanics, where going to a rotating frame causes the emergence of fictitious forces like the centrifugal and Coriolis forces (which, among many other things, strongly influence the weather on the spinning sphere on which we live). As it turns out, we're somewhat in luck: there will be no additional fictitious forces here, only a numerical correction factor.

Given the magnetic dipole moment $\bm{\mu} = \gamma \bm{S}$, the Hamiltonian for the interaction between the dipole and the magnetic field is simply $\hat{H} = - \bm{\mu} \cdot \bm{B}$. For the magnitude of the magnetic field, we invoke the Biot-Savart law from electromagnetism, which states that a current&nbsp;$I$ in a closed loop of radius $r$ generates a field of magnitude $B = \mu_0 I / 2 r$, with $\mu_0$ the permeability of free space. If the electron takes time $T$ to complete a loop, we have $I = e/T$. The magnitude of the angular momentum of the electron in its circular orbit is $L = r m_\mathrm{e} v = 2 \pi m r^2 / T$. As the magnetic field and the angular momentum moreover point in the same direction, we can thus write

$$
\bm{B} = \frac{1}{4 \pi \varepsilon_0} \frac{e}{m c^2 r^3} \bm{L},
$$

```{index} spin-orbit coupling
```
where we used $c = 1/\sqrt{\varepsilon_0 \mu_0}$. The Hamiltonian will thus contain a dot product between the spin and the orbital angular momentum; the resulting perturbation to the hydrogen Hamiltonian is therefore known as *spin-orbit coupling*. As I've stated above, we should expect the actual correction to include an additional factor due to the transition to a non-inertial frame<sup>[^7]</sup>; this additional factor turns out to be $\frac12$. Finally, we use again that for a relativistic electron, $\gamma = - e / m_\mathrm{e}$. Putting everything together, the spin-orbit correction Hamiltonian reads:

$$
\hat{H}'_\mathrm{SO} = \left(\frac{e^2}{8 \pi \varepsilon_0} \right)^2 \frac{1}{m_\mathrm{e}^2 c^2 r^3} \hat{\bm{S}} \cdot \hat{\bm{L}}.
$$ (SOHamiltonian)

Unlike the relativistic correction, the spin-orbit Hamiltonian&nbsp;{eq}`SOHamiltonian` is not radially symmetric, and therefore does not commute with either $\bm{L}$ or $\bm{S}$ (and specifically, not with either $\hat{L}_z$ or $\hat{S}_z$. However, it does still commute with both $\hat{L}^2$ and $\hat{S}^2$, and it commutes with the sum of the orbital and spin angular momentum, $\bm{J} = \bm{L} + \bm{S}$. Therefore, eigenstates of $\hat{L}_z$ and $\hat{S}_z$ are no longer 'good' eigenstates, but eigenstates of $\hat{L}^2$, $\hat{S}^2$, $\hat{J}^2$ and $\hat{J}_z$ are. Moreover, we can relate the first three to $\bm{L} \cdot \bm{S}$, making the calculation of the eigenvalues of our perturbed Hamiltonian in these eigenstates easy. We have:
```{math}
\begin{align*}
\hat{J}^2 &= \left(\hat{\bm{L}} + \hat{\bm{S}}\right) \cdot \left(\hat{\bm{L}} + \hat{\bm{S}}\right) = \hat{L}^2 + \hat{S}^2 + 2 \hat{\bm{L}} \cdot \hat{\bm{S}}  \\
\hat{\bm{L}} \cdot \hat{\bm{S}} &= \frac12 \left( \hat{J}^2 - \hat{L}^2 - \hat{S}^2 \right),
\end{align*}
```
and therefore the eigenvalues of $\hat{\bm{L}} \cdot \hat{\bm{S}}$ are

$$
\frac12 \hbar^2 \left[ j(j+1) - l(l+1) - s(s+1) \right].
$$ (LSeigenvalues)

Calculating the expectation value of the perturbation&nbsp;{eq}`SOHamiltonian` in terms of the unperturbed eigenstates takes some work, but is in principle straightforward. From it, we get for the energies (setting $s=\frac12$ for our electron):
```{math}
\begin{align*}
E_\mathrm{SO}^1 &= \Braket{\hat{H}'_\mathrm{SO}} = \left(\frac{e^2}{8 \pi \varepsilon_0} \right)^2 \frac{1}{m_\mathrm{e}^2 c^2} \Braket{\frac{1}{r^3} \hat{\bm{S}} \cdot \hat{\bm{L}}}  \\
&= \left(\frac{e^2}{8 \pi \varepsilon_0} \right)^2 \frac{1}{m_\mathrm{e}^2 c^2} \Braket{\frac{1}{r^3}} \frac12 \hbar^2 \left[ j(j+1) - l(l+1) - s(s+1) \right] \\
&= \frac{e^2}{8 \pi \varepsilon_0} \frac{1}{(m_\mathrm{e}c)^2} \frac{\hbar^2}{2} \frac{j(j+1) - l(l+1) - \frac34}{l(l+\frac12)(l+1) n^3 a_0^3}  \\
&= \frac{\left(E_n^0\right)^2}{m c^2} \frac{j(j+1) - l(l+1) - \frac34}{l(l+\frac12)(l+1)} n.
\end{align*}
```
Remarkably, the relativistic correction and the spin-orbit coupling effect are thus of the same order of magnitude: both scale as $\alpha^2 = (E_n^0)^2/2mc^2$. We therefore need to consider them together, which actually simplifies the final correction energies to

$$
E_{FS}^1 = \frac{\left(E_n^0\right)^2}{2 m c^2} \left( 3 - \frac{4n}{j + \frac12}\right).
$$ (hydrogenfinestructurecorrection)

Including fine structure, the energy levels of hydrogen are no longer just functions of the principal quantum number&nbsp;$n$; they now also contain contributions from the combined angular momentum quantum number $j$:

$$
E_{nj} = - \frac{R_\mathrm{E}}{n^2} \left[1 + \frac{\alpha^2}{n^2} \left( \frac{n}{j + \frac12} - \frac34 \right) \right].
$$

Including fine structure thus breaks some of the symmetry of the excited states of the hydrogen atom: the degeneracy of the $l$ orbitals for $n>1$ is lifted. Some symmetry is preserved though, as states with the same value of $j$ still have the same energy. Moreover, $m_l$ and $m_s$ are no longer 'good' quantum numbers; the eigenstates of hydrogen including fine structure are characterized by their values of $n$, $l$, $s$, $j$ and $m_j$. The corrections to energy levels for the first three states are plotted in {numref}`fig:hydrogenfinestructureenergylevels`.

```{code-cell} ipython3
:tags: [hide-input, remove-output]

%config InlineBackend.figure_formats = ['svg']
import numpy as np
import matplotlib.pyplot as plt
from myst_nb import glue

def Efs(n, j, alpha):
    # Fine structure correction in terms of prefactor En^2 / 2 m c^2.
    return alpha * alpha * (3- (4 * n)/(j+0.5)) / (n*n*n*n) - 1/(n*n)

fig,ax = plt.subplots(figsize=(10,8))

alpha = np.linspace(0, 1, 100)
lw = 2
fs1 = 18
fs2 = 14

ax.plot(alpha, Efs(1, 0.5, alpha),lw = lw)
ax.text(1.01, Efs(1, 0.5, 1)-0.002, r'$E_{(1, \frac{1}{2})}$', c = 'C0', fontsize = fs2)

ax.plot(alpha, Efs(2, 0.5, alpha),lw = lw)
ax.text(1.01, Efs(2, 0.5, 1)-0.002, r'$E_{(2, \frac{1}{2})}$', c = 'C1', fontsize = fs2)
ax.plot(alpha, Efs(2, 1.5, alpha),lw = lw)
ax.text(1.01, Efs(2, 1.5, 1)-0.002, r'$E_{(2, \frac{3}{2})}$', c = 'C2', fontsize = fs2)

ax.plot(alpha, Efs(3, 0.5, alpha),lw = lw)
ax.text(1.01, Efs(3, 0.5, 1)-0.01, r'$E_{(3, \frac{1}{2})}$', c = 'C3', fontsize = fs2)
ax.plot(alpha, Efs(3, 1.5, alpha),lw = lw)
ax.text(1.01, Efs(3, 1.5, 1)-0.02, r'$E_{(3, \frac{3}{2})}$', c = 'C4', fontsize = fs2)
ax.plot(alpha, Efs(3, 2.5, alpha),lw = lw)
ax.text(1.01, Efs(3, 1.5, 1)+.04, r'$E_{(3, \frac{5}{2})}$', c = 'C5', fontsize = fs2)

hline = {"color":"k","linestyle":":","linewidth":lw}

ax.axhline(y = Efs(1,0,0), **hline)
ax.axhline(y = Efs(2,0,0), **hline)
ax.axhline(y = Efs(3,0,0), **hline)

ax.set_xlim(0,1)

ax.set_yticks([Efs(1,0,0),Efs(2,0,0),Efs(3,0,0), 0])
ax.set_yticklabels([r'$E_1$', r'$E_2$', r'$E_3$', 0],fontsize = fs1)

ax.set_xlabel(r'$\alpha^2$', fontsize=fs1)
# Save graph to load in figure later (special Jupyter Book feature)
glue("hydrogenfinestructuresplitting", fig, display=False)
```

```{glue:figure} hydrogenfinestructuresplitting
:name: fig:hydrogenfinestructureenergylevels
Fine-structure corrections (equation&nbsp;{eq}`hydrogenfinestructurecorrection` to the first three energy levels of the hydrogen atom, as a function of the value of the fine-structure constant&nbsp;$\alpha$. Note that the actual value of $\alpha$ is very small (approximately $1/137$), so the corrections are also small. The fine-structure correction lifts the degeneracy in the quantum number&nbsp;$l$, but not in&nbsp;$j$.
```


## Problems
````{exercise} Corrections to the wave function and energy in non-degenerate perturbation theory 
:label: pb:nondegenerateperturbationsecondorder
:class: dropdown
In this problem, we'll derive the expressions&nbsp;{eq}`firstorderperturbationwavefunction` and&nbsp;{eq}`secondorderperturbationenergy` for the first-order correction to the wave function and the second-order correction to the energy of a non-degenerate system. First, we re-write equation&nbsp;{eq}`SEfirstorderperturbation` as
```{math}
:label: SEfirstorderperturbationaspde
\left( \hat{H}^0 - E_n^0 \right) \psi_n^1 = - \left( \hat{H}' - E_n^1 \right) \psi_n^0.
```
We already have an expression for $E_n^1$, and we assumed we knew&nbsp;$\psi_n^0$, so the right-hand side of equation&nbsp;{eq}`SEfirstorderperturbationaspde` is a known function, and we're left with a differential equation for the unknown function&nbsp;$\psi_n^1$. As the $\psi_n^0$ are the eigenfunctions of the unperturbed Hamiltonian&nbsp;$\hat{H}^0$, which is a Hermitian operator, they form a complete set, and we can therefore express $\psi_n^1$ as a linear combination of the $\psi_n^0$:
```{math}
:label: perturbedwavefunctionexpansion
\psi_n^1 = \sum_{k=0}^\infty c_k \psi_k^0.
```
1. Argue why we can leave out the term with $k=n$ in the sum in equation&nbsp;{eq}`perturbedwavefunctionexpansion`.
1. Substitute&nbsp;{eq}`perturbedwavefunctionexpansion`, without the $k=n$ term, into equation&nbsp;{eq}`SEfirstorderperturbationaspde`, then take the inner product with $\psi_m^0$, to show that you get $0$ if $m=n$ and, for $m \neq n$,
	```{math}
	:label: firstorderperturbationwavefunctioncoefficient
	\left( E_m^0 - E_n^0 \right) c_m = - \Braket{\psi_m^0 | \hat{H}' | \psi_n^0}.
	```
1. From equations&nbsp;{eq}`firstorderperturbationwavefunctioncoefficient` and&nbsp;{eq}`perturbedwavefunctionexpansion`, derive equation&nbsp;{eq}`firstorderperturbationwavefunction`.
1. Repeat the calculation in {numref}`sec:nondegenerateperturbationtheory`, now retaining terms to order&nbsp;$\varepsilon^2$, to derive equation&nbsp;{eq}`secondorderperturbationenergy` for the  second-order correction to the energy. You'll need to use the fact that (by part (a)) $\psi_n^0$ and $\psi_n^1$ are orthogonal.
````

```{exercise} The induced dipole perturbation
:label: pb:vanderWaalsperturbation
:class: dropdown
In {numref}`sec:vanderWaalsforce` we studied the perturbation to a hydrogen atom due to the presence of another hydrogen atom nearby, and found that this perturbation leads to an attractive van der Waals force. In this problem we'll derive the dipole-dipole interaction energy, equation&nbsp;{eq}`vanderWaalsdipole`, in the limit that the separation&nbsp;$R$ between the atoms is much larger than the Bohr radius&nbsp;$a_0$.
1. We choose coordinates such that the two atomic nuclei are on the $z$ axis, separated by a distance&nbsp;$R$. Let $\bm{r_1} = (x_1, y_1, z_1)$ be the position of the electron in atom A, in a coordinate system where the nucleus of atom A is at the origin. Likewise, let $\bm{r_2} = (x_2, y_2, z_2)$ the position of the electron in atom B, in a (shifted) coordinate system where the nucleus of atom B is at the origin. Express the distances&nbsp;$r_{12}$, $r_{1\mathrm{B}}$ and&nbsp;$r_{2\mathrm{A}}$ between the electrons and the electrons and opposite nuclei (see {numref}`fig:vanderWaalsforces`(a)) in terms of the coordinates $x_1, x_2, y_1, y_2, z_1, z_2$ and the nuclear separation&nbsp;$R$.
1. Your expressions from (a) should contain one term that's linear in $R$, and others of order $a_0$ (e.g. $x_1$ and $z_1$). Pull out a factor $R$ from these expressions, and expand $1/r_{1\mathrm{B}}$, $1/r_{2\mathrm{A}}$ and $1/r_{12}$ to second order in terms of magnitude $a_0/R$.
1. Finally, substitute your answers to (b) in the perturbation term $\hat{H}'$ in the van der Waals Hamiltonian&nbsp;{eq}`vanderWaalsHamiltonian` to get the dipole-dipole interaction of equation&nbsp;{eq}`vanderWaalsdipole`.
```

````{exercise} Relativistic corrections to the hydrogen atom
:label: pb:hydrogenrelativisticcorrections
:class: dropdown
In this problem, we'll derive the expressions in equation&nbsp;{eq}`hydrogenrelativisticcorrectionexpectationvalues` for the expectation values of $1/r$ and $1/r^2$ in the unperturbed eigenstates of the hydrogen atom.
1. For the expectation value of $1/r$, use the virial {prf:ref}`thm:virialtheorem` to derive the expression given in equation&nbsp;{eq}`hydrogenrelativisticcorrectionexpectationvalues`.
	We can find the expectation values of both $1/r$ and $1/r^2$ from the Hellmann-Feynman theorem (see {numref}`pb:HFtheorem`), which states that if the Hamiltonian depends on a parameter&nbsp;$u$, then its eigenvalues (i.e., the energies) are also functions of&nbsp;$u$, and we have
	```{math}
	:label: HF2
	\frac{\mathrm{d}E}{\mathrm{d}u} = \Braket{\frac{\partial \hat{H}}{\partial u}}.
	```
	We'll apply the Hellmann-Feynman theorem to the effective Hamiltonian of the radial part of the wavefunction of a hydrogen atom, see equation&nbsp;{eq}`centralpotentialSEcentrifugal`:
	```{math}
	:label: radialhydrogenHamiltonian
	\hat{H} = -\frac{\hbar^2}{2\me} \frac{\mathrm{d}^2}{\mathrm{d}r^2} + \frac{\hbar^2}{2\me} \frac{l(l+1)}{r^2} - \frac{e^2}{4 \pi \varepsilon_0} \frac{1}{r}.
	```
	As the Hamiltonian in equation&nbsp;{eq}`radialhydrogenHamiltonian` is given in terms of the quantum number&nbsp;$l$, for the purposes of this problem, we'd also like to write the (unperturbed!) energies in terms of&nbsp;$l$. We can do so by invoking equation&nbsp;{eq}`hydrogenseriestermination`, which states that a series solution to the Schr&ouml;dinger equation that terminates must satisfy $\rho_0 = 2(j_\mathrm{max}+l+1) = 2n$. We used this equation to define the quantum number $n$, but now we'll use it to convert $n$ to $l$, as $n = l + j_\mathrm{max} + 1 = N + l$ (where, for a given wavefunction, $N$ is a fixed integer). Substituting this expression into the energy&nbsp;{eq}`hydrogenenergies` of the unperturbed state with quantum number $n$, we can write $E_n$ as
	```{math}
	:label: hydrogenenergiesasfunctionofl
	E_n = - \frac{\me e^4}{2 (4 \pi \varepsilon_0 \hbar)^2 (N+l)^2}.
	```
1. Apply the Hellmann-Feynman theorem to the Hamiltonian&nbsp;{eq}`radialhydrogenHamiltonian` with energies&nbsp;{eq}`hydrogenenergiesasfunctionofl` with $\lambda = e$ to re-derive your expression from (a) for the expectation value of $1/r$.
1. Now apply the Hellmann-Feynman theorem to the same Hamiltonian and energies with $\lambda = l$ to get $\Braket{1/r^2}$.
````

[^1]: The coordinates $x_1$, $x_2$ etc. in equation&nbsp;{eq}`vanderWaalsdipole` are with respect to the center of the atom the electron belongs to. See {numref}`pb:vanderWaalsperturbation` for the derivation of&nbsp;{eq}`vanderWaalsdipole` from&nbsp;{eq}`vanderWaalsHamiltonian`.

[^2]: The atomic polarizability has a direct relation with the dielectric constant&nbsp;$\varepsilon$ of a macroscopic sample of the material, where $\varepsilon = 1 + 4 \pi n \alpha$, with $n$ the number density of the atoms in the sample.

[^3]: Close-up of the underside of a gecko's foot as it walks on a glass wall, by [Bjørn Christian Tørrissen](https://commons.wikimedia.org/wiki/User:Uspn), obtained from [Wikimedia commons](https://commons.wikimedia.org/wiki/File:Gecko_foot_on_glass.JPG), CC-BY SA 3.0.

[^4]: To avoid confusion, I'll write $m_\mathrm{e}$ for the mass of the electron throughout this section. Naturally, equation&nbsp;{eq}`relativsiticenergymomentumequation` also holds for other relativistic particles if we substitute their mass instead of $m_\mathrm{e}$.

[^5]: There are many texts in which you can find a derivation of equation&nbsp;{eq}`relativsiticenergymomentumequation`, including my own open textbook [Mechanics and Relativity](https://textbooks.open.tudelft.nl/textbooks/catalog/book/14).

[^6]: The rest energy is simply the energy of a particle at rest, so for the electron, we have $E_\mathrm{rest} = m_\mathrm{e} c^2$.

[^7]: The correction is known as Thomas precession, and rather than introducing a factor&nbsp;$\frac12$, it actually subtracts $1$ from the gyromagnetic ratio's&nbsp;$g$ factor, which, as we've seen before, is almost exactly&nbsp;$2$.

