%Problemtitle: The LCAO for the molecular hydrogen ion
\textbf{The LCAO for the molecular hydrogen ion}
\begin{enumerate}[(a)]
\item Derive the expressions for the direct ($D$) and exchange~($X$) integrals given in equations~(\bookref{directintegral}) and (\bookref{exchangeintegral}).
\item Derive the expression~(\bookref{antisymmetricmolecularhydrogenionLCAOHamiltonianexpvalue}) for the expectation value of the molecular hydrogen ion Hamiltonian (equation~\bookref{molecularhydrogenionHamiltonian}) in the antisymmetric LCAO state (equation~\bookref{antisymmetricmolecularhydrogenionLCAO}).
\item Re-derive the equations for the energy of the symmetric and antisymmetric LCAO of the hydrogen ion from the general condition~(\bookref{LCAOmatrixequation}) in section~\bookref{sec:varprinciplemolecules}. \textit{Hint}: first express the matrix elements $H_{ik}$ and $S_{ik}$ in terms of the integrals $D$, $X$ and $S$ of section~\bookref{sec:molecularhydrogenion}; you'll get two symmetric $2 \times 2$ matrices, after which solving for the energies from equation~(\bookref{LCAOmatrixequation}) will be easy.
\end{enumerate}

\ifincludesolutions
\Solution
\begin{enumerate}[(a)]
\item We have $\mu = \frac{r_1+r_2}{R}$ and $\nu = \frac{r_1 - r_2}{R}$, so we can express $r_1$ and $r_2$ in $\mu$ and $\nu$ as $r_1 = \frac12 R (\mu+\nu)$ and $r_2 = \frac12 R (\mu-\nu)$, which gives
\begin{align*}
D &= \Braket{\psi_{100}(\bvec{r}_1) | \frac{a}{r_2} | \psi_{100}(\bvec{r}_1)} \\
&= \frac{1}{\pi a^3}\int e^{-2 r_1/a} \frac{a}{r_2} \dd^3 \bvec{r} \\
&= \frac{1}{\pi a^3} \int_1^\infty \dd\mu \int_{-1}^1 \dd\nu \int_0^{2\pi} \dd\phi \, e^{-R(\mu+\nu)/a} \frac{2a}{R(\mu-\nu)} \frac{R^3}{8} (\mu^2 - \nu^2) \\
&= \frac{R^3}{2 a^3} \int_1^\infty \dd\mu \int_{-1}^1 \dd\nu \, e^{-R(\mu+\nu)/a} \frac{a}{R} (\mu + \nu) \\
&= \frac{R^2}{2 a^2} \left(\frac{a}{R}\right)^3 \int_{R/a}^\infty \dd x \int_{-R/a}^{R/a} \dd y e^{-(x+y)} (x+y) \\
&=  \frac{a}{2R} \int_{-R/a}^{R/a} \dd y \left\lbrace -y e^{-y} \left[ e^{-x} \right]_{R/a}^\infty + e^{-y} \left[-x e^{-x} - e^{-x}\right]_{R/a}^\infty \right\rbrace \\
&= \frac{a}{2R} e^{-R/a} \int_{-R/a}^{R/a} \dd y \left\lbrace y e^{-y} + e^{-y} \left(\frac{R}{a} + 1 \right) \right\rbrace \\
&= \frac{a}{2R} e^{-R/a} \left[ -y e^{-y} - e^{-y} - \left(\frac{R}{a} + 1 \right) e^{-y} \right]_{-R/a}^{R/a} \\
&= \frac{a}{R} - \left(1 + \frac{a}{R} \right) e^{-2R/a},
\end{align*}
where we defined $x = (R/a)\mu$ and $y = (R/a) \nu$ in the fifth line, and integrated over $x$ before $y$ (because we know that the $x$ integral vanishes at the top limit). The exchange integral goes completely analogously (but easier because the exponential depends on $\mu$ alone):
\begin{align*}
X &= \Braket{\psi_{100}(\bvec{r}_1) | \frac{a}{r_1} | \psi_{100}(\bvec{r}_2)} \\
&= \frac{1}{\pi a^3}\int e^{-(r_1+r_2)/a} \frac{a}{r_1} \dd^3 \bvec{r} \\
&= \frac{1}{\pi a^3} \int_1^\infty \dd\mu \int_{-1}^1 \dd\nu \int_0^{2\pi} \dd\phi e^{-R \mu/a} \frac{2a}{R(\mu+\nu)} \frac{R^3}{8} (\mu^2 - \nu^2) \\
&= \frac{R^3}{2 a^3} \int_1^\infty \dd\mu \int_{-1}^1 \dd\nu e^{-R \mu/a} \frac{a}{R} (\mu-\nu) \\
&= \frac{R^2}{2a^2} \int_1^\infty \dd\mu e^{-R \mu/a} \left[\mu \nu - \frac12 \nu^2 \right]_{-1}^1 = \frac{R^2}{a^2} \int_1^\infty \dd\mu \mu e^{-R \mu/a} \\
&= \frac{R^2}{a^2} \left(\frac{a}{R}\right)^2 \int_{R/a}^\infty x e^{-x} \dd x = \left(1 + \frac{R}{a}\right) e^{-R/a}.
\end{align*}
\item We have (normalization):
\[ 1 = \braket{\psi(\bvec{r}) | \psi(\bvec{r})} = A^2 \int \left[ \psi_{100}^2(\bvec{r}_1) + \psi_{100}^2(\bvec{r}_2) - 2 \psi_{100}^2(\bvec{r}_1) \psi_{100}^2(\bvec{r}_2) \right] \dd^3 \bvec{r} = 2 A (1-S), \]
with $S = \braket{\psi_{100}(\bvec{r}_1) | \psi_{100}(\bvec{r}_2)}$ the overlap integral, so $A = 1 / \sqrt{2(1-S)}$. Likewise, we find for the expectation value of the Hamiltonian:
\begin{align*}
\braket{\hat{H}} &= A^2 \braket{\psi_{100}(\bvec{r}_1) - \psi_{100}(\bvec{r}_2) | \hat{H} | \psi_{100}(\bvec{r}_1) - \hat{H}\psi_{100}(\bvec{r}_2)}\nonumber \\
&= A^2 \braket{\psi_{100}(\bvec{r}_1) - \psi_{100}(\bvec{r}_2) | E_1 \psi_{100}(\bvec{r}_1) - E_1 \psi_{100}(\bvec{r}_2)} \\
& \quad  - \frac{e^2}{4\pi\varepsilon_0} A^2 \Braket{\psi_{100}(\bvec{r}_1) - \psi_{100}(\bvec{r}_2) |\left(\frac{1}{r_2} \psi_{100}(\bvec{r}_1) - \frac{e^2}{4\pi\varepsilon_0} \frac{1}{r_1} \psi_{100}(\bvec{r}_2)\right)} + \braket{V_\mathrm{pp}} \nonumber \\
&= \Braket{\psi(\bvec{r})| E_1 \psi(\bvec{r})} - 2 A^2 \frac{e^2}{4\pi\varepsilon_0} \braket{\psi_{100}(\bvec{r}_1) | \frac{1}{r_2} | \psi_{100}(\bvec{r}_1)} \\
& \quad + 2 A^2 \frac{e^2}{4\pi\varepsilon_0} \Braket{\psi_{100}(\bvec{r}_1) | \frac{1}{r_1} | \psi_{100}(\bvec{r}_2)} + \braket{V_\mathrm{pp}} \nonumber \\
&= E_1 + 4 A^2 E_1 \left( D - X \right) + V_\mathrm{pp}\\
&= E_1 \left(1 - \frac{a}{R} + 2 \frac{D-X}{1-S} \right).
\end{align*}
\item We have
\begin{align*}
S_{ik} &= \braket{\psi_i | \psi_k} \\
H_{ik} &=  \braket{\psi_i | \hat{H} | \psi_k},
\end{align*}
so we have $S_{11} = S_{22} = 1$, $S_{12} = S_{21} = S$ (the exchange integral), $H_{11} = H_{22} = E_1 (1-\frac{a}{R}+D)$, and $H_{12} = H_{21} = E_1[(1-\frac{a}{R})S + X]$. The given equation (the `secular determinant') then reads:
\begin{align*}
0 &= \det(\bvec{H}-E\bvec{S}) \\
&= (H_{11}-ES_{11})(H_{22}-ES_{22}) - (H_{12}-ES_{12})(H_{21}-ES_{21}) \\
&= \left[E_1 \left(1-\frac{a}{R}+D\right) - E\right]^2 - \left[E_1 \left(\left(1-\frac{a}{R}\right)S + X\right) - ES\right]^2 \\
&= E_1^2 \left[ \left(1 -\frac{a}{R} + D - \varepsilon\right)^2 - \left(S - \frac{a}{R}S + X - S\varepsilon\right)^2 \right],
\end{align*}
where we defined $\varepsilon = E/E_1$. We thus get
\[ \left(1 -\frac{a}{R} + D - \varepsilon\right)^2 = \left(S - \frac{a}{R}S + X - S\varepsilon\right)^2. \]
Taking the positive and negative root, we get two solutions:
\[ E_\pm = \frac{1-\frac{a}{R}+D \pm \left(X+S- \frac{a}{R}S\right)}{1 \pm S} = 1 -\frac{a}{R} + \frac{D \pm X}{1 \pm S}, \]
as we found before.
\end{enumerate}
\fi