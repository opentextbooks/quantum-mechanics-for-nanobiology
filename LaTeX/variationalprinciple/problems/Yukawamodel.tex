%Problemtitle: Yukawa model for the strong force
%Source: Griffiths 3rd ed. 8.28
The strong force models interactions between quarks (with gluons as the messenger bosons). As such, it both confines quarks in hadrons (particles composed of quarks, like protons and neutrons, but also e.g. pions) and binds protons and neutrons together in atomic nuclei. The first model for the strong force by Yukawa (1934) did not include gluons, but had a mediation between protons and neutrons (`nucleons') by means of $\pi$-mesons, resulting in a potential energy for the nucleons given by
\begin{equation}
\label{Yukawapotential}
V(r) = - V_0 \frac{r_0}{r} e^{-r/r_0},
\end{equation}
where $r_0$ is defined in terms of the mass of the pions as $r_0 = \hbar / m_\pi c$. We will use this theory to study the stability of the deuterium nucleus, which consists of one proton and one neutron (`heavy water' is water in which the hydrogens are deuterium rather than regular hydrogen atoms with only a proton in the nucleus). Because the proton and neutron have almost identical mass (we'll take them to be equal here for simplicity and set both to $m$), their situation is different from the hydrogen atom, where an electron orbits a much heavier nucleus; here we have to work with the reduced mass~$\mu = m_\mathrm{p} m_\mathrm{n} / (m_\mathrm{p} + m_\mathrm{n})$ and associated Schr\"odinger equation in terms of $\mu$ (cf. problem~\bookref{ch:SEsolutions}.\bookref{pb:reducedmass}):
\begin{equation}
\label{deutronSE}
\hat{H}\psi(\bvec{r}) = -\frac{\hbar^2}{2\mu} \nabla^2 \psi(\bvec{r}) + V(r) \psi(\bvec{r}) = E \psi(\bvec{r}),
\end{equation}
where $\bvec{r} = \bvec{r}_\mathrm{n} - \bvec{r}_\mathrm{p}$ is the separation vector between the proton and the neutron. To prove that a stable deuterium nucleus can exist, all we have to do is to show that the ground state has negative energy. As the potential is radially symmetric, we know that the spherical harmonics solve the angular part of the Schr\"odinger equation, and that they don't contribute to the energy; we can therefore ignore the angular part, and work with just the radial part of equation~(\ref{deutronSE}):
\begin{equation}
\label{deutronSEradial}
-\frac{\hbar^2}{2 \mu} \frac{1}{r^2} \frac{\dd}{\dd r} \left(r^2 \frac{\dd\psi(r)}{\dd r} \right)  + l (l+1) \frac{\hbar^2}{2 \mu} \frac{1}{r^2} \psi(r) + V(r) \psi(r) = E \psi(r),
\end{equation}
where $l$ is the orbital quantum number (zero for the ground state) and $\psi(r)$ the radial part of the total wave function, i.e. $\psi(\bvec{r}) = \psi(r) Y_l^m(\cos(\theta))$. To prove that a stable deuterium nucleus can exist, all we have to do is to show that the ground state has negative energy, which we'll do using the variational principle. As our trial wave function we'll take
\begin{equation}
\label{deutrontrialfunction}
\psi(\bvec{r}) = A e^{-b r / r_0},
\end{equation}
where~$b$ is our free parameter.
\begin{enumerate}[(a)]
\item Determine the value of the normalization constant~$A$ (note that the trial wave function~(\ref{deutrontrialfunction}) is three dimensional, so you'll need a three-dimensional integral, though of course the angular integral will be trivial).
\item Show that the expectation value of the (ground-state) Hamiltonian~(\ref{deutronSEradial}) in the state~(\ref{deutrontrialfunction}) is given by
\begin{equation}
\label{deutrongroundstateestimate}
\ev{\hat{H}} = \frac{\hbar^2 b^2}{2 \mu r_0^2} \left[ 1 - \frac{4 \gamma b}{(1 + 2 b)^2} \right],
\end{equation}
where $\gamma$ is a combination of the parameters of the problem (for which you should find the value in your solution).
\item Optimize the answer you got at (b) to show that the minimum value of the expectation value (and thus the best estimate of the ground state energy) is given by
\begin{equation}
E_\mathrm{min} = \frac{\hbar^2}{2 \mu r_0^2} \frac{(1-2b)b^2}{(3 + 2b)}.
\end{equation}
\textit{Hint}: you won't be able to solve for $b$, but you can solve for $\gamma$ in terms of $b$, and use that to eliminate $\gamma$.
\item For which value(s) of~$b$ in the range $0 \leq b \leq 1$ will there be a bound state of the deuterium nucleus? Translate this value into a minimum value for $V_0$, and use the following to get an actual number out: $m_\pi c^2 = 135\;\mathrm{MeV}$, $m_\mathrm{p} c^2 = 938\;\mathrm{MeV}$ (the experimental value is $52\;\mathrm{MeV}$). 
\end{enumerate}

\ifincludesolutions
\Solution
\begin{enumerate}[(a)]
\item We have
\begin{align*}
1 &= \Braket{\psi|\psi} = \int_{\mathbb{R}^3} |\psi(\bvec{r})|^2 \dd^3\bvec{r} \\
&= A^2 \int_0^\infty \dd r \int_0^\pi \dd \theta \int_0^{2\pi} \dd\phi e^{-2 b r / r_0} r^2 \sin(\theta) \\
&= 4 \pi A^2 \left(\frac{r_0}{2b} \right)^3 \int_0^\infty z^2 e^{-z} \dd z = A^2 \frac{\pi r_0^3}{b^3},
\end{align*}
where the last integral is given on the equations sheet~\score{2 pt}. The value of the normalization constant~$A$ is thus $A = \sqrt{b^3/p r_0^3}$ \score{1 pt}.
\item We have (setting $l=0$ for the ground state):
\begin{align*}
1 &= \Braket{\psi|\hat{H}|\psi} = 4 \pi \int \psi^*(r) \left[-\frac{\hbar^2}{2 \mu} \frac{1}{r^2} \frac{\dd}{\dd r} \left(r^2 \frac{\dd\psi(r)}{\dd r} \right) + V(r) \psi(r) \right] r^2 \dd r \\
&= 4 \pi A^2 \int_0^\infty e^{-b r / r_0} \left[ -\frac{\hbar^2}{2 \mu} \frac{1}{r^2} \frac{\dd}{\dd r} \left(-\frac{b}{r_0} r^2 e^{-b r / r_0} \right) - V_0 \frac{r_0}{r} e^{(b+1) r / r_0} \right] r^2 \dd r \\
&= 4 \pi A^2 \int_0^\infty e^{-b r / r_0} \left[ -\frac{\hbar^2}{2 \mu} \frac{1}{r^2} \left(\frac{b^2}{r_0^2} r^2 - 2 \frac{b}{r_0} r \right) e^{-b r / r_0} - V_0 \frac{r_0}{r} e^{-(b+1) r / r_0} \right] r^2 \dd r \\
&= - \frac{4 \pi A^2 \hbar^2}{2 \mu} \frac{b}{r_0^2} \int_0^\infty \left(b\frac{r^2}{r_0^2} - 2 \frac{r}{r_0} \right) e^{-2 b r / r_0} \dd r - 4 \pi A^2 V_0 r_0^2 \int_0^\infty \frac{r}{r_0} e^{-(2b+1) r / r_0} \dd r \\
&= \frac{\hbar^2 b^2}{\mu r_0^2} \int_0^\infty \left(z - \frac14 z^2 \right) e^{-z} \dd z - \frac{4 b^3 V_0}{(2 b + 1)^2} \int_0^\infty z e^{-z} \dd z \\
&= \frac{\hbar^2 b^2}{\mu r_0^2} \left(1 - \frac12 \right) - \frac{4 b^3 V_0}{(2 b + 1)^2} \\
&= \frac{\hbar^2 b^2}{2 \mu r_0^2} \left[1 - \frac{4 b \gamma}{(2 b + 1)^2} \right],
\end{align*}
where $\gamma = 2 \mu r_0^2 V_0 / \hbar^2$ \score{4 pt}.
\item We simply take the derivative of~(\ref{deutrongroundstateestimate}) with respect to~$b$ and equate to $0$ to find the optimum. Note that we already need $b \neq -1/2$; we also assume $b \neq 0$ in step 3.
\begin{align*}
0 &= \frac{\dd \ev{\hat{H}}}{\dd b} = \frac{\hbar^2 }{2 \mu r_0^2} \left[2b - 4 \gamma \frac{(1+2b)^2 \cdot 3b^2 - 2 b^3 (1+2b) \cdot 2}{(1+2b)^4} \right] \\
0 &= b(1+2b)^3 - 6 \gamma b^2(1+2b) + 8 \gamma b^3 \\
0 &= (1-2b)^3 - 2 b (3 + 2 b) \gamma \\
\gamma &= \frac{(1+2b)^3}{2b(3+2b)}
\end{align*}
\score{2 pt}. Substituting this expression in $\ev{\hat{H}}$ we get
\begin{align*}
E_\mathrm{min} &= \frac{\hbar^2 b^2}{2 \mu r_0^2} \left[1 - \frac{4 b}{(2 b + 1)^2} \frac{(1+2b)^3}{2b(3+2b)} \right] = \frac{\hbar^2 b^2}{2 \mu r_0^2} \left[1 - \frac{2+4b}{3 + 2b} \right] = \frac{\hbar^2 b^2}{2 \mu r_0^2} \frac{1 - 2b}{3 + 2b}
\end{align*}
\score{1 pt}.
\item We can simply read off that the energy is zero at $b = \frac12$, and from either a plot or substituting a value get that the energy becomes negative for $b > \frac12$ \score{1 pt}. Substituting $b = \frac12$ in our expression for $\gamma$ from (c) we get
\[ \gamma = \frac{(1+2b)^3}{2b(3+2b)} = \frac{2^3}{1 \cdot 4} = 2 \]
and thus (using our expression for $\gamma$ from (b)):
\[ V_0 = \frac{\gamma \hbar^2}{2 \mu r_0^2} = \frac{(m_\pi c)^2 (m_\mathrm{p} + m_\mathrm{n})}{m_\mathrm{p} \cdot m_\mathrm{n}} = \frac{2 (m_\pi c^2)^2}{m_\mathrm{p} c^2} = \frac{2 \cdot 135^2}{938} = 39\;\mathrm{MeV}\]
\score{1 pt}.
\end{enumerate}
\fi